namespace efj
{

static efj::net_addr
_make_addr(u32 addr)
{
    efj::net_addr result = {};
    result.type          = efj::net_addr_type_ipv4;
    result.ipv4.s_addr   = htonl(addr);
    return result;
}

efj::net_addr net_addr_any       = _make_addr(INADDR_ANY);
efj::net_addr net_addr_broadcast = _make_addr(INADDR_BROADCAST);

// TODO: .inl
extern efj::net_sock_addr
net_make_sock_addr(const char* ip, u16 port)
{
    efj::net_sock_addr result = {};

    result.family              = AF_INET;
    result.address.type        = efj::net_addr_type_ipv4;
    result.address.ipv4.s_addr = inet_addr(ip);
    result.port                = htons(port);

    return result;
}

extern efj::net_sock_addr
net_make_sock_addr(const struct sockaddr_in& addr)
{
    efj::net_sock_addr result = {};

    result.family       = addr.sin_family;
    result.address.type = efj::net_addr_type_ipv4;
    result.address.ipv4 = addr.sin_addr;
    result.port         = addr.sin_port;

    return result;
}

extern efj::net_sock_addr
net_make_sock_addr(const struct sockaddr_un& addr)
{
    efj::net_sock_addr result = {};

    result.family       = addr.sun_family;
    result.address.type = efj::net_addr_type_unix;
    memcpy(&result.address.unix, addr.sun_path, sizeof(result.address.unix));
    result.port         = 0;

    return result;
}

extern efj::net_sock_addr
net_make_sock_addr(const struct sockaddr_in6& addr)
{
    efj::net_sock_addr result = {};

    result.family       = addr.sin6_family;
    result.address.type = efj::net_addr_type_ipv6;
    result.address.ipv6 = addr.sin6_addr;
    result.port         = addr.sin6_port;

    return result;
}

extern efj::string
net_get_ip(const efj::net_sock_addr& addr)
{
    if (addr.address.type == efj::net_addr_type_unix)
        return addr.address.unix;

    char* ipText = (char*)efj_push_bytes(efj::temp_memory(), INET6_ADDRSTRLEN);

    // All the addresses start at the same address.
    if (!inet_ntop(addr.family, addr.address.data, ipText, INET6_ADDRSTRLEN))
        return "Invalid address";

    return efj::string::shallow_copy(ipText, INET6_ADDRSTRLEN);
}

extern u16
net_get_port(const efj::net_sock_addr& addr)
{
    return ntohs(addr.port);
}

} // namespace efj
