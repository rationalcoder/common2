namespace efj
{

struct xml_attribute
{
    efj::xml_attribute* _next = nullptr;
    efj::string         _name;
    efj::string         _value;

    auto next()  const -> const efj::xml_attribute* { return _next; }
    auto next()        ->       efj::xml_attribute* { return _next; }

    auto name()  const -> const efj::string { return _name; }
    auto name()        ->       efj::string { return _name; }

    auto value() const -> const efj::string { return _value; }
    auto value()       ->       efj::string { return _value; }
};

struct xml_element
{
    efj::xml_element*    _parent         = nullptr;
    efj::xml_element*    _next           = nullptr;

    efj::xml_element*    _children       = nullptr;
    efj::xml_element**   _childrenSorted = nullptr;
    umm                  _sortedIndex    = 0;
    umm                  _childrenCount  = 0;

    efj::string          _name;
    efj::string          _content;

    efj::xml_attribute*  _attribs        = nullptr;
    efj::xml_attribute** _attribsSorted  = nullptr;
    umm                  _attribCount    = 0;

    auto name() const -> efj::string { return _name; }
    auto content() const -> efj::string { return _content; }
    auto parent() const -> const efj::xml_element* { return _parent; }
    auto next() const -> const efj::xml_element* { return _next; };
    auto first_child() const -> const efj::xml_element* { return _children; }
    auto first_child(const efj::string& name) const -> const efj::xml_element*;
    auto next_sibling() const -> const efj::xml_element*;
    auto first_attribute() const -> const efj::xml_attribute* { return _attribs; }
    auto find_attribute(const efj::string& name) const -> const efj::xml_attribute*;
    umm attribute_count() const { return _attribCount; }
    umm child_count() const { return _childrenCount; }
    umm child_count(const efj::string& name) const;
};

struct xml_document
{
    efj::xml_element* _root = nullptr;

    auto root() const -> const efj::xml_element* { return _root; }
    auto root()       ->       efj::xml_element* { return _root; }
};

struct parsed_xml_file
{
    efj::string       error;
    efj::xml_document doc;
};

auto parse_xml_file(const efj::buffer& buf) -> efj::parsed_xml_file;

} // namespace efj
