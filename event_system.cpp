namespace efj
{

// TODO: There's a lot of redundant stale checking in the timer API.

enum
{
    // This is set reasonably low b/c epoll() does round-robin event handling for us, and
    // the max time between an event happening and actually being handled is related to this
    // number. For example, if we pull 100 events off of epoll() at once, the last event is
    // handled after handling 99 other events. If those events each take around 1 ms, that's
    // a 99 ms delay...
    // TODO: Does this even matter? We have to handle all the events at some point anyway, right?
    EVENTSYS_MAX_EVENTS_PER_POLL = 16,
};

struct eventsys_event;

// This struct is initialized to zero with memset.
struct eventsys_event_data
{
    // The event contexts are actually stored in the event data structs, b/c we don't want to have to
    // allocate these separately. Putting them here means we get allocation and free-listing handled for us.
    efj::debugsys_event_ctx    debugContext;

    efj::eventsys_event_data*  nextFree;
    efj::eventsys_event_data*  nextStale;
    efj::eventsys_event*       event;
    efj::eventsys_udata        udata;
    efj::eventsys_dispatch_fn* dispatch; // for translation / internal system callbacks.
    efj::callback              objectCb; // final, object callback passed through public API.
    efj::eventsys_event_type   type;
    u32                        id;

    // This is cached data that's relevant to the event loop along with data that internal
    // systems need to dispatch the event associated with this data with just a pointer to this struct.
    union
    {
        efj::eventsys_epoll_data    epoll;
        efj::eventsys_waitable_data wait;
        efj::eventsys_timer_data    timer;
    };

    // A stale event is one that we shouldn't process b/c something about how we should handle the event has
    // changed since reading it off the event queue. Someone changed a callback, the set of monitored events, etc.
    bool                       stale;
    // A destroyed event is like a stale one, except the event source has been destroyed and needs to be freed.
    // Destroyed implies stale.
    bool                       destroyed;
    // :EventFlushing
    bool                       handleBeforeKill;
};

void eventsys_breakpoint::init()
{
    sem_init(&hitSem, 0, 0);
    sem_init(&waitSem, 0, 0);
};

void eventsys_breakpoint::hit_and_wait()
{
    sem_post(&hitSem);
    sem_wait(&waitSem);
};

void eventsys_breakpoint::wait_for_hits(int count)
{
    efj::thread* thisThread = efj::this_thread();
    __atomic_store_n(&thisThread->waitingOnOtherThreads, true, __ATOMIC_RELAXED);

    waitCount = count;
    for (int i = 0; i < count; ) {
        int result = sem_wait(&hitSem);
        if (result != 0) {
            efj_internal_log(efj::log_level_crit, "sem_wait() for i=%d/%d failed with %s",
                i, count, strerror(result));
            continue;
        }

        ++i;
    }

    __atomic_store_n(&thisThread->waitingOnOtherThreads, false, __ATOMIC_RELAXED);
};

void eventsys_breakpoint::continue_all()
{
    for (int i = 0; i < waitCount; ) {
        int result = sem_post(&waitSem);
        if (result != 0) {
            efj_internal_log(efj::log_level_crit, "sem_wait() for i=%d/%d failed with %s",
                i, waitCount, strerror(result));
            continue;
        }

        ++i;
    }
}

void eventsys_fence::init()
{
    sem_init(&hitSem, 0, 0);
};

void eventsys_fence::hit()
{
    sem_post(&hitSem);
}

u32 eventsys_fence::wait_for_hits(u32 count)
{
    efj::thread* thisThread = efj::this_thread();
    __atomic_store_n(&thisThread->waitingOnOtherThreads, true, __ATOMIC_RELAXED);

    u32 i = 0;
    for ( ; i < count; i++) {
        int result = sem_wait(&hitSem);
        if (result != 0)
            break;
    }

    __atomic_store_n(&thisThread->waitingOnOtherThreads, false, __ATOMIC_RELAXED);
    return i;
}

u32 eventsys_fence::wait_for_hits(u32 count, int timeoutMs, int intervalMs)
{
    efj_assert(intervalMs <= 1000);

    efj::msec before = efj::now_real_msec();
    efj::msec waitTime = {};

    u32 successfulWaits = 0;
    for (;;) {
        // We have to use the realtime clock since that's the clock that sem_timedwait uses.
        struct timespec clockValue = {};
        ::clock_gettime(CLOCK_REALTIME, &clockValue);

        clockValue.tv_nsec += intervalMs * 1000000;
        if (clockValue.tv_nsec > 1000000000) {
            clockValue.tv_nsec -= 1000000000;
            ++clockValue.tv_sec;
        }

        for (;;) {
            if (successfulWaits >= count)
                return successfulWaits;

            int result = sem_timedwait(&hitSem, &clockValue);
            if (result == -1) {
                if (errno != ETIMEDOUT) {
                    efj_internal_log(efj::log_level_crit, "sem_timedwait returned \"%s\" when passed %u.%ld",
                            strerror(errno), (u32)clockValue.tv_sec, clockValue.tv_nsec);

                    return successfulWaits;
                }

                break;
            }

            ++successfulWaits;
        }

        efj::msec after = efj::now_real_msec();
        waitTime += after - before;
        before = after;

        if (waitTime.value >= (u64)timeoutMs)
            break;
    }

    return successfulWaits;
}

void eventsys_thread_inbox::push(efj::eventsys_thread_event* events)
{
    pthread_mutex_lock(&mutex);
    *tail = events;
    tail  = &events->next;
    pthread_mutex_unlock(&mutex);
}

efj::eventsys_thread_event* eventsys_thread_inbox::pop_all()
{
    pthread_mutex_lock(&mutex);
    efj::eventsys_thread_event* result = events;
    tail = &events;
    pthread_mutex_unlock(&mutex);
    return result;
}

static void
_eventsys_pre_init(efj::event_system* sys, efj::memory_arena* arena)
{
    efj_panic_if(sys->initted);
    sys->arena = arena;
}

static void
_eventsys_init_thread_context(efj::event_system* sys, efj::thread* thread, efj::memory_arena* arena)
{
    efj::eventsys_thread_ctx* ctx = thread->eventContext;
    efj_panic_if(sys->initted);
    efj_panic_if(ctx->initted);

    ctx->sys = sys;
    ctx->arena = arena;
    ctx->epollFd = ::epoll_create1(EPOLL_CLOEXEC);
    efj_assert(ctx->epollFd != -1);

    ctx->timerQueue.init();
    ctx->timerQueueFd = ::timerfd_create(CLOCK_MONOTONIC, TFD_CLOEXEC);
    efj_assert(ctx->timerQueueFd != -1);

    ctx->initted = true;
}

static void
_eventsys_pre_object_init(efj::event_system* sys)
{
    _eventsys_create_breakpoint(&sys->syncBpBefore);
    _eventsys_create_breakpoint(&sys->syncBpAfter);
    _eventsys_create_fence(&sys->killEventFence);

    // We add a non-null dispatch just so we can always assert that dispatch functions are non-null during creation.
    auto nullDispatch = [](efj::eventsys_event_data*){};

    for (efj::thread* thread : efj::threads()) {
        efj::eventsys_thread_ctx* ctx = thread->eventContext;

        efj::eventsys_epoll_desc timerQueueDesc = {};
        timerQueueDesc.udata.as_ptr = ctx;
        timerQueueDesc.dispatch     = nullDispatch;
        timerQueueDesc.events       = efj::io_in;
        _eventsys_create_epoll_event(thread, "Timer Queue", ctx->timerQueueFd, &timerQueueDesc, &ctx->timerQueueEvent);
        _eventsys_add_epoll_event(thread, &ctx->timerQueueEvent);

        efj::eventsys_waitable_desc killDesc = {};
        killDesc.completedFence = &sys->killEventFence;
        killDesc.dispatch = nullDispatch;
        _eventsys_create_waitable_event(thread, "Kill", &killDesc, &ctx->killEvent);
        _eventsys_add_waitable_event(thread, &ctx->killEvent);

        efj::eventsys_waitable_desc threadEventsAvailableDesc = {};
        threadEventsAvailableDesc.udata.as_ptr = ctx;
        threadEventsAvailableDesc.dispatch     = &_eventsys_handle_async_events;
        _eventsys_create_waitable_event(thread, "Thread Events", &threadEventsAvailableDesc, &ctx->threadEventsAvailable);
        _eventsys_add_waitable_event(thread, &ctx->threadEventsAvailable);

        ctx->outboxes = efj_push_array(*ctx->arena, efj::thread_count(), efj::eventsys_thread_outbox);
        for (umm i = 0; i < efj::thread_count(); i++)
            ctx->outboxes[i].init();

        ctx->threadTouchedThisEvent = efj_push_array_zero(*ctx->arena, efj::thread_count(), bool);
        ctx->touchedThreads         = efj_push_array_zero(*ctx->arena, efj::thread_count(), efj::thread*);
        ctx->touchedThreadCount     = 0;

        efj::eventsys_waitable_desc syncEventAvailableDesc = {};
        syncEventAvailableDesc.udata.as_ptr = ctx;
        syncEventAvailableDesc.dispatch     = &_eventsys_handle_sync_event;
        _eventsys_create_waitable_event(thread, "Sync Event", &syncEventAvailableDesc, &ctx->syncEventAvailable);
        _eventsys_add_waitable_event(thread, &ctx->syncEventAvailable);
    }

    sys->initted = true;
}

static void
_eventsys_start(efj::event_system* sys)
{
    // :TESTING If we are running a test, we want to use abstract time.
    if (efj::testing()) {
        // This is called before background threads have started handling events and we know that
        // we are running a test. We assume for now that all IO-specific objects have been disabled
        // by the testing code. All we should have to worry about is making timers and timer queues
        // use abstract testing time.
        efj_assert(sys->initted);
        sys->testPosixTime = efj::now_ptime_real();
        sys->testTimerTime = efj::now_real();

        // Disable all the timer queues' timerfds. We want to make those expire instantly
        // whenever abstract time is updated.
        for (efj::thread* thread : efj::threads()) {
            efj::eventsys_thread_ctx* threadContext = thread->eventContext;

            struct itimerspec disabled = {};
            int result = ::timerfd_settime(threadContext->timerQueueFd, TFD_TIMER_ABSTIME, &disabled, nullptr);
            if (result != 0) {
                efj_internal_log(efj::log_level_crit, "timerfd_settime(): %s", strerror(errno));
                efj_panic("timerfd_settime()");
            }
        }
    }

    // In case any user code or system initialization logged or something during init().
    _eventsys_handle_deferred_actions(efj::main_thread());
}

static EFJ_MACRO void
_mark_stale(efj::eventsys_thread_ctx* ctx, efj::eventsys_event* e)
{
    if (!e->edata->stale) {
        e->edata->stale = true;
        efj_slist_push(ctx->staleEventDataList, e->edata, nextStale);
    }
}

// Re-arm events that have been marked stale, and free events that have been destroyed.
static EFJ_MACRO void
_handle_stale_events(efj::eventsys_thread_ctx* ctx)
{
    efj::eventsys_event_data** prev = &ctx->staleEventDataList;
    for (efj::eventsys_event_data* edata = ctx->staleEventDataList; edata; ) {
        //efj_log_info("[%u] Dealing with stale %p", efj::logical_thread_id(), edata);

        // If an event is stale _and_ the event source is destroyed, add it to the free list. Otherwise,
        // its just stale and we can reset it so we continue handle events for this source next time we poll
        // an event for it.
        if (edata->destroyed) {
            efj_slist_push(ctx->eventDataFreeList, edata, nextFree);
        }

        efj::eventsys_event_data* next = edata->nextStale;
        efj_slist_remove(prev, edata, nextStale);
        edata->stale = false;

        edata = next;
    }
}

static void
_update_timer_queue_expire_time(efj::eventsys_thread_ctx* ctx)
{
    // :TESTING We don't to update the timer queue fd during testing.
    if (efj::testing())
        return;

    // If we have timers running, set the timer-queue timer to expire at the next expire time.
    // Otherwise, just leave the expire time set to zero, disabling the queue timer.

    // :TESTING We don't want to update the timerfd like this if we are testing. During testing, time is
    // manually updated and we make timerfds expire instantly when we want them to.
    struct itimerspec expireTime = {};
    if (ctx->timerQueue.size()) {
        // Our timer queue's ticks match the value of CLOCK_MONOTONIC.
        // FIXME: timespec values will wrap before the timer queue's will.
        // We have a 2038 problem here, and we will run into problems if this code is running when the
        // 32-bit value for seconds starts wrapping. Right now, the timerfd will start expiring
        // b/c the next expire time will appear to be before the current clock value. However, no events
        // will be available on the timer queue, so the event loop will run as fast as possible and eat
        // CPU until the value of CLOCK_MONOTONIC wraps. Unfortunately, expire-times in the timer queue
        // past UINT32_MAX absolute seconds will now never expire b/c they can't be reached. Newer versions
        // of Linux provide a 64-bit API for this. :2038.
        //
        u64 nextExpireTimeNs = ctx->timerQueue.next_expire_time();
        time_t sec  = nextExpireTimeNs / 1000000000u;
        u32    nsec = nextExpireTimeNs % 1000000000u;
        expireTime.it_value = {sec, (long)nsec};
    }

    struct itimerspec prevExpireTime = {};
    int result = ::timerfd_settime(ctx->timerQueueFd, TFD_TIMER_ABSTIME, &expireTime, &prevExpireTime);
    if (result != 0) {
        efj_internal_log(efj::log_level_crit, "timerfd_settime(): %s", strerror(errno));
        efj_panic("timerfd_settime()");
    }

    // We need to mark the timer queue event as stale when we change the expire time.
    if (expireTime.it_value.tv_sec != prevExpireTime.it_value.tv_sec ||
        expireTime.it_value.tv_nsec != prevExpireTime.it_value.tv_nsec) {
        _mark_stale(ctx, &ctx->timerQueueEvent);
    }
}

static efj::eventsys_update_result
_eventsys_update_thread(efj::thread* thread, int timeoutMs, efj::exit_code* code)
{
    efj::eventsys_thread_ctx* ctx = thread->eventContext;

    // IMPORTANT: We logically want to update the timer queue when we start new timers and when we
    // pull events off the timer queue. However, the timer queue timer can only expire once per batch
    // of events, so we just update it once per batch, before we poll.
    // Also, this call needs to happen before we handle stale events to prevent the initial setting of
    // the timeout from causing the first timer-queue event to be skipped for the first iteration of the event loop.
    _update_timer_queue_expire_time(ctx);

    // IMPORTANT: We handle stale events before polling (instead of after) to allow code to mark events
    // as stale outside of the epoll event batch. For example, if we did this after the event loop, timers
    // started during init() would be marked as stale and any expirations during the first iteration of the event
    // loop would not be handled. We could also solve this problem by checking whether we are in this function
    // or outstide of it and only really mark events stale if they are marked stale during event handling, but
    // I decided to just to order this properly.
    _handle_stale_events(ctx);

    struct epoll_event events[EVENTSYS_MAX_EVENTS_PER_POLL];
    int readyCount = ::epoll_wait(ctx->epollFd, events, EVENTSYS_MAX_EVENTS_PER_POLL, timeoutMs);
    if (readyCount < 0) {
        // epoll_wait() may be intenionally broken out of when using a debugger, when we communicate
        // with signals, etc. No other error should occur.
        if (errno != EINTR) {
            efj_internal_log(efj::log_level_crit, "epoll_wait(): %s", strerror(errno));
            efj_panic("epoll_wait()");
        }
    }

    // NOTE: Events that are backed by file descriptors and waited on through epoll get round-robin'd and/or
    // are otherwise returned out of order. This means that triggering an event A then sending the kill event
    // and waiting for a thread to be killed doesn't guarantee that the thread handled A. To fix this, we flag
    // events like A and increment a counter of events that need to be flushed. Then, we wait for that number
    // of flagged events to be handled before processing the kill event.
    // :EventFlushing
    //
    bool gotKillEvent = false;

    for (int i = 0; i < readyCount; i++) {
        efj_arena_scope(efj::temp_memory());
        efj_arena_scope(efj::event_memory());
        // NOTE: We need to grab the allocator every event so the arena debug code will work.
        efj_allocator_scope(efj::event_memory());

        efj::_watchdog_on_thread_activity(thread);

        auto* edata = (efj::eventsys_event_data*)events[i].data.ptr;
        efj::epoll_events eventFlags = events[i].events;
        // All of the highest-level events are epoll events right now.
        int fd = edata->epoll.fd;

        efj::context().thisEventName = edata->event->name;

        //efj::print("[%d/%s] Handling event %s\n", getpid(), thread->name, edata->event->name);

        if (fd == ctx->timerQueueFd) {
            if (!edata->stale) {
                //efj::print("[%u] handling timer queue\n", efj::logical_thread_id());
                efj::timer_queue_event timerEvents[EVENTSYS_MAX_EVENTS_PER_POLL];

                // :TESTING
                // We update the time again in case enough time has passed that more timers are ready, but
                // we also have to do this in order for the testing-time update to be thread safe. When testing,
                // we make the timerfd expire manually, but don't try to update another thread's timer queue b/c
                // that wouldn't be thread safe.
                //
                ctx->timerQueue.set_time();
                umm n = ctx->timerQueue.poll(timerEvents);
                //efj::print("[%u] got %zu timer events\n", efj::logical_thread_id(), n);

                // If the timer queue timer expires, and there aren't any events available, that's either
                // a bug or it's the year 2038 and time_t just wrapped, in which case the issue is unrecoverable
                // until the app is restarted after CLOCK_MONOTONIC has wrapped. :2038
                // :TESTING While testing, we currently implement setting abstract time by forcing the timer queue
                // timer to expire, so we check for that.
                if (n == 0 && !efj::testing()) {
                    efj::print_error("timer queue size: %zu\n", ctx->timerQueue.size());
                    if (ctx->timerQueue.size()) {
                        efj::print_error("clock: %llu, next expire time: %llu\n", (efj::llu)efj::now_nsec().value,
                            (efj::llu)ctx->timerQueue.next_expire_time());
                    }

                    efj_panic("Timer queue timeout with no events available");
                }

                for (umm i = 0; i < n; i++) {
                    const efj::timer_queue_event& e = timerEvents[i];
                    auto* timerEvent = (efj::eventsys_timer_event*)e.handle->timer;
                    efj::eventsys_event_data* timerEdata = timerEvent->edata;

                    efj::context().thisEventName = timerEvent->name;
                    efj::_set_this_object(timerEvent->owningObject EFJ_LOC_ARGS);
                    efj::_debugsys_begin_event(*thread->debugContext, timerEdata);

                    if (!timerEdata->stale) {
                        efj_panic_if(!timerEvent->running);

                        // Oneshot timers stop automatically. The timer queue handles this, but we
                        // need to update our user-facing understanding of the timer state.
                        if (timerEvent->type == efj::timer_oneshot) {
                            _eventsys_stop_timer(timerEvent);
                        }

                        //efj::print("Dispatching timer event %s\n", timerEvent->name);
                        timerEdata->timer.expiredTime  = e.expiredTime;
                        timerEdata->timer.expiredCount = e.expiredCount;
                        timerEdata->dispatch(timerEdata);
                    }
                    else {
                        //efj::print("Timer event for %s is stale\n", timerEvent->name);
                    }

                    _eventsys_handle_deferred_actions(thread);
                    efj::_debugsys_end_event(*thread->debugContext, timerEdata);
                    efj::_set_this_object(nullptr EFJ_LOC_ARGS);
                }
            }
        }
        else if (edata->type == efj::eventsys_event_waitable) {
            efj::_set_this_object(edata->event->owningObject EFJ_LOC_ARGS);
            efj::_debugsys_begin_event(*thread->debugContext, edata);

            static_assert(std::is_base_of<efj::eventsys_epoll_event, efj::eventsys_waitable_event>::value,
                "We assume waitable events are epoll events to get the fd");

            //efj::print("[%s] Handling waitable event %s (hbk:%d,stale:%d)\n", efj::this_thread()->name, edata->event->name,
            //        edata->handleBeforeKill, edata->stale);

            // :StaleEventProcessing
            if (!edata->stale) {
                efj_read_eventfd(fd);

                if (edata->wait.beginBp) {
                    //efj_internal_log(efj::log_level_debug, "hitting bp");
                    edata->wait.beginBp->hit_and_wait();
                }

                if (edata->dispatch)
                    edata->dispatch(edata);

                if (edata->wait.endBp) {
                    //efj_internal_log(efj::log_level_debug, "hitting end bp");
                    edata->wait.endBp->hit_and_wait();
                    //efj_internal_log(efj::log_level_debug, "done end bp");
                }

                // We don't want to hit the completedFence on the kill event until we are
                // done with :EventFlushing. This is a bit of a @Hack, really.
                static thread_local bool killed = false;
                if (edata->event == &ctx->killEvent) {
                    efj_assert(!killed);
                    killed = true;

                    //efj::print("[%s] Handling kill event.\n", thread->name);
                    gotKillEvent = true;
                }
                else if (edata->wait.completedFence)
                    edata->wait.completedFence->hit();
            }

            _eventsys_handle_deferred_actions(thread);
            efj::_debugsys_end_event(*thread->debugContext, edata);
            efj::_set_this_object(nullptr EFJ_LOC_ARGS);
        }
        else if (edata->type == efj::eventsys_event_epoll) {
            efj::_set_this_object(edata->event->owningObject EFJ_LOC_ARGS);
            efj::_debugsys_begin_event(*thread->debugContext, edata);

            // :StaleEventProcessing
            if (!edata->stale) {
                efj::_set_this_fd(edata->epoll.fd);
                efj::_set_this_fd_events(eventFlags);
                efj::_set_this_epoll(ctx->epollFd);

                edata->epoll.events = eventFlags;
                edata->dispatch(edata);
            }

            _eventsys_handle_deferred_actions(thread);
            efj::_debugsys_end_event(*thread->debugContext, edata);

            // TODO: probably get rid of these APIs. They haven't been used yet and are easy to add back.
            efj::_set_this_fd(-1);
            efj::_set_this_fd_events(0);
            efj::_set_this_epoll(-1);
            efj::_set_this_object(nullptr EFJ_LOC_ARGS);
        }
        else {
            efj_invalid_code_path();
        }

        // :EventFlushing
        // We require that the function used to mark an event to be handled before the kill-event is called
        // before that event is triggered. Therefore, we must have called epoll_wait(), which
        // has appropriate memory barriers such that the number of events to handle should be accurate.
        if (!edata->stale && __atomic_load_n(&edata->handleBeforeKill, __ATOMIC_RELAXED)) {
            u32 valueBefore = __atomic_fetch_sub(&ctx->eventsToFlushBeforeKill, 1, __ATOMIC_RELAXED);
            efj_panic_if(valueBefore == 0);

            __atomic_store_n(&edata->handleBeforeKill, false, __ATOMIC_RELAXED);
        }

        //efj::print("[%s] Handling event done\n", thread->name);
    }

    // The rest of this code intentionally happens after we've processed a batch of events.
    // We need to:
    // 1. Finish or continue processing the kill event.
    // 2. Process events that were marked stale during the last batch.

    // WARNING: This code is vulnerable to people marking events as needing to be handled before kill
    // _after_ triggering the kill event, which is not supported.
    // FIXME: We need a thread-local flag in thread-killing API itself to make sure nobody uses it incorrectly.
    //
    // :EventFlushing
    //
    if (gotKillEvent || ctx->flushingEvents) {
        u32 eventsToFlushBeforeKill = __atomic_load_n(&ctx->eventsToFlushBeforeKill, __ATOMIC_RELAXED);
        if (eventsToFlushBeforeKill == 0) {
            ctx->flushingEvents = false;

            // RELEASE to publish any changes we made before marking this thread as killed.
            // This is mostly pedantic, since at the time of writing we signal the the completed fence
            // (which issues the appropriate memory barriers) right after.
            __atomic_store_n(&thread->killed, true, __ATOMIC_RELEASE);
            //efj::print("[%s] Hitting kill event fence\n", efj::this_thread()->name);
            ctx->sys->killEventFence.hit();

            *code = efj::exit_normal;
            return efj::eventsys_update_killed;
        }

        ctx->flushingEvents = true;
    }

    efj_assert(!efj::this_object());
    return efj::eventsys_update_continue;
}

// Called from any thread on an even that lives on any other thread.
// IMPORTANT: May be called from a signal handler until there are no more :EventSysOnThreadCrashed tags.
static void
_eventsys_handle_before_kill(efj::eventsys_event* e)
{
    // We don't allow calling this function twice on the same event, but the error handling code is
    // racy anyway if multiple threads call this function for the same event. We just make sure that
    // we don't increment the events-to-handle count more than once.
    //
    // TODO: We can still do some single-threaded error handling that will catch most of the errors.
    // We don't want a single thread to call this more than once on a single event, and we don't want
    // a thread to call this after starting to kill other threads.
    //
    if (!__atomic_test_and_set(&e->edata->handleBeforeKill, __ATOMIC_RELAXED)) {
        //efj::print("[%s] Marking %s to be handled before kill\n", efj::this_thread()->name, e->name);
        __atomic_fetch_add(&e->owningThread->eventContext->eventsToFlushBeforeKill, 1, __ATOMIC_RELAXED);
    }
}

static void
_eventsys_kill_all_threads(efj::event_system* sys)
{
    efj::_logsys_handle_deferred(efj::this_thread());

    // FIXME: We don't seem to detect crashed threads when we are running a test
    u32 nThreadsCrashed = 0;

    for (efj::thread* thread : efj::threads()) {
        _eventsys_trigger(&thread->eventContext->killEvent);
        if (efj::_watchdog_is_thread_crashed(thread)) {
            ++nThreadsCrashed;
        }
    }

    u32 nThreadsToKill  = efj::thread_count() - nThreadsCrashed;
    u32 nThreadsKilled  = 0;
    int remainingTimeMs = 5000;

    efj::exit_code code;
    for (;;) {
        efj::msec before = efj::now_msec();
        // TODO: Make update_thread() take a max event count parameter as well, so we don't have to
        // specify a maximum wait time, here.
        _eventsys_update_thread(efj::this_thread(), 5, &code);
        nThreadsKilled += sys->killEventFence.wait_for_hits(nThreadsToKill - nThreadsKilled, 10);
        if (nThreadsKilled >= nThreadsToKill)
            break;

        efj::msec after = efj::now_msec();
        remainingTimeMs -= (int)(after - before).value;
        if (remainingTimeMs <= 0)
            break;

        // TODO: The expected hit count is the total number of threads minux the number we expect to have crashed,
        // so we need to get the expected number from the watchog system.
        //efj::print("Hit count: %u/%u, %zu ms\n", nThreadsKilled, efj::thread_count(), (after-before).value);
    }

    if (nThreadsToKill != nThreadsKilled) {
        efj::string_builder msg(efj::alloc_temp);
        char startText[] = "The following threads could not be killed gracefully: ";
        char endText[] = "Exiting anyway.\n";

        msg.append(startText);
        for (efj::thread* thread : efj::threads()) {
            if (!thread->killed) {
                msg.append_fmt("(%u)%s, ", thread->logicalId, thread->name);
            }
        }
        msg.append(endText);

        efj::print_error(msg.str().c_str());
    }
}

// We can only create/destroy/modify events for other threads
// before background threads start processing events.
static EFJ_MACRO void
_check_op_for_another_thread_is_safe(efj::thread* otherThread)
{
    if (efj::this_thread() != otherThread) {
        bool backgroundThreadsContinued =
            __atomic_load_n(&efj::app()._backgroundThreadsContinued, __ATOMIC_RELAXED);
        efj_panic_if(backgroundThreadsContinued);
    }
}

static void
_create_event(efj::thread* thread, const char* name, efj::eventsys_event* e)
{
    efj::eventsys_thread_ctx* ctx = thread->eventContext;
    efj_assert(ctx->initted);
    efj_assert(!e->created || e->destroyed);

    e->owningThread = thread;
    e->owningObject = efj::this_object(); // May be NULL, as intended.
    e->name = name;

    u32 eventId = 0;
    if (ctx->eventDataFreeList) {
        e->edata = ctx->eventDataFreeList;
        ctx->eventDataFreeList = ctx->eventDataFreeList->nextFree;
        eventId = e->edata->id; // Recycle the event id, nothing else.
    }
    else {
        e->edata = efj_push_type(*ctx->arena, efj::eventsys_event_data);
        eventId = ++ctx->lastEventId; // Zero is an invalid event ID.
    }

    memset(e->edata, 0, sizeof(efj::eventsys_event_data));
    e->edata->id = eventId;
    e->edata->event = e;
    e->created = true;

    efj::_debugsys_register_event(*thread->debugContext, *e->edata);
}

static void
_destroy_event(efj::thread* thread, efj::eventsys_event* e)
{
    _check_op_for_another_thread_is_safe(thread);
    efj_panic_if(e->owningThread != thread);
    efj_assert(e->created);

    efj::_debugsys_deregister_event(*thread->debugContext, *e->edata);

    e->edata->destroyed = true;
    _mark_stale(thread->eventContext, e);
}

static bool
_eventsys_create_fence(efj::eventsys_fence* fence)
{
    fence->init();
    return true;
}

static bool
_eventsys_create_breakpoint(efj::eventsys_breakpoint* bp)
{
    bp->init();
    return true;
}

static bool
_eventsys_create_timer(efj::thread* thread, const char* name,
    const efj::eventsys_timer_desc* desc, efj::eventsys_timer_event* e)
{
    efj_assert(desc->dispatch);

    // We don't have to assert that we can create a timer for another
    // thread because _create_event does that.
    _create_event(thread, name, e);
    e->edata->type     = efj::eventsys_event_timer;
    e->edata->udata    = desc->udata;
    e->edata->dispatch = desc->dispatch;
    e->edata->objectCb = desc->objectCallback;
    e->initial         = desc->initial;
    e->interval        = desc->interval;
    e->type            = desc->type;
    e->absolute        = desc->absolute;
    // We don't have a timer handle until this timer is started.
    // The null/zero handle is valid.

    return true;
}

static void
_eventsys_monitor_timer(efj::eventsys_timer_event* e, const efj::callback& cb)
{
    _check_op_for_another_thread_is_safe(e->owningThread);
    e->edata->objectCb = cb;
}

static void
_eventsys_start_timer(efj::eventsys_timer_event* e)
{
    // NOTE: We only check if timer ops are safe in create, start, and stop to avoid a bunch of redundant checks.
    // This makes certain ops that are unsafe (like setting the timeout) potentially do unsafe things before
    // finally panicing. This shouldn't be a problem though, since this bug is usually a developer error.
    _check_op_for_another_thread_is_safe(e->owningThread);
    efj::eventsys_thread_ctx* ctx = e->owningThread->eventContext;

    _mark_stale(ctx, e);
    e->running = true;

    // NOTE: This call makes relative timers start relative to when this call to set_time(). We could
    // have made timers be relative to the timestamp at the start of the event by calling set_time() at
    // the start of each event, but that doesn't fully solve the problem of timers lurching forward in
    // time due to processing overhead. That's what absolute expire-times are for.
    ctx->timerQueue.set_time();

    // This stops the timer if necessary.
    //efj::print("[%u] Starting \"%s\" (absolute=%d)\n", efj::logical_thread_id(), e->name, e->absolute);
    ctx->timerQueue.start(&e->handle, e, e->initial.value, e->interval.value, e->absolute);
    _update_timer_queue_expire_time(ctx);
    //efj_log_debug("%s running=true", e->name);
}

static void
_eventsys_start_timer(efj::eventsys_timer_event* e, efj::nsec initial)
{
    //efj_log_debug("Starting timer %s with timeout %llu ns", e->name, timeout.value);
    _eventsys_set_timeout(e, initial);
    _eventsys_start_timer(e);
}

static void
_eventsys_start_timer(efj::eventsys_timer_event* e, efj::nsec initial, efj::nsec interval)
{
    //efj_log_debug("Starting timer %s with timeout %llu ns", e->name, timeout.value);
    _eventsys_set_timeout(e, initial, interval);
    _eventsys_start_timer(e);
}

static void
_eventsys_start_timer_absolute(efj::eventsys_timer_event* e, efj::nsec expireTime)
{
    //efj_log_debug("Starting timer %s with timeout %llu ns", e->name, timeout.value);
    _eventsys_set_absolute_timeout(e, expireTime);
    _eventsys_start_timer(e);
}

static void
_eventsys_start_timer_absolute(efj::eventsys_timer_event* e, efj::nsec expireTime, efj::nsec interval)
{
    //efj_log_debug("Starting timer %s with timeout %llu ns", e->name, timeout.value);
    _eventsys_set_absolute_timeout(e, expireTime, interval);
    _eventsys_start_timer(e);
}

static void
_eventsys_set_timeout(efj::eventsys_timer_event* e, efj::nsec initial)
{
    e->initial  = initial;
    e->absolute = false;
    _eventsys_stop_timer(e);
}

static void
_eventsys_set_timeout(efj::eventsys_timer_event* e, efj::nsec initial, efj::nsec interval)
{
    e->initial  = initial;
    e->interval = interval;
    e->absolute = false;
    _eventsys_stop_timer(e);
}

static void
_eventsys_set_absolute_timeout(efj::eventsys_timer_event* e, efj::nsec expireTime)
{
    e->initial  = expireTime;
    e->absolute = true;
    _eventsys_stop_timer(e);
}

static void
_eventsys_set_absolute_timeout(efj::eventsys_timer_event* e, efj::nsec expireTime, efj::nsec interval)
{
    e->initial  = expireTime;
    e->interval = interval;
    e->absolute = true;
    _eventsys_stop_timer(e);
}

static void
_eventsys_stop_timer(efj::eventsys_timer_event* e)
{
    _check_op_for_another_thread_is_safe(e->owningThread);

    //efj_log_debug("Stop timer %s", e->name);
    efj_assert(e->created);
    efj_assert(efj::this_thread() == e->owningThread);
    efj::eventsys_thread_ctx* ctx = e->owningThread->eventContext;

    _mark_stale(ctx, e);
    e->running = false;

    ctx->timerQueue.stop(&e->handle);
    _update_timer_queue_expire_time(ctx);
}

static void
_eventsys_destroy_timer_event(efj::eventsys_timer_event* e)
{
    _eventsys_stop_timer(e);
    _destroy_event(e->owningThread, e);
}

static bool
_eventsys_create_waitable_event(efj::thread* thread, const char* name,
    const efj::eventsys_waitable_desc* desc, efj::eventsys_waitable_event* e)
{
    int fd = ::eventfd(0, EFD_CLOEXEC);
    if (fd == -1)
        return false;

    efj::eventsys_epoll_desc epollDesc = {};
    epollDesc.udata          = desc->udata;
    epollDesc.dispatch       = desc->dispatch;
    epollDesc.objectCallback = desc->objectCallback;
    epollDesc.events         = efj::io_in;

    _eventsys_create_epoll_event(thread, name, fd, &epollDesc, e);
    e->edata->type                = efj::eventsys_event_waitable;
    e->edata->wait.completedFence = desc->completedFence;
    e->edata->wait.beginBp        = desc->beginBp;
    e->edata->wait.endBp          = desc->endBp;

    return false;
}

static void
_eventsys_add_waitable_event(efj::thread* thread, efj::eventsys_waitable_event* e)
{
    _eventsys_add_epoll_event(thread, e);
}

// May be called from a signal handler.
static void
_eventsys_trigger(efj::eventsys_waitable_event* e)
{
    efj_assert(e->created);
    //efj::print("Triggering %s from thread %s (pid=%d)\n", e->name, efj::this_thread()->name, getpid());
    efj_trigger_eventfd(e->fd);
}

static bool
_eventsys_create_epoll_event(efj::thread* thread, const char* name, int fd,
    const efj::eventsys_epoll_desc* desc, efj::eventsys_epoll_event* e)
{
    efj_assert(desc->dispatch);

    _create_event(thread, name, e);
    e->edata->type         = efj::eventsys_event_epoll;
    e->edata->udata        = desc->udata;
    e->edata->dispatch     = desc->dispatch;
    e->edata->objectCb     = desc->objectCallback;
    e->epollEvent.data.ptr = e->edata;
    e->epollEvent.events   = desc->events;
    e->edata->epoll.fd     = fd;
    e->fd                  = fd;
    // XXX
    //efj::print("Creating event %s (fd: %d, events: %s)\n", name, fd, efj::string_from_fd_events(desc->events).c_str());

    return false;
}

static int
_eventsys_destroy_epoll_event(efj::thread* thread, efj::eventsys_epoll_event* e)
{
    //efj::print("Destroying event %s on thread %s\n", e->name, thread->name);
    _eventsys_del_epoll_event(thread, e);
    int result = ::close(e->fd);
    e->fd = -1;
    efj::_destroy_event(thread, e);

    return result;
}

static void
_eventsys_add_epoll_event(efj::thread* thread, efj::eventsys_epoll_event* e)
{
    _check_op_for_another_thread_is_safe(thread);
    efj_assert(e->created);
    efj_assert(thread == e->owningThread);
    // TODO: Figure out if there are any valid use-cases for just monitoring for implicitly monitored events
    // like errors. Every time I have seen events == 0, it has been a bug, which is why I'm asserting here.
    efj_assert(e->epollEvent.events);

    // XXX
    //efj::print("Adding epoll event %s (%d) to thread %s\n", e->edata->event->name, e->fd, thread->name);

    int result = ::epoll_ctl(thread->eventContext->epollFd, EPOLL_CTL_ADD, e->fd, &e->epollEvent);
    efj_panic_if(result == -1);
    e->monitored = true;
}

static void
_eventsys_mod_epoll_event(efj::thread* thread, efj::eventsys_epoll_event* e,
    efj::epoll_events* events, efj::callback* objectCb)
{
    efj_assert(e->created);
    efj_assert(e->monitored);

    //XXX
    //efj::print("%s: %s.%s (%d.%d)\n", __func__, thread->name, e->name, thread->eventContext->epollFd, e->fd);

    if (objectCb) {
        auto* edata = ((efj::eventsys_event_data*)e->epollEvent.data.ptr);
        edata->objectCb = *objectCb;
    }

    if (events)
        e->epollEvent.events = *events;

    efj_panic_if(::epoll_ctl(thread->eventContext->epollFd, EPOLL_CTL_MOD, e->fd, &e->epollEvent) == -1);
}

// This removes the event from the epoll set. It does _not_ destroy the event / close the fd.
static void
_eventsys_del_epoll_event(efj::thread* thread, efj::eventsys_epoll_event* e)
{
    efj_assert(e->created);
    efj_panic_if(::epoll_ctl(thread->eventContext->epollFd, EPOLL_CTL_DEL, e->fd, &e->epollEvent) == -1);
    _mark_stale(thread->eventContext, e);
}

static void
_eventsys_monitor_epoll_event(efj::thread* thread, efj::eventsys_epoll_event* e,
    efj::epoll_events* events, efj::callback* objectCb)
{
    if (!e->monitored) {
        // Adjusting the events and callback are both optional.
        if (events)
            e->epollEvent.events = *events;

        if (objectCb) {
            auto* edata = ((efj::eventsys_event_data*)e->epollEvent.data.ptr);
            edata->objectCb = *objectCb;
        }

        // monitored=true set by add_epoll_event.
        _eventsys_add_epoll_event(thread, e);
    }
    else {
        // If the event is already monitored, we just pass the optional values through to be dealt with later.
        _eventsys_mod_epoll_event(thread, e, events, objectCb);
    }

    efj_assert(e->monitored);
}


static efj::eventsys_allocated_event
_eventsys_allocate_event(efj::thread* sourceThread, efj::thread* targetThread, umm eventSize)
{
    efj::eventsys_allocated_event result = {};

    efj::eventsys_thread_outbox* outbox = &sourceThread->eventContext->outboxes[targetThread->logicalId];
    outbox->maybe_allocate();

    // WARNING: We assume the derived event type has an alignment <= to eventsys_thread_event.
    // :ThreadEventAlignment
    result.arena = &outbox->arena;
    result.e     = (efj::eventsys_thread_event*)efj_push(outbox->arena, eventSize,
                                                         alignof(efj::eventsys_thread_event));

    // For debugging.
    memset(result.e, 0, eventSize);

    return result;
}

static void
_eventsys_send_event(efj::thread* sourceThread, efj::thread* targetThread, const efj::eventsys_allocated_event* event)
{
    efj::eventsys_thread_ctx*    sourceContext = sourceThread->eventContext;
    u32                          targetLtid    = targetThread->logicalId;
    efj::eventsys_thread_outbox* outbox        = &sourceContext->outboxes[targetLtid];
    efj::memory_arena*           outboxArena   = &outbox->arena;

    event->e->srcOutbox = outbox;
    event->e->endChunk  = efj::get_current_chunk(*outboxArena);

    //efj::print("[%s]: Sending event to thread %s\n", sourceThread->name, targetThread->name);
    outbox->push(event->e);

    if (!sourceContext->threadTouchedThisEvent[targetLtid]) {
        sourceContext->threadTouchedThisEvent[targetLtid] = true;
        sourceContext->touchedThreads[sourceContext->touchedThreadCount] = targetThread; // TODO: :ArrayAPI
        sourceContext->touchedThreadCount++;
        _eventsys_add_deferred_action(sourceThread, efj::eventsys_defer_events);
    }
}

static efj::eventsys_allocated_event
_eventsys_allocate_sync_event(efj::thread* sourceThread, umm eventSize,
    efj::thread** targetThreads, umm targetThreadCount)
{
    efj::eventsys_allocated_event result = {};
    efj::thread* targetThread = efj::app()._eventSchedulingThread;

    efj::eventsys_thread_outbox* outbox = &sourceThread->eventContext->outboxes[targetThread->logicalId];
    outbox->maybe_allocate();

    // WARNING: We assume the derived event type has an alignment <= to eventsys_thread_event.
    // :ThreadEventAlignment
    result.arena = &outbox->arena;
    result.e     = (efj::eventsys_thread_event*)efj_push(outbox->arena, eventSize,
                                                         alignof(efj::eventsys_thread_event));
    // For debugging.
    memset(result.e, 0, eventSize);

    // We don't collate the allocations b/c we would have to worry about aligning the thread array anyway.
    result.e->targetThreads     = efj_push_array(outbox->arena, targetThreadCount, efj::thread*);
    result.e->targetThreadCount = targetThreadCount;

    // We assume that the number of threads will be small enough that this will be faster than memcpy.
    for (umm i = 0; i < targetThreadCount; i++)
        result.e->targetThreads[i] = targetThreads[i];

    return result;
}

static void
_eventsys_send_sync_event(efj::thread* sourceThread, const efj::eventsys_allocated_event* e)
{
    // NOTE: We could to a check to see if there is only one target thread and send the event
    // directly to that thread without involving the scheduling thread, but we would need the
    // same check in the allocation function and in here. That's not that big of a deal, since it
    // should be optimized out, but we just don't do that, yet. Callers that want that optimization
    // can implement the special-case of one target thread themselves for now.

    efj::thread* schedulingThread = efj::app()._eventSchedulingThread;
    _eventsys_send_event(sourceThread, schedulingThread, e);
}

static void
_eventsys_add_deferred_action(efj::thread* thread, efj::eventsys_deferred deferred)
{
    thread->eventContext->deferred |= deferred;
}

// We generally defer inter-thread communication to the end of an event to be
// sure that any side effects of an event (like writing to a socket) are completed
// before letting other threads run. There are some cases where this is suboptimal,
// but it generally gives us the opportunity to minimize context switches, and any
// performance impacts can be worked around by adjusting the threading situation.
//
static void
_eventsys_handle_deferred_actions(efj::thread* thread)
{
    efj::eventsys_deferred deferred = thread->eventContext->deferred;

    // It should be fine to not handle this generically, since the number of internal systems
    // shouldn't change very often. Also, having an explicit order gives us control over the priority
    // of various deferred actions.
    if (deferred) {
        if (deferred & efj::eventsys_defer_events) {
            efj::eventsys_thread_ctx* sourceContext = thread->eventContext;

            for (umm i = 0; i < sourceContext->touchedThreadCount; i++) {
                efj::thread*                 targetThread = sourceContext->touchedThreads[i];
                efj::eventsys_thread_inbox*  targetInbox  = &targetThread->eventContext->inbox;
                efj::eventsys_thread_outbox* sourceOutbox = &sourceContext->outboxes[targetThread->logicalId];

                efj::eventsys_thread_event* events = sourceOutbox->consume_nodes();
                targetInbox->push(events);

                sourceOutbox->recycle_memory();

                _eventsys_trigger(&targetThread->eventContext->threadEventsAvailable);
                sourceContext->threadTouchedThisEvent[targetThread->logicalId] = false;
            }

            sourceContext->touchedThreadCount = 0;
        }

        // Call into other systems in case they have deferred actions that can't be/aren't
        // implemented with thread events.
        if (deferred & efj::eventsys_defer_pc)
            efj::_pcsys_handle_deferred(thread);
        if (deferred & efj::eventsys_defer_user_events)
            efj::_uesys_handle_deferred(thread);
        if (deferred & efj::eventsys_defer_logging)
            efj::_logsys_handle_deferred(thread);

        thread->eventContext->deferred = 0;
    }
}

static void
_eventsys_handle_thread_crashed(efj::thread* thread)
{
    efj::_logsys_handle_deferred(thread);
}

// Common handler for async events received on any thread.
// All events start as async events, since sync events are just async events that are sent to the scheduling thread.
static void
_eventsys_handle_async_events(efj::eventsys_event_data* edata)
{
    auto* ctx = (efj::eventsys_thread_ctx*)edata->udata.as_ptr;
    efj::eventsys_thread_inbox* inbox = &ctx->inbox;

    // FIXME: This whole thing will be reported as one big event as far as the debug system is concerned.
    // We want to end the current event (which is the thread-event queue event) and begin/end each event
    // we pull off the queue individually.

    efj::eventsys_thread_event* events = inbox->pop_all();

    for (efj::eventsys_thread_event* cur = events; cur; cur = cur->next) {
        // We are basically checking to see if we are running this event on the event-scheduling thread,
        // because no other thread events should have the targetThreads field populated.
        if (cur->targetThreads)
            _eventsys_dispatch_sync_event(cur);
        else
            cur->dispatch(cur);

        cur->srcOutbox->mark_handled(cur);
    }
}

// Runs on the sync-event scheduling thread when it's been sent an event that needs to be sent out.
// We want sync events to be handled atomically by all subscribed threads, _on_ those threads.
// To do that, we
// 1. Store a pointer to the current event as a global.
// 2. Trigger an OS event letting subscribed threads know an event is available.
// 3. Wait for all subscribed threads to receive the event (but not handle it).
// 4. Tell all threads to handle the event.
// 5. Wait for all threads to have handled the event (and stopped).
// 5. Tell all threads to continue handling other events as usual.
//
static void
_eventsys_dispatch_sync_event(efj::eventsys_thread_event* e)
{
    efj::thread* schedulingThread = efj::this_thread();
    efj_assert(schedulingThread->does_event_scheduling());

    efj::event_system* sys = schedulingThread->eventContext->sys;

    efj::thread** targetThreads = e->targetThreads;
    umm targetThreadCount = e->targetThreadCount;

    // Doesn't matter if this is atomic, since there are memory barriers in event triggering.
    sys->syncEvent = e;
    sys->syncCompletedFence = e->completedFence;

    for (umm i = 0; i < targetThreadCount; i++) {
        _eventsys_trigger(&targetThreads[i]->eventContext->syncEventAvailable);
    }

    sys->syncBpBefore.wait_for_hits(targetThreadCount);
    sys->syncBpBefore.continue_all();
    sys->syncBpAfter.wait_for_hits(targetThreadCount);
    sys->syncBpAfter.continue_all();
}

static void
_eventsys_handle_sync_event(efj::eventsys_event_data* edata)
{
    efj::eventsys_thread_ctx* threadContext = (efj::eventsys_thread_ctx*)edata->udata.as_ptr;
    efj::event_system* sys = threadContext->sys;

    // We don't check for null here b/c if a sync event didn't have any wait state, that's
    // a developer error, and these are all essentially assertions. The fence is optional, though.

    sys->syncBpBefore.hit_and_wait();

    // Force all object communication to be queued, even if objects live on the same thread.
    // We do this do make sure objects that may be also be subscribed to this event don't
    // receive events originating from other subscribers before they have a chance to handle
    // the event completely themselves.
    //
    // TODO: @Speed @Space We technically don't have to queue to an object if that object is not subcribed
    // to the current event, but that would require architectural desicions and introduce overhead
    // in the event-dispatching code when making the decision to queue or not. For now, we just queue
    // all side-effects during a sync event.
    //
    threadContext->forceQueueSideEffects = true;
    sys->syncEvent->dispatch(sys->syncEvent);
    threadContext->forceQueueSideEffects = false;

    sys->syncBpAfter.hit_and_wait();

    if (sys->syncCompletedFence)
        sys->syncCompletedFence->hit();
}

static bool
_eventsys_force_queue_side_effects(efj::thread* thread, bool value)
{
    bool prev = thread->eventContext->forceQueueSideEffects;
    thread->eventContext->forceQueueSideEffects = value;
    return prev;
}

extern efj::string
string_from_fd_events(flag32 events)
{
    bool first = true;

    // @Speed: We can bake the pipe in to the string here.
    efj::string_builder result;
    if (events & efj::io_et) { result.append("et"); first = false; }
    if (events & efj::io_in) { if (!first) result.append("|"); result.append("in"); first = false; }
    if (events & efj::io_out) { if (!first) result.append("|"); result.append("out"); first = false; }
    if (events & efj::io_hup) { if (!first) result.append("|"); result.append("hup"); first = false; }
    if (events & efj::io_rdhup) { if (!first) result.append("|"); result.append("rdhup"); first = false; }
    if (events & efj::io_oneshot) { if (!first) result.append("|"); result.append("oneshot"); first = false; }
    if (events & efj::io_pri) { if (!first) result.append("|"); result.append("pri"); first = false; }
    if (events & efj::io_err) { if (!first) result.append("|"); result.append("err"); first = false; }

    return result.str();
}

// :EventSourceTypes
template <> const char*
c_str<>(efj::eventsys_event_type type)
{
    switch (type) {
    case eventsys_event_invalid: return "invalid"; break;
    case eventsys_event_timer: return "timer"; break;
    case eventsys_event_waitable: return "waitable"; break;
    case eventsys_event_epoll: return "fd"; break;
    default:
        efj_assert(type >= efj::eventsys_event_count_);
        return "invalid";
    }
}

//{ [TESTING]

static efj::timestamp
_eventys_test_get_now(efj::event_system* sys)
{
    efj::timestamp result;
    pthread_rwlock_rdlock(&sys->testTimeMutex);
    result = sys->testTimerTime;
    pthread_rwlock_unlock(&sys->testTimeMutex);

    return result;
}

static efj::posix_time
_eventsys_test_get_ptime(efj::event_system* sys)
{
    efj::posix_time result;
    pthread_rwlock_rdlock(&sys->testTimeMutex);
    result = sys->testPosixTime;
    pthread_rwlock_unlock(&sys->testTimeMutex);

    return result;
}

static void
_eventsys_test_advance(efj::event_system* sys, efj::nsec amount)
{
    efj::thread* thread = efj::this_thread();
    // We currently only expect this to happen on the main thread, but we can it
    // work from any thread if we want to.
    efj_panic_if(thread != efj::main_thread());

    pthread_rwlock_wrlock(&sys->testTimeMutex);
    sys->testPosixTime += amount;
    sys->testTimerTime += amount;
    pthread_rwlock_unlock(&sys->testTimeMutex);

    // Set an immediate timeout for all timerfds on all threads so the event loops will
    // process the timer queues.
    for (efj::thread* thread : efj::threads()) {
        efj::eventsys_thread_ctx* threadContext = thread->eventContext;

        struct itimerspec instant = {};
        instant.it_value.tv_nsec = 1;

        int result = ::timerfd_settime(threadContext->timerQueueFd, 0, &instant, nullptr);
        efj_panic_if(result != 0);
    }

    efj::exit_code code = efj::exit_count_;
    _eventsys_update_thread(thread, -1, &code);

    // We dont't expect/can't currently handle an exit code from here.
    // We only really need to update the timer queue, but it seems best to have
    // all event handling go through the same function whether we're testing or not.
    efj_assert(code == efj::exit_count_);
}
//} [TESTING]

} // end namespace efj

