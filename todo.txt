---
IO/Work queues, concurrency / parallelism
---
Better Debug visualization
---
Type erase commonly used data structures like efj::blist to improve compile times and reduce bloat. :TypeErasure
---
Integration/Unit Testing
---
Timers with millisecond granularity can be handled more efficiently with a min-heap and
having epoll() timeout like libevent and friends.
---
serial_port
---
* in progress
---
Rename tcp_connected_client to tcp_remote_client b/c the name is too long...
---
