namespace efj
{

template <typename T, typename KeyT, typename CompareT> inline T*
binary_search(T* base, umm n, const KeyT& key, CompareT cmp)
{
    while (n > 0) {
        T* pivot = base + (n >> 1);

        int result = cmp(key, *pivot);
        if (result == 0)
            return pivot;

        if (result > 0) {
            base = pivot + 1;
            n--;
        }

        n >>= 1;
    }

    return nullptr;
}


#define efj_reverse_list(head, next)\
efj::reverse_list((head), offsetof(std::remove_reference<decltype(**(head))>::type, next))

template <typename T> inline void
reverse_list(T** head, umm offset = 0)
{
    T*  prev = nullptr;

    for (auto* cur = *head; cur; ) {
        auto* next = *(T**)((char*)cur + offset);
        *(T**)((char*)cur + offset) = prev;

        prev = cur;
        cur  = next;
    }

    *head = prev;
}

} // namespace efj
