namespace efj
{

inline
log_queue::log_queue(umm bufferSize)
{
    // @Temporary
    efj_assert(bufferSize >= 4096 /*TODO: EFJ_LOG_MESSAGE_MAX*/);

    _arena1 = efj::allocate_arena("Log Buffer", bufferSize);
    _arena2 = efj::allocate_arena("Log Buffer", bufferSize);

    _writeBuffer = &_arena1;
    _readBuffer  = &_arena2;

    pthread_mutex_init(&_swapMutex, NULL);
}

inline void
log_queue::push(const efj::log_message_ctx* ctx, const efj::string& msg)
{
    // @Temporary
    efj_assert(msg.size <= 4096 /*TODO: EFJ_LOG_MESSAGE_MAX*/);

    pthread_mutex_lock(&_swapMutex);

    // If the queue is backed up because a thread is spamming log messages, we start
    // messages, but only from that thread.
    if (_queueSize > 500 /*TODO: EFJ_LOG_QUEUE_SIZE_MAX*/) {
        pthread_mutex_unlock(&_swapMutex);
        efj_temp_scope();

        bool hasNewline = msg.data[msg.size-1] == '\n';
        auto warning = efj::fmt("[c2] warning: dropping log message from thread %s: %.*s\n",
                            efj::thread_name(), (int)msg.size - hasNewline, msg.data);

        efj_check(::write(STDERR_FILENO, warning.data, warning.size) != -1);
        return;
    }

    umm sizeWithNul = msg.size + 1;

    auto* copy = (efj::log_message*)efj_push(*_writeBuffer,
        sizeof(efj::log_message) + sizeWithNul, alignof(efj::log_message));

    copy->next     = nullptr;
    copy->ctx      = *ctx;
    copy->msg.data = (char*)(copy + 1);
    copy->msg.size = msg.size;
    memcpy(copy->msg.data, msg.data, sizeWithNul);

    *_tailNext = copy;
    _tailNext  = &copy->next;

    ++_queueSize;
    pthread_mutex_unlock(&_swapMutex);
}

inline efj::log_message*
log_queue::swap_buffers()
{
    efj::log_message* result = nullptr;

    pthread_mutex_lock(&_swapMutex);
    result     = _head;
    _head      = nullptr;
    _tailNext  = &_head;
    _queueSize = 0;

    efj::swap(_writeBuffer, _readBuffer);
    efj::reset(*_writeBuffer);

    pthread_mutex_unlock(&_swapMutex);

    return result;
}

static void
_logsys_queue_log_message(efj::log_system* sys, const efj::log_message_ctx* ctx, const efj::string& msg)
{
    if (efj::testing()) {
        if (!efj::_testsys_on_log(ctx, msg))
            return;
    }

    if (!sys->initted) {
        char prefix[] = "[c2] pre-init log: ";
        struct iovec buffers[] = { {prefix, sizeof(prefix-1)}, {(void*)msg.data, msg.size} };
        efj_ignored_writev(STDOUT_FILENO, buffers, efj::array_size(buffers));
        return;
    }

    sys->logQueue->push(ctx, msg);
    efj::_eventsys_add_deferred_action(efj::this_thread(), efj::eventsys_defer_logging);
}
//}

static void
_logsys_pre_init(efj::log_system* sys, efj::memory_arena* arena)
{
    sys->arena = arena;
    sys->logQueue = efj_push_new(*sys->arena, efj::log_queue)(8_KB);

    sys->initted = true;
}

static void
_logsys_init_thread_context(efj::log_system* sys, efj::thread* thread, efj::memory_arena* arena)
{
    thread->logContext->arena = arena;
    thread->logContext->sys   = sys;
}

static void
_logsys_init_object_context(efj::log_system* sys, efj::object* object)
{
    object->logContext = efj_push_new(efj::perm_memory(), efj::logsys_object_ctx);
}

static void
_logsys_pre_object_init(efj::log_system* sys, efj::thread* loggingThread, const efj::log_config* config)
{
    sys->loggingThread = loggingThread;
    if (!config->object)
        sys->logFd = STDOUT_FILENO;
}

static void
_logsys_post_object_init(efj::log_system* sys)
{
    efj::eventsys_waitable_desc desc = {};
    desc.udata.as_ptr = sys;
    desc.dispatch     = &_logsys_handle_logs_available_event;
    efj::_eventsys_create_waitable_event(sys->loggingThread, "Logs Available", &desc, &sys->logsAvailableEvent);
    efj::_eventsys_add_waitable_event(sys->loggingThread, &sys->logsAvailableEvent);
}

// Split out so we can flush log messages on exit().
static void
_logsys_write_log_messages(efj::log_system* sys)
{
    efj::log_queue* queue = sys->logQueue;

    efj::object*    o  = sys->loggerObject;
    efj::logger_fn* cb = sys->loggerCb;

    // If a custom logger is set, use it. Otherwise, log to a file (stdout by default).
    efj::log_message* messages = queue->swap_buffers();

    if (o && cb) {
        cb(o, *messages);
    }
    // TODO(bmartin): Consider writev.
    else {
        int fd = sys->logFd;
        efj_assert(fd != -1);

        efj::log_formatter* formatter = efj::get_log_formatter();

        for (auto* msg = messages; msg; msg = msg->next) {
            if (msg->ctx.formatted) {
                efj_temp_scope();
                efj::string formatted = formatter(msg->ctx, msg->msg);

                if (::write(fd, formatted.data, formatted.size) != (int)formatted.size) {
                    auto error = efj::fmt("[c2] error writing log message: %s\n", strerror(errno));
                    efj_check(::write(STDERR_FILENO, error.data, error.size) != -1);
                }
            }
            else {
                if (::write(fd, msg->msg.data, msg->msg.size) != (int)msg->msg.size) {
                    auto error = efj::fmt("[c2] error writing log message: %s\n", strerror(errno));
                    efj_check(::write(STDERR_FILENO, error.data, error.size) != -1);
                }
            }
        }
    }
}

// On the logging thread.
static void
_logsys_handle_logs_available_event(efj::eventsys_event_data* edata)
{
    auto* sys = (efj::log_system*)edata->udata.as_ptr;
    efj::_logsys_write_log_messages(sys);
}

// On any thread that logs. This is usually called at the end of an event but may be called early
// to flush log message to the logging thread under special conditions like exiting the app.
// May be called from a signal handler. :EventSysOnThreadCrashed
static void
_logsys_handle_deferred(efj::thread* thread)
{
    efj::log_system* sys = thread->logContext->sys;
    efj::_eventsys_handle_before_kill(&sys->logsAvailableEvent);
    efj::_eventsys_trigger(&sys->logsAvailableEvent);
}

} // namespace efj
