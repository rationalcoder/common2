namespace efj
{

// Right now, and probably forever, we don't support alignments greater than the cache-line size.
// :CacheAlignmentMax

extern efj::memory_arena
allocate_arena(const char* tag, umm chunkSize, umm maxSize, efj::allocator_impl* impl)
{
    efj::memory_arena result = {};

    // Non-default max size implies a fixed-size arena.
    //
    // NOTE(bmartin): Right now, only fixed-size arenas get a guard page. The assumption is that
    // a guard page per chunk in a dynamic arena would be too wasteful.
    //
    if (maxSize != efj::arena_size_dynamic) {
        efj_panic_if(chunkSize > maxSize);

        // NOTE(bmartin): I am being too careful with alignment. The mmap/mprotect calls
        // will do the right thing with non-page-aligned lengths, but I am adjusting the request
        // itself to be aligned just so the page granularity can't be forgotten by callers printing
        // out arena values or whatever. Since I need the aligned values anyway, I might as well use them.
        umm alignedInitialSize = efj::align_up(chunkSize, efj::page_size());
        // IMPORTANT: The max size of fixed-size arenas is adjusted to include the chunk-struct size, which
        // does not happen for dynamic arenas. I decided to do this only to avoid clients having to do math
        // when allocating fixed arenas if they want to have room for 16KB, for example.
        efj_rare_assert(sizeof(memory_arena_chunk) <= alignedInitialSize);
        umm alignedMaxSize = efj::align_up(maxSize + alignedInitialSize, efj::page_size());

        // Tack a guard page onto the end.
        umm maxGuardedSize = alignedMaxSize + efj::page_size();

        // _Reserve_ the full possible range of pages, to be commited as needed.
        u8* mappingStart = (u8*)::mmap(nullptr, maxGuardedSize, PROT_NONE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
        if (mappingStart == MAP_FAILED) {
            efj_log_crit("Failed to allocate arena '%s' (Max Size: %zu): %s",
                    tag, maxSize, strerror(errno));
            efj_panic("Arena allocation failure");
        }

        ::mprotect(mappingStart, alignedInitialSize, PROT_READ | PROT_WRITE);

        result.type      = efj::arena_type_fixed;
        result.tag       = tag;
        result.expand    = &efj::expand_arena;
        result.user      = nullptr;
        result.chunkSize = alignedInitialSize;
        result.maxSize   = alignedMaxSize;

        auto* firstChunk  = (efj::memory_arena_chunk*)mappingStart;
        firstChunk->start = mappingStart + sizeof(efj::memory_arena_chunk);
        firstChunk->at    = firstChunk->start;
        firstChunk->end   = mappingStart + alignedInitialSize;

        firstChunk->next = firstChunk;
        firstChunk->prev = firstChunk;
        result.curChunk  = firstChunk;
        result.tailChunk = firstChunk;
    }
    // Dynamic arena. Expands as needed. Not contiguous.
    else {
        //printf("%zu, Allocating dynamic arena: %s %zu %zu\n", chunkSize, tag, chunkSize, maxSize);
        umm alignedInitialSize = efj::align_up(chunkSize, efj::page_size());

        // Just allocate the first chunk.
        u8* mappingStart = (u8*)::mmap(NULL, alignedInitialSize, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);
        if (mappingStart == MAP_FAILED) {
            efj_log_crit("Failed to allocate arena '%s' (Max Size: %zu): %s",
                    tag, maxSize, strerror(errno));
            efj_panic("Arena allocation failure");
        }

        result.type      = efj::arena_type_dynamic;
        result.tag       = tag;
        result.expand    = &efj::expand_arena;
        result.user      = nullptr;
        result.chunkSize = alignedInitialSize;
        result.maxSize   = efj::arena_size_dynamic;

        auto* firstChunk  = (efj::memory_arena_chunk*)mappingStart;
        firstChunk->start = mappingStart + sizeof(efj::memory_arena_chunk);
        firstChunk->at    = firstChunk->start;
        firstChunk->end   = mappingStart + alignedInitialSize;

        firstChunk->next = firstChunk;
        firstChunk->prev = firstChunk;
        result.curChunk  = firstChunk;
        result.tailChunk = firstChunk;
    }

    result.alloc = impl ? impl : efj::get_linear_allocator_impl();
    return result;
}

extern void
free_arena(efj::memory_arena& arena)
{
    if (!arena.curChunk)
        return;

    if (arena.type == efj::arena_type_fixed) {
        efj_assert(arena.curChunk == arena.tailChunk);
        // Don't forget to unmap the guard page.
        efj_check(::munmap(arena.curChunk->start - sizeof(efj::memory_arena_chunk), arena.maxSize + efj::page_size()) == 0);
    }
    else {
        // Chunk structs are stored in the chunks themselves, so just like a normal heap-allocated list deletion,
        // we need to save the 'next' pointer before unmapping.
        efj::memory_arena_chunk* next = arena.tailChunk->next;
        for (efj::memory_arena_chunk* chunk = next; ; chunk = next) {
            next = chunk->next;
            // NOTE: For all chunks after the first one, the chunk struct is stored in the chunk.
            // This means end-start is not the full size of the mapped space. However, 1) munmap doesn't
            // care about getting an exact size, 2) we store the chunks on one end of the space or another,
            // and 3) the chunk struct is smaller than a page, so this should never matter.

            // We allow chunks to be collated, so the allocated chunk size isn't necessarily the arena chunk size.
            // Chunks can get collated when fixed arenas expand or if we ever figure out how to potentially get
            // contiguous pages for dynamic arenas from Linux with appropriate hints.
            u8* mappingStart = chunk->start - sizeof(efj::memory_arena_chunk);
            umm mappedChunkSize = chunk->end - mappingStart;
            efj_assert(efj::is_aligned(mappedChunkSize, 4_KB));
            efj_check(::munmap(mappingStart, mappedChunkSize) == 0);

            if (chunk == arena.tailChunk)
                break;
        }
    }

    arena.curChunk  = nullptr;
    arena.tailChunk = nullptr;
}

extern u8*
failed_expand_arena(efj::memory_arena* arena, umm size, umm alignment)
{
    efj::print_error("Failed to expand arena '%s': last allocation (size: %zu, align: %zu)\n",
        arena->tag, size, alignment);

    efj_panic("Arena expansion failure");
    return nullptr;
}

static EFJ_FORCE_INLINE efj::memory_arena_chunk*
_allocate_new_chunk(efj::memory_arena* arena, umm size, umm alignment)
{
    umm allocationWithChunkSize = size + efj::align_up(sizeof(efj::memory_arena_chunk), alignment);

    umm chunksNeeded = (allocationWithChunkSize / arena->chunkSize);
    if (chunksNeeded * arena->chunkSize < allocationWithChunkSize)
        chunksNeeded++;

    umm sizePaddedToChunkBoundary = chunksNeeded * arena->chunkSize;

    u8* mappingStart = (u8*)::mmap(nullptr, sizePaddedToChunkBoundary,
        PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);

    if (mappingStart == MAP_FAILED)
        return nullptr;

    auto* newChunk = (efj::memory_arena_chunk*)(mappingStart);
    newChunk->start = mappingStart + sizeof(efj::memory_arena_chunk);
    newChunk->end   = mappingStart + sizePaddedToChunkBoundary;
    newChunk->at    = newChunk->start; // :CacheAlignmentMax

    return newChunk;
}


// TODO: We could pull the fixed and dynamic cases out into separate functions and assign them based on
// the type of arena in the allocation function, but it's useful to swap out the expansion function
// in the unit test without worrying about types. We could just have a general one that is only used
// by the unit test.
extern u8*
expand_arena(efj::memory_arena* arena, umm size, umm alignment)
{
    // :CacheAlignmentMax
    if (alignment > EFJ_CACHELINE_SIZE)
        alignment = EFJ_CACHELINE_SIZE;

    if (arena->type == efj::arena_type_fixed) {
        // TODO: Allocate fixed-size arenas completely and don't support expansion. Linux uses copy-on-write
        // pages anyway, so pages won't be allocated until they are written to. I just wrote the code this
        // way because I ported it from Windows.

        efj::memory_arena_chunk* chunk = arena->curChunk;
        u8* alignedAt = (u8*)efj::align_up(chunk->at, alignment);
        u8* nextAt    = alignedAt + size;

        u8* mappingStart = chunk->start - sizeof(efj::memory_arena_chunk);
        if (nextAt > mappingStart + arena->maxSize)
            return efj::failed_expand_arena(arena, size, alignment);

        // Convert exact space needed to whole chunks needed (at least 1).
        umm spaceNeeded  = nextAt - chunk->end;
        umm chunksNeeded = spaceNeeded / arena->chunkSize;
        if (chunksNeeded * arena->chunkSize < spaceNeeded)
            chunksNeeded++;

        // Make the new chunks readable and writable.
        umm mapSize = chunksNeeded * arena->chunkSize;
        if (::mprotect(chunk->end, mapSize, PROT_READ | PROT_WRITE) == -1)
            return efj::failed_expand_arena(arena, size, alignment);

        chunk->at   = nextAt;
        chunk->end += mapSize;

        return alignedAt;
    }
    else {
        efj_assert(alignment <= efj::page_size());

        if (arena->curChunk != arena->tailChunk) {
            efj::memory_arena_chunk* next = arena->curChunk->next;

            // NOTE(bmartin): The arena was reset, and we have a next chunk already allocated, we want to see if it has enough
            // space in it to support this allocation. If so, we can use it, instead. Otherwise, we need to insert a new chunk
            // with enough space _and_ unmap the chunk that wasn't big enough. We want to unmap the chunk that wasn't big enough
            // so we don't run into usages of arenas that cause a bunch of wasted chunks to pile up at the end of the chunk list.
            // For example, if we have 4_KB chunks, and we do: push(4_KB) * 3, reset(), push(8_KB) * 3, push(16_KB) * 3, etc.,
            // we would have three 4_KB and three 8_KB chunks on the end of the list that might not get used.
            //

            u8* nextAlignedStart = (u8*)efj::align_up(next->start, alignment);
            if ((umm)(next->end - nextAlignedStart) >= size) {
                // Existing chunk is big enough.

                next->at = nextAlignedStart + size;
                arena->curChunk = next;
                return nextAlignedStart;
            }
            else {
                // Existing chunk is _not_ big enough. Unmap it, and replace it with a new one that is big enough.
                //printf("Wasn't big enough (%zu/%zu)\n", size, next->end - next->start);

                efj::memory_arena_chunk* afterNext = next->next;

                u8* mappingStart = next->start - sizeof(efj::memory_arena_chunk);
                // TODO: Insert new chunk without unmapping if munmap fails?
                if (::munmap(mappingStart, (next->end - mappingStart)) != 0)
                    return efj::failed_expand_arena(arena, size, alignment);

                efj::memory_arena_chunk* newChunk = _allocate_new_chunk(arena, size, alignment);
                if (!newChunk)
                    return efj::failed_expand_arena(arena, size, alignment);

                afterNext->prev       = newChunk;
                newChunk->next        = afterNext;
                newChunk->prev        = arena->curChunk;
                arena->curChunk->next = newChunk;

                // We could've just unmapped the current tail chunk.
                if (next == arena->tailChunk)
                    arena->tailChunk = newChunk;

                arena->curChunk = newChunk;

                //printf("(before) Space remaining: %zu/%zu\n", newChunk->end - newChunk->at, newChunk->end - newChunk->start);
                //printf("Alignment: %zu\n", alignment);
                u8* alignedStart = (u8*)efj::align_up(newChunk->start, alignment);
                newChunk->at = alignedStart + size;

                //printf("Space remaining: %zu/%zu\n", newChunk->end - newChunk->at, newChunk->end - newChunk->start);
                return alignedStart;
            }
        }
        else {
            // First time expanding out this far for this arena. We need a new chunk that will be the new cur/tail.
            efj::memory_arena_chunk* newChunk = _allocate_new_chunk(arena, size, alignment);
            if (!newChunk)
                return efj::failed_expand_arena(arena, size, alignment);

            efj::memory_arena_chunk* afterTail = arena->tailChunk->next;

            afterTail->prev        = newChunk;
            newChunk->next         = afterTail;
            newChunk->prev         = arena->tailChunk;
            arena->tailChunk->next = newChunk;

            arena->curChunk  = newChunk;
            arena->tailChunk = newChunk;

            u8* alignedStart = (u8*)efj::align_up(newChunk->start, alignment);
            newChunk->at = alignedStart + size;

            //printf("Space remaining: %zu/%zu\n", newChunk->end - newChunk->at, newChunk->end - newChunk->start);
            return alignedStart;
        }
    }

    efj_invalid_code_path();
    return nullptr;
}

} // namespace efj
