namespace efj
{

// [TCP CLIENT]

extern efj::tcp_client
_tcp_create_client(const char* name, efj::callback cb)
{
    efj_not_implemented();
    efj::tcp_client result;
#if 0

    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd == -1) efj_not_implemented();

    efj::_create_event_source(name, &result.eventSource);

    result.handle.fd = fd;
    result.cb        = cb;

    if (!efj::make_nonblocking(fd))
        efj_not_implemented();

#endif
    return result;
}

extern bool
tcp_bind(efj::tcp_client& client, u16 port)
{
    struct sockaddr_in address;
    address.sin_family      = AF_INET;
    address.sin_port        = htons(port);
    address.sin_addr.s_addr = INADDR_ANY;

    return ::bind(client.event.fd, (struct sockaddr*)&address, sizeof(address)) == 0;
}

extern bool
tcp_connect(efj::tcp_client& client, const char* ip, u16 port)
{
    efj_not_implemented();
#if 0
    struct sockaddr_in address;
    address.sin_family      = AF_INET;
    address.sin_port        = htons(port);
    address.sin_addr.s_addr = inet_addr(ip);

    // If the client has already been connected or has been closed, we need
    // to create a new one. In the case of a connection (success or failure),
    // we need to close it first.
    //
    if (client.handlingConnect || client.connectHandled)
        efj::close(client);

    auto& handle = client.handle;

    // This should now catch both the closed and previously connected cases.
    if (handle.fd == -1) {
        handle.fd = socket(AF_INET, SOCK_STREAM, 0);
        if (handle.fd == -1)
            return false;

        if (!efj::make_nonblocking(handle.fd))
            efj_not_implemented();
    }

    if (::connect(handle.fd, (struct sockaddr*)&address, sizeof(address)) == -1) {
        if (errno != EINPROGRESS)
            return false;
    }

    // Setup flags for waiting on the result of connect();
    handle.edata = efj::_add_fd(&client.eventSource, handle.fd, efj::io_out | efj::io_oneshot,
                                efj::event_source_tcp_client, &client, client.cb);

    if (!efj::make_blocking(handle.fd))
        efj_not_implemented();

    client.connectHandled  = false;
    client.handlingConnect = false;

#endif
    return true;
}

extern void
tcp_close(efj::tcp_client& c)
{
    efj::thread* thread = efj::this_thread();
    efj::_eventsys_destroy_epoll_event(thread, &c.event);

    c.connectHandled  = false;
    c.handlingConnect = false;
}

// [TCP SERVER]

inline void
tcp_connected_client_list::_init(efj::memory_arena* arena)
{
    _arena = arena;
}

inline efj::tcp_connected_client*
tcp_connected_client_list::_add_default()
{
    _size++;

    auto* node  = _freeList.get_or_allocate<efj::tcp_connected_client>(*_arena);
    if (!_head) {
        node->next = nullptr;
        node->prev = nullptr;
        _head      = node;
        return efj_placement_new(node) efj::tcp_connected_client();
    }

    node->next  = _head;
    node->prev  = _head->prev;
    _head->prev = node;
    _head       = node;

    return node;
}

inline void
tcp_connected_client_list::_remove(efj::tcp_connected_client* c)
{
    _size--;
    _freeList.add(c);

    auto next = c->next;
    if (c == _head) {
        _head = next;

        if (!next) return;
    }

    auto prev = c->prev;
    if (prev) prev->next = next;
    if (next) next->prev = prev;
}

extern bool
tcp_create_server(const char* name, const efj::tcp_server_info& info,
                  efj::tcp_server& result)
{
    efj_not_implemented();
#if 0
    int fd = socket(AF_INET, SOCK_STREAM, 0);
    if (fd == -1) return false;

    efj::_create_event_source(name, &result.eventSource);

    result.handle.fd    = fd;
    result.handle.edata = nullptr;
    result.cb           = efj::null_callback();
    result.clients._init(&efj::perm_memory());

    if (!efj::make_nonblocking(fd))
        efj_not_implemented();

#endif
    return true;
}

extern bool
tcp_bind(efj::tcp_server& server, u16 port)
{
    struct sockaddr_in address;
    address.sin_family      = AF_INET;
    address.sin_port        = htons(port);
    address.sin_addr.s_addr = INADDR_ANY;

    return ::bind(server.event.fd, (struct sockaddr*)&address, sizeof(address)) == 0;
}

extern bool
_tcp_listen(efj::tcp_server& server, int backlog, const efj::callback& cb)
{
    efj_not_implemented();

#if 0
    auto& handle = server.handle;
    // TODO(bmartin): close and recreate the server if it is already listen()ing.
    // It sounds convenient, but I'm not sure that it's practical, yet.
    efj_assert(handle.edata == NULL);

    if (::listen(handle.fd, backlog) == -1)
        return false;

    server.cb = cb;

    // Setup flags for waiting for a connection to be ready to accept.
    // NOTE(bmartin): I am passing io_oneshot here b/c someone might want to
    // defer accepting for some reason. If we check for efj::io_in and leave it
    // level triggered, the callback will get spammed.
    //
    handle.edata = efj::_add_fd(&server.eventSource, handle.fd, efj::io_in | efj::io_oneshot,
                                efj::event_source_tcp_server, &server, server.cb);

#endif
    return true;
}

extern bool
_tcp_accept(efj::tcp_server& server, struct sockaddr_in* clientAddress,
            const efj::callback& cb, void* udata)
{
    efj_not_implemented();
#if 0
    socklen_t len = sizeof(*clientAddress);
    int       fd  = -1;
    if ((fd = ::accept(server.handle.fd, (struct sockaddr*)clientAddress, &len)) == -1)
        return false;

    if (!efj::make_nonblocking(fd))
        efj_not_implemented();

    efj::tcp_connected_client* client = server.clients._add_default();
    client->server = &server;
    client->cb     = cb;
    client->udata  = udata;

    efj::_create_event_source("Connected Client", &client->eventSource);

    auto& handle = client->handle;
    handle.fd    = fd;
    handle.edata = efj::_add_fd(&client->eventSource, handle.fd,
                                efj::io_in | efj::io_rdhup | efj::io_oneshot,
                                efj::event_source_tcp_connected_client, client, client->cb);

#endif
    return true;
}

// We don't always want to remove the client from the
// server's list of clients.
static inline void
close_minimal(efj::tcp_connected_client& c)
{
    efj::thread* thread = efj::this_thread();
    efj::_eventsys_destroy_epoll_event(thread, &c.event);
    c.cb = efj::null_callback();
}

extern void
close(efj::tcp_connected_client& c)
{
    if (c.event.fd == -1) return;

    efj::close_minimal(c);

    auto& server = *c.server;
    server.clients._remove(&c);
}

extern void
close(efj::tcp_server& s)
{
    efj::thread* thread = efj::this_thread();
    if (s.event.fd == -1) return;

    for (auto& c : s.clients)
        efj::close_minimal(c);

    efj::_eventsys_destroy_epoll_event(thread, &s.event);
    // @Leak(bmartin): client list memory.
}

} // namespace efj
