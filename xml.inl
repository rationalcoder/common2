namespace efj
{

inline const efj::xml_element*
xml_element::first_child(const efj::string& name) const
{
    auto  n    = _childrenCount;
    auto* base = _childrenSorted;

    while (n > 0) {
        auto* pivot = base + (n >> 1);

        int result = strcmp(name.c_str(), (*pivot)->_name.c_str());
        if (result == 0) {
            // Found a matching child, but we want the first one, so
            // walk backwards from the one we found until we know we
            // have the first one.
            while (pivot != base) {
                if ((*(pivot - 1))->_name != name)
                    break;

                pivot--;
            }

            return *pivot;
        }

        if (result > 0) {
            base = pivot + 1;
            n--;
        }

        n >>= 1;
    }

    return nullptr;
}

inline const efj::xml_element*
xml_element::next_sibling() const
{
    if (_sortedIndex < _parent->_childrenCount-1) {
        auto* sibling = _parent->_childrenSorted[_sortedIndex + 1];
        if (sibling->_name == _name)
            return sibling;
    }

    return nullptr;
}

inline umm
xml_element::child_count(const efj::string& name) const
{
    auto* first = first_child(name);
    if (!first) return 0;

    umm count = 1;
    for (umm i = first->_sortedIndex + 1; i < _childrenCount; i++, count++) {
        if (name != _childrenSorted[i]->_name)
            break;
    }

    return count;
}

inline const efj::xml_attribute*
xml_element::find_attribute(const efj::string& name) const
{
    auto  n    = _attribCount;
    auto* base = _attribsSorted;

    while (n > 0) {
        auto* pivot = base + (n >> 1);

        int result = strcmp(name.c_str(), (*pivot)->_name.c_str());
        if (result == 0)
            return *pivot;

        if (result > 0) {
            base = pivot + 1;
            n--;
        }

        n >>= 1;
    }

    return nullptr;
}

}
