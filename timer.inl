namespace efj
{

inline bool timer_is_running(efj::timer& timer) { return timer.event.running; }
inline bool timer_is_oneshot(efj::timer& timer) { return timer.type == efj::timer_oneshot; }

// Create + monitor, but don't start it.
template <typename T> inline void
create_oneshot_timer(const char* name, efj::nsec delay,
    efj::timer* result, void (T::*callback)(efj::timer_event& e))
{
    efj::object* thisObject = efj::this_object();
    efj_assert(thisObject);

    efj::create_oneshot_timer(name, delay, result);

    efj::callback cb = efj::_make_object_callback(thisObject, callback);
    efj::_eventsys_monitor_timer(&result->event, cb);
}

template <typename T> inline void
create_periodic_timer(const char* name, efj::nsec interval,
    efj::timer* result, void (T::*callback)(efj::timer_event& e))
{
    efj::object* thisObject = efj::this_object();
    efj_assert(thisObject);

    efj::create_periodic_timer(name, interval, result);

    efj::callback cb = efj::_make_object_callback(thisObject, callback);
    efj::_eventsys_monitor_timer(&result->event, cb);
    efj::_eventsys_start_timer(&result->event);
}

template <typename T> inline void
create_periodic_timer(const char* name, efj::nsec initial, efj::nsec interval,
    efj::timer* result, void (T::*callback)(efj::timer_event& e))
{
    efj::object* thisObject = efj::this_object();
    efj_assert(thisObject);

    efj::create_periodic_timer(name, initial, interval, result);

    efj::callback cb = efj::_make_object_callback(thisObject, callback);
    efj::_eventsys_monitor_timer(&result->event, cb);
}

template <typename T> inline void
add_oneshot_timer(const char* name, efj::nsec delay,
    efj::timer* result, void (T::*callback)(efj::timer_event& e))
{
    efj::object* thisObject = efj::this_object();
    efj_assert(thisObject);

    efj::create_oneshot_timer(name, delay, result);

    efj::callback cb = efj::_make_object_callback(thisObject, callback);
    efj::_eventsys_monitor_timer(&result->event, cb);
    efj::_eventsys_start_timer(&result->event);
}

template <typename T> inline void
add_periodic_timer(const char* name, efj::nsec interval,
    efj::timer* result, void (T::*callback)(efj::timer_event& e))
{
    efj::object* thisObject = efj::this_object();
    efj_assert(thisObject);

    efj::create_periodic_timer(name, interval, result);

    efj::callback cb = efj::_make_object_callback(thisObject, callback);
    efj::_eventsys_monitor_timer(&result->event, cb);
    efj::_eventsys_start_timer(&result->event);
}

template <typename T> inline void
add_periodic_timer(const char* name, efj::nsec initial, efj::nsec interval,
    efj::timer* result, void (T::*callback)(efj::timer_event& e))
{
    efj::object* thisObject = efj::this_object();
    efj_assert(thisObject);

    efj::create_periodic_timer(name, initial, interval, result);

    efj::callback cb = efj::_make_object_callback(thisObject, callback);
    efj::_eventsys_monitor_timer(&result->event, cb);
    efj::_eventsys_start_timer(&result->event);
}

template <typename T> inline void
timer_monitor(efj::timer& t, void (T::*callback)(efj::timer_event& e))
{
    efj::object* thisObject = efj::this_object();
    efj_assert(thisObject);

    efj::callback cb = efj::_make_object_callback(thisObject, callback);
    efj::_eventsys_monitor_timer(&t.event, cb);
}

} // namespace efj
