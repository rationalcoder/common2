#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <common2.hpp>
#include "proto.hpp"

struct Peer
{
    EFJ_OBJECT("Peer");
    EFJ_CONSUMER(mock::Message);

    efj::udp_socket _sock;
    u16             _local_port;
    u16             _remote_port;

    Peer(u16 local_port, u16 remote_port)
        : _local_port(local_port), _remote_port(remote_port)
    {
        printf("[Peer] local port: %u, remote port: %u\n", local_port, remote_port);
    }

    bool init()
    {
        efj::add_udp_socket("Peer", _sock, &Peer::notify);
        if (!efj::udp_bind(_sock, "127.0.0.1", _local_port)) {
            fprintf(stderr, "Failed to bind\n");
            return false;
        }

        return true;
    }

    void notify(efj::udp_socket& sock, flag32 events)
    {
        if (events & efj::io_in) {
            char buf[256] = {};
            int  len      = 0;

            struct sockaddr_in remote_address;
            socklen_t          remote_address_size = sizeof(remote_address);

            while ((len = ::recvfrom(sock.fd(), buf, sizeof(buf), 0,
                (struct sockaddr*)&remote_address, &remote_address_size)) > 0) {

                printf("Got '%.*s' from %s:%u\n", len, buf,
                    inet_ntoa(remote_address.sin_addr), ntohs(remote_address.sin_port));
                // efj::send_to(sock, buf, len, "127.0.0.1", _remote_port);
            }
        }
    }

    void consume(const mock::Message& msg)
    {
        //printf("Consumed on thread %llu %u (%.*s)\n", efj::real_thread_id(), msg.size, msg.size, (char*)msg.data);
        //printf("Result: %d\n", efj::send_to(_sock, msg.data, msg.size, "127.0.0.1", 6780));
        efj::send_to(_sock, msg.data, msg.size, "127.0.0.1", _remote_port);
    }
};

struct Broadcaster
{
    EFJ_OBJECT("Broadcaster");
    EFJ_PRODUCER(mock::Message);

    efj::timer _timer;

    bool init()
    {
        efj::add_periodic_timer("Timer", 1_s, _timer, &Broadcaster::broadcastTimerExpired);

        return true;
    }

    void broadcastTimerExpired(efj::timer&)
    {
        //printf("From thread: %llu\n", efj::real_thread_id());
        efj_produce(*mock::make_message("Test"));
    }
};

int main(int argc, const char** argv)
{
    u16 local_port  = argc > 1 ? atoi(argv[1]) : 0;
    u16 remote_port = argc > 2 ? atoi(argv[2]) : 0;

    Peer        peer(local_port, remote_port);
    Broadcaster bc;

    efj_connect(bc, mock::Message, peer);

    return efj::exec(argc, argv);
}

#define EFJ_COMMON2_IMPLEMENTATION
#include <common2.hpp>
