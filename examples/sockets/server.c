#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

void error(char const* func)
{
    printf("ERROR: %s: %s\n", func, strerror(errno));
    exit(EXIT_FAILURE);
}

int main()
{
    struct sockaddr_in serverAddress;
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_addr.s_addr = INADDR_ANY;
    serverAddress.sin_port = htons(6666);

    int server_sock = socket(AF_INET, SOCK_STREAM, 0 /*IPPROTO_TCP*/);
    if (server_sock == -1)
        error("socket()");

    int enable = 1;
    setsockopt(server_sock, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(enable));

    if (bind(server_sock, (struct sockaddr*)&serverAddress, sizeof(struct sockaddr_in)) == -1)
    {
        if (close(server_sock) == -1)
            error("close after bind failed");
        error("bind()");
    }
    
    // 0 means only one client at a time.
    if (listen(server_sock, 0) == -1)
    {
        if (close(server_sock) == -1)
            error("close after listen failed");
        error("listen()");
    }

    
    int client_sock = -1;
    struct sockaddr_in clientAddress = { 0 };
    socklen_t clientAddressLen = sizeof(struct sockaddr_in);

    printf("(SERVER) Waiting for connection...\n");
    client_sock = accept(server_sock, (struct sockaddr*)&clientAddress, &clientAddressLen);
    if (client_sock == -1)
    {
        if (close(server_sock) == -1) 
            error("accept/close()");
        error("accept()");
    }
    if (clientAddressLen != sizeof(struct sockaddr_in))
    {
        if (close(client_sock) == -1)
            error("client address len != sizeof sockaddr_in, not sure what to do/close failed");
        error("client address len != sizeof sockaddr_in, not sure what to do");
    }

    char const* clientIP = inet_ntoa(clientAddress.sin_addr);
    printf("(SERVER) Accepted connection from: %s\n", clientIP);

    char inbound_msg[512] = { 0 };
    size_t inbound_size = 0;
    while (1)
    {
        if ((inbound_size = recv(client_sock, inbound_msg, 512, 0)) == -1)
        {
            printf("recv failed with: %s\n", strerror(errno));
            break;
        }

        if (inbound_size == 0) {
            usleep(2000);
            continue;
        }

        printf("(SERVER) Received: %s", inbound_msg);
        if (memcmp(inbound_msg, "quit", 4) == 0) {
            write(client_sock, "Hi", 3);
            sleep(1);
            write(client_sock, "Hi 2", 5);
            break;
        }
    }

    printf("(SERVER) Shutting Down...\n");
    if (close(client_sock) == -1)
        error("close after accepting client normally");

    if (close(server_sock))
        error("final close()");

    return EXIT_SUCCESS;
}

