namespace mock
{

struct Message
{
    void* data;
    umm   size;
};

enum DataType : u8
{
    DATA_TYPE_HEADER,
    DATA_TYPE_MESSAGE,
    DATA_TYPE_COUNT_,
};

struct Header
{
    Header*        next;
    void*          data;
    mock::DataType type;
};

inline mock::Message*
make_message(const char* text /*literal*/)
{
    auto* result = efj_allocate_type(mock::Message);
    result->data = (void*)text;
    result->size = strlen(text);

    return result;
}

inline mock::Header*
add_header(const char* text, mock::Message* data)
{
    auto* result = efj_allocate_array(2, mock::Header);
    result[0].next = nullptr;
    result[0].data = data;
    result[0].type = mock::DATA_TYPE_MESSAGE;

    result[1].next = result;
    result[1].data = (void*)text;
    result[1].type = mock::DATA_TYPE_HEADER;

    return &result[1];
}

inline mock::Header*
add_header(const char* text, mock::Header* hdr)
{
    auto* result = efj_allocate_type(mock::Header);
    result->next = hdr;
    result->data = (void*)text;
    result->type = mock::DATA_TYPE_HEADER;

    return result;
}

}

namespace efj
{

template <> inline mock::Message*
copy_view(const mock::Message& view)
{
    // @Speed. Collate these allocations.
    auto* result = efj_allocate_type(mock::Message);
    result->data = efj_allocate(view.size, 1);
    result->size = view.size;

    memcpy(result->data, view.data, view.size);

    return result;
}

template <> inline mock::Header*
copy_view(const mock::Header& view)
{
    mock::Header* result = nullptr;

    if (view.type == mock::DATA_TYPE_MESSAGE) {
        result       = efj_allocate_type(mock::Header);
        result->next = nullptr;
        result->data = efj::copy_view(*(mock::Message*)view.data);
        result->type  = mock::DATA_TYPE_MESSAGE;
        return result;
    }

    auto* head = efj_allocate_type(mock::Header);
    head->next = nullptr;
    head->data = efj_allocate_array_copy((umm)strlen((char*)view.data), char, view.data);
    head->type = mock::DATA_TYPE_HEADER;

    mock::Header* tail = head;

    auto* cur = view.next;
    for (; cur && cur->next; cur = cur->next) {
        auto* newTail = efj_allocate_type(mock::Header);
        newTail->next = nullptr;
        newTail->data = efj_allocate_array_copy((umm)strlen((char*)cur->data), char, cur->data);
        newTail->type = mock::DATA_TYPE_HEADER;

        tail->next = newTail;
        tail       = newTail;
    }

    auto* newTail = efj_allocate_type(mock::Header);
    newTail->next = nullptr;
    newTail->data = efj::copy_view<mock::Message>(*(mock::Message*)cur->data);

    tail->next = newTail;

    return head;
}

}

