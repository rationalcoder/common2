#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <c2.hpp>
#include "proto.hpp"

struct Client
{
    EFJ_OBJECT("Sockets Client");
    EFJ_PRODUCER(mock::Message);
    EFJ_CONSUMER(mock::Message);

    efj::tcp_client _client;
    efj::timer      _timer;

    int _connectRetryCount    = 0;
    int _maxConnectRetryCount = 5;

    bool init()
    {
        efj::tcp_create_client("Client", _client, &Client::notify);
        //efj::tcp_connect(_client, "127.0.0.1", 666);

        efj::add_oneshot_timer("Timer", 1_s, _timer, &Client::connectTimerExpired);

        return true;
    }

    void connectTimerExpired(efj::timer&)
    {
        printf("=== Connecting...\n");
        efj::tcp_connect(_client, "127.0.0.1", 6666);
    }

    void notify(efj::tcp_client& client, flag32 events)
    {
        if (efj::connection_handled(client)) {
            if (!efj::connection_succeeded(client, events)) {
                printf("Connection failed\n");
                if (_connectRetryCount++ < _maxConnectRetryCount) {
                    if (!efj::tcp_connect(client, "127.0.0.1", 6666))
                        printf("connect() failed: %s\n", strerror(errno));
                }
                else {
                    printf("Retry limit exceeded\n");
                }

                return;
            }

            printf("=== Connected\n");
            efj::write(client, "hi", 3);
            //efj::write(client, "quit", 5);

            return;
        }

        if (events & efj::io_rdhup || events & efj::io_hup) {
            printf("Hangup: %s\n", efj::to_string(efj::now_utc()).c_str());
            efj::close(client);
            efj::exit();
            printf("called exit()\n");
            return;
        }

        char buf[256] = {};
        if (events & efj::io_in) {
            efj::read(client, buf, 256);
            printf("Got: %s\n", buf);
        }
    }
};

#if 0
struct JitterBuffer
{
    EFJ_OBJECT("Jitter Buffer");
    EFJ_PRODUCER(mock::Message);
    EFJ_CONSUMER(mock::Message);

    efj::message_queue<mock::Message> _queue;
    efj::timer _sendTimer;

    bool init()
    {
        _sendTimer = efj::add_periodic_timer(3_s, timerExpired);

        return true;
    }

    void consume(mock::Message msg)
    {
        //_queue.push(msg);
    }

    void timerExpired(const efj::timer& t)
    {
        //efj_produce(_queue.pop());
    }

    void notify(const efj::user_event& e)
    {
    }
};

struct HeartBeatGenerator
{
    EFJ_OBJECT("HB Gen");
    EFJ_PRODUCER(mock::Message);
};
#endif

int main(int argc, const char** argv)
{
    Client client;
    //HeartBeatGenerator hbg;
    //JitterBuffer jb;

#if 0
    efj_create_chain("Main", {
        efj::add_objects(client);

        //efj_connect(client, mock::Message, jb);
        //efj_connect(jb,     mock::Message, client);
    });
#endif

    //efj_create_background_chain("Background", {
    //    efj::add_objects(hbg);

    //    efj_connect(hbg, mock::Message, client);
    //});

    return efj::exec(argc, argv);
}

#define EFJ_C2_IMPLEMENTATION
#include "c2.hpp"

