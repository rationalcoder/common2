#include <stdlib.h>
#include <stdio.h>
#include <common2.hpp>

static void
print_tree(const efj::xml_element* e, int level = 0)
{
    efj_temp_scope();

    if (!e) return;

    char indent[level];
    memset(indent, ' ', sizeof(indent));
    printf("%.*s", level, indent);

    printf("%s", e->name().c_str());
    for (auto* attr = e->first_attribute(); attr; attr = attr->next()) {
        printf(" '%s'='%s'", attr->name().c_str(), attr->value().c_str());
    }

    for (auto* child = e->first_child(); child; child = child->next()) {
        printf("\n");
        print_tree(child, level + 1);
    }

    auto trimmed = e->content().trim();
    if (trimmed.size && e->child_count() == 0)
        printf(" = \"%s\"", trimmed.c_str());
}

static bool
print_dom(const efj::string& path)
{
    printf("Path: %s\n", path.c_str());
    efj::buffer buf = efj::read_file(path);
    if (!buf.data) {
        perror("read_file");
        return false;
    }

    auto parsed = efj::parse_xml_file(buf);
    if (parsed.error) {
        fprintf(stderr, "error: %s\n", parsed.error.c_str());
        return false;
    }

    efj::xml_document& doc = parsed.doc;
    print_tree(doc.root());
    printf("\n");

    return true;
}

static bool
test_lookup()
{
    efj::string test = "test.xml";
    printf("Test: %s\n", test.c_str());

    efj::buffer buf = efj::read_file("test.xml");
    if (!buf.data) {
        perror("read_file");
        return false;
    }

    auto parsed = efj::parse_xml_file(buf);
    if (parsed.error) {
        fprintf(stderr, "error: %s\n", parsed.error.c_str());
        return false;
    }

    auto* root = parsed.doc._root;
    if (!root) {
        fprintf(stderr, "NULL root");
        return false;
    }

    root->first_child()->next_sibling();
    efj_assert(!root->next());

    auto* firstChild = root->first_child();
    efj_assert(firstChild);
    efj_assert(firstChild == root->_children);

    printf("First child of %s: %s\n", root->_name.c_str(), firstChild->_name.c_str());

    efj::string secondChildName = "second_child";

    auto* secondChild = root->first_child(secondChildName);
    efj_assert(secondChild);

    printf("First child of %s (name='%s'): %s\n", root->_name.c_str(), secondChildName.c_str(), secondChild->_name.c_str());

    printf("Attributes in order:\n");
    auto* withAttributes = root->first_child()->first_child()->next()->next();
    for (auto* attr = withAttributes->first_attribute(); attr; attr = attr->next()) {
        printf("  Attr %s='%s'\n", attr->name().c_str(), attr->value().c_str());
    }

    printf("Attributes by name:\n");
    for (umm i = 0; i <= 3; i++) {
        efj::string name = i == 0 ? "id" : efj::fmt("attr%zu", i);

        auto* attr = withAttributes->find_attribute(name);
        if (!attr) {
            fprintf(stderr, "Missing attribute %s\n", name.c_str());
            return false;
        }

        printf("  Attr %s='%s'\n", attr->name().c_str(), attr->value().c_str());
    }

    return true;
}

int main(int argc, const char** argv)
{
    auto xmlArena = efj::allocate_arena("XML", 16_KB);
    efj_allocator_scope(xmlArena);

    efj::string path = "example.atlas4500.cfg.xml";
    //efj::string path = "test.xml";

    if (!print_dom(path))
        return EXIT_FAILURE;

    efj_reset(xmlArena);

    if (!test_lookup())
        return EXIT_FAILURE;

    return EXIT_SUCCESS;
}

#define EFJ_COMMON2_IMPLEMENTATION
#include <common2.hpp>
