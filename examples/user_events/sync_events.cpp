#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <utility>

#include <c2.hpp>

enum UserEventType
{
    EVENT_CHANNEL_TYPE_CHANGED,
    USER_EVENT_TYPE_COUNT_
};

enum ChannelType
{
    CHANNEL_TYPE_TRAFFIC,
    CHANNEL_TYPE_CONTROL,
    CHANNEL_TYPE_COUNT_,
};

struct ChannelTypeChangedEvent
{
    ChannelType from;
    ChannelType to;
};

namespace efj
{

struct user_event
{
    UserEventType type;
    union
    {
        ChannelTypeChangedEvent channelTypeChanged;
    };
};

//EFJ_DEFINE_TRIVIAL_COPY_VIEW(efj::user_event);
EFJ_DEFINE_UNION_CONTENT_CAST(efj::user_event, type, EVENT_CHANNEL_TYPE_CHANGED, channelTypeChanged);


template <> inline const char*
c_str(ChannelType type)
{
    switch (type) {
    case CHANNEL_TYPE_TRAFFIC: return "Traffic";
    case CHANNEL_TYPE_CONTROL: return "Control";
    default:                   return "Invalid";
    }
}

}

inline efj::user_event
make_channel_type_changed_event(ChannelType from, ChannelType to)
{
    efj::user_event result;
    result.type                    = EVENT_CHANNEL_TYPE_CHANGED;
    result.channelTypeChanged.from = from;
    result.channelTypeChanged.to   = to;

    return result;
}

struct ChannelController
{
    EFJ_OBJECT("Controller");

    efj::timer  _changedTimer;
    ChannelType _cur          = CHANNEL_TYPE_CONTROL;

    bool init()
    {
        efj::add_periodic_timer("Event source", 70_ms, &_changedTimer, &ChannelController::notify);
        return true;
    }

    void notify(efj::timer_event&)
    {
        for (int i = 0; i < 7; i++) {
            auto nextState = _cur == CHANNEL_TYPE_CONTROL ? CHANNEL_TYPE_TRAFFIC : CHANNEL_TYPE_CONTROL;
            efj_trigger(EVENT_CHANNEL_TYPE_CHANGED, make_channel_type_changed_event(_cur, nextState));
            _cur = nextState;
        }
    }
};

struct Heartbeater
{
    EFJ_OBJECT("Heartbeater");

    efj::timer _heartbeatTimer;
    ChannelType _type = CHANNEL_TYPE_CONTROL;

    bool init()
    {
        efj::add_periodic_timer("Heartbeat", 1_ms, &_heartbeatTimer, &Heartbeater::beat);
        //efj::subscribe(EVENT_CHANNEL_TYPE_CHANGED, &Heartbeater::notify, efj::sub_sync);
        efj::subscribe(EVENT_CHANNEL_TYPE_CHANGED, &Heartbeater::notify, efj::sub_async);
        return true;
    }

    void beat(efj::timer_event&)
    {
        printf("[%u] Beating as %s\n", efj::logical_thread_id(), efj::c_str(_type));
    }

    void notify(const ChannelTypeChangedEvent& e)
    {
        printf("[%u] Heartbeater from %s to %s\n", efj::logical_thread_id(), efj::c_str(e.from), efj::c_str(e.to));
        _type = e.to;
    }
};

struct MessageEmitter
{
    EFJ_OBJECT("Message Emitter");

    efj::timer _emitTimer; // Pretend this is a socket or something.
    ChannelType _type = CHANNEL_TYPE_CONTROL;

    bool init()
    {
        efj::add_periodic_timer("Emit", 1_ms, &_emitTimer, &MessageEmitter::emit);
        //efj::subscribe(EVENT_CHANNEL_TYPE_CHANGED, &MessageEmitter::notify, efj::sub_sync);
        efj::subscribe(EVENT_CHANNEL_TYPE_CHANGED, &MessageEmitter::notify, efj::sub_async);

        return true;
    }

    void emit(efj::timer_event&)
    {
        printf("[%u] Emitting data as %s\n", efj::logical_thread_id(), efj::c_str(_type));
    }

    void notify(const ChannelTypeChangedEvent& e)
    {
        printf("[%u] Emitter from %s to %s\n", efj::logical_thread_id(), efj::c_str(e.from), efj::c_str(e.to));
        _type = e.to;
    }
};

int main(int argc, const char** argv)
{
    efj::register_user_events(USER_EVENT_TYPE_COUNT_);

    ChannelController controller;
    Heartbeater       heartbeater;
    MessageEmitter    emitter;

    auto& userEventThread = *efj::create_thread("Event Scheduling");
    userEventThread.use_for_event_scheduling(true);

    auto& t1 = *efj::create_thread("T1");
    t1.add_objects(heartbeater);

    auto& t2 = *efj::create_thread("T2");
    t2.add_objects(emitter);

    return efj::exec(argc, argv);
}

#define EFJ_C2_IMPLEMENTATION
#include <c2.hpp>

