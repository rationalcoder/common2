#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <common2.hpp>
#include <dbp.hpp>

#define MAX_THREAD_COUNT 64


union RegisterThreadBuf
{
    efj::dbp_register_thread info;
    u8 buf[sizeof(efj::dbp_register_thread) + 128];
};

struct Client
{
    EFJ_OBJECT("Client");

    efj::timer _retry_timer;
    efj::timer _edit_timer;
    efj::tcp_client _client;

    efj::dbp_parser       _parser;
    efj::dbp_event_parser _event_parsers[MAX_THREAD_COUNT];
    RegisterThreadBuf     _thread_info[MAX_THREAD_COUNT];

    u8 _event_bufs[MAX_THREAD_COUNT][EFJ_DBP_PARSE_BUF_MIN];
    u8 _buf[EFJ_DBP_PARSE_BUF_MIN];

    bool _registered_log_debug = false;
    bool _log_debug            = false;
    u64  _log_debug_object_id  = 0;
    u64  _log_debug_watch_key  = 0;

    bool _registered_value = false;
    u32  _value            = 0;
    u64  _value_object_id  = 0;
    u64  _value_watch_key  = 0;

    bool init()
    {
        efj::tcp_create_client("DBP", _client, &Client::notify);
        efj::tcp_connect(_client, "127.0.0.1", 65000);
        //efj::tcp_connect(_client, "129.120.151.97", 65000);

        efj::create_oneshot_timer("Connect Retry", 1_s, _retry_timer);
        efj::timer_monitor(_retry_timer, &Client::retryConnect);

        //efj::create_periodic_timer("Edit", 1_s, _edit_timer);
        //efj::timer_monitor(_edit_timer, &Client::editValue);

        return true;
    }

    void notify(efj::tcp_client_event& e)
    {
        //printf("Events: %s\n", efj::events_to_string(events).c_str());

        switch (e.type) {
        case efj::tcp_client_event_error:
        case efj::tcp_client_event_hangup:
        case efj::tcp_client_event_read_hangup:
            efj::tcp_close(_client);
            break;
        case efj::tcp_client_event_connection_failed:
            printf("Connection failed. Retrying...\n");
            efj::timer_start(_retry_timer);
            break;
        case efj::tcp_client_event_connected: {
            printf("Connected. Checking protocol support.\n");
            efj::dbp_connect msg = {};
            msg.header.type = efj::dbp_message_connect;
            msg.header.size = sizeof(efj::dbp_connect);
            msg.versionMajor = 0;
            msg.versionMinor = 1;

            efj::tcp_send(client, &msg, sizeof(msg), MSG_NOSIGNAL);

            _parser.reset(_buf, sizeof(_buf));

            for (int i = 0; i < MAX_THREAD_COUNT; i++) {
                printf("Resetting all buffers\n");
                _event_parsers[i].reset(_event_bufs[i], sizeof(_event_bufs[i]));
            }
            else {
                printf("Connection failed. Trying again...\n");
                efj::start_timer(_retry_timer);
            }
            break;
        }
        case efj::tcp_client_event_disconnected:
            efj::tcp_close(_client);
            efj::timer_start(_retry_timer);
            break;
        case efj::tcp_client_event_data:
            ssize_t n = efj::read(client, _parser.read_at(), _parser.read_size());
            if (n <= 0) {
                printf("Disconnect\n");
                efj::tcp_connect(_client, "127.0.0.1", 65000);
            }
            else {
                handleDbp(n);
            }
            break;
        default:
            efj_invalid_code_path();
            break;
        }
    }

    void retryConnect(efj::timer_event& e)
    {
        printf("retry\n");
        efj::tcp_connect(_client, "127.0.0.1", 65000);
        //efj::tcp_connect(_client, "45.79.56.137", 65000);
    }

    void handleDbp(umm n)
    {
        //printf("got %d bytes\n", n);
        _parser.read_bytes(n);
        for (auto* tok = _parser.parse(); tok; tok = _parser.parse()) {
            switch (tok->type) {
            case efj::dbp_token_connect_resp: {
                efj::dbp_connect_resp msg = {};
                efj_assert(tok->size == sizeof(msg));
                memcpy(&msg, tok->data, tok->size);
                printf(msg.success ? "Protocol agreed upon.\n" : "Protocol version not supported. Not implemented.\n");
                break;
            }
            case efj::dbp_token_shutdown: {
                printf("Shutdown\n");
                efj::dbp_shutdown_resp msg = {};
                msg.header.type = efj::dbp_message_shutdown_resp;
                msg.header.size = sizeof(msg);

                efj::tcp_send(_client, &msg, sizeof(msg));
                efj::tcp_close(_client);

                //efj::timer_stop(_edit_timer);
                efj::timer_start(_retry_timer);
                break;
            }
            case efj::dbp_token_thread_events_begin:
            case efj::dbp_token_thread_events_content:
            case efj::dbp_token_thread_events_end: {
                handleThreadEvents(tok);
                break;
            }
            default:
                //printf("Default: %d\n", tok->type);
                handleMeta(tok);
                break;
            }
        }

        if (_parser.error) {
            printf("Error: %s\n", _parser.error);
        }
    }

    void handleMeta(const efj::dbp_token* token)
    {
        static u8 register_thread_buf[sizeof(efj::dbp_register_thread) + 128] alignas(efj::dbp_register_thread);
        static u8 register_object_buf[sizeof(efj::dbp_register_object) + 128] alignas(efj::dbp_register_object);
        static u8 register_watch_buf [sizeof(efj::dbp_register_watch)  + 128] alignas(efj::dbp_register_watch);

        static umm register_thread_progress = 0;
        static umm register_object_progress = 0;
        static umm register_watch_progress  = 0;

        switch (token->type) {
        case efj::dbp_token_register_thread_begin:
            register_thread_progress = 0;
        case efj::dbp_token_register_thread_content:
        case efj::dbp_token_register_thread_end: {
            printf("Accumulating token of size %zu at %p\n", token->size, token->data);
            efj::dbp_accumulate(token, register_thread_buf, sizeof(register_thread_buf), &register_thread_progress);

            if (token->type == efj::dbp_token_register_thread_end) {
                //printf("Thread end\n");
                if (register_thread_progress == sizeof(register_thread_buf)) {
                    register_thread_buf[sizeof(register_thread_buf)-1] = '\0';
                }
                auto* msg = (efj::dbp_register_thread*)register_thread_buf;
                memcpy(&_thread_info[msg->ltid].buf, register_thread_buf, register_thread_progress);

                printf("Registered thread %s (%u | %llu)\n", msg->name(), msg->ltid, (long long unsigned)msg->rtid);
            }

            break;
        }
        case efj::dbp_token_register_object_begin:
            register_object_progress = 0;
        case efj::dbp_token_register_object_content:
        case efj::dbp_token_register_object_end: {
            efj::dbp_accumulate(token, register_object_buf, sizeof(register_object_buf), &register_object_progress);
            if (token->type == efj::dbp_token_register_object_end) {
                auto* msg = (efj::dbp_register_object*)register_object_buf;
                printf("Registered object %s\n", msg->name());
            }

            break;
        }
        case efj::dbp_token_register_watch_begin:
            register_watch_progress = 0;
        case efj::dbp_token_register_watch_content:
        case efj::dbp_token_register_watch_end: {
            efj::dbp_accumulate(token, register_watch_buf, sizeof(register_watch_buf), &register_watch_progress);

            if (token->type == efj::dbp_token_register_watch_end) {
                auto* msg = (efj::dbp_register_watch*)register_watch_buf;

                switch (msg->valueType) {
                case efj::dbp_value_u32:
                    u32 value = 0;
                    memcpy(&value, msg->value(), sizeof(value));
                    printf("Registered watch %s: %u\n", msg->name(), value);
                    break;
                }
                case efj::dbp_value_u64: {
                    u64 value = 0;
                    memcpy(&value, msg->value(), sizeof(value));
                    printf("Registered watch %s: %llu\n", msg->name(), (long long unsigned)value);
                    break;
                }
                case efj::dbp_value_bool: {
                    printf("Registered watch %s: %d\n", msg->name(), *(bool*)msg->value());
                    break;
                }
                default:
                    printf("Registered watch %s\n", msg->name());
                    break;
                }

 #if 0
                if (!_registered_value) {
                    if (strcmp(msg->name(), "Value") == 0 && msg->valueType == efj::dbp_value_u32) {
                        memcpy(&_value, msg->value(), sizeof(_value));
                        _value_object_id  = msg->objectId;
                        _value_watch_key  = msg->watchKey;
                        _registered_value = true;
                    }
                }

                if (!_registered_log_debug) {
                    if (strcmp(msg->name(), "Log debug") == 0 && msg->valueType == efj::dbp_value_bool) {
                        _log_debug            = *(bool*)msg->value();
                        _log_debug_object_id  = msg->objectId;
                        _log_debug_watch_key  = msg->watchKey;

                        _registered_log_debug = true;
                    }

                    efj::start_timer(_edit_timer);
                }
            }
#endif

            break;
        }
        case efj::dbp_token_watch_changed_begin:
            //printf("Change begin\n");
            break;
        case efj::dbp_token_watch_changed_content:
            //printf("Change content\n");
            break;
        case efj::dbp_token_watch_changed_end:
            //printf("Change end\n");
            break;
        default:
            //printf("token: %d\n",token->type);
            break;
        }
    }

    void editValue(efj::timer&)
    {
        if (!(_registered_value && _registered_log_debug))
            return;

        _value++;
        _log_debug = !_log_debug;

        u8 valueEditBuf[sizeof(efj::dbp_watch_edited) + sizeof(_value)] alignas(efj::dbp_watch_edited) = {};
        auto* valueEditMsg = (efj::dbp_watch_edited*)valueEditBuf;

        valueEditMsg->header.type    = efj::dbp_message_watch_edited;
        valueEditMsg->header.size    = sizeof(valueEditBuf);
        valueEditMsg->type           = efj::dbp_value_u32;
        valueEditMsg->watchKey       = _value_watch_key;
        *(u32*)valueEditMsg->value() = _value;

        u8 logDebugEditBuf[sizeof(efj::dbp_watch_edited) + sizeof(_log_debug)] alignas(efj::dbp_watch_edited) = {};
        auto* logDebugEditMsg = (efj::dbp_watch_edited*)logDebugEditBuf;

        logDebugEditMsg->header.type     = efj::dbp_message_watch_edited;
        logDebugEditMsg->header.size     = sizeof(logDebugEditBuf);
        logDebugEditMsg->type            = efj::dbp_value_bool;
        logDebugEditMsg->watchKey        = _log_debug_watch_key;
        *(bool*)logDebugEditMsg->value() = _log_debug;

        printf("Value: %u, Log debug: %d\n", _value, _log_debug);

        if (efj::send_all(_client, valueEditBuf, sizeof(valueEditBuf), MSG_NOSIGNAL) <= 0) {
            fprintf(stderr, "send(): %s\n", strerror(errno));
        }

        if (efj::send_all(_client, logDebugEditBuf, sizeof(logDebugEditBuf), MSG_NOSIGNAL) <= 0) {
            fprintf(stderr, "send(): %s\n", strerror(errno));
        }
    }

    void handleThreadEvents(const efj::dbp_token* token)
    {
        static u32 cur_thread_id = 0;
        static efj::dbp_event_parser* parser = nullptr;

        efj::dbp_thread_events events = {};

        if (token->type < efj::dbp_token_thread_events_begin ||
            token->type > efj::dbp_token_thread_events_end) {
            fprintf(stderr, "Bad thread event token %d\n", token->type);
            return;
        }

        if (token->type == efj::dbp_token_thread_events_begin) {
            efj_assert(token->size == sizeof(events));
            memcpy(&events, token->data, token->size);
            cur_thread_id = events.ltid;
            parser        = &_event_parsers[cur_thread_id];
            printf("Events on %s [%u]\n", _thread_info[cur_thread_id].info.name(), _thread_info[cur_thread_id].info.ltid);
        }

        //printf("New token\n");
        while (parser->read(token)) {
            //printf("Reading...\n");
            for (auto* tok = parser->parse(); tok; tok = parser->parse()) {
                //printf("Tok %u\n", tok->type);

                efj::dbp_thread_event event = {};
                if (tok->type == efj::dbp_token_thread_event) {
                    memcpy(&event, tok->data, sizeof(event));

                    u32 begin_us = event.begin.sec * 1000000 + event.begin.nsec / 1000000;
                    u32 end_us   = event.end.sec   * 1000000 + event.end.nsec   / 1000000;

                    u32 total_us = end_us - begin_us;
                    printf("[%s] Event (%u) %u us (%f s)\n", _thread_info[cur_thread_id].info.name(), event.source, total_us, total_us / 1000000.0f);
                }

                if (tok->type == efj::dbp_token_thread_crash_begin ||
                    tok->type == efj::dbp_token_thread_crash_content ||
                    tok->type == efj::dbp_token_thread_crash_end) {

                    static u8 buf[sizeof(efj::dbp_thread_crashed) + 1024] alignas(efj::dbp_thread_crashed);
                    static umm progress = 0;

                    if (tok->type == efj::dbp_token_thread_crash_begin) {
                        progress = 0;
                    }

                    efj::dbp_accumulate(tok, buf, sizeof(buf), &progress);

                    if (tok->type == efj::dbp_token_thread_crash_end) {
                        auto* msg = ((efj::dbp_thread_crashed*)buf);
                        printf("Thread \"%s\" crashed with signal %d (%s):\n{\n%s}\n", _thread_info[cur_thread_id].info.name(),
                            msg->sig, msg->signal_name(), msg->trace());
                    }
                }

                if (tok->type == efj::dbp_token_thread_exit) {
                    efj::dbp_thread_exited msg = {};
                    memcpy(&msg, tok->data, sizeof(msg));
                    printf("Thread \"%s\" (%u) exited with code %d\n", _thread_info[cur_thread_id].info.name(), msg.ltid, msg.code);
                }

                if (tok->type == efj::dbp_token_watch_changed_count) {
                    u16 count = 0;
                    memcpy(&count, tok->data, sizeof(count));

                    printf("\tChange count = %u\n", count);
                }

                if (tok->type == efj::dbp_token_watch_changed_begin ||
                    tok->type == efj::dbp_token_watch_changed_content ||
                    tok->type == efj::dbp_token_watch_changed_end) {

                    static u8 buf[sizeof(efj::dbp_watch_changed) + 128] alignas(efj::dbp_watch_changed);
                    static umm progress = 0;

                    if (tok->type == efj::dbp_token_watch_changed_begin) {
                        progress = 0;
                    }

                    efj::dbp_accumulate(tok, buf, sizeof(buf), &progress);

                    if (tok->type == efj::dbp_token_watch_changed_end) {
                        if (progress == sizeof(buf)) {
                            buf[sizeof(buf)-1] = '\0';
                        }

                        auto* msg = (efj::dbp_watch_changed*)buf;

                        printf("\tWatch = %u %u\n", msg->watchId , msg->type);
                    }
                }
            }
        }
    }
};

int main(int argc, const char** argv)
{
    Client client;

    return efj::exec(argc, argv);
}

#define EFJ_COMMON2_IMPLEMENTATION
#include <common2.hpp>
