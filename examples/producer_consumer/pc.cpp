#include <stdio.h>
#include <c2.hpp>

struct Producer
{
    EFJ_OBJECT("Producer");
    EFJ_OUTPUT(Int, const int);
    EFJ_OUTPUT(Float, const float);

    efj::timer _timer;

    bool init()
    {
        efj::add_periodic_timer("Tick", 200_ms, &_timer, &Producer::onTick);
        return true;
    }

    void onTick(efj::timer_event& e)
    {
        printf("producing values\n");
        efj_produce(Int, 1);
        efj_produce(Int, 2);
        efj_produce(Int, 3);
        efj_produce(Float, 1.1f);
        efj_produce(Float, 1.2f);
        efj_produce(Float, 1.3f);
    }
};

struct Consumer
{
    EFJ_OBJECT("Consumer");
    EFJ_INPUT(Int, const int);
    EFJ_INPUT(Float, const float);

    void consumeInt(const int& val)
    {
        printf("[%u] %s: Got %d\n", efj::logical_thread_id(), efj::this_object()->name, val);
    }

    void consumeFloat(const float& val)
    {
        printf("[%u] %s: Got %f\n", efj::logical_thread_id(), efj::this_object()->name, val);
    }
};

struct Consumer2
{
    EFJ_OBJECT("Consumer 2");
    EFJ_INPUT(Int, const int);

    void consumeInt(const int& val)
    {
        printf("[%u] %s: Got %d\n", efj::logical_thread_id(), efj::this_object()->name, val);
    }
};

int main(int argc, const char** argv)
{
    Producer producer1;
    Producer producer2;
    Consumer consumer1;
    Consumer2 consumer2;

    efj_connect(producer1, Int, consumer1, Int);
    efj_connect(producer1, Int, consumer2, Int);
    efj_connect(producer2, Float, consumer1, Float);

    efj::thread* backgroundThread = efj::create_thread("Background");
    backgroundThread->add_objects(producer2, consumer1);

    return efj::exec(argc, argv);
}

#define EFJ_C2_IMPLEMENTATION
#include <c2.hpp>
