#include "dfsi_interface.hpp"

bool DfsiInterface::init()
{
    efj::subscribe_cached(EVENT_CONFIG, &config, &DfsiInterface::onConfig, efj::sub_sync);
    efj::subscribe(EVENT_DFSI_CONNECT, &DfsiInterface::onConnectionEvent);
    efj::subscribe(EVENT_DFSI_DISCONNECT, &DfsiInterface::onConnectionEvent);

    return true;
}

void DfsiInterface::onConfig(const AppConfig& cfg)
{
    printf("rxFrequency: %u\n", config->rxFrequency);
    printf("rxBandwidth: %u\n", config->rxBandwidth);
    printf("txFrequency: %u\n", config->txFrequency);
    printf("txBandwidth: %u\n", config->txBandwidth);
    // 0 => MXDR_MOD_LSM => C6K_MOD_CQSPK, 1 => MXDR_MOD_FM => C6K_MOD_C4FM.
    printf("txModType: %u\n", config->txModType);
    printf("txPower: %u\n", config->txPower);
    printf("txFrequencyOffset: %u\n", config->txFrequencyOffset);

    printf("txMxdrOptions: %u\n", config->txMxdrOptions);
    printf("txRxDelay: %u\n", config->txRxDelay);
    printf("exciterBufferCount: %u\n", config->exciterBufferCount);

    printf("dfsiControlPort: %u\n", config->dfsiControlPort);
    printf("dfsiVoicePort: %u\n", config->dfsiVoicePort);
    printf("dfsiHeartbeatIntervalSec: %u\n", config->dfsiHeartbeatIntervalSec);
    printf("dfsiControlRetryIntervalMsec: %u\n", config->dfsiControlRetryIntervalMsec);
    createSockets();
}

void DfsiInterface::consumeInboundControl(const FsiMessage& msg)
{
    if (haveLogicalConnection)
    {
        //efj_log_debug("Sending control msg %u,%u to %s:%u", msg.data[0], msg.size, efj::net_get_ip(controlAddr).c_str(), efj::net_get_port(controlAddr));
        //efj_log_debug("Resp code: %u", fsi_get_resp_code((FsiAck&)msg));

        // 0 ip => send to current host. Otherwise, we are responding to some other device.
        if (msg.ipv4 == 0)
            efj::udp_send_to(controlSocket, msg.data, msg.size, controlAddr);
        else
            efj::udp_send_to(controlSocket, msg.data, msg.size, msg.ipv4, msg.port);
    }
    else
    {
        //efj_log_debug("Sending control msg %u,%u to %x:%u", msg.data[0], msg.size, msg.ipv4, ntohs(msg.port));

        efj_panic_if(msg.ipv4 == 0);
        efj::udp_send_to(controlSocket, msg.data, msg.size, msg.ipv4, msg.port);
    }
}

void DfsiInterface::consumeInboundVoice(const RtpP25Message& msg)
{
    if (haveLogicalConnection)
    {
        efj_log_debug("Sending: %s", efj::to_hex(msg).c_str());
        efj::udp_send_to(voiceSocket, msg.data, msg.size, voiceAddr);
    }
    else
    {
        efj_log_warn("Ignoring voice sent without a logical connection");
    }
}

void DfsiInterface::onConnectionEvent(const efj::user_event& e)
{
    switch (e.type)
    {
    case EVENT_DFSI_CONNECT:
    {
        auto* dfsiConnect = e.as<DfsiConnectEvent>();
        lastConnectEvent = *dfsiConnect;

        // The control address is already set, since we can read it when reading control messages.
        voiceAddr = controlAddr;
        voiceAddr.port = htons(lastConnectEvent.host_vc_base_port);

        efj::string ip = efj::net_get_ip(controlAddr);
        u16 port = efj::net_get_port(controlAddr);

        efj_log_info("DFSI Connect: control: %s:%u, voice port: %u", ip.c_str(), port, lastConnectEvent.host_vc_base_port);

        haveLogicalConnection = true;
        break;
    }
    case EVENT_DFSI_DISCONNECT:
        efj_log_info("DFSI Disconnect");
        haveLogicalConnection = false;
        break;
    default:
        efj_invalid_code_path();
        break;
    }
}

void DfsiInterface::onControlSocketEvent(efj::udp_socket_event& e)
{
    switch (e.type)
    {
    case efj::udp_socket_event_read:
    {
        char readBuf[4096] = {};
        umm size = efj::udp_recv_from(e, readBuf, sizeof(readBuf), &controlAddr);

        FsiMessage msg {readBuf, size};
        msg.ipv4 = controlAddr.address.ipv4.s_addr;
        msg.port = controlAddr.port;

        //u8* bytes = (u8*)&msg.ipv4;
        //printf("Read type: %u, size: %zu (%u.%u.%u.%u:%u)\n", readBuf[0], size, bytes[0], bytes[1], bytes[2], bytes[3], msg.port);
        efj_produce(OutboundControl, msg);
        break;
    }
    case efj::udp_socket_event_error:
        break;
    default:
        efj_invalid_code_path();
        break;
    }
}

void DfsiInterface::onVoiceSocketEvent(efj::udp_socket_event& e)
{
    switch (e.type)
    {
    case efj::udp_socket_event_read:
    {
        char readBuf[4096] = {};
        umm size = efj::udp_recv_from(e, readBuf, sizeof(readBuf), &voiceAddr);
        //printf("Read type: %u, size: %zu\n", readBuf[0], size);

        RtpP25Message msg { readBuf, size };
        msg.ipv4 = voiceAddr.address.ipv4.s_addr;
        msg.port = voiceAddr.port;

        efj_produce(OutboundVoice, msg);
        break;
    }
    case efj::udp_socket_event_error:
        break;
    default:
        efj_invalid_code_path();
        break;
    }
}

void DfsiInterface::createSockets()
{
    efj_assert(config);

    if (interfacesCreated)
    {
        interfacesCreated = false;
        efj::udp_destroy_socket(controlSocket);
        efj::udp_destroy_socket(voiceSocket);
    }

    printf("Creating sockets on localhost:%u, localhost:%u\n", config->dfsiControlPort, config->dfsiVoicePort);
    efj::udp_create_socket("Control Socket", &controlSocket);
    efj::udp_bind(controlSocket, "0.0.0.0", config->dfsiControlPort);
    efj::udp_set_high_priority(controlSocket);
    efj::udp_monitor(controlSocket, &DfsiInterface::onControlSocketEvent);

    efj::udp_create_socket("Voice Socket", &voiceSocket);
    efj::udp_bind(voiceSocket, "0.0.0.0", config->dfsiVoicePort);
    efj::udp_set_high_priority(voiceSocket);
    efj::udp_monitor(voiceSocket, &DfsiInterface::onVoiceSocketEvent);

    interfacesCreated = true;
}

