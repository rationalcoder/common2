//! This is a place for c2 related definitions.
//!

//{ Config
enum
{
    CONFIG_MAIN,
    CONFIG_TUNING,
};
//}

//{ User events

struct DfsiConnectEvent
{
    u32 host_ip;
    u16 host_port;

    u32 fs_vc_ssrc;
    u16 host_vc_base_port;
    u16 fs_vc_base_port;
    u8  fs_hb_period;
    u8  host_hb_period;
    u8  connectivity_tx_timer;
};
EFJ_DEFINE_USER_EVENT(DfsiConnectEvent);

struct DfsiDisconnectEvent
{
};
EFJ_DEFINE_USER_EVENT(DfsiDisconnectEvent);

//{ DFSI control stream -> MXDR
struct SetTxMode
{
    u32 freq;
    u16 power;
    u8  number;
    u8  mode;
};
EFJ_DEFINE_USER_EVENT(SetTxMode);

struct SetRxMode
{
    u32 freq;
    u8  number;
};
EFJ_DEFINE_USER_EVENT(SetRxMode);
//}

enum CallType
{
    CALL_TYPE_INVALID,
    CALL_TYPE_ANALOG,
    CALL_TYPE_P1_CONTROL, // TODO: Pretty sure we don't need data to be separate, but maybe.
    CALL_TYPE_P1_VOICE,
    CALL_TYPE_P2,
    CALL_TYPE_COUNT_,
};

struct BeginCall
{
    CallType type;
};

struct EndCall
{
    CallType type;
};

struct PruInit {};
EFJ_DEFINE_USER_EVENT(PruInit);

struct PruInitted
{
    bool result;
};
EFJ_DEFINE_USER_EVENT(PruInitted);

struct DspInit {};
EFJ_DEFINE_USER_EVENT(DspInit);

struct DspInitted
{
    bool result;
};
EFJ_DEFINE_USER_EVENT(DspInitted);

struct MxdrInit {};
EFJ_DEFINE_USER_EVENT(MxdrInit);

struct MxdrInitted
{
    bool result;
};
EFJ_DEFINE_USER_EVENT(MxdrInitted);

struct MxdrDown {};
EFJ_DEFINE_USER_EVENT(MxdrDown);

enum HardwareStatusValue
{
    HARDWARE_STATUS_INVALID_,
    HARDWARE_STATUS_CONFIG,
    HARDWARE_STATUS_READY,
    HARDWARE_STATUS_NOT_READY,
    HARDWARE_STATUS_COUNT_,
};

struct HardwareStatus
{
    HardwareStatusValue value;
    // Valid when the status is READY.
    // These values should be equal to their respective enum values.
    // 0 => CQSPK
    // 1 => C4FM
    u32 p1_tx_mod;
};
EFJ_DEFINE_USER_EVENT(HardwareStatus);

struct UpdateLeds
{
    // These use MXDR LED flags.
    int leds_on;
    int leds_off;
};
EFJ_DEFINE_USER_EVENT(UpdateLeds);

struct AllocateDsp
{
    bool inbound;
    bool outbound;
};
EFJ_DEFINE_USER_EVENT(AllocateDsp);

struct DspGrant
{
    bool inbound;
    bool outbound;
};
EFJ_DEFINE_USER_EVENT(DspGrant);

struct DspDeny
{
    bool inbound;
    bool outbound;
};
EFJ_DEFINE_USER_EVENT(DspDeny);

enum EventType
{
    EVENT_INVALID,
    EVENT_CONFIG,
    EVENT_DFSI_CONNECT,
    EVENT_DFSI_DISCONNECT,
    EVENT_SET_TX_MODE,
    EVENT_SET_RX_MODE,
    EVENT_BEGIN_CALL,
    EVENT_END_CALL,
    EVENT_UPDATE_LEDS,
    EVENT_INIT_PRU,
    EVENT_PRU_INITTED,
    EVENT_INIT_DSP,
    EVENT_DSP_INITTED,
    EVENT_INIT_MXDR,
    EVENT_MXDR_INITTED,
    EVENT_MXDR_DOWN,
    EVENT_HARDWARE_STATUS,
    EVENT_ALLOCATE_DSP,
    EVENT_DSP_AVAILABLE,
    EVENT_DSP_GRANT,
    EVENT_DSP_DENY,
    EVENT_COUNT_,
};

namespace efj
{
template <> inline const char*
c_str(EventType e)
{
    switch (e)
    {
    case EVENT_INVALID: return "INVALID";
    case EVENT_CONFIG: return "CONFIG";
    case EVENT_DFSI_CONNECT: return "DFSI_CONNECT";
    case EVENT_DFSI_DISCONNECT: return "DFSI_DISCONNECT";
    case EVENT_SET_TX_MODE: return "SET_TX_MODE";
    case EVENT_SET_RX_MODE: return "SET_RX_MODE";
    case EVENT_BEGIN_CALL: return "BEGIN_CALL";
    case EVENT_END_CALL: return "END_CALL";
    case EVENT_UPDATE_LEDS: return "UPDATE_LEDS";
    case EVENT_INIT_PRU: return "INIT_PRU";
    case EVENT_PRU_INITTED: return "PRU_INITTED";
    case EVENT_INIT_DSP: return "INIT_DSP";
    case EVENT_DSP_INITTED: return "DSP_INITTED";
    case EVENT_INIT_MXDR: return "INIT_MXDR";
    case EVENT_MXDR_INITTED: return "MXDR_INITTED";
    case EVENT_MXDR_DOWN: return "MXDR_DOWN";
    case EVENT_HARDWARE_STATUS: return "HARDWARE_STATUS";
    case EVENT_ALLOCATE_DSP: return "ALLOCATE_DSP";
    case EVENT_DSP_AVAILABLE: return "DSP_AVAILABLE";
    case EVENT_DSP_GRANT: return "DSP_GRANT";
    case EVENT_DSP_DENY: return "DSP_DENY";
    default: return "Unknown";
    }
}

} // namespace efj

//}
