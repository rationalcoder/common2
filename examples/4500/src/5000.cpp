#include <stdio.h>
#include <errno.h>

#include <c2.hpp>
#include "c2_5000.hpp"

#include "proto/proto.hpp"
#include "config_manager.hpp"
#include "dfsi_interface.hpp"
#include "dfsi_control_client.hpp"
#include "dfsi_voice_client.hpp"
#include "dsp/dsp.hpp"
#include "hw/hw.hpp"

#define PUT_EVERY_OBJECT_ON_ITS_OWN_THREAD 0

// File/line numbers can get annoying.
efj::string formatLogMessage(const efj::log_message_ctx& ctx, const efj::string& msg)
{
    efj::string now = efj::to_string(efj::to_utc(ctx.time));
    if (ctx.object)
        return efj::fmt("%.*s %s: %.*s\n", (int)now.size, now.data, ctx.object->name, (int)msg.size, msg.data);
    else
        return efj::fmt("%.*s: %.*s\n", (int)now.size, now.data, (int)msg.size, msg.data);
}

void devInit();

int main(int argc, const char** argv)
{
    efj::test_use_dev_init(devInit);

    efj::register_user_events(EVENT_COUNT_);
    efj::set_log_formatter(&formatLogMessage);

    efj::config_add_file(CONFIG_MAIN, "atlas4500.cfg.xml", efj::config_xml);
    efj::config_add_file(CONFIG_TUNING, "atlas4500Tuning.cfg.xml", efj::config_xml);

    ConfigManager configManager;

    // Analog, Phase 1, data, and control are all considered DFSI here.
    DfsiInterface     dfsiInterface;
    DfsiControlClient dfsiControlClient;
    DfsiVoiceClient   dfsiVoiceClient;
    //Phase2Interface   p2Interface;

    // It's not clear we need a data processor.
    //AnalogProcessor   analog;
    //ControlProcessor  p1Control;
    P1VoiceProcessor  p1Voice;
    //Phase2Processor   p2Proc;

    MxdrTransporter    mxdrTransporter;
    MxdrInterface      mxdrInterface;
    DspInterface       dspInterface;
    PruInterface       pruInterface;
    HardwareController hwController;

    // TODO: Split DfsiInterface into separate control/voice interfaces.
    // DfsiInterface <-> DfsiControlClient <-> XxxProcessor <-> HardwareController <-> DspInterface
    //                                                                             <-> ... <-> MxdrInterface
    //               <-> DfsiVoiceClient   <-> XxxProcessor  ^

    // DfsiInterface <-> DfsiControlClient
    //               <-> DfsiVoiceClient
    //
    efj_connect(dfsiInterface,     OutboundControl, dfsiControlClient, OutboundControl);
    efj_connect(dfsiInterface,     OutboundVoice,   dfsiVoiceClient,   OutboundVoice);
    efj_connect(dfsiControlClient, InboundControl,  dfsiInterface,     InboundControl);
    efj_connect(dfsiVoiceClient,   InboundVoice,    dfsiInterface,     InboundVoice);

    // DfsiVoiceClient <-> P1VoiceProcessor
    efj_connect(dfsiVoiceClient, OutboundVoice, p1Voice,         OutboundVoice);
    efj_connect(p1Voice,         InboundVoice,  dfsiVoiceClient, InboundVoice);

    // P1VoiceProcessor <-> HardwareController
    efj_connect(p1Voice,      OutboundMxdr, hwController, OutboundMxdr);
    efj_connect(p1Voice,      OutboundC6k,  hwController, OutboundC6k);
    efj_connect(hwController, InboundC6k,   p1Voice,      InboundC6k);
    efj_connect(hwController, InboundMxdr,  p1Voice,      InboundMxdr);

    // HardwareController <-> MxdrTransporter
    efj_connect(hwController,    OutboundMxdr, mxdrTransporter, OutboundMxdr);
    efj_connect(mxdrTransporter, InboundMxdr,  hwController,    InboundMxdr);
    // HardwareController <-> DspInterface
    efj_connect(hwController, OutboundC6k, dspInterface, OutboundC6k);
    efj_connect(dspInterface, InboundC6k,  hwController, InboundC6k);

    // MxdrTransporter <-> MxdrInterface
    efj_connect(mxdrTransporter, OutboundMxdr, mxdrInterface,   OutboundMxdr);
    efj_connect(mxdrInterface,   InboundMxdr,  mxdrTransporter, InboundMxdr);

#if PUT_EVERY_OBJECT_ON_ITS_OWN_THREAD
    for (efj::object* o : efj::objects())
    {
        efj::thread* thread = efj::create_thread(o->name);
        thread->add_object(o);
    }
#endif

    // We pretty much want everything on the main thread except for stuff like Disk IO and other things
    // that are on other threads by default in C2.
    // FIXME: Set thread priorities so the Disk IO thread is low priority. Otherwise, on our ARMv5 platform the Disk IO thread
    // could spend up to 10 ms doing whatever before we get back to call handling due to scheduling behavior.
    //
    // The rest should be handled in C2, but currently isn't.
    //efj::thread_set_disk_io_priority(1);

    return efj::exec(argc, argv);
}

#include "config_manager.cpp"
#include "dfsi_interface.cpp"
#include "dfsi_control_client.cpp"
#include "dfsi_voice_client.cpp"
#include "dsp/dsp.cpp"
#include "hw/hw.cpp"

#include "dev.cpp"
#include "test/test.cpp"

#define EFJ_C2_IMPLEMENTATION
#include <c2.hpp>
