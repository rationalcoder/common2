#pragma once
#include "app_config.hpp"

enum TxState
{
    TX_STATE_IDLE,
    TX_STATE_STARTING, // Sent start outbound. Haven't received starting message.
    TX_STATE_QUEUEING, // Receiving initial transmit requests.
    TX_STATE_ACTIVE,   // Received outbound serial port started. Ready to key transmitter.
    TX_STATE_STOPPING, // Sent stop message. Haven't received stopped message. Back to idle when done.
    TX_STATE_COUNT_,
};

struct P1VoiceProcessor
{
    EFJ_OBJECT("P1 Voice Processor");
    EFJ_INPUT(OutboundVoice, const RtpP25Message); // ->
    EFJ_INPUT(InboundC6k,    const C6kMessage);    // <-
    EFJ_INPUT(InboundMxdr,   const MxdrMessage);   // <-

    EFJ_OUTPUT(InboundVoice, const RtpP25Message); // <-
    EFJ_OUTPUT(OutboundC6k,  const C6kMessage);    // ->
    EFJ_OUTPUT(OutboundMxdr, const MxdrMessage);   // ->

    const AppConfig* _config = nullptr;

    bool _hardwareReady = false;
    bool _dspGranted    = false;

    //{ RX
    efj::timer _rxActivityTimer;
    bool _receiving = false;

    C6kRxPolarity _rxPolarity = C6K_RX_POLARITY_INVERTED;
    //}

    //{ TX
    efj::message_queue<RtpP25Message> _outboundQueue;
    efj::usec                         _otaTimeInQueue;
    C6kBlockTransmitMulti*            _txMulti = c6k_make_block(&_txMultiStorage);
    C6kBlockTransmitMulti             _txMultiStorage;

    // @Leak. Address sanitizer will complain about this, but we don't care.
    C6kMessage    _latestTxStatus = c6k_make_tx_status(C6K_TX_STATUS_AUTO); // uses current allocator
    TxState       _txState = TX_STATE_IDLE;
    int           _outstandingTransmitRequests = 0;
    // TODO: Not sure when/why we would want to change this to something other than 6.
    u32           _microslotsPerSlot = 6;
    C6kModulation _latestModulation = C6K_MOD_COUNT_;

    // We have to increment this during receive to know how to convert C6K into DFSI.
    // This is problematic since we could theoretically lose an arbitrary amount of content
    // over the air, resulting in us translating incorrectly.
    u32        _nextImbeFrameType = DFSI_FT_IMBE1;
    u32        _superframeCounter = 1;

    // TODO: Figure out what this is supposed to be and why. Is it supposed to be set to whatever
    // out current SET_TX_STATUS is set to? Why would we be reporting that inbound by
    // setting that state in voice packets?
    C6kTxStatusSymbol _inboundBusyStatus = C6K_TX_STATUS_UNKNOWN;

    efj::timer _keepTxActiveTimer;
    // Time between underflow warning (on last 60ms buffer) and when we decide
    // to de-key the transmitter if we haven't received content.
    efj::timer _txActivityTimer;
    u32        _nextLaunchTimeNs = 0;
    u32        _nac = 6; // TODO: replace with config
    bool       _sentTerminator = false;
    //}

    bool init();
    void onEvent(const efj::user_event& event);

    void consumeOutboundVoice(const RtpP25Message& msg);
    void consumeInboundMxdr(const MxdrMessage& msg);
    void consumeInboundC6k(const C6kMessage& msg);

    void onRxActivityTimeout(efj::timer_event&);
    void onKeepTxActiveTimeout(efj::timer_event&);
    void onTxActivityTimeout(efj::timer_event&);

    void onTransition(TxState newState);
    void startOutboundIfWeHaveEnoughData();
    void queueOutbound(const RtpP25Message& msg);
    void handleInboundVoice(const C6kMessage& msg);
    void handleOutstandingTransmitRequests();
    void handleOutboundStarting();
    void handleTransmitRequest();
    void handleOutboundSerialPortStarted();
    void handleUnderflowWarning();
    void handleModulationComplete();
    void handleOutboundStopped();
    void translateToTxMulti(const RtpP25Message& msg);
};

