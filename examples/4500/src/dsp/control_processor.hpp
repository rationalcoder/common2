#pragma once
#include "app_config.hpp"

struct ControlProcessor
{
    EFJ_OBJECT("Control Processor");
    EFJ_INPUT(OutboundVoice, const RtpP25Message); // ->
    EFJ_INPUT(InboundC6k,    const C6kMessage);    // <-
    EFJ_INPUT(InboundMxdr,   const MxdrMessage);   // <-

    EFJ_OUTPUT(InboundVoice, const RtpP25Message); // <-
    EFJ_OUTPUT(OutboundC6k,  const C6kMessage);    // ->
    EFJ_OUTPUT(OutboundMxdr, const MxdrMessage);   // ->

    const AppConfig* _config = nullptr;
    efj::timer _rxActivityTimer;
    efj::timer _txActivityTimer;
    efj::timer _keepTxActiveTimer;

    bool init();
    void onEvent(const efj::user_event& event);
    void consumeOutboundVoice(const RtpP25Message& msg);
    void consumeInboundMxdr(const MxdrMessage& msg);
    void consumeInboundC6k(const C6kMessage& msg);

    void onRxActivityTimeout(efj::timer_event&);
    void onKeepTxActiveTimeout(efj::timer_event&);
    void onTxActivityTimeout(efj::timer_event&);
};

