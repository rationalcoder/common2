
bool AnalogProcessor::init()
{
    efj::create_oneshot_timer("RX Activity", 80_ms, &_rxActivityTimer,
        &AnalogProcessor::onRxActivityTimeout);
    efj::create_oneshot_timer("Keep TX Active", 800_ms, &_keepTxActiveTimer,
        &AnalogProcessor::onKeepTxActiveTimeout);
    efj::create_oneshot_timer("TX Activity", 60_ms, &_txActivityTimer,
        &AnalogProcessor::onTxActivityTimeout);

    efj::subscribe_cached(EVENT_CONFIG, &_config, &AnalogProcessor::onEvent, efj::sub_sync);

    efj::subscribe(EVENT_HARDWARE_STATUS, &AnalogProcessor::onEvent);
    efj::subscribe(EVENT_DSP_GRANT, &AnalogProcessor::onEvent);
    efj::subscribe(EVENT_DSP_DENY, &AnalogProcessor::onEvent);

    return true;
}

void AnalogProcessor::onEvent(const efj::user_event& event)
{
}

void AnalogProcessor::consumeOutboundVoice(const RtpP25Message& msg)
{
}

void AnalogProcessor::consumeInboundMxdr(const MxdrMessage& msg)
{
}

void AnalogProcessor::consumeInboundC6k(const C6kMessage& msg)
{
}

void AnalogProcessor::onRxActivityTimeout(efj::timer_event&)
{
}

void AnalogProcessor::onKeepTxActiveTimeout(efj::timer_event&)
{
}

void AnalogProcessor::onTxActivityTimeout(efj::timer_event&)
{
}
