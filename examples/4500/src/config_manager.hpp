#pragma once
#include "app_config.hpp"

struct TuningConfig
{
    // NOTE: We don't set the intermediate frequency anymore. The MXDR decides and tells
    // us what it is in the RX Freq Ack.
    u32 rxFrequency;
    u32 rxBandwidth;
    u32 txFrequency;
    u32 txBandwidth;
    u32 txModType; // 0 => MXDR_MOD_LSM => C6K_MOD_CQSPK, 1 => MXDR_MOD_FM => C6K_MOD_C4FM.
    u32 txPower;

    // Tuning config only.
    u32 txRxDelay = 56;
    u32 exciterBufferCount;
    u32 txMxdrOptions;

    u16 dfsiControlPort;
    u16 dfsiVoicePort;
};

struct MainConfig
{
    efj::optional<u32> rxFrequency;
    efj::optional<u32> rxBandwidth;
    efj::optional<u32> txFrequency;
    efj::optional<u32> txBandwidth;
    efj::optional<u32> txModType; // 0 => MXDR_MOD_LSM => C6K_MOD_CQSPK, 1 => MXDR_MOD_FM => C6K_MOD_C4FM.
    efj::optional<u32> txPower;
    efj::optional<u32> txFrequencyOffset;

    // Required values that are only in the main config.
    u16 dfsiControlPort;
    u16 dfsiVoicePort;
};

//! Loads config files and combines the tuning and app config, abstracting this detail from the reset
//! of the application. We want to remove the tuning config file in the future, so this makes sense.
struct ConfigManager
{
    EFJ_OBJECT("Config Manager");

    TuningConfig* _curTuningConfig = nullptr;
    TuningConfig* _pendingTuningConfig = &_tuningConfig1;
    TuningConfig* _nextTuningConfig = &_tuningConfig2;

    TuningConfig _tuningConfig1;
    TuningConfig _tuningConfig2;

    MainConfig* _curMainConfig = nullptr;
    MainConfig* _pendingMainConfig = &_mainConfig1;
    MainConfig* _nextMainConfig = &_mainConfig2;

    MainConfig _mainConfig1;
    MainConfig _mainConfig2;

    AppConfig _config;
    bool _configReadyAndValid = false;

    bool init();

    void onTuningConfigChanged(efj::config_event&);
    void onMainConfigChanged(efj::config_event&);
    void onInitialConfigDone(efj::config_event&);

    void combineConfig();
};

