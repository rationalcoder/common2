
EFJ_TEST(MxdrInterface, crc)
{
    char buf1[] = "123456789";
    char buf2[] = "hello world";
    char buf3[] = "Hello world";
    char buf4[] = "a";

    TEST_EQ(calc_crc(buf1, sizeof(buf1)-1), 0xBB3D);
    TEST_EQ(calc_crc(buf2, sizeof(buf2)-1), 0x39C1);
    TEST_EQ(calc_crc(buf3, sizeof(buf3)-1), 0xF96A);
    TEST_EQ(calc_crc(buf4, sizeof(buf4)-1), 0xE8C1);
}

