#pragma once

struct DspInterface
{
    EFJ_OBJECT("DSP Interface");

    EFJ_INPUT(OutboundC6k, const C6kMessage);
    EFJ_OUTPUT(InboundC6k, const C6kMessage);

    const char* _dsp_path = "/dev/dsp";
    const char* _dsp_image_path = "/mnt/extra/atlas4500/Repeater.hex";

    efj::memory_arena _dsp_image_arena = {};
    efj::buffer       _dsp_image_buf   = {};

    efj::fd _dsp_fd;
    int _dsp_raw_fd = -1;

    bool init();
    void onInitDsp(const efj::user_event& e);
    void onDspEvent(efj::fd_event& e);
    void consumeOutboundC6k(const C6kMessage& msg);

    bool resetDsp(int fd);
};

