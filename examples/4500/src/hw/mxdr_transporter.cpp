#include "mxdr_transporter.hpp"

bool MxdrTransporter::init()
{
    efj::subscribe(EVENT_MXDR_INITTED, &MxdrTransporter::onEvent);
    return true;
}

void MxdrTransporter::onEvent(const efj::user_event& e)
{
    // TODO: What do we do when we get an MXDR down event?

    if (e.type == EVENT_MXDR_INITTED)
    {
        _mxdr_controller_ready = true;
        _waiting_for_ack = true;

        // IMPORTANT: We expect the unrequested ack for ReqGenInfo every time we
        // get this event. If we reset the MXDR communication without actually
        // resetting the controller, we will hold onto queued MXDR messaged
        // indefinitely.
        efj_log_debug("MXDR initted");
    }
    else if (e.type == EVENT_MXDR_DOWN)
    {
        efj_log_debug("MXDR down");
        _mxdr_controller_ready = false;
    }
}

void MxdrTransporter::consumeInboundMxdr(MxdrMessage& msg)
{
    // The extra mxdr message type field isn't set yet.

    // If it's an ack, we can send a queued message. Otherwise, we pass it through.
    if (mxdr_is_ack(msg))
    {
        if (!_waiting_for_ack)
            efj_log_debug("Received unexpected ack from the MXDR Controller");
        else
            _waiting_for_ack = false;

        efj_log_debug("Ack for %s: %s", efj::c_str(_last_sent_type), efj::to_hex(msg).c_str());

        // Don't pass up empty acks. Those are only useful for the transport logic.
        if (mxdr_get_data_len(msg) > 0)
            efj_produce(InboundMxdr, msg);

        efj_log_debug("Queue size: %zu", _outbound_queue.size());
        sendQueuedMessage();
    }
    else
    {
        efj_log_debug("Received %s from MXDR. Sending ack", efj::c_str(mxdr_get_valid_type(msg)));
        auto ack = mxdr_make_ack();
        efj_produce(OutboundMxdr, ack);

        efj_produce(InboundMxdr, msg);
    }
}

void MxdrTransporter::consumeOutboundMxdr(const MxdrMessage& outbound)
{
    //efj_log_debug("Queue size: %zu: write: %zu, read: %zu", _outbound_queue.size(),
    //    _outbound_queue._writeHead, _outbound_queue._readHead);

    efj_log_debug("Queueing %s", efj::c_str((MxdrMessageType)mxdr_get_type(outbound)));
    // XXX
    if (mxdr_get_type(outbound) == MXDR_SET_TX_STATE)
    {
        auto* block = mxdr_get_block<MxdrBlockTxStatus>(outbound);
        efj_log_debug("On: %d", block->getOn());
    }

    bool replaced = false;
    for (MxdrOutboundBlock& block : _outbound_queue)
    {
        MxdrMessage pending = mxdr_block_to_message(block);

        u8 pending_type  = mxdr_get_type(pending);
        u8 outbound_type = mxdr_get_type(outbound);

        if (pending_type == outbound_type)
        {
            // At the time of writing, there aren't variable sized messages in the MXDR protocol. If two messages
            // are different sizes for some reason, we assume it's a bug or something this code doesn't know about, in
            // which case we queue the message and do not replace the existing one.
            if (pending.size != outbound.size)
            {
                efj_log_crit("Tried to replace queued MXDR msg type 0x%x, but there was a size mismatch (%zu/%zu). "
                    "Queueing instead of replacing", outbound_type, pending.size, outbound.size);
                continue;
            }

            efj_log_debug("Replaced outbound MXDR message 0x%x", outbound_type);
            bool before = _mxdr_controller_ready;
            memcpy(pending.data, outbound.data, pending.size);
            bool after = _mxdr_controller_ready;
            if (before != after)
                efj_log_debug("Corruption after memcpy: %zu", pending.size);

            replaced = true;
        }
    }

    bool before = _mxdr_controller_ready;
    if (!replaced)
    {
        efj::error_code ec;
        _outbound_queue.push(outbound, &ec);

        if (ec)
        {
            efj_log_crit("Failed to queue outbound MXDR, queue size: %zu, msg size: %zu, error: %s",
                _outbound_queue.size(), outbound.size, efj::c_str(ec));

            efj_panic("Failed to queue outbound MXDR");
        }
    }
    bool after = _mxdr_controller_ready;
    if (before != after)
        efj_log_debug("Corruption after push");

    // FIXME: We still need an ack timer and stuff. I just haven't done that.
    // NOTE: Make sure we we are actually queueing this message before messing with the ack-expected logic.
    // Also, keep this logic below the queueing logic (even if that means we queue a message that we could have
    // sent immediately) to make sure this code works regardless.
    //
    if (_mxdr_controller_ready && !_waiting_for_ack)
    {
        efj_log_debug("Sending unqueued MXDR message 0x%x", mxdr_get_type(outbound));
        sendQueuedMessage();
    }
    else
    {
        efj_log_debug("Can't send queued message b/c: ready:%d, waiting:%d", _mxdr_controller_ready, _waiting_for_ack);
    }
}

void MxdrTransporter::sendQueuedMessage()
{
    if (_outbound_queue.size())
    {
        const MxdrOutboundBlock& next = _outbound_queue.front();
        MxdrMessage outbound = mxdr_block_to_message(next);

        sendOutboundMessage(outbound);
        _outbound_queue.pop();
    }
    else
    {
        efj_log_debug("Can't send queued message b/c the queue is empty");
    }
}

void MxdrTransporter::sendOutboundMessage(const MxdrMessage& msg)
{
    efj_log_debug("Sending MXDR message %s: %s",
            efj::c_str((MxdrMessageType)mxdr_get_type(msg)), efj::to_hex(msg).c_str());

    _last_sent_type = (MxdrMessageType)mxdr_get_type(msg);
    _waiting_for_ack = true;
    efj_produce(OutboundMxdr, msg);
}

