#pragma once
#include "app_config.hpp"

//! Acts as a facade for the MXDR, DSP, and PRU interfaces. The idea is that
//! dsp processors request access to DSP/MXDR communication, and this object
//! grants/denies access and provides a simplified interface.
//!
//! Responsibilities:
//! * Handle hardware initialization, configuration, and failure recovery.
//! * Handle dsp allocation (making sure a phase 2 call can't start while
//!   anything else is active, etc.)
//! * Route content to processors.
//!
struct HardwareController
{
    EFJ_OBJECT("Hardware Controller");

    // app <- hardware
    EFJ_INPUT(InboundMxdr,  const MxdrMessage);
    EFJ_INPUT(OutboundMxdr, const MxdrMessage);
    EFJ_INPUT(InboundC6k,   const C6kMessage);
    EFJ_INPUT(OutboundC6k,  const C6kMessage);

    // app -> hardware
    EFJ_OUTPUT(InboundMxdr,  const MxdrMessage);
    EFJ_OUTPUT(OutboundMxdr, const MxdrMessage);
    EFJ_OUTPUT(InboundC6k,   const C6kMessage);
    EFJ_OUTPUT(OutboundC6k,  const C6kMessage);

    const AppConfig* _config = nullptr;
    u32 _latestP1TxMod = 0;

    //{ DFSI state.
    SetTxMode  _txMode = {};
    SetTxMode* _latestTxMode = nullptr;

    SetRxMode  _rxMode = {};
    SetRxMode* _latestRxMode = nullptr;
    //}

    // From RX frequency ack.
    MxdrRxModuleId _rxModuleId = MXDR_RX_ID_COUNT_;

    bool _c6kStarted = false;
    bool _inittedDsp = false;
    bool _hardwareReady = false;
    MxdrLedFlags _leds = 0;

    bool init();
    void onConfig(const AppConfig&);

    void onHardwareEvent(const efj::user_event& e);
    void onControlEvent(const efj::user_event& e);

    void consumeInboundC6k(const C6kMessage& msg);
    void consumeOutboundC6k(const C6kMessage& msg);
    void consumeInboundMxdr(const MxdrMessage& msg);
    void consumeOutboundMxdr(const MxdrMessage& msg);

    void sendOutboundMxdr(const MxdrMessage& msg);

    void configureHardware();
    void configureDsp();
    void configureMxdr();
};

