
// TODO: We should probably read the PRU config fields from the tuning config file in the config_manager
// with everything else.

struct PruInterface
{
    EFJ_OBJECT("PRU Interface");

    int _pru_binary_fd = -1;
    int _pru_firmware_fd = -1;
    int _pru_run_fd = -1;
    int _pru_pps_polarity_fd = -1;
    int _pru_pps_high_fd = -1;
    int _pru_pps_low_fd = -1;
    int _pru_status_fd = -1;

    char _pru_binary_path[1024];
    char _pru_firmware_path[1024];
    char _pru_run_path[1024];
    char _pru_polarity_path[1024];
    char _pru_pps_high_path[1024];
    char _pru_pps_low_path[1024];
    char _pru_status_path[1024];

    bool _have_config = false;

    bool init();
    void onConfig(efj::config_event& e);
    void onInitPru(const efj::user_event& e);

    bool resetPru();
};


