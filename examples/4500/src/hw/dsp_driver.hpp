#pragma once

struct DspMemSection
{
    uintptr_t address;
    size_t    size;
    uint8_t*  mappedMemory;
};

bool dsp_reset(int fd);
bool dsp_load_software(int fd, const void* buf, size_t size);
bool dsp_execute(int fd);

