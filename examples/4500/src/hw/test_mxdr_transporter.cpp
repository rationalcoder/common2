
EFJ_TEST(MxdrTransporter, queues_until_init)
{
    auto* mxdrTransporter = efj::test_get_object<MxdrTransporter>();
    //efj::test_enable_log_level(mxdrTransporter, efj::log_level_all);
    efj::test_disconnect_output(mxdrTransporter);

    efj::test_output_queue output;
    efj::test_capture(mxdrTransporter, &output);

    // This is how we would use separate output queues.
    //efj::test_output_queue inboundMxdrOutput;
    //efj::test_capture(mxdrTransporter->InboundMxdr, &inboundMxdrOutput);

    //efj::test_output_queue outboundMxdrOutput;
    //efj::test_capture(mxdrTransporter->OutboundMxdr, &outboundMxdrOutput);

    MxdrMessage setTxFreq    = mxdr_make_set_tx_freq(0);
    MxdrMessage setRxFreq    = mxdr_make_set_rx1_freq(0);
    MxdrMessage setLedStatus = mxdr_make_set_led_status(0);

    MxdrProductInfo info = {};
    MxdrMessage productInfo = mxdr_make_reply_product_info(info);

    efj::test_produce(mxdrTransporter->inputOutboundMxdr, setTxFreq);
    efj::test_produce(mxdrTransporter->inputOutboundMxdr, setRxFreq);
    efj::test_produce(mxdrTransporter->inputOutboundMxdr, setLedStatus);
    TEST_EQ(output.size(), 0u);

    efj::test_send_event(mxdrTransporter, EVENT_MXDR_INITTED, MxdrInitted{true});
    TEST_EQ(output.size(), 0u);

    efj::test_produce(mxdrTransporter->inputInboundMxdr, productInfo);
    TEST_EQ(output.size(), 2u);

    // TODO: Here we expect the initial product info to be sent upstream before
    // the first queued message is sent to the controller. This is potentially an implementation
    // detail. We could just use separate output queues to do a more generic test.
    MxdrMessage* msg = output.front_as<MxdrMessage>();
    TEST_EQ(mxdr_get_valid_type(*msg), MXDR_REPLY_PRODUCT_INFO);
    output.pop();

    msg = output.front_as<MxdrMessage>(0);

    //TEST_EQ(mxdr_get_valid_type(*msg), MXDR_REPLY_PRODUCT_INFO);
    TEST_EQ(mxdr_get_valid_type(*msg), MXDR_SET_TX_FREQUENCY);
    output.pop();
}

