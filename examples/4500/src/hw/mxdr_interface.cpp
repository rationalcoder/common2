#include "mxdr_interface.hpp"

#include <termios.h>

// C2 doesn't have serial port support, yet, so we are doing it with the lower-level fd API for now.
// Reference: Serial Port Programming Guide for POSIX.

bool MxdrInterface::init()
{
    _mxdr_raw_fd = ::open(_path, O_RDWR | O_NOCTTY | O_NDELAY);
    if (_mxdr_raw_fd == -1)
    {
        efj_log_crit("Failed to open MXDR serial port %s", _path);
        return false;
    }

    struct termios options;
    tcgetattr(_mxdr_raw_fd, &options);
    cfmakeraw(&options);
    // We can't have textual flow control when using a binary protocol. IXON should be unset by makeraw, though.
    options.c_iflag &= ~(IXON | IXOFF);
    options.c_iflag |= IGNPAR;
    options.c_cflag |= (CLOCAL | CREAD);

    options.c_cflag &= ~CSTOPB;
    options.c_cflag &= ~CSIZE;
    options.c_cflag |= CS8;

    cfsetispeed(&options, B57600);
    cfsetospeed(&options, B57600);

    tcsetattr(_mxdr_raw_fd, TCSAFLUSH, &options);

    efj::subscribe(EVENT_INIT_MXDR, &MxdrInterface::onEvent);
    return true;
}

void MxdrInterface::onEvent(const efj::user_event& e)
{
    if (e.type == EVENT_INIT_MXDR)
    {
        _ack_count = 0;
        _cur_message_size = 0;
        _parse_offset = 0;
        _state = WaitingForResetAck;

        efj_log_debug("Initting MXDR");
        if (!efj::fd_is_created(_mxdr_fd))
        {
            efj::fd_add_os_handle("MXDR", _mxdr_raw_fd, efj::io_in, &_mxdr_fd, &MxdrInterface::onMxdrEvent);
        }

        auto msg = mxdr_make_reset_controller();
        writeMessage(msg);
    }
}

void MxdrInterface::consumeOutboundMxdr(const MxdrMessage& msg)
{
    if (_state != Active)
    {
        efj_log_warn("dropping MXDR message recieved while the interface was down");
        return;
    }

    writeMessage(msg);
}

void MxdrInterface::writeMessage(const MxdrMessage& msg)
{
    efj_log_debug("-> %s: %s", mxdr_get_type_name(msg), efj::to_hex(msg).c_str());

    size_t result = efj::fd_write(_mxdr_fd, msg.data, msg.size);
    efj_panic_if(result != msg.size);
}

// FIXME: We do all this validation and waiting for the controller to send us a valid Reply Product Info,
// but we really have to add timeouts to these states for them to handle cases where the controller isn't
// responding.
void MxdrInterface::onMxdrEvent(efj::fd_event& e)
{
    // TODO: handle errors.
    if (!(e.events & efj::io_in))
        return;

    efj_log_debug("read: state: %d, size: %zu/%zu", _state,
        _parse_offset, sizeof(_parse_buf) - _parse_offset);
    
    // We can/should always read available data. We can parse it or throw it out depending on our state.
    ssize_t n = efj::fd_read(_mxdr_fd, _parse_buf + _parse_offset, sizeof(_parse_buf) - _parse_offset);
    //efj_log_debug("read result: %zd: %s", n, efj::to_hex(_parse_buf, _parse_offset).c_str());

    if (n <= 0)
    {
        // If we get this, it most likely means application is communicating with the controller, since
        // we don't set our fd to non-blocking. I'm printing out the fd flags just to be sure we aren't.
        if (errno == EAGAIN)
        {
            int val = fcntl(_mxdr_raw_fd, F_GETFL, 0);
            efj_log_crit("MXDR controller is unavailable, likely due to application interference (flags: %d)", val);
        }
        else
        {
            efj_log_crit("Failed to read from MXDR controller: %s", strerror(errno));
        }

        // If we get into a state where our buffer is full and we asked for a zero-sized read, that's a bug.
        efj_panic_if(_parse_offset == sizeof(_parse_buf));
        efj_panic("Failed to read from MXDR controller");
        return;
    }

    _parse_offset += n;
    //efj_log_debug("Read %zu/%zu bytes", _parse_offset, _cur_message_size);

    for (;;)
    {
        if (_state == Active)
        {
            break;
        }
        else if (_state == WaitingForResetEvent)
        {
            // We could be in this state if we had CRC errors and are resetting while the controller
            // is still sending us data. We want to throw the data out.
            _parse_offset = 0;
            return;
        }
        else if (_state == WaitingForResetAck)
        {
            if (!waitForResetAck())
                return;
        }
        else if (_state == WaitingForProductInfo)
        {
            // Return if we are supposed to. Otherwise, there may still be content to parse, so continue.
            if (!waitForProductInfo())
                return;

            break;
        }
    }
    efj_panic_if(_state != Active);

    // We have to loop in case we read more than one message.
    for (;;)
    {
        if (_parse_offset < MXDR_HEADER_SIZE)
            return;

        MxdrMessage pending_msg = { _parse_buf, _parse_offset };

        _cur_message_size = mxdr_get_full_size(pending_msg);

        efj_assert(_cur_message_size <= sizeof(_parse_buf));
        if (_parse_offset < _cur_message_size)
            return;

        MxdrMessage msg = { _parse_buf, _cur_message_size };
        //efj_log_debug("Checking CRC of %s (size: %zu)\n", mxdr_get_type_name(msg), _cur_message_size);

        u16 stored_crc = mxdr_get_crc(msg);
        u16 computed_crc = mxdr_calc_crc(msg);
        if (stored_crc != computed_crc)
        {
            // The init/re-init process is controlled externally.
            efj_log_crit("MXDR CRC error: expected 0x%x, got 0x%x. Resetting", computed_crc, stored_crc);
            efj_log_crit("msg: %s\n", efj::to_hex(msg).c_str());
            requireReset();
            return;
        }

        //efj_log_debug("HDR: %0x, SOH: %0x,FN: %0x", mxdr_get_soh_fn(msg), mxdr_get_soh(msg), mxdr_get_fn(msg));
        if (!mxdr_is_ack(msg))
        {
            u8 type = mxdr_get_type(msg);
            efj_log_debug("Got MXDR message: %x", type);
        }
        else
        {
            efj_log_debug("Got MXDR ack of size %zu", _cur_message_size);
        }

        efj_log_debug("<- %s: %s", mxdr_get_type_name(msg), efj::to_hex(msg).c_str());

        efj_produce(InboundMxdr, msg);
        consumeBytes(_cur_message_size);
    }
}

void MxdrInterface::requireReset()
{
    _parse_offset = 0;
    _state = WaitingForResetEvent;
    efj_trigger(EVENT_MXDR_DOWN, MxdrDown{});
}

bool MxdrInterface::waitForResetAck()
{
    if (_parse_offset < MXDR_FRAMING_SIZE)
        return false;

    // If we don't see 0xC1009051, it's not an empty ack.
    if (_parse_buf[0] != 0xC1 || _parse_buf[1] != 0x00 || _parse_buf[2] != 0x90 || _parse_buf[3] != 0x51)
    {
        efj_log_info("Invalid ack for Reset Controller (buf: %s). Resetting again",
            efj::to_hex(_parse_buf, _parse_offset).c_str());
        requireReset();
        return false;
    }

    efj_log_debug("<- ACK: C1009051");
    efj_log_debug("Got ack for Reset Controller");
    consumeBytes(MXDR_FRAMING_SIZE);
    _state = WaitingForProductInfo;
    return true;
}

bool MxdrInterface::waitForProductInfo()
{
    // The controller responds with a Reply Product Info after a reset and an initial ack for the Reset,
    // and we have to wait for it b/c the MXDR controller won't respond to any messages besides Reset
    // before it has sent that message (as far as I can tell).

    // We want to throw away content until we see something that could be a valid Reply Product Info.
    // We have to do this because we don't know the state of the controller when we sent the reset message.
    // Another application (or this one) could have been talking to it and crashed, or we could have detected
    // a CRC error and triggered a reset ourselves, etc.

    if (_parse_offset < MXDR_FRAMING_SIZE)
        return false;

    size_t msg_offset = 0;
    for (size_t i = 0; i < _parse_offset; i++)
    {
        if (_parse_buf[i] == 0xC1)
        {
            msg_offset = i;
            break;
        }
    }

    if (msg_offset == 0 && _parse_offset == sizeof(_parse_buf))
    {
        efj_log_info("Received too much unexpected data after reset. Resetting again");
        requireReset();
        return false;
    }

    consumeBytes(msg_offset);

    // If we don't see 0xC15109... it's not the right message.
    if (_parse_buf[0] != 0xC1 || _parse_buf[1] != 0x51 || _parse_buf[2] != 0x09)
    {
        efj_log_info("Invalid Reply Product Info after reset (buf: %s). Resetting again",
            efj::to_hex(_parse_buf, _parse_offset).c_str());
        requireReset();
        return false;
    }

    static_assert(sizeof(MxdrBlockReplyProductInfo) == 85, "size of message changed");
    if (_parse_offset < sizeof(MxdrBlockReplyProductInfo))
        return false;

    //efj_log_debug("Parsed Reply Product Info. Checking CRC");

    MxdrMessage msg = { _parse_buf, sizeof(MxdrBlockReplyProductInfo) };

    u16 stored_crc = mxdr_get_crc(msg);
    u16 computed_crc = mxdr_calc_crc(msg);
    if (stored_crc != computed_crc)
    {
        efj_log_info("Got Reply Product Info with bad CRC. Resetting again");
        requireReset();
        return false;
    }

    efj_log_debug("<- REPLY_PRODUCT_INFO: %s", efj::to_hex(msg).c_str());

    // IMPORTANT: We can't produce before changing states. Otherwise, we risk indirectly calling back
    // into this object to send data to the controller, and we will drop the data in that case because
    // we wouldn't be in the right state.
    _state = Active;
    efj_trigger(EVENT_MXDR_INITTED, MxdrInitted{true});
    efj_produce(InboundMxdr, msg);
    consumeBytes(sizeof(MxdrBlockReplyProductInfo));
    efj_log_info("MXDR reset complete");

    return true;
}

// Moves bytes after and including buf[n] to the start of the buffer, overwriting the first n bytes.
void MxdrInterface::consumeBytes(size_t n)
{
    efj_panic_if(_parse_offset < n);

    size_t unparsed_size = _parse_offset - n;
    memmove(_parse_buf, _parse_buf + n, unparsed_size);
    _parse_offset -= n;
}

