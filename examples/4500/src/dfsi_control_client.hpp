#pragma once

enum DfsiControlClientState
{
    DFSI_CONTROL_NOT_CONNECTED,
    DFSI_CONTROL_CONNECTED,
};

struct DfsiAckEntry
{
    u64  time_sent_msec = 0;
    u8   timeout_count  = 0;
    bool pending        = false;
    u32  msg_size       = 0;
    // @Temporary. TODO: efj::message_queue
    char msg_data[128];
};

// [FSHost] <-> this
struct DfsiControlClient
{
    EFJ_OBJECT("DFSI Control Client");
    EFJ_INPUT(OutboundControl, const FsiMessage);
    EFJ_OUTPUT(OutboundControl, const FsiMessage);
    EFJ_OUTPUT(InboundControl, const FsiMessage);

    DfsiControlClientState _state = DFSI_CONTROL_NOT_CONNECTED;
    u32 _connected_ipv4 = 0;
    u16 _connected_port = 0;

    u8 _voter_report_period_sec = 10;
    u8 _tx_report_period = 10;

    // TODO: use main config.

    //{ Connectivity (heartbeat and acks). Connectivity* => heartbeat stuff.
    DfsiConnectEvent _connect_event = {};
    u8               _connectivity_loss_limit = 5;
    u8               _connectivity_counter = 0;
    efj::timer       _connectivity_timer;

    // Our generated cor tags and ack information. If we ever wrap cor tags, even if the
    // timeout hasn't been reached, we disconnect.
    u16              _control_retry_timeout_msec = 100;
    u32              _control_attempt_limit = 3;
    efj::timer       _control_retry_timer;

    DfsiAckEntry     _ack_table[256];
    u8               _next_cor_tag_to_use = 0;
    //}


    // XXX
    u64 _last_hb_msec = 0;

    // RX/TX operating mode settings.
    u8  _rx_operating_number = 0;
    u32 _rx_operating_freq   = 0;
    u32 _tx_operating_number = 0;
    u32 _tx_operating_freq   = 0;
    u8  _tx_operating_mode   = 0;
    u16 _tx_operating_power  = 0;

    bool init();

    void consumeInboundControl(const FsiMessage& msg);
    void consumeOutboundControl(const FsiMessage& msg);

    void consumeNotConnected(const FsiMessage& msg);
    void consumeConnected(const FsiMessage& msg);
    void onOutboundEfj(const FsiEfjMessage& msg, u8 id, u8 version);

    //{
    //! Common path to track ack state for inbound messages that need to be ackd/retried.
    void produceAckdInboundMessage(FsiMessage& msg);

    //! Manage connectivity maintainence like acks and heartbeats.
    //! If we detect a disconnect, we just move to the disconnected state without letting
    //! the host know (it will figure out due to unresponsiveness). That's the behavior defined in the spec.
    //!
    void onHeartbeatTimeout(efj::timer_event& e);
    void onAckTableTimeout(efj::timer_event& e);
    //}

    void disconnect();
};

