#include "rtp.hpp"
#include "dfsi.hpp"
#include "c6k.hpp"
#include "mxdr.hpp"
#include "cai.hpp"

inline C6kRxPolarity
mxdr_to_c6k_polarity(MxdrRxModuleId id)
{
    switch (id)
    {
    case MXDR_RX_ID_LOW_SIDE_55_MHZ: return C6K_RX_POLARITY_NOT_INVERTED;
    case MXDR_RX_ID_LOW_SIDE_90_MHZ: return C6K_RX_POLARITY_NOT_INVERTED;
    case MXDR_RX_ID_HIGH_SIDE_55_MHZ: return C6K_RX_POLARITY_INVERTED;
    case MXDR_RX_ID_HIGH_SIDE_90_MHZ: return C6K_RX_POLARITY_INVERTED;
    default:
        efj_invalid_code_path();
        return C6K_RX_POLARITY_INVERTED;
    }
}

inline C6kInterFreq
mxdr_to_c6k_inter_freq(MxdrRxModuleId id)
{
    switch (id)
    {
    case MXDR_RX_ID_LOW_SIDE_55_MHZ: return C6K_IF_55_MHZ;
    case MXDR_RX_ID_HIGH_SIDE_55_MHZ: return C6K_IF_55_MHZ;
    case MXDR_RX_ID_LOW_SIDE_90_MHZ: return C6K_IF_90_MHZ;
    case MXDR_RX_ID_HIGH_SIDE_90_MHZ: return C6K_IF_90_MHZ;
    default:
        efj_invalid_code_path();
        return C6K_IF_COUNT_;
    }
}

EFJ_MACRO void
dfsi_to_c6k_hdu1(C6kBlockTransmitHeader1* c6kBlock, const DfsiBlockHeaderPart1& dfsiBlock)
{
    c6k_make_transmit_hdu1(c6kBlock, dfsiBlock.getHeader());
}

EFJ_MACRO C6kMessage
dfsi_to_c6k_hdu1(const DfsiBlockHeaderPart1& dfsiBlock)
{
    return c6k_make_transmit_hdu1(dfsiBlock.getHeader());
}

EFJ_MACRO void
dfsi_to_c6k_hdu2(C6kBlockTransmitHeader2* c6kBlock, const DfsiBlockHeaderPart2& dfsiBlock)
{
    c6k_make_transmit_hdu2(c6kBlock, dfsiBlock.getHeader());
}

EFJ_MACRO C6kMessage
dfsi_to_c6k_hdu2(const DfsiBlockHeaderPart2& block)
{
    return c6k_make_transmit_hdu2(block.getHeader());
}

EFJ_MACRO void
dfsi_to_c6k_voice(C6kBlockTransmitLduVoice* c6kBlock, const DfsiBlockVoice& dfsiBlock)
{
    c6k_make_transmit_voice(c6kBlock, dfsiBlock.getVoice());
}

EFJ_MACRO C6kMessage
dfsi_to_c6k_voice(const DfsiBlockVoice& block)
{
    return c6k_make_transmit_voice(block.getVoice());
}

EFJ_MACRO void
dfsi_to_c6k_voice_lc_es(C6kBlockTransmitLduVoiceLcEs* c6kBlock, const DfsiBlockVoiceLcEs& dfsiBlock)
{
    c6k_make_transmit_voice_lc_es(c6kBlock, dfsiBlock.getVoice(), dfsiBlock.getLcEs());
}

EFJ_MACRO C6kMessage
dfsi_to_c6k_voice_lc_es(const DfsiBlockVoiceLcEs& block)
{
    return c6k_make_transmit_voice_lc_es(block.getVoice(), block.getLcEs());
}

EFJ_MACRO void
dfsi_to_c6k_voice_lsd(C6kBlockTransmitLduVoiceLsd* c6kBlock, const DfsiBlockVoiceLsd& dfsiBlock)
{
    c6k_make_transmit_voice_lsd(c6kBlock, dfsiBlock.getVoice(), dfsiBlock.getLsd());
}

EFJ_MACRO C6kMessage
dfsi_to_c6k_voice_lsd(const DfsiBlockVoiceLsd& block)
{
    return c6k_make_transmit_voice_lsd(block.getVoice(), block.getLsd());
}

EFJ_MACRO void
dfsi_to_c6k_ltdu(C6kBlockTransmitTermWithLc* c6kBlock, const DfsiBlockTdu& dfsiBlock)
{
    efj_panic_if(dfsiBlock.getType() != DFSI_TDU_LONG);
    c6k_make_transmit_term_with_lc(c6kBlock, dfsiBlock.getFormat(), dfsiBlock.getLc());
}

EFJ_MACRO C6kMessage
dfsi_to_c6k_ltdu(const DfsiBlockTdu& block)
{
    efj_panic_if(block.getType() != DFSI_TDU_LONG);
    return c6k_make_transmit_term_with_lc(block.getFormat(), block.getLc());
}

template <typename DfsiHeaderBlockT, typename C6kHeaderBlockT> inline RtpP25Message
c6k_to_dfsi_hdu(const C6kMessage& msg)
{
    // Leave sequence number, timestamp, and SSRC zero.
    RtpP25Message result = dfsi_make_message<DfsiHeaderBlockT>(0, 0, 0);

    auto* c6kBlock  = c6k_get_block<C6kHeaderBlockT>(msg);
    auto* dfsiBlock = p25_get_only_block<DfsiHeaderBlockT>(result);

    const uint8_t* c6kHeader      = c6kBlock->getHeader();
    const uint8_t* c6kGolayErrors = c6kBlock->getGolayErrors();

    uint8_t* dfsiHeader = dfsiBlock->getHeader();
    // Represents 3 bytes that don't really make up an integer. We just use an integer to do bitwise
    // operations on it. We have to write these bytes out in MSBit (big endian) when we're done.
    uint32_t hostDfsiStatus = 0;

    dfsiBlock->setFrameType();

    for (size_t i = 0; i < sizeof(typename C6kHeaderBlockT::Buffer); i++)
    {
        uint8_t golayErrorByte = c6kGolayErrors[i];
        //! Cap the golay errors to 3 bits.
        golayErrorByte &= 0x7;

        //! Each byte of the DFSI header consists of the least significant 6 bits of the corresponding
        //! C6K header byte combined with bits two and one (0x6) of the corresponding GOLERR byte.
        //! The RTP status byte consists of the or-ing of bit zero of all of the GOLERR bytes.
        dfsiHeader[i] = ((c6kHeader[i] & 0x3f) | ((golayErrorByte & 0x6) << 5));
        hostDfsiStatus |= (golayErrorByte & 0x1) << i;
    }

    // This is host-endian agnostic even though we can assume little endian, but whatever.
    // It should be optimizable by the compiler. Just to be clear, we aren't swapping to big endian
    // becaues of network byte order for integers, since this field is really just three bytes.
    uint8_t* status = dfsiBlock->getStatus();
    status[0] = hostDfsiStatus >> 16;
    status[1] = (hostDfsiStatus >> 8) & 0xff;
    status[2] = hostDfsiStatus & 0xff;

    return result;
}

EFJ_MACRO RtpP25Message
c6k_to_dfsi_hdu1(const C6kMessage& msg)
{
    return c6k_to_dfsi_hdu<DfsiBlockHeaderPart1, C6kBlockReceiveHeader1>(msg);
}

EFJ_MACRO RtpP25Message
c6k_to_dfsi_hdu2(const C6kMessage& msg)
{
    return c6k_to_dfsi_hdu<DfsiBlockHeaderPart2, C6kBlockReceiveHeader2>(msg);
}

inline RtpP25Message
c6k_to_dfsi_voice(const C6kMessage& msg, uint32_t ft, uint32_t sf, C6kTxStatusSymbol status)
{
    // Leave sequence number, timestamp, and SSRC zero.
    RtpP25Message result = dfsi_make_message<DfsiBlockVoice>(0, 0, 0);

    auto* c6kBlock  = c6k_get_block<C6kBlockReceiveLduVoice>(msg);
    auto* dfsiBlock = p25_get_only_block<DfsiBlockVoice>(result);

    dfsiBlock->setFrameType((uint8_t)ft);
    dfsiBlock->setVoice(c6kBlock->getVoice());
    dfsiBlock->setVoiceStatus(c6kBlock->getEt(), c6kBlock->getEr(), c6kBlock->getM(),
        c6kBlock->getL(), c6kBlock->getE4(), c6kBlock->getE1(), sf, status);

    return result;
}

inline RtpP25Message
c6k_to_dfsi_voice_lc_es(const C6kMessage& msg, uint32_t ft, uint32_t sf, C6kTxStatusSymbol status)
{
    // Leave sequence number, timestamp, and SSRC zero.
    RtpP25Message result = dfsi_make_message<DfsiBlockVoiceLcEs>(0, 0, 0);
    auto* c6kBlock  = c6k_get_block<C6kBlockReceiveLduVoiceLcEs>(msg);
    auto* dfsiBlock = p25_get_only_block<DfsiBlockVoiceLcEs>(result);

    dfsiBlock->setFrameType((uint8_t)ft);
    dfsiBlock->setVoice(c6kBlock->getVoice());
    dfsiBlock->setVoiceStatus(c6kBlock->getEt(), c6kBlock->getEr(), c6kBlock->getM(),
        c6kBlock->getL(), c6kBlock->getE4(), c6kBlock->getE1(), sf, status);

    const uint8_t* c6kLcEs  = c6kBlock->getLcEs();
    uint8_t*       dfsiLcEs = dfsiBlock->getLcEs();
    dfsiLcEs[0] = c6kLcEs[0];
    dfsiLcEs[1] = c6kLcEs[1];
    dfsiLcEs[2] = c6kLcEs[2];

    //! The DFSI status field is calculated from the Haming error bytes where
    //! status = (ec3 * 3**3) + (ec2 * 3**2) + (ec1 * 3) + ec0
    //! Where ec = 0, 1 or 2 (for all values 2 or greater in the ham error byte)

    // TODO: This code looks different from the one in the current ATLAS 4500, but I'm pretty
    // sure this is correct. Also, we probably don't have to worry about capping the errors
    // to 2, since that would be an error in the DSP, but we do anyway. @NeedsTesting
    const uint8_t* hamingErrors = c6kBlock->getHamingErrors();
    uint8_t lcEsErrorStatus =
         27 * (hamingErrors[3] < 2 ? hamingErrors[3] : 2)
        + 9 * (hamingErrors[2] < 2 ? hamingErrors[2] : 2)
        + 3 * (hamingErrors[1] < 2 ? hamingErrors[1] : 2)
        +     (hamingErrors[0] < 2 ? hamingErrors[0] : 2);

    dfsiBlock->setErrorStatus(lcEsErrorStatus);

    return result;
}

inline RtpP25Message
c6k_to_dfsi_voice_lsd(const C6kMessage& msg, uint32_t ft, uint32_t sf, C6kTxStatusSymbol status)
{
    // Leave sequence number, timestamp, and SSRC zero.
    RtpP25Message result = dfsi_make_message<DfsiBlockVoiceLsd>(0, 0, 0);

    auto* c6kBlock  = c6k_get_block<C6kBlockReceiveLduVoiceLsd>(msg);
    auto* dfsiBlock = p25_get_only_block<DfsiBlockVoiceLsd>(result);

    dfsiBlock->setFrameType((uint8_t)ft);
    dfsiBlock->setVoice(c6kBlock->getVoice());
    dfsiBlock->setVoiceStatus(c6kBlock->getEt(), c6kBlock->getEr(), c6kBlock->getM(),
        c6kBlock->getL(), c6kBlock->getE4(), c6kBlock->getE1(), sf, status);

    const uint8_t* c6kLsd  = c6kBlock->getLsd();
    uint8_t*       dfsiLsd = dfsiBlock->getLsd();
    dfsiLsd[0] = c6kLsd[0];
    dfsiLsd[1] = c6kLsd[1];

    // The setter clamps the values for us.
    const uint8_t* c6kLsdErrors  = c6kBlock->getLsdErrors();
    dfsiBlock->setErrorStatus(c6kLsdErrors[0], c6kLsdErrors[1]);

    return result;
}

