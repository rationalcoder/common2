#pragma once

struct FsiMessage : efj::message { using message::message; };

namespace efj
{
EFJ_DEFINE_MESSAGE(FsiMessage, "1.0");
} // namespace efj

enum FsiMessageType
{
    FSC_CONNECT,
    FSC_HEARTBEAT,
    FSC_ACK,
    FSC_SBC,
    FSC_MAN_EXT,
    FSC_SEL_CHAN,
    FSC_SEL_RPT,
    FSC_SEL_SQUELCH,
    FSC_REPORT_SEL,
    FSC_DISCONNECT,
    FSC_SCAN,
    FSC_RCV_CNTRL,
    FSC_TX_CNTRL,
    FSC_MBC,
    FSC_START_DATA,
    FSC_STOP_DATA,
    FSC_CAI_BUSY_BITS,
};

template <FsiMessageType TypeV, size_t SizeV>
struct FsiBlock
{
    static const FsiMessageType Type = TypeV;

    u8 data[SizeV];
};

//{ All messages
EFJ_MACRO u8   fsi_get_id(const FsiMessage& msg)            { return msg.data[0]; }
EFJ_MACRO void fsi_set_id(FsiMessage& msg, u8 id)           { msg.data[0] = id; }
EFJ_MACRO u8   fsi_get_version(const FsiMessage& msg)       { return msg.data[1]; }
EFJ_MACRO void fsi_set_version(FsiMessage& msg, u8 version) { msg.data[1] = version; }
//}

//! All messages except acks and heartbeats have correlation tags. Returns 0 in error cases.
inline u8
fsi_get_cor_tag(const FsiMessage& msg)
{
    efj_panic_if(msg.size < 3 || msg.data[0] == FSC_HEARTBEAT || msg.data[0] == FSC_ACK);
    return msg.data[2];
}

inline void
fsi_set_cor_tag(FsiMessage& msg, u8 corTag)
{
    u8 id = fsi_get_id(msg);
    efj_panic_if(id == FSC_HEARTBEAT || id == FSC_ACK);

    msg.data[2] = corTag;
}

enum FsiAckCode
{
    FSI_CONTROL_ACK,
    FSI_CONTROL_NAK,
    FSI_CONTROL_NAK_CONNECTED,
    FSI_CONTROL_NAK_M_UNSUPP,
    FSI_CONTROL_NAK_V_UNSUPP,
    FSI_CONTROL_NAK_F_UNSUPP,
    FSI_CONTROL_NAK_PARAMS,
    FSI_CONTROL_NAK_BUSY,
    FSI_CONTROL_NAK_P_UNSUPP,
    FSI_CONTROL_NAK_OPT_UNSUPP,
};

// Size of the data sent with acks by message being ack'd.
// TODO: FSI v2+ sizes.
enum FsiAckRespSize
{
    FSC_CONNECT_RESP_SIZE                = 3,
    FSC_CONNECT_NAK_CONNECTED_RESP_SIZE  = 6,
    FSC_CONNECT_NAK_OPT_UNSUPP_RESP_SIZE = 1,
};

struct FsiAck : FsiMessage
{
    EFJ_MACRO u8 getAckdId() const { return data[2]; }
    EFJ_MACRO void setAckdId(u8 id) { data[2] = id; }
    EFJ_MACRO u8 getAckdVersion() const { return data[3]; }
    EFJ_MACRO void setAckdVersion(u8 version) { data[3] = version; }
    EFJ_MACRO u8 getAckdCorTag() const { return data[4]; }
    EFJ_MACRO void setAckdCorTag(u8 cor_id) { data[4] = cor_id; }
    EFJ_MACRO u8 getResponseCode() const { return data[5]; }
    EFJ_MACRO void setResponseCode(int code) { data[5] = (u8)code; }
    EFJ_MACRO u8 getResponseLength() const { return data[6]; }
    EFJ_MACRO void setResponseLength(u8 len) { data[6] = len; }
    EFJ_MACRO u8 getResponseVersion() const { return data[7]; }
    EFJ_MACRO void setResponseVersion(u8 version) { data[7] = version; }
};

// Internal type used for safer casting.
template <FsiMessageType TypeV, size_t SizeV>
struct FsiAckWithData : FsiAck
{
    enum FsiMessageType { ResponseType = TypeV };
    enum size_t { ResponseSize = SizeV };
};

inline FsiAck
fsi_make_ack(u8 ackdId, u8 ackdVersion, u8 corTag, FsiAckCode code)
{
    FsiAck result = efj_allocate_message(FsiAck, 7);
    fsi_set_id(result, FSC_ACK);
    fsi_set_version(result, 1);

    result.setAckdId(ackdId);
    result.setAckdVersion(ackdVersion);
    result.setAckdCorTag(corTag);
    result.setResponseCode(code);
    result.setResponseLength(0);

    return result;
}

template <typename AckT> EFJ_MACRO AckT
fsi_make_ack(u8 ackdId, u8 ackdVersion, u8 corTag, FsiAckCode code)
{
    FsiAck result = efj_allocate_message(FsiAck, 7 + AckT::ResponseSize);
    memset(result.data, 0, result.size);

    fsi_set_id(result, FSC_ACK);
    fsi_set_version(result, 1);

    result.setAckdId(ackdId);
    result.setAckdVersion(ackdVersion);
    result.setAckdCorTag(corTag);
    result.setResponseCode(code);
    result.setResponseLength(AckT::ResponseSize);

    return efj::message_cast<AckT>(result);
}

struct FsiConnect : FsiMessage
{
    EFJ_MACRO u8  getCorTag() const { return data[2]; }
    EFJ_MACRO u16 getVcBasePort() const { return efj::get_ntoh_u16(*this, 3); }
    EFJ_MACRO u32 getVcSsrc() const { return efj::get_ntoh_u32(*this, 5); }
    EFJ_MACRO u8  getFsHbPeriod() const { return data[9]; }
    EFJ_MACRO u8  getHostHbPeriod() const { return data[10]; }
};

struct FsiAckConnect : FsiAckWithData<FSC_CONNECT, 3>
{
    EFJ_MACRO void setVersion(u8 version) { efj::set_byte(*this, 7, version); }
    EFJ_MACRO u16  getVcBasePort() const { return efj::get_ntoh_u16(*this, 8); }
    EFJ_MACRO void setVcBasePort(u16 port) { efj::set_hton_u16(*this, 8, port); }
};

struct FsiHeartbeat : FsiMessage {};

inline FsiHeartbeat
fsi_make_heartbeat()
{
    FsiHeartbeat result = efj_allocate_message(FsiHeartbeat, 2);
    fsi_set_id(result, FSC_HEARTBEAT);
    fsi_set_version(result, 1);
    return result;
}

struct FsiSbc : FsiMessage {};

struct FsiManExt : FsiMessage {};

enum
{
    FSI_MAN_DATA_OFFSET = 4,
};

EFJ_MACRO u8        fsi_get_mf_id(const FsiManExt& msg)    { return msg.data[3]; }
EFJ_MACRO void      fsi_set_mf_id(FsiManExt& msg, u8 mfid) { msg.data[3] = mfid; }
EFJ_MACRO const u8* fsi_get_mf_data(const FsiManExt& msg)  { efj_panic_if(msg.size < 5); return &msg.data[4]; }
EFJ_MACRO u8*       fsi_get_mf_data(FsiManExt& msg)        { efj_panic_if(msg.size < 5); return &msg.data[4]; }

inline void
fsi_set_man_ext_fields(FsiManExt& msg, u8 corTag, u8 mfid)
{
    fsi_set_id(msg, FSC_MAN_EXT);
    fsi_set_version(msg, 1);
    fsi_set_cor_tag(msg, corTag);
    fsi_set_mf_id(msg, mfid);
}


struct FsiSelChan : FsiMessage {};
struct FsiSelRpt : FsiMessage {};
struct FsiSelSquelch : FsiMessage {};
struct FsiReportSel : FsiMessage {};
struct FsiDisconnect : FsiMessage {};
struct FsiScan : FsiMessage {};
struct FsiRcvCntrl : FsiMessage {};
struct FsiTxCntrl : FsiMessage {};
struct FsiMbc : FsiMessage {};


