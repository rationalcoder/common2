#pragma once
#include "app_config.hpp"

struct DfsiInterfaceConfig
{
    u16 control_port = 0;
    u16 voice_port = 0;
    u32 heartbeat_interval_s = 4;
    u32 control_retry_interval_ms = 100;
};

// TODO: DfsiControlInterface/DfsiVoiceInterface, once interface for each of the sockets.
struct DfsiInterface
{
    EFJ_OBJECT("DFSI Interface");

    EFJ_OUTPUT(OutboundControl, const FsiMessage);
    EFJ_OUTPUT(OutboundVoice,   const RtpP25Message);

    EFJ_INPUT(InboundControl, const FsiMessage);
    EFJ_INPUT(InboundVoice,   const RtpP25Message);

    const AppConfig* config = nullptr;

    DfsiConnectEvent lastConnectEvent = {};
    bool haveLogicalConnection = false;

    efj::net_sock_addr controlAddr;
    efj::net_sock_addr voiceAddr;

    efj::udp_socket controlSocket;
    efj::udp_socket voiceSocket;

    bool interfacesCreated = false;

    bool init();
    void onConfig(const AppConfig&);

    void consumeInboundControl(const FsiMessage& msg);
    void consumeInboundVoice(const RtpP25Message& msg);

    void onConnectionEvent(const efj::user_event& e);
    void onControlSocketEvent(efj::udp_socket_event& e);
    void onVoiceSocketEvent(efj::udp_socket_event& e);
    void createSockets();
};

