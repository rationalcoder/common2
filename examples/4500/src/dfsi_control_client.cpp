#include "dfsi_control_client.hpp"

bool DfsiControlClient::init()
{
    efj::create_periodic_timer("Connectivity", 4_s, &_connectivity_timer);
    efj::timer_monitor(_connectivity_timer, &DfsiControlClient::onHeartbeatTimeout);

    efj::create_oneshot_timer("Control Retry", &_control_retry_timer);
    efj::timer_monitor(_control_retry_timer, &DfsiControlClient::onAckTableTimeout);

    return true;
}

void DfsiControlClient::consumeOutboundControl(const FsiMessage& msg)
{
    switch (_state)
    {
    case DFSI_CONTROL_NOT_CONNECTED:
        consumeNotConnected(msg);
        break;
    case DFSI_CONTROL_CONNECTED:
        consumeConnected(msg);
        break;
    default:
        efj_invalid_code_path();
    }
}

void DfsiControlClient::consumeNotConnected(const FsiMessage& msg)
{
    u8 id      = fsi_get_id(msg);
    u8 version = fsi_get_version(msg);

    switch (id)
    {
    case FSC_CONNECT:
    {
        u8* ip_bytes = (u8*)&msg.ipv4;

        efj_log_info("DFSI connection attempt from %u.%u.%u.%u",
            ip_bytes[0], ip_bytes[1], ip_bytes[2], ip_bytes[3]);

        FsiConnect conn = efj::message_cast<FsiConnect>(msg); // TODO: content_cast
        u8 cor_tag = fsi_get_cor_tag(conn);

        if (version != 1)
        {
            efj_log_info("Rejecting DFSI connection from %u.%u.%u.%u:%u b/c it requested FSI version %u",
                ip_bytes[0], ip_bytes[1], ip_bytes[2], ip_bytes[3], msg.port, version);

            FsiAck ack = fsi_make_ack(id, version, cor_tag, FSI_CONTROL_NAK_V_UNSUPP);
            efj_produce(InboundControl, ack);
            break;
        }

        auto ack = fsi_make_ack<FsiAckConnect>(id, version, cor_tag, FSI_CONTROL_ACK);
        ack.setVersion(1);
        ack.setVcBasePort(8000);

        DfsiConnectEvent e = {};
        e.host_ip               = msg.ipv4;
        e.host_port             = msg.port;
        e.host_vc_base_port     = conn.getVcBasePort();
        e.host_hb_period        = conn.getHostHbPeriod();
        e.fs_vc_base_port       = 8000;
        e.fs_vc_ssrc            = conn.getVcSsrc();
        e.fs_hb_period          = conn.getFsHbPeriod();
        // TIA-102.BAHA-A 8.3.2.4.4 says this is set to the *host* hb period for protocol version 1.
        e.connectivity_tx_timer = e.host_hb_period;
        // *but* we allow this to come from config, so we just make the heartbeat interval up.
        // At the time of writing, the default we use is 4 seconds. :FSICompliance

        _connect_event = e;

        efj_trigger(EVENT_DFSI_CONNECT, e);
        efj_produce(InboundControl, ack);

        //efj_log_debug("Setting TX timer to %u sec", e.connectivity_tx_timer);
        _last_hb_msec = efj::now_msec().value;
        efj::timer_start(_connectivity_timer, efj::sec{e.connectivity_tx_timer});
        _state = DFSI_CONTROL_CONNECTED;

        _connected_ipv4 = msg.ipv4;
        _connected_port = msg.port;
        break;
    }
    case FSC_DISCONNECT:
    {
        efj_log_info("DFSI disconnect");
        FsiAck ack = fsi_make_ack(id, version, fsi_get_cor_tag(msg), FSI_CONTROL_ACK);
        efj_produce(InboundControl, ack);
        break;
    }
    default:
    {
        // TIA-102.BAHA-A 8.3.2.2 Says silently discard everything else.
        efj_log_warn("Unhandled fsi msg: %u", id);
        break;
    }
    }
}

void DfsiControlClient::consumeConnected(const FsiMessage& msg)
{
    u8 id      = fsi_get_id(msg);
    u8 version = fsi_get_version(msg);

    //efj_log_debug("msg while connected: %u, version %u", id, version);

    // Ignore messages from hosts that aren't the one we are currently connected to except if the message
    // is a connect message, in which case we want to tell them that we are connected to another host.
    if ((msg.ipv4 != _connect_event.host_ip || msg.port != _connect_event.host_port) && id != FSC_CONNECT)
        return;

    switch (id)
    {
    case FSC_CONNECT:
    {
        if (msg.ipv4 == _connected_ipv4 && msg.port == _connected_port)
        {
            efj_log_info("Acking CONNECT to connected host");

            auto ack = fsi_make_ack<FsiAckConnect>(id, version, fsi_get_cor_tag(msg), FSI_CONTROL_ACK);
            ack.setVcBasePort(8000);

            efj_produce(InboundControl, ack);
        }
        else
        {
            efj_log_info("Nacking CONNECT to non-connected host");
            FsiAck ack = fsi_make_ack(id, version, fsi_get_cor_tag(msg), FSI_CONTROL_NAK_CONNECTED);
            ack.ipv4 = msg.ipv4;
            ack.port = msg.port;

            efj_produce(InboundControl, ack);
        }

        break;
    }
    case FSC_DISCONNECT:
    {
        u8* ip_bytes = (u8*)&msg.ipv4;

        efj_log_info("Disconnecting DFSI connection from %u.%u.%u.%u:%u by request",
            ip_bytes[0], ip_bytes[1], ip_bytes[2], ip_bytes[3], msg.port);

        FsiAck ack = fsi_make_ack(id, version, fsi_get_cor_tag(msg), FSI_CONTROL_ACK);
        efj_produce(InboundControl, ack);

        disconnect();
        break;
    }
    case FSC_HEARTBEAT:
    {
        _connectivity_counter = 0;
        _last_hb_msec = efj::now_msec().value;
        efj::timer_start(_connectivity_timer);

        FsiHeartbeat hb = fsi_make_heartbeat();
        efj_produce(InboundControl, hb);
        break;
    }
    case FSC_ACK:
    {
        FsiAck ack = efj::message_cast<FsiAck>(msg);

        // TODO: Check ack'd id to make sure it matches the one with this cor tag.
        u8 id = ack.getAckdCorTag();
        DfsiAckEntry* entry = &_ack_table[id];

        //efj_log_debug("ACK for %u", id);
        entry->pending = false;
        entry->timeout_count = 0;
        break;
    }
    case FSC_SBC:
    {
        FsiAck ack = fsi_make_ack(id, version, fsi_get_cor_tag(msg), FSI_CONTROL_ACK);
        efj_produce(InboundControl, ack);
        break;
    }
    case FSC_MAN_EXT:
    {
        FsiEfjMessage efjMessage = efj::message_cast<FsiEfjMessage>(msg);
        if (fsi_get_mf_id(efjMessage) != FSI_EFJ_MFID)
        {
            FsiAck ack = fsi_make_ack(id, version, fsi_get_cor_tag(msg), FSI_CONTROL_NAK_M_UNSUPP);
            efj_produce(InboundControl, ack);
            break;
        }

        onOutboundEfj(efjMessage, id, version);
        break;
    }
    case FSC_SEL_CHAN:
    {
        FsiAck ack = fsi_make_ack(id, version, fsi_get_cor_tag(msg), FSI_CONTROL_ACK);
        efj_produce(InboundControl, ack);
        break;
    }
    case FSC_SEL_RPT:
    {
        FsiAck ack = fsi_make_ack(id, version, fsi_get_cor_tag(msg), FSI_CONTROL_ACK);
        efj_produce(InboundControl, ack);
        break;
    }
    case FSC_SEL_SQUELCH:
    {
        FsiAck ack = fsi_make_ack(id, version, fsi_get_cor_tag(msg), FSI_CONTROL_ACK);
        efj_produce(InboundControl, ack);
        break;
    }
    case FSC_REPORT_SEL:
    {
        FsiAck ack = fsi_make_ack(id, version, fsi_get_cor_tag(msg), FSI_CONTROL_ACK);
        efj_produce(InboundControl, ack);
        break;
    }
    case FSC_SCAN:
    {
        FsiAck ack = fsi_make_ack(id, version, fsi_get_cor_tag(msg), FSI_CONTROL_ACK);
        efj_produce(InboundControl, ack);
        break;
    }
    default:
    {
        FsiAck ack = fsi_make_ack(id, version, fsi_get_cor_tag(msg), FSI_CONTROL_NAK_M_UNSUPP);
        efj_produce(InboundControl, ack);
        break;
    }
    }
}

// NOTE: Right now, we don't behave exactly like the 4500 here, since we send acks after handling/sending new messages, whereas
// the 4500 send acks before doing anything else. I didn't do this for any particular reason, though, so it can be changed whenever.
//
void DfsiControlClient::onOutboundEfj(const FsiEfjMessage& msg, u8 id, u8 version)
{
    u8 type = fsi_get_man_type(msg);
    //efj_log_info("Man Type: 0x%x (%s) 0x%02x%02x%02x%02x%02x%02x", type, efj::c_str((FsiEfjMessageType)type), msg.data[0], msg.data[1], msg.data[2], msg.data[3], msg.data[4], msg.data[5]);

    switch (type)
    {
    case FSC_LAUNCH_TIME:
        break;
    case FSC_CONNECT_EXT:
    {
        FsiEfjConnectExt connect_ext = efj::message_cast<FsiEfjConnectExt>(msg);

        efj_log_info("ConnectExt: D=%u,V=%u,C=%u,F=%u,S=%u,M=%u,P2=%u,ExtVer=%u",
            fsi_get_data_mode(connect_ext), fsi_get_voice_mode(connect_ext), fsi_get_control_mode(connect_ext),
            fsi_get_combine_fragments(connect_ext), fsi_get_report_rssi(connect_ext), fsi_get_use_mac(connect_ext),
            fsi_get_use_p2(connect_ext), fsi_get_extensions_version(connect_ext));

        bool data_supported      = false;
        bool voice_supported     = true;
        bool control_supported   = true;
        bool fragments_supported = true;
        bool rssi_supported      = true;
        bool rtp_supported       = true;
        bool p2_supported        = false;
        u8   extensions_version  = 2;

        FsiEfjConnectExtComplete complete = fsi_make_connect_ext_complete(0, data_supported, voice_supported,
            control_supported, fragments_supported, rssi_supported, rtp_supported, p2_supported,
            extensions_version);

        produceAckdInboundMessage(complete);
        break;
    }
    case FSC_DATA_END:
        break;
    case FSC_DATA_TDU_LC:
        break;
    case FSC2_CONF_FRAG:
        break;
    case FSC2_UNCONF_MSG:
        break;
    case FSC2_BLK_ACK:
        break;
    case FSC2_TSBK:
        break;
    case FSC2_TX_STATUS:
        break;
    case FSC_CONF_FRAG:
        break;
    case FSC_UNCONF_FRAG:
        break;
    // We expect version 2 of these:
    //case FSC_BLK_ACK:
    //    break;
    //case FSC_TX_STATUS:
    //    break;
    //case FSC_TSBK:
    //    break;
    case FSC_MBT:
        break;
    case FSC_PARAMETER_QUERY:
        break;
    case FSC_EVENT_QUERY:
        break;
    case FSC_CWID:
        break;
    case FSC_SET_PARAMETER:
    {
        FsiEfjSetParameter set_param = efj::message_cast<FsiEfjSetParameter>(msg);

        u32 ip = fsi_get_ip(set_param);
        u8* bytes = (u8*)&ip;
        efj_log_info("IP: %u.%u.%u.%u", bytes[0], bytes[1], bytes[2], bytes[3]);
        efj_log_info("Param name: %.*s", (int)fsi_get_param_name_length(set_param), fsi_get_param_name(set_param));
        efj_log_info("Param value: %.*s", (int)fsi_get_param_value_length(set_param), fsi_get_param_value(set_param));

        break;
    }
    case FSC_RX_OPERATING_MODE:
    {
        FsiEfjRxOperatingMode oper_mode = efj::message_cast<FsiEfjRxOperatingMode>(msg);
        u8  number = fsi_get_rx_number(oper_mode);
        u32 freq   = fsi_get_rx_freq(oper_mode);

        _rx_operating_number = number;
        _rx_operating_freq   = freq;

        efj_log_info("number: %u, freq: %u", number, freq);

        auto complete = fsi_make_oper_mode_complete(0, FSC_RX_OPERATING_MODE, number, true);
        produceAckdInboundMessage(complete);

        SetRxMode set_rx_mode = {};
        set_rx_mode.freq   = freq;
        set_rx_mode.number = number;
        efj_trigger(EVENT_SET_RX_MODE, set_rx_mode);
        break;
    }
    case FSC_TX_OPERATING_MODE:
    {
        FsiEfjTxOperatingMode oper_mode = efj::message_cast<FsiEfjTxOperatingMode>(msg);

        u8  number = fsi_get_tx_number(oper_mode);
        u8  mode   = fsi_get_tx_mode(oper_mode);
        u32 freq   = fsi_get_tx_freq(oper_mode);
        u16 power  = fsi_get_tx_power(oper_mode);

        _tx_operating_number = number;
        _tx_operating_freq   = freq;
        _tx_operating_mode   = mode;
        _tx_operating_power  = power;

        efj_log_info("number: %u, mode: %u, freq: %u, power: %u", number, mode, freq, power);

        auto complete = fsi_make_oper_mode_complete(0, FSC_TX_OPERATING_MODE, number, true);
        produceAckdInboundMessage(complete);

        SetTxMode set_tx_mode = {};
        set_tx_mode.freq   = freq;
        set_tx_mode.power  = power;
        set_tx_mode.number = number;
        set_tx_mode.mode   = mode;

        efj_trigger(EVENT_SET_TX_MODE, set_tx_mode);
        break;
    }
    case FSC_RX_STATUS_QUERY:
    {
        bool alarm      = false;
        bool warn       = false;
        bool off        = false;
        bool rx_p25     = false;
        bool rx_carrier = false;
        bool ready      = true;
        u8   rssi       = 31;

        auto response = fsi_make_rx_status_response(0, _rx_operating_number, _rx_operating_freq,
            alarm, warn, off, rx_p25, rx_carrier, ready, rssi);
        //efj_log_info("RSSI: %u, %u", fsi_get_rssi(response), fsi_get_rssi_type(response));
        produceAckdInboundMessage(response);
        break;
    }
    case FSC_TX_STATUS_QUERY:
    {
        bool alarm = false;
        bool warn  = false;
        bool off   = false;
        bool ready = true;

        auto response = fsi_make_tx_status_response(0, _tx_operating_number, _tx_operating_mode,
            _tx_operating_freq, _tx_operating_power, alarm, warn, off, ready);

        efj_log_debug("Byte: %x", response[FSI_MAN_DATA_OFFSET + 10]);
        efj_assert(response[FSI_MAN_DATA_OFFSET + 10] == 0x1);
        efj_panic_if(fsi_get_off(response) || fsi_get_warn(response));
        produceAckdInboundMessage(response);
        break;
    }
    case FSC_RX_CAPABILITIES_QUERY:
        break;
    case FSC_TX_CAPABILITIES_QUERY:
        break;
    case FSC_REMOVE_TCM:
        break;
    case FSC_REPLACE_LC:
        break;
    default:
        FsiAck ack = fsi_make_ack(id, version, fsi_get_cor_tag(msg), FSI_CONTROL_NAK_M_UNSUPP);
        efj_produce(InboundControl, ack);
        break;
    }

    FsiAck ack = fsi_make_ack(id, version, fsi_get_cor_tag(msg), FSI_CONTROL_ACK);
    efj_produce(InboundControl, ack);
}

void DfsiControlClient::produceAckdInboundMessage(FsiMessage& msg)
{
    u8 id = fsi_get_id(msg);
    efj_panic_if(id == FSC_ACK || id == FSC_HEARTBEAT || msg.size < 3);

    u8 cor_tag = _next_cor_tag_to_use++;
    fsi_set_cor_tag(msg, cor_tag);

    DfsiAckEntry* entry = &_ack_table[cor_tag];

    // FIXME: @Robustness. If we wrap around, the robust thing to do would be to queue up outbound messages until
    // the timeout for this cor tag has expired, but right now we just disconnect b/c that's easier, even if
    // we received a burst of messages that needed responses.
    if (entry->pending)
    {
        efj_log_debug("Ack table wrapped");
        disconnect();
        return;
    }

    // We just don't retry messages that are too big to fit in our buffer right now. If this happens, just increase the
    // buffer size for each ack entry until we switch to a message queue. :MessageTooBig
    if (msg.size > efj::array_size(entry->msg_data))
    {
        efj_log_warn("Unable to storage message for retrying b/c it's too big (id=%u, size=%zu)", id, msg.size);
        entry->msg_size = 0;
    }
    else
    {
        entry->msg_size = msg.size;
        memcpy(entry->msg_data, msg.data, msg.size);
    }

    if (!efj::timer_is_running(_control_retry_timer))
    {
        //efj_log_info("Starting ack timer with retry of %zu", _control_retry_timeout_msec);
        efj::timer_start(_control_retry_timer, efj::msec{_control_retry_timeout_msec});
    }

    entry->pending        = true;
    entry->timeout_count  = 0;
    entry->time_sent_msec = efj::now_msec().value;

    // Uncomment to test packet dropping.
#if 0
    static u32 i = 0;
    if (i++ % 2)
    {
        if (id == FSC_MAN_EXT)
        {
            const char* name = efj::c_str((FsiEfjMessageType)fsi_get_man_type(efj::message_cast<FsiEfjMessage>(msg)));
            efj_log_debug("Actually sending %s", name);
        }
        else
        {
            efj_log_debug("Actually sending %u", id);
        }

        efj_produce(InboundControl, msg);
    }
    else
    {
        if (id == FSC_MAN_EXT)
        {
            const char* name = efj::c_str((FsiEfjMessageType)fsi_get_man_type(efj::message_cast<FsiEfjMessage>(msg)));
            efj_log_debug("Not actually sending %s", name);
        }
        else
        {
            efj_log_debug("Not actually sending %u", id);
        }
    }
#else
    efj_produce(InboundControl, msg);
#endif
}

void DfsiControlClient::onAckTableTimeout(efj::timer_event& e)
{
    //efj_log_info("Ack table timeout");

    // Update all times remaining, and send any retries that are necessary. We ignore a bit of processing delay, but
    // that shouldn't matter too much, since this just acking logic.
    // :FSICompliance

    u32 next_retry_timeout_msec = UINT32_MAX;

    for (u32 tag = 0; tag < efj::array_size(_ack_table); tag++)
    {
        DfsiAckEntry* entry = &_ack_table[tag];

        if (!entry->pending)
            continue;

        //efj_log_info("Message %u pending", tag);

        // May need a retry, but it was too big, so forget it. :MessageTooBig
        if (entry->msg_size == 0)
        {
            efj_log_info("Not retrying a message b/c it was too big");
            entry->pending = false;
            continue;
        }

        u64 now = efj::now_msec().value;
        u32 diff = (u32)(now - entry->time_sent_msec);

        u32 cur_timeout_count = diff / _control_retry_timeout_msec;
        u32 next_timeout_msec = _control_retry_timeout_msec - diff % _control_retry_timeout_msec;

        if (cur_timeout_count >= _control_attempt_limit)
        {
            efj_log_info("ACK for %u took too long (%u ms)", tag, diff);
            disconnect();
            return;
        }

        if (next_timeout_msec < next_retry_timeout_msec)
            next_retry_timeout_msec = next_timeout_msec;

        efj_assert(cur_timeout_count >= entry->timeout_count);
        if (cur_timeout_count <= entry->timeout_count)
        {
            //efj_log_debug("Not sending %u b/c timeout count == %u. Next timeout = %u ms", tag, cur_timeout_count, next_timeout_msec);
            continue;
        }

        // Retry.
        entry->timeout_count = cur_timeout_count;

        efj_log_info("Sending retry for %u", tag);
        FsiMessage msg { entry->msg_data, entry->msg_size };
        efj_produce(InboundControl, msg);
    }

    if (next_retry_timeout_msec < UINT32_MAX)
    {
        // Don't wake up more than once every 20 ms, b/c retrying is pretty low priority.
        if (next_retry_timeout_msec < 20)
            next_retry_timeout_msec = 20;

        //efj_log_debug("Starting next timer with %zu", next_retry_timeout_msec);
        efj::timer_start(_control_retry_timer, efj::msec{next_retry_timeout_msec});
    }
}

void DfsiControlClient::onHeartbeatTimeout(efj::timer_event& e)
{
    //u64 time = efj::now_msec().value;

    ++_connectivity_counter;

    if (_connectivity_counter > _connectivity_loss_limit)
    {
        efj_log_info("connection lost");
        disconnect();
        return;
    }

    //u64 diff = time - _last_hb_msec;

    //efj_log_debug("heartbeating... (%u ms)", (u32)diff);
    efj::timer_start(_connectivity_timer);

    FsiHeartbeat hb = fsi_make_heartbeat();
    efj_produce(InboundControl, hb);
}

void DfsiControlClient::disconnect()
{
    efj_log_info("DFSI disconnect");
    efj_trigger(EVENT_DFSI_DISCONNECT, DfsiDisconnectEvent());
    efj::timer_stop(_connectivity_timer);
    efj::timer_stop(_control_retry_timer);

    _connectivity_counter = 0;

    for (umm i = 0; i < efj::array_size(_ack_table); i++)
        efj_placement_new(&_ack_table[i]) DfsiAckEntry;

    _next_cor_tag_to_use = 0;
    _connected_ipv4 = 0;
    _connected_port = 0;
    _state = DFSI_CONTROL_NOT_CONNECTED;
}


