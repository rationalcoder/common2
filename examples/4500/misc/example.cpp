#include <c2.hpp>

struct Foo
{
    EFJ_OBJECT("Foo");
    EFJ_INPUT(OutboundBigger, const BiggerMessage)
    EFJ_OUTPUT(OutboundSmaller, const SmallerMessage);

    bool init() { return true; }
    void consumeOutboundBigger(const BiggerMessage& msg);
};

void Foo::consumeBigMessage(const BiggerMessage& msg)
{
    efj_produce(OutboundSmaller, getSubMessage(msg, 0));
    efj_produce(OutboundSmaller, getSubMessage(msg, 1));
    efj_trigger(EVENT_WHATEVER, WhateverEvent{});
    efj_produce(OutboundSmaller, getSubMessage(msg, 2));
    efj_produce(OutboundSmaller, getSubMessage(msg, 3));
}

struct Bar
{
    EFJ_OBJECT("Bar");
    EFJ_INPUT(OutboundSmaller, const SmallerMessage);

    bool init()
    void consumeSmallerMessage(const SmallerMessage&) {}
    void onWhateverEvent(const Whatever&) {}
};

bool Bar::init()
{
    efj::subscribe(EVENT_WHATEVER, &Bar::onWhateverEvent);
    return true;
}


int main()
{
    Foo foo;
    Bar bar;

    efj_connect(foo, OutboundSmaller, bar, OutboundSmaller);

    efj::thread* thread = efj::create_thread("Background");
    thread->add_object(bar);

    return efj::exec();
}
