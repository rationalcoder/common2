<atlas4500>
  <!-- Example config file for the Atlas 4500 -->
  <log>
    <ipport>1.2.3.4:40000</ipport>      <!-- Dest for log messages -->
    <!--Log levels are:
        LVL_ALWAYS=0, LVL_FATAL=1, LVL_ERROR=2, LVL_WARN=3,
        LVL_INFO=4, LVL_DEBUG=5, LVL_TRACE=6, LVL_UNIT=7
    -->  
    <logLevel>4</logLevel> 
  </log>
  <sysevent>
    <!-- #optional - Dest for SysEvent messages -default value of <atlas4500.log.ipport> -->
    <ipport>2.3.4.5:30000</ipport>
  </sysevent>
  <wacn>1</wacn>
  <system>2</system>
  <ssrc>1</ssrc>
  <dfsiclient>
    <ctrlport>7000</ctrlport>
    <vcport>8000</vcport>
    <ctrl_retry_interval_msec>100</ctrl_retry_interval_msec>
    <ctrl_max_msg_attempts>3</ctrl_max_msg_attempts>
    <hb_interval>4</hb_interval>
    <connectivity_loss_limit>5</connectivity_loss_limit>
    <!-- #optional - Outbound Jitter Buffer delay in millisceconds -default 30 -->
    <jitter_time>30</jitter_time>
  </dfsiclient>
  <simulcast>
    <!-- #optional - Signed Offset in Hz for transmit Frequency -default 0, range +/-1250 Hz-->
    <txfreq_offset>0</txfreq_offset>    
    <!-- #optional - Signed Offset in dB for transmit Power -default 0, range +/-30 -->
    <txpower_offset>0</txpower_offset>
    <!-- #optional - Delay in microseconds - float (0-200 uSec) default 0 uSec -->
    <delay>0.0</delay>
  </simulcast>
  <dsp>
    <efjmac>
      <!--
        Inbound/Outbound refers to the direction of the RF stream from the perspective
        of the base station. Here outbound_port is listening on a UDP port for
        packets from the channel manager to be eventually sent out of the base
        station to radios (outbound packets), and inbound_ipport is the IP/Port of the
        channel manager where we send packets received on the inbound RF link.
      -->
      <outbound_port>9000</outbound_port>
      <inbound_ipport>1.2.3.4:8201</inbound_ipport>
    </efjmac>
    <!--
      ignore_slot_timing
      0 - Phase 2 CAI packets will not be delivered to the DSP if the timing indicated by the
          requested slot number relative to the time when the packet request is being processed,
          would result in a time ambiguity for the DSP. i.e The time associated with a requested
          slot that is in excess of 1.0 seconds of the current time will be ambiguous.  In this
          case, a warning will be logged and the packet discarded.
      1 - Phase 2 CAI packets will be forwarded to the DSP regardless of the timing indicated by
          the current slot and the slot number of the packet.
    -->
    <ignore_slot_timing>0</ignore_slot_timing>
    <!--    
      send_p25p1_voter_metrics - Determines whether or not to send Voter metrics for P25 
      (phase 1) CAI packets with the inbound RTP stream.  These are not needed 
      with non-simulcast.  They can offer slight improvement with voting
      for scenarios where neighboring systems might interfere.  The tradeoff is with
      either, network overhead, or processing performance within the Atlas4500.
      false - Voter Metrics will not be sent for P25 phase 1 inbound CAI packets.
      true - Voter Metrics will be sent for P25 phase 1 inbound CAI packets.
    -->
    <send_p25p1_voter_metrics>false</send_p25p1_voter_metrics>
    <!--    
      send_p25p2_voter_metrics - Determines whether or not to send Voter metrics for P25 
      (phase 2) CAI packets with the inbound RTP stream.  These are not needed 
      with non-simulcast.  They can offer slight improvement with voting
      for scenarios where neighboring systems might interfere.  The tradeoff is with
      either, network overhead, or processing performance within the Atlas4500.
      false - Voter Metrics will not be sent for P25 phase 2 inbound CAI packets.
      true - Voter Metrics will be sent for P25 phase 2 inbound CAI packets.
    -->
    <send_p25p2_voter_metrics>false</send_p25p2_voter_metrics>
    <!--    
      send_analog_voter_metrics - Determines whether or not to send Voter metrics for analog 
      CAI packets with the inbound RTP stream.  These are not needed 
      with non-simulcast, and add to network traffic.
      false - Voter Metrics will not be sent for analog inbound CAI packets.
      true - Voter Metrics will be sent for phase 2 inbound CAI packets.
    -->
    <send_analog_voter_metrics>false</send_analog_voter_metrics>

    <!-- Analog minimum launch time
         Defines the minimum allowed launch time for analog calls in a
         simulcast site. The default value is valid when 
         config parameter exciter_buffers is set to  a value of 2
         and should be increased by 60 for 3 exciter buffers  -->

    <anlg_min_launch_time>160</anlg_min_launch_time>

    <colorcode>2345</colorcode>       <!-- 12 bit NAC -->
    <!--
      Queue Length Threshold
      Sets the number of packets that must be queued up for the DSP before
      the DSP start command is sent. Valid values are 0-100, default 5.
    -->
    <q_len_threshold>5</q_len_threshold>

    <code_image>Repeater.hex</code_image>
      
    <!-- Specifies, to the DSP, the duration/size for status symbol demarcation.
         The outbound signaling stream supplies synchronization information to be 
         used by the receiving subscriber units to define the inbound slot times. 
         5 - 37.5ms slot size
         6 - 45 ms slot size. (default) --> 
    <slot_size>6</slot_size>

    <!-- Specifies, to the DSP, idle mode inbound packet monitoring mode.
         The DSP can be configured to be in either Phase 1 Monitoring for Voice/Data
         Analog Voice, or mixed Phase1 and Analog
         1 - Analog
         2 - P25 Phase 1 (default)
         3 - Mixed  (analog and P25 Phase 1) -->
    <receive_mode>2</receive_mode>

    <valueNAC>3966</valueNAC><!-- NAC value to compare the incoming call,
            the default value of 3966 = 0xF7E (anyNAC) value -->  
            
    <!-- inbound_enable - Specifies whether RTP and DFSI messages generated by the 
         inbound DSPs should be relayed to their respective hosts. This value does not affect
         responses to messages generated by the DFSI host, such as FSC_REPORT_SEL.
         true - normal operation
         false - receiver disabled.  - silence inbound traffic (used for tx only) -->
    <inbound_enable>true</inbound_enable>

    <!-- outbound_enable - Specifies whether RTP and DFSI messages received from the 
         attached FSHost should result in transmissions.
         true - normal operation - transmitter enabled
         false - Transmitter disabled - silence outbound traffic (used for rx only) -->
    <outbound_enable>true</outbound_enable>
            
    <!-- Number of preamble packets to send at start of each analog call. 
         Each Analog packet is 60 mSec,
         Overrides the value in atlas4500Tuning.cfg.xml -->   
    <num_anlg_tx_preamble_pkts>0</num_anlg_tx_preamble_pkts>
        
    <!-- Amount of time in mSec that should be allowed for outbound packets to accumulate        
         in the host's dsp queue before sending the Start_Analog_Outbound message
         Needs to be sufficient to not starve the DSP, but not to much, as to add excessive      
         throughput delay.
         This overrides the value in atlas4500Tuning.cfg.xml -->   
    <anlg_sos_to_start_outbound_delay>100</anlg_sos_to_start_outbound_delay>
    
    <!-- Amount of time (in mSec) between receiving the 
         SERIAL_PORT_STARTED message from the DSP, until we turn on the PA.
         This overrides the value in atlas4500Tuning.cfg.xml -->   
    <anlg_serial_port_started_to_start_tx_delay>15</anlg_serial_port_started_to_start_tx_delay>

     <!-- Number of Phase 1 Control Channel preamble packets to send at start of each Phase 1 Control Channel session. 
         For P25, each packet is 7.5 mSec
         This overrides the value in atlas4500Tuning.cfg.xml -->   
    <num_p1_cc_tx_preamble_pkts>0</num_p1_cc_tx_preamble_pkts>
        
    <!-- Amount of time in mSec that should be allowed for Phase 1 Control Channel outbound packets to accumulate        
         in the host's dsp queue before sending the Start Project 25 Phase 1 Oubound message
         Needs to be sufficient to not starve the DSP, but not to much, as to add excessive      
         throughput delay.
         This overrides the value in atlas4500Tuning.cfg.xml -->   
    <p1_cc_sos_to_start_outbound_delay>100</p1_cc_sos_to_start_outbound_delay>
    
    <!--  Amount of time (in mSec) between receiving the 
          SERIAL_PORT_STARTED message from the DSP, until we turn on the PA.
          This overrides the value in atlas4500Tuning.cfg.xml -->   
    <p1_cc_serial_port_started_to_start_tx_delay>15</p1_cc_serial_port_started_to_start_tx_delay>

     <!-- Number of Phase 1 Data Channel preamble packets to send at start of each Phase 1 Data Channel session. 
         For P25, each packet is 7.5 mSec
         This overrides the value in atlas4500Tuning.cfg.xml -->   

    <num_p1_dc_tx_preamble_pkts>0</num_p1_dc_tx_preamble_pkts>
        
    <!-- Amount of time in mSec that should be allowed for Phase 1 Data Channel outbound packets to accumulate        
         in the host's dsp queue before sending the Start Project Phase 1 Outbound message.
         Needs to be sufficient to not starve the DSP, but not to much, as to add excessive      
         throughput delay.
         This overrides the value in atlas4500Tuning.cfg.xml -->   
    <p1_dc_sos_to_start_outbound_delay>100</p1_dc_sos_to_start_outbound_delay>
    
    <!--  Amount of time (in mSec) between receiving the 
          SERIAL_PORT_STARTED message from the DSP until we turn on the PA 
          This overrides the value in atlas4500Tuning.cfg.xml -->   
    <p1_dc_serial_port_started_to_start_tx_delay>15</p1_dc_serial_port_started_to_start_tx_delay>

     <!-- Number of Phase 1 Voice Channel preamble packets to send at start of each Phase 1 Voice  call. 
         For P25, each packet is 7.5 mSec
         This overrides the value in atlas4500Tuning.cfg.xml -->   

    <num_p1_vc_tx_preamble_pkts>0</num_p1_vc_tx_preamble_pkts>
        
    <!-- Amount of time in mSec that should be allowed for Phase 1 Voice Channel outbound packets to accumulate        
         in the host's dsp queue before sending the Start Project 25 Phase 1 Outbound message
         Needs to be sufficient to not starve the DSP, but not to much, as to add excessive      
         throughput delay.
         This overrides the value in atlas4500Tuning.cfg.xml -->   
    <p1_vc_sos_to_start_outbound_delay>11</p1_vc_sos_to_start_outbound_delay>

    <!--  Amount of time (in mSec) between receiving the 
          SERIAL_PORT_STARTED message from the DSP, until we turn on the PA.
          This overrides the value in atlas4500Tuning.cfg.xml -->   
    <p1_vc_serial_port_started_to_start_tx_delay>15</p1_vc_serial_port_started_to_start_tx_delay>

    <!-- P1 Voice minimum launch time
         Defines the minimum allowed launch time for P1 Voice calls in a
         simulcast site. The default value is valid when 
         config parameter exciter_buffers is set to  a value of 2 
         and should be increased by 60 for 3 exciter buffers -->

    <p1_vc_min_launch_time>90</p1_vc_min_launch_time>
                 
    <!--  Amount of time (in mSec) between receiving the 
          SERIAL_PORT_STARTED message from the DSP, until we turn on the PA for phase 2.
          This overrides the value in atlas4500Tuning.cfg.xml -->   
    <p2_vc_serial_port_started_to_start_tx_delay>15</p2_vc_serial_port_started_to_start_tx_delay>

    <!-- Number of packets that must be detected for analog carrier to be delared present
         the default value is 3 -->
    <carrierFilterCount>3</carrierFilterCount>

    <!-- Number of packets that must be detected for with no squelch carrier detected
         for open squelch to be declared. The default value is 20 -->
    <openSquelchDetectCount>20</openSquelchDetectCount>
 
   <!--  Number of exciter buffers allocated by the DSP for Phase 1 and analog calls 
          where each buffer holds 60 mSec of content -->           
   <exciter_buffers>3</exciter_buffers>                                                      

  </dsp>
  <!-- Specifies, the mode of operation and parameters of Atlas400 behavior when
       it is disconnedted from the DFSI --> 
  <fallback>
    <mode>2</mode><!-- Mode to take when disconnected from the DFSI where 
                              FAILSOFT = 0   CONVENTIONAL = 1  IDLE_STANDBY = 2 
                              P2_AND_ANALOG = 3 (special test mode for switching
                              back and forth between P2 Test Pattern and Analog Station ID-->

    <!-- CONVENTIONAL and IDLE_STANDBY - have no additional configuration parameters
            CONVENTIONAL - repeat mode, any incoming analog or P25 call should be transmitted out. 
            IDLE_STANDBY - the Atlas4500 should stay idle when disconnected from the DFSI Host. --> 


    <failsoftParams><!-- Items may be used if the Atlas4500 is in trunking mode -->
      <!-- If the DFSI Client is disconnected from the DFSI Host, it should continuously
            send LTDUs (Long TDUs) on the channel until a call is received. 
            Any incoming P25 call that is received should be repeated back out. --> 

      <!-- Do not want to start this during startup, or a temporary disconnect/reconnect of DFSI -->      
      <delayStartToRecheck>10000</delayStartToRecheck><!-- delay to (re)check connection 
                                                                to FSH in (milliseconds) -->      

      <toneEnable>1</toneEnable><!-- Select to periodicaly send tones to radios listening to a 
              failsoft channel. bool (false(0) | true (1)) default true -->
      <toneInterval>5000</toneInterval><!-- if toneEnable - period between tones (milliseconds) 
              Valid values are 500-60000 ms (half second to 1 minute) 
              The default 5 sec is complying with Motorolas failsoft feature -->      

    </failsoftParams>                              
  </fallback>
  <alarms><!-- Atlas4500 alarm configuration items -->

    <lowTxPowerPercentThreshold>50</lowTxPowerPercentThreshold> <!-- Threshold for Low Power alarm.  0-99 ,where 0 is disabled 
                                                                     This param overrides parameters lowTxPowerThreshold and 
                                                                     lowTxPowerPercentThreshold which are specified in 
                                                                     file atlas4500Tuning.cfg.xml if a value 
                                                                     other than 0 is specified-->

    <noTxPowerPercentThreshold>25</noTxPowerPercentThreshold> <!-- Threshold for no Power alarm.  0-99 ,where 0 is disabled 
                                                                     This param overrides parameter
                                                                     noTxPowerPercentThreshold (which can be specified in 
                                                                     file atlas4500Tuning.cfg.xml )-->
    <noTxPowerDeclareThreshold>2</noTxPowerDeclareThreshold> <!-- Threshold for consecutive number of No Tx Power fault detections
                                                                     before reporting alarm and taking repeater out of service -->

    
    <vswrRecovery>
      <testPower>10.0</testPower> <!-- Txmt PWR for the VSWR (re)try testing, range [5.0 - 100.0] Watts-->
      <testPeriod>120</testPeriod>  <!-- VSWR (re)try testing period, range [0 - 65535] Seconds
                                          Disabled for selection less than 10 seconds -->
    </vswrRecovery>  

      <excessiveLatePacketsDeclareThreshold> 50 </excessiveLatePacketsDeclareThreshold> <!-- Threshold for Excessive Late Packet alarm. 1-xx
                                                                                             Number of consectutive late packets received to
                                                                                             declare alarm. This alarm is not service affecting--> 

      <excessiveLatePacketsClearThreshold> 10 </excessiveLatePacketsClearThreshold>     <!-- Threshold for non Late Packet alarm. 1-xx
                                                                                             Number of consectutive non late packets received to
                                                                                             clear alarm--> 

      <severeLatePacketsDeclareThreshold> 16000 </severeLatePacketsDeclareThreshold>       <!-- Threshold for Severe Late Packet alarm. 1-xx
                                                                                              Number of consectutive late packets received to
                                                                                              declare alarm. This is a service affecting alarm--> 
      <ppsSettledRate> 6000 </ppsSettledRate>                                            <!-- Threshold for the absolute value of the 
                                                                                              rate of change (uSec/Minute) in the PPS timing
                                                                                              error reported by the DSP which is acceptable for
                                                                                              restoring the Repeater to service-->
      <ppsSettledMinDuration> 300 </ppsSettledMinDuration>                               <!-- Amount of time in seconds that the PPS Error Rate
                                                                                              must persist below the pp2SettledRate before the
                                                                                              repeater can be taken out of the alarm condition-->


    <renderNotReady>
    <!-- The values of these fields correspond to values of  hws::OutOfSrvcCnst, and control how a particular alarm should declare OOS status.
         STAY_IN_SERVICE                   = 0, 
         RX_OOS                            = 1, 
         TX_OOS                            = 2, 
         RX_AND_TX_OOS                     = 3,
         STAY_IN_SERVICE_RX_AND_TX_WARNING = 4, 
         STAY_IN_SERVICE_RX_WARNING        = 5, 
         STAY_IN_SERVICE_TX_WARNING        = 6
         RESTART_ON_DETECTION              = 7
    -->
      <highVswr>2</highVswr>           <!-- MAJ  - High VSWR Fault Detected -->
      <loTxPower>6</loTxPower>         <!-- MAJ  - Low TX Power Fault Detected -->
      <hiPaTemp>2</hiPaTemp>           <!-- CRIT - High PA Temp Fault Detected -->
      <fanFault>6</fanFault>           <!-- MAJ  - fan Fault Detected --> 
      <mnPllUnlock>3</mnPllUnlock>     <!-- CRIT - Main PLL Unlock Fault Detected -->
      <axPllUnlock>3</axPllUnlock>     <!-- CRIT - AUX PLL Unlock Fault Detected -->
      <extRefFlt>2</extRefFlt>         <!-- CRIT - External Reference Fault -->
      <rx1Unlock>5</rx1Unlock>         <!-- MAJ  - RX1 PLL Unlock Fault -->
      <rx2Unlock>5</rx2Unlock>         <!-- MIN  - RX2 PLL Unlock Fault -->
      <hiDcVoltage>0</hiDcVoltage>     <!-- MIN  - High DC Volts Fault -->
      <loDcVoltage>0</loDcVoltage>     <!-- MIN  - Low DC Volts Fault -->
      <pruRun>2</pruRun>               <!-- CRIT - PRU RUN fault -->
      <clkPpsPresent>2</clkPpsPresent>       <!-- CRIT - PPS clock fault. Activated when PPS signal is lost -->
      <clk10Mhz>2</clk10Mhz>           <!-- CRIT - 10MHZ clock fault-->
      <excessLatePkt>6</excessLatePkt> <!-- MAJ  - Excessive Late Pkts Threshold Crossed Fault -->
      <severeLatePkt>2</severeLatePkt> <!-- CRIT - Severe Late Pkts Threshold- Crossed Fault-->
      <clkPpsAccuracy>3</clkPpsAccuracy> <!-- CRIT - PPS Timing Failure. Activated when the accuracy of
                                                     the PPS signal has been outside of the allowed amount
                                                     for a specified interval of time-->
      <noTxPower>2</noTxPower>         <!-- Crit  - No TX Power Fault Detected -->
      <dpdFault>7</dpdFault>           <!-- Restart on Detection - DPD fault detected -->
    </renderNotReady>                              
  </alarms>
  <channel_info>
      <default_channel>0</default_channel>  <!-- Channel to operate in absence of instruction
                                                 from FSHost  (includes conventional) -->
      <channel_0>
        <rx>
            <Frequency>0</Frequency>  <!-- Receive Frequency in Hz for this channel -->
            <Bandwidth>12500</Bandwidth> <!-- Receive Bandwidth in Hz for this channel where 
                                                    valid choices are 12500, 20000, or 25000-->
            <IFFrequency>90000000</IFFrequency> <!-- Intermediate Frequency when operating on this channel
                                                     90000000 or 55000000 -->
        </rx>
        <tx>
            <Frequency>0</Frequency>  <!-- Transmit Frequency in Hz for this channel -->
            <Bandwidth>12500</Bandwidth> <!-- Transmit Bandwidth in Hz for this channel where 
                                                    valid choices are 12500, 20000, or 25000-->
            <Power>5</Power>  <!-- Transmit Power in watts for this channel -->
            <ModulationType>0</ModulationType> <!-- Modulation Type for this channel where 
                                                          LSM = 0
                                                          C4FM = 1 -->
        </tx>
      </channel_0>
      <channel_1>
        <rx>
            <Frequency>0</Frequency>  <!-- Receive Frequency in Hz for this channel -->
            <Bandwidth>12500</Bandwidth> <!-- Receive Bandwidth in Hz for this channel where 
                                                    valid choices are 12500, 20000, or 25000-->
            <IFFrequency>90000000</IFFrequency> <!-- Intermediate Frequency when operating on this channel
                                                     90000000 or 55000000 -->
        </rx>
        <tx>
            <Frequency>0</Frequency>  <!-- Transmit Frequency in Hz for this channel -->
            <Bandwidth>12500</Bandwidth> <!-- Transmit Bandwidth in Hz for this channel where 
                                                    valid choices are 12500, 20000, or 25000-->
            <Power>5</Power>  <!-- Transmit Power in watts for this channel -->
            <ModulationType>0</ModulationType> <!-- Modulation Type for this channel where 
                                                          LSM = 0
                                                          C4FM = 1 -->
            
        </tx>
      </channel_1>
      <channel_15>
        <rx>
            <Frequency>0</Frequency>  <!-- Receive Frequency in Hz for this channel -->
            <Bandwidth>12500</Bandwidth> <!-- Receive Bandwidth in Hz for this channel where 
                                                    valid choices are 12500, 20000, or 25000-->
            <IFFrequency>90000000</IFFrequency> <!-- Intermediate Frequency when operating on this channel
                                                     90000000 or 55000000 -->
        </rx>
        <tx>
            <Frequency>0</Frequency>  <!-- Transmit Frequency in Hz for this channel -->
            <Bandwidth>12500</Bandwidth> <!-- Transmit Bandwidth in Hz for this channel where 
                                                    valid choices are 12500, 20000, or 25000-->
            <Power>5</Power>  <!-- Transmit Power in watts for this channel -->
            <ModulationType>0</ModulationType> <!-- Modulation Type for this channel where 
                                                          LSM = 0
                                                          C4FM = 1 -->
            
        </tx>
      </channel_15>
      
  </channel_info>
</atlas4500>
