#include <stdio.h>
#include <errno.h>

#include <c2.hpp>

struct Foo
{
    EFJ_OBJECT("Foo");

    efj::timer relativeOneshot;
    efj::timer oneshot;
    efj::timer periodic;

    bool init()
    {
        efj::add_oneshot_timer("Test relative oneshot", 1_s, &relativeOneshot, &Foo::onRelativeTimerExpired);
        efj::add_oneshot_timer("Test oneshot", 1_s, &oneshot, &Foo::onTimerExpired);
        efj::add_periodic_timer("Test periodic", 1_s, &periodic, &Foo::onTimerExpired);
        return true;
    }

    void onTimerExpired(efj::timer_event& e)
    {
        efj_log_info("\"%s\" expired at %llu", e.source->event.name, (efj::llu)e.expiredTime.value);
        // We start relative to the theoretical expire time of the timer to not lose time due to processing delay.
        efj::timer_start_absolute(*e.source, e.expiredTime + 1_s);
    }

    void onRelativeTimerExpired(efj::timer_event& e)
    {
        //efj_log_info("Timer %s expired at %llu", e.source->event.name, (efj::llu)e.expiredTime.value);
        // NOTE: The event fields are visible, but aren't part of the public API. We just don't have accessors for these, yet.
        efj_log_info("\"%s\" is drifting forward in time! It expired at %llu", e.source->event.name, (efj::llu)e.expiredTime.value);
        efj::timer_start(*e.source, 1_s);
    }
};

// File/line numbers can get annoying.
efj::string formatLogMessage(const efj::log_message_ctx& ctx, const efj::string& msg)
{
    efj::string now = efj::to_string(efj::to_utc(ctx.time));
    return efj::fmt("%.*s %s: %.*s\n", (int)now.size, now.data, ctx.object ? ctx.object->name : "[c2]", (int)msg.size, msg.data);
}

int main(int argc, const char** argv)
{
    efj::set_log_formatter(formatLogMessage);

    Foo foo;
    return efj::exec(argc, argv);
}

#define EFJ_C2_IMPLEMENTATION
#include <c2.hpp>

