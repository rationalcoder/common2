namespace efj
{

// NOTE: We intend users to be able to define new message types with extra fields like so:
//
// struct MyMessage : efj::message
// {
//     using message::message;
//
//     int extraField1;
//     int extraField2;
// };
// EFJ_DEFINE_MESSAGE(MyMessage, "1.0");
//
// Functions and data structures in C2 that allocate messages or copy them around are expected
// to take messages as a template parameter so they will copy the user-defined fields around as
// well. IMPORTANT: To keep things simple, we asssume any extra fields are trivial. That is, they
// are memcpyable and aren't dynamically allocated. Non-trivial data associated with messages should
// be dealt with either in a context-specific way or with heavier weight efj::component APIs.
//
//
// NOTE: We assume host byte order is LE in here, which should be a safe assumption. If we use a BE platform in the future,
// civilization has likely collapsed and more fundamental assumptions than endianess likely no longer hold. Good luck.
//

struct message
{
    // Keep data and size like this at the top. A lot of code relies on { data size } initializers.
    u8* data = nullptr;
    umm size = 0;

    // Wanting the ipv4 address/port is so common that we just have these in here for now.
    // We can figure out what we want to do about ipv6 later.
    u32 ipv4 = 0;
    u16 port = 0;

    message() {}
    message(const void* data, umm size) : data((u8*)data), size(size) {}

    template <size_t ExtentV>
    message(const u8(&arr)[ExtentV]) : data(arr), size(ExtentV) {}

    const u8& operator [] (size_t index) const { efj_panic_if(index >= size); return data[index]; }
    u8&       operator [] (size_t index)       { efj_panic_if(index >= size); return data[index]; }
};

EFJ_MACRO u8
get_bit(const efj::message& msg, umm byteOffset, umm bitOffset)
{
    return (u8)((msg.data[byteOffset] & (1 << bitOffset)) >> bitOffset);
}

EFJ_MACRO u8
get_bit(const void* data, umm byteOffset, umm bitOffset)
{
    return (u8)((((u8*)data)[byteOffset] & (1 << bitOffset)) >> bitOffset);
}

EFJ_MACRO void
set_bit(const efj::message& msg, umm byteOffset, umm bitOffset, bool bitValue)
{
    u8 byte = ((u8*)msg.data)[byteOffset];
    u8 bit  = 1 << bitOffset;
    msg.data[byteOffset] = (byte & ~bit) | (bitValue << bitOffset);
}

EFJ_MACRO void
set_bit(const void* data, umm byteOffset, umm bitOffset, bool bitValue)
{
    u8 byte = ((u8*)data)[byteOffset];
    u8 bit  = 1 << bitOffset;
    ((u8*)data)[byteOffset] = (byte & ~bit) | (bitValue << bitOffset);
}

//! This is for "compile-time" bounds checking.
template <size_t ExtentV> EFJ_MACRO u8
get_byte(const u8 (&data)[ExtentV], umm offset)
{
    efj_panic_if(offset >= ExtentV);
    return data[offset];
}

//! This is for "compile-time" bounds checking.
template <size_t ExtentV> EFJ_MACRO void
set_byte(u8 (&data)[ExtentV], umm offset, u8 value)
{
    efj_panic_if(offset >= ExtentV);
    data[offset] = value;
}

//! This is for uniformity in case using the default [] operator on
//! efj::message is annoying for some reason.
EFJ_MACRO u8
get_byte(const efj::message& msg, umm offset, u8 value)
{
    return msg[offset];
}

//! This is for uniformity in case using the default [] operator on
//! efj::message is annoying for some reason.
EFJ_MACRO void
set_byte(efj::message& msg, umm offset, u8 value)
{
    msg[offset] = value;
}

EFJ_MACRO u16
get_ntoh_u16(const void* data)
{
    u16 result = 0;
    ((u8*)&result)[1] = ((u8*)data)[0];
    ((u8*)&result)[0] = ((u8*)data)[1];

    return result;
}

EFJ_MACRO u32
get_ntoh_u32(const void* data)
{
    u32 result = 0;
    ((u8*)&result)[3] = ((u8*)data)[0];
    ((u8*)&result)[2] = ((u8*)data)[1];
    ((u8*)&result)[1] = ((u8*)data)[2];
    ((u8*)&result)[0] = ((u8*)data)[3];

    return result;
}

EFJ_MACRO void
set_ntoh_u16(void* data, u16 value)
{
    u8* valueBytes = (u8*)&value;

    ((u8*)data)[1] = valueBytes[0];
    ((u8*)data)[0] = valueBytes[1];
}

EFJ_MACRO void
set_ntoh_u32(void* data, u32 value)
{
    u8* valueBytes = (u8*)&value;
    ((u8*)data)[3] = valueBytes[0];
    ((u8*)data)[2] = valueBytes[1];
    ((u8*)data)[1] = valueBytes[2];
    ((u8*)data)[0] = valueBytes[3];
}

EFJ_MACRO u16  get_ntoh_u16(u16 value) { return efj::get_ntoh_u16(&value); }
EFJ_MACRO u32  get_ntoh_u32(u32 value) { return efj::get_ntoh_u32(&value); }
EFJ_MACRO u16  get_hton_u16(u16 value) { return efj::get_ntoh_u16(&value); }
EFJ_MACRO u32  get_hton_u32(u32 value) { return efj::get_ntoh_u32(&value); }

EFJ_MACRO u16  get_hton_u16(const void* data) { return efj::get_ntoh_u16(data); }
EFJ_MACRO u32  get_hton_u32(const void* data) { return efj::get_ntoh_u32(data); }
EFJ_MACRO void set_hton_u16(void* data, u16 value) { efj::set_ntoh_u16(data, value); }
EFJ_MACRO void set_hton_u32(void* data, u32 value) { efj::set_ntoh_u32(data, value); }


EFJ_MACRO u16
get_ntoh_u16(const efj::message& msg, umm offset)
{
    efj_assert(offset + sizeof(u16) <= msg.size);

    return efj::get_ntoh_u16(msg.data + offset);
}

EFJ_MACRO u32
get_ntoh_u32(const efj::message& msg, umm offset)
{
    efj_assert(offset + sizeof(u32) <= msg.size);

    return efj::get_ntoh_u32(msg.data + offset);
}

EFJ_MACRO void
set_ntoh_u16(efj::message& msg, umm offset, u16 value)
{
    efj_assert(offset + sizeof(u16) <= msg.size);

    efj::set_ntoh_u16(msg.data + offset, value);
}

EFJ_MACRO void
set_ntoh_u32(efj::message& msg, umm offset, u32 value)
{
    efj_assert(offset + sizeof(u32) <= msg.size);

    efj::set_ntoh_u32(msg.data + offset, value);
}

EFJ_MACRO u16  get_hton_u16(const efj::message& msg, umm offset) { return efj::get_ntoh_u16(msg, offset); }
EFJ_MACRO u32  get_hton_u32(const efj::message& msg, umm offset) { return efj::get_ntoh_u32(msg, offset); }
EFJ_MACRO void set_hton_u16(efj::message& msg, umm offset, u16 value) { efj::set_ntoh_u16(msg, offset, value); }
EFJ_MACRO void set_hton_u32(efj::message& msg, umm offset, u32 value) { efj::set_ntoh_u32(msg, offset, value); }

template <size_t ExtentV> EFJ_MACRO u16
get_ntoh_u16(const u8 (&data)[ExtentV], umm offset)
{
    efj_panic_if(offset >= ExtentV);
    return get_ntoh_u16(&data[offset]);
}

template <size_t ExtentV> EFJ_MACRO u32
get_ntoh_u32(const u8 (&data)[ExtentV], umm offset)
{
    efj_panic_if(offset >= ExtentV);
    return get_ntoh_u32(&data[offset]);
}

template <size_t ExtentV> EFJ_MACRO u16
get_hton_u16(const u8 (&data)[ExtentV], umm offset)
{
    efj_panic_if(offset >= ExtentV);
    return get_hton_u16(&data[offset]);
}

template <size_t ExtentV> EFJ_MACRO u32
get_hton_u32(const u8 (&data)[ExtentV], umm offset)
{
    efj_panic_if(offset >= ExtentV);
    return get_hton_u32(&data[offset]);
}


template <size_t ExtentV> EFJ_MACRO void
set_ntoh_u16(u8 (&data)[ExtentV], umm offset, u16 value)
{
    efj_panic_if(offset >= ExtentV);
    set_ntoh_u16(&data[offset], value);
}

template <size_t ExtentV> EFJ_MACRO void
set_ntoh_u32(u8 (&data)[ExtentV], umm offset, u32 value)
{
    efj_panic_if(offset >= ExtentV);
    set_ntoh_u32(&data[offset], value);
}

template <size_t ExtentV> EFJ_MACRO void
set_hton_u16(u8 (&data)[ExtentV], umm offset, u16 value)
{
    efj_panic_if(offset >= ExtentV);
    set_hton_u16(&data[offset], value);
}

template <size_t ExtentV> EFJ_MACRO void
set_hton_u32(u8 (&data)[ExtentV], umm offset, u32 value)
{
    efj_panic_if(offset >= ExtentV);
    set_hton_u32(&data[offset], value);
}


//{ 16-bit
EFJ_MACRO u16
read_u16(const void* data)
{
    u16 result = 0;
    ((u8*)&result)[0] = ((u8*)data)[0];
    ((u8*)&result)[1] = ((u8*)data)[1];

    return result;
}

EFJ_MACRO u16
read_u16(const void* data, umm offset)
{
    u16 result = 0;
    ((u8*)&result)[0] = ((u8*)data)[offset];
    ((u8*)&result)[1] = ((u8*)data)[offset + 1];

    return result;
}

EFJ_MACRO void
write_u16(void* data, umm offset, u16 value)
{
    ((u8*)data)[offset]     = ((u8*)&value)[0];
    ((u8*)data)[offset + 1] = ((u8*)&value)[1];
}

template <size_t ExtentV> EFJ_MACRO void
write_u16(u8 data[ExtentV], umm offset, u16 value)
{
    efj_panic_if(offset + sizeof(u16) > ExtentV);

    data[offset]     = ((u8*)&value)[0];
    data[offset + 1] = ((u8*)&value)[1];
}

EFJ_MACRO void
write_u16(efj::message& msg, umm offset, u16 value)
{
    efj_panic_if(offset + sizeof(u16) > msg.size);

    msg.data[offset]     = ((u8*)&value)[0];
    msg.data[offset + 1] = ((u8*)&value)[1];
}
//}

//{ 32-bit

EFJ_MACRO u32
read_u32(const void* data)
{
    u32 result = 0;
    ((u8*)&result)[0] = ((u8*)data)[0];
    ((u8*)&result)[1] = ((u8*)data)[1];
    ((u8*)&result)[2] = ((u8*)data)[2];
    ((u8*)&result)[3] = ((u8*)data)[3];

    return result;
}

EFJ_MACRO u32
read_u32(const void* data, umm offset)
{
    u32 result = 0;
    ((u8*)&result)[0] = ((u8*)data)[offset];
    ((u8*)&result)[1] = ((u8*)data)[offset + 1];
    ((u8*)&result)[2] = ((u8*)data)[offset + 2];
    ((u8*)&result)[3] = ((u8*)data)[offset + 3];

    return result;
}

EFJ_MACRO u32
read_u32(const efj::message& msg, umm offset)
{
    efj_assert(offset + sizeof(u32) <= msg.size);

    u32 result = 0;
    ((u8*)&result)[0] = msg.data[offset];
    ((u8*)&result)[1] = msg.data[offset + 1];
    ((u8*)&result)[2] = msg.data[offset + 2];
    ((u8*)&result)[3] = msg.data[offset + 3];

    return result;
}

EFJ_MACRO void
write_u32(void* data, u32 value)
{
    ((u8*)data)[0] = ((u8*)&value)[0];
    ((u8*)data)[1] = ((u8*)&value)[1];
    ((u8*)data)[2] = ((u8*)&value)[2];
    ((u8*)data)[3] = ((u8*)&value)[3];
}

EFJ_MACRO void
write_u32(void* data, umm offset, u32 value)
{
    ((u8*)data)[offset]     = ((u8*)&value)[0];
    ((u8*)data)[offset + 1] = ((u8*)&value)[1];
    ((u8*)data)[offset + 2] = ((u8*)&value)[2];
    ((u8*)data)[offset + 3] = ((u8*)&value)[3];
}

//! C array version with bounds checking.
template <size_t ExtentV> EFJ_MACRO void
write_u32(u8 data[ExtentV], umm offset, u32 value)
{
    efj_panic_if(offset + sizeof(u32) > ExtentV);

    data[offset]     = ((u8*)&value)[0];
    data[offset + 1] = ((u8*)&value)[1];
    data[offset + 2] = ((u8*)&value)[2];
    data[offset + 3] = ((u8*)&value)[3];
}

EFJ_MACRO void
write_u32(efj::message& msg, umm offset, u32 value)
{
    efj_assert(offset + sizeof(u32) <= msg.size);

    msg.data[offset]     = ((u8*)&value)[0];
    msg.data[offset + 1] = ((u8*)&value)[1];
    msg.data[offset + 2] = ((u8*)&value)[2];
    msg.data[offset + 3] = ((u8*)&value)[3];
}
//}

//{ 64-bit

inline u64
read_u64(const void* data)
{
    u64 result = 0;
    ((u8*)&result)[0] = ((u8*)data)[0];
    ((u8*)&result)[1] = ((u8*)data)[1];
    ((u8*)&result)[2] = ((u8*)data)[2];
    ((u8*)&result)[3] = ((u8*)data)[3];
    ((u8*)&result)[4] = ((u8*)data)[4];
    ((u8*)&result)[5] = ((u8*)data)[5];
    ((u8*)&result)[6] = ((u8*)data)[6];
    ((u8*)&result)[7] = ((u8*)data)[7];

    return result;
}

inline  u64
read_u64(const void* data, umm offset)
{
    u64 result = 0;
    ((u8*)&result)[0] = ((u8*)data)[offset];
    ((u8*)&result)[1] = ((u8*)data)[offset + 1];
    ((u8*)&result)[2] = ((u8*)data)[offset + 2];
    ((u8*)&result)[3] = ((u8*)data)[offset + 3];
    ((u8*)&result)[4] = ((u8*)data)[offset + 4];
    ((u8*)&result)[5] = ((u8*)data)[offset + 5];
    ((u8*)&result)[6] = ((u8*)data)[offset + 6];
    ((u8*)&result)[7] = ((u8*)data)[offset + 7];

    return result;
}

inline u64
read_u64(const efj::message& msg, umm offset)
{
    efj_assert(offset + sizeof(u64) <= msg.size);

    u64 result = 0;
    ((u8*)&result)[0] = msg.data[offset];
    ((u8*)&result)[1] = msg.data[offset + 1];
    ((u8*)&result)[2] = msg.data[offset + 2];
    ((u8*)&result)[3] = msg.data[offset + 3];
    ((u8*)&result)[4] = msg.data[offset + 4];
    ((u8*)&result)[5] = msg.data[offset + 5];
    ((u8*)&result)[6] = msg.data[offset + 6];
    ((u8*)&result)[7] = msg.data[offset + 7];

    return result;
}

inline void
write_u64(void* data, u64 value)
{
    ((u8*)data)[0] = ((u8*)&value)[0];
    ((u8*)data)[1] = ((u8*)&value)[1];
    ((u8*)data)[2] = ((u8*)&value)[2];
    ((u8*)data)[3] = ((u8*)&value)[3];
    ((u8*)data)[4] = ((u8*)&value)[4];
    ((u8*)data)[5] = ((u8*)&value)[5];
    ((u8*)data)[6] = ((u8*)&value)[6];
    ((u8*)data)[7] = ((u8*)&value)[7];
}

inline void
write_u64(void* data, umm offset, u64 value)
{
    ((u8*)data)[offset]     = ((u8*)&value)[0];
    ((u8*)data)[offset + 1] = ((u8*)&value)[1];
    ((u8*)data)[offset + 2] = ((u8*)&value)[2];
    ((u8*)data)[offset + 3] = ((u8*)&value)[3];
    ((u8*)data)[offset + 4] = ((u8*)&value)[4];
    ((u8*)data)[offset + 5] = ((u8*)&value)[5];
    ((u8*)data)[offset + 6] = ((u8*)&value)[6];
    ((u8*)data)[offset + 7] = ((u8*)&value)[7];
}

//! C array version with bounds checking.
template <size_t ExtentV> inline void
write_u64(u8 data[ExtentV], umm offset, u64 value)
{
    efj_panic_if(offset + sizeof(u64) > ExtentV);

    data[offset]     = ((u8*)&value)[0];
    data[offset + 1] = ((u8*)&value)[1];
    data[offset + 2] = ((u8*)&value)[2];
    data[offset + 3] = ((u8*)&value)[3];
    data[offset + 4] = ((u8*)&value)[4];
    data[offset + 5] = ((u8*)&value)[5];
    data[offset + 6] = ((u8*)&value)[6];
    data[offset + 7] = ((u8*)&value)[7];
}

inline void
write_u64(efj::message& msg, umm offset, u64 value)
{
    efj_assert(offset + sizeof(u64) <= msg.size);

    msg.data[offset]     = ((u8*)&value)[0];
    msg.data[offset + 1] = ((u8*)&value)[1];
    msg.data[offset + 2] = ((u8*)&value)[2];
    msg.data[offset + 3] = ((u8*)&value)[3];
    msg.data[offset + 4] = ((u8*)&value)[4];
    msg.data[offset + 5] = ((u8*)&value)[5];
    msg.data[offset + 6] = ((u8*)&value)[6];
    msg.data[offset + 7] = ((u8*)&value)[7];
}

//}

//! Returns on temp memory.
efj::string to_hex(const void* data, umm size, bool addNewline = false);
//! Returns on temp memory.
EFJ_MACRO efj::string to_hex(const efj::message& msg, bool addNewLine = false)
{
    return efj::to_hex(msg.data, msg.size, addNewLine);
}

//! Copies something that inherits from efj::message with the current allocator.
//! \note This copy is only correct if the derived struct can be shallow-copied!
//!
//! Call this in user-defined custom message aliases to avoid having to write the same code.
//! This is different than an allocation + copy construct b/c we can do this in one allocation.
//! // :MessageHasOneAllocation
//!
template <typename MessageT> EFJ_MACRO MessageT*
copy_message(const MessageT& msg)
{
    static_assert(std::is_base_of<efj::message, MessageT>::value, "not an efj::message");

    // :MessageHasOneAllocation
    auto* result = (MessageT*)efj_allocate(sizeof(msg) + msg.size, alignof(msg));
    *result = msg; // copy any extra fields over; let the compiler handle removing the extra store.
    result->data = (u8*)(result + 1);
    memcpy(result->data, msg.data, msg.size);

    return result;
}

template <typename MessageT> EFJ_MACRO MessageT&
copy_construct_message(MessageT& lhs, const MessageT& rhs)
{
    static_assert(std::is_base_of<efj::message, MessageT>::value, "not an efj::message");

    // :MessageHasOneAllocation
    lhs = rhs;
    lhs.data = (u8*)efj_allocate(rhs.size, 1);
    memcpy(lhs.data, rhs.data, rhs.size);

    return lhs;
}

template <typename MessageT> inline MessageT
allocate_message(umm size)
{
    MessageT result = {};
    result.data = (u8*)efj_allocate(size, 1);
    result.size = size;

    return result;
}

#define efj_allocate_message(_type, _size) efj::allocate_message<_type>(_size)

template <typename ToT, typename FromT> EFJ_MACRO ToT&
message_cast(FromT& from)
{
    static_assert(std::is_base_of<efj::message, FromT>::value, "not an efj::message");
    static_assert(std::is_base_of<efj::message, ToT>::value, "not an efj::message");

    return (ToT&)from;
}

struct common_message_functions
{
    static efj::message* copy(const efj::message* val, efj::allocator* alloc)
    {
        efj_allocator_scope(*alloc);
        return (efj::message*)efj::copy_message(*val);
    }

    // We don't need destruction as long as we allocate the data and the struct together.
    // :MessageHasOneAllocation
    static void destroy(efj::message*, efj::allocator*)
    {
    }

    static void serialize(efj::serializer& sz, const efj::message* msg)
    {
        sz.writeBytes("bytes", msg->data, msg->size);
    }
};

#define EFJ_DEFINE_MESSAGE(_type, _version)                    \
template <>                                                    \
struct component_spec<_type> : efj::common_message_functions   \
{                                                              \
    enum { kIsTrivial = false };                               \
                                                               \
    EFJ_COMPONENT(_type);                                      \
    static const char* version() { return _version; }          \
}

EFJ_DEFINE_MESSAGE(efj::message, "1.0");

} // namespace efj

