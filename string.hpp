namespace efj
{

struct string
{
    char* data;
    umm   size;

    // WARNING: `size` does _not_ include the nul, but one is expected to exist.
    static EFJ_MACRO efj::string shallow_copy(char* data, umm size)
    {
        efj::string s = { efj::uninitialized };
        s.data = data;
        s.size = size;

        return s;
    }

    string() : data(), size() {}
    string(const char* s);
    string(efj::uninitialized_tag) {}

    // TODO: Rest of string API

    bool empty() const { return size == 0; }

    explicit operator bool() const { return data != nullptr; }
    // Haven't decided about the index operator returning references yet, b/c
    // we almost always want these to be immutable.
    char operator [] (umm i) const { efj_assert(i <= size /*data[size] == '\0'*/); return data[i]; }
    bool operator == (const efj::string& rhs) const;
    bool operator != (const efj::string& rhs) const;

    bool starts_with(const char* s) const;
    bool starts_with(char c) const;

    efj::string trim() const;

    // :StringHasNul
    umm capacity() const { return size + 1; }

    auto c_str() const -> const char* { return data; }
    auto dup(efj::allocator alloc = efj::current_allocator()) const -> efj::string;
    auto dup(efj::memory_arena& arena) const -> efj::string;

    //{ Conversions
    int to_int(int def = 0, bool* ok = nullptr) const;
    s8  to_s8 (s8  def = 0, bool* ok = nullptr) const;
    s16 to_s16(s16 def = 0, bool* ok = nullptr) const;
    s32 to_s32(s32 def = 0, bool* ok = nullptr) const;
    s64 to_s64(s64 def = 0, bool* ok = nullptr) const;

    auto to_unsigned(unsigned def = 0, bool* ok = nullptr) const -> unsigned;
    u8   to_u8      (u8       def = 0, bool* ok = nullptr) const;
    u16  to_u16     (u16      def = 0, bool* ok = nullptr) const;
    u32  to_u32     (u32      def = 0, bool* ok = nullptr) const;
    u64  to_u64     (u64      def = 0, bool* ok = nullptr) const;
    umm  to_usize   (umm      def = 0, bool* ok = nullptr) const;

    auto to_float (float  def = 0, bool* ok = nullptr) const -> float;
    auto to_double(double def = 0, bool* ok = nullptr) const -> double;

    bool to_bool(bool def = false, bool* ok = nullptr) const;
    //}
};

// Returns on temporary memory.
efj::string fmt(const char* fmt, ...) __attribute__((format (printf, 1, 2)));
// NOTE: overloading doesn't work here, since va_list can have type char*.
efj::string vfmt(const char* fmt, va_list va);

inline efj::string copy_string(const char* text, umm size, efj::memory_arena& arena); // into arena
inline efj::string copy_string(const char* text, umm size); // current allocator
inline efj::string temp_string(const char* text, umm size); // temp memory

// Copy to fixed buffer. Always nul-terminates, even if truncated.
template <umm SizeV> inline efj::string
copy_string(const char* text, umm size, char(&buf)[SizeV]);


enum { string_chunk_size = 64 };

struct string_chunk
{
    efj::string_chunk* next;
    char               data[efj::string_chunk_size];
};

// @Incomplete
struct string_builder
{
    efj::allocator     _alloc;

    u32                _nChunks;
    u32                _tailFilled;
    efj::string_chunk* _tail;
    efj::string_chunk  _head;

    // TODO: Remove once the allocator supports pools.
    efj::free_list     _freeList;

    string_builder(); // uses current allocator
    string_builder(efj::allocator alloc);
    string_builder(const efj::string& s); // uses temp memory

    ~string_builder();

    void append(const char*);
    void append(const efj::string&);
    void append_fmt(const char* fmt, ...) __attribute__((format (printf, 2, 3)));

    void clear();
    void set(const efj::string& s);
    void set(const char* s);

    umm size() const { return (_nChunks-1) * efj::string_chunk_size + _tailFilled; }

    efj::string str() const; // uses temp memory.
    efj::string dup(efj::allocator alloc = efj::current_allocator()) const;
    efj::string dup(efj::memory_arena& arena) const;
};

template <> EFJ_MACRO efj::string to_string(const u64& value) { return efj::fmt("%llu", (efj::llu)value); }
template <> EFJ_MACRO efj::string to_string(const u32& value) { return efj::fmt("%u", value); }
template <> EFJ_MACRO efj::string to_string(const u16& value) { return efj::fmt("%u", value); }
template <> EFJ_MACRO efj::string to_string(const u8& value) { return efj::fmt("%u", value); }
template <> EFJ_MACRO efj::string to_string(const s64& value) { return efj::fmt("%lld", (efj::lld)value); }
template <> EFJ_MACRO efj::string to_string(const s32& value) { return efj::fmt("%d", value); }
template <> EFJ_MACRO efj::string to_string(const s16& value) { return efj::fmt("%d", value); }
template <> EFJ_MACRO efj::string to_string(const s8& value) { return efj::fmt("%d", value); }
template <> EFJ_MACRO efj::string to_string(const char& value) { return efj::fmt("%c", value); }
template <> EFJ_MACRO efj::string to_string(const bool& value) { return efj::fmt("%s", value ? "true" : "false"); }

} // end namespace efj

