
// This file exists mostly so we can have access to all C2 header types like efj::timer for debugging.

namespace efj
{

inline void
timer_queue::init(u64 origin)
{
    if constexpr (TIMER_QUEUE_LOG_INPUTS) {
        efj::print("[%p] timer_queue::init: %llu\n", this, (efj::llu)origin);
    }

    _origin = origin;
    _currentTime = origin;
}

EFJ_MACRO void
timer_queue::start(efj::timer_handle* handle, void* timer, u64 initial, u64 duration, bool absolute)
{
    if constexpr (TIMER_QUEUE_LOG_INPUTS) {
        efj::print("[%p] timer_queue::start: %p, %p, %llu, %llu, %d\n", this, handle, timer,
                (efj::llu)initial, (efj::llu)duration, absolute);
    }

    // NOTE: I'm not sure that we can do some sort of key update here, since it seems important that we always
    // insert new timers as low in the tree as possible, after any that have equal expire times.
    // This is important so that all entries with an equal expire time are retired (end up in entries[0])
    // before any that have been re-inserted.
    if (handle->entry)
        remove(handle);

    if (absolute)
        insert_absolute(timer, initial, duration, handle);
    else
        insert_relative(timer, initial, duration, handle);
}

EFJ_MACRO void
timer_queue::stop(efj::timer_handle* handle)
{
    if constexpr (TIMER_QUEUE_LOG_INPUTS) {
        efj::print("[%p] timer_queue::stop: %p\n", this, handle);
    }

    if (handle->entry)
        remove(handle);
}

inline void
timer_queue::_pop()
{
    // IMPORTANT: We assume _size > 0 assertions have happened already.
    _entries[0] = _entries[_size-1];
    --_size;

    umm i = 0;

    for (;;) {
        umm left  = (i << 1) + 1; // :ZeroBasedHeap
        umm right = (i << 1) + 2; // :ZeroBasedHeap

        umm smallest = i;

        u64 curExpireTime = _entries[i].expireTime;
        if (left < _size && _entries[left].expireTime < curExpireTime)
            smallest = left;
        if (right < _size && _entries[right].expireTime < _entries[smallest].expireTime)
            smallest = right;

        if (i == smallest)
            break;

        // IMPORTANT: A swap on the entries is not enough. We need to update the handle->entry back references of any
        // parent nodes we replace on our way down the tree.
        // We don't have to update the entry's back reference until it's in its final position (always at the lowest level).

        _entries[smallest].handle->entry = &_entries[i];
        efj::swap(_entries[i], _entries[smallest]);
        i = smallest;
    }

    // We just moved the lowest priority down some, so updates its handle to itself.
    _entries[i].handle->entry = &_entries[i];
}

inline void
timer_queue::_insert(const efj::timer_entry& entry)
{
    // IMPORTANT: We assume _size < max assertions have happened already.
    _entries[_size] = entry;

    umm i = _size;
    while (i != 0) {
        umm parent = (i - 1) >> 1; // :ZeroBasedHeap
        if (_entries[parent].expireTime <= _entries[i].expireTime) {
            //printf("Timer %p (%llu) <= Timer %p (%llu)\n", _entries[parent].handle->timer, (efj::llu)_entries[parent].expireTime,
            //    _entries[i].handle->timer, (efj::llu)_entries[i].expireTime);
            //printf("Order: %p < %p\n", _entries[parent].handle->timer, _entries[i].handle->timer);
            break;
        }

        // IMPORTANT: A swap on the entries is not enough. We need to update the handle->entry back references of any
        // parent nodes we replace on our way up the tree.
        // We don't have to update the newly-inserted entry's back reference until it's in its final position.

        _entries[parent].handle->entry = &_entries[i];
        efj::swap(_entries[parent], _entries[i]);
        i = parent;
    }

    ++_size;

    // We potentially moved the timer entry, so update the user's handle to it.
    //printf("Inserting entry = %p (%zu), size: %zu\n", _entries + i, i, _size);
    entry.handle->entry = &_entries[i];
    //printf("[%s] %p:%s: %s setting entry to %p\n", efj::this_thread()->name, this, __PRETTY_FUNCTION__, ((efj::eventsys_timer_event*)entry.handle->timer)->name,
    //    entry.handle->entry);
}

inline umm
timer_queue::poll(efj::timer_queue_event* events, umm n)
{
    if constexpr (TIMER_QUEUE_LOG_INPUTS) {
        efj::print("[%p] timer_queue::poll: %p, %zu\n", this, events, n);
    }

    u64 currentTime  = _currentTime - _origin;
    umm expiredCount = 0;

    umm startingSize = _size;
    for (umm i = 0; i < startingSize && i < n; i++) {
        efj::timer_entry minEntry = _entries[0]; // :ZeroBasedHeap
        efj::timer_queue_event* event = &events[i];
        u64 minExpireTime = minEntry.expireTime - _origin;

        // Expire times are relative to the origin.
        if (minExpireTime > currentTime) {
            //efj::print("Min expire time (%llu, %llu-%llu) > currentTime (%llu). Nothing to do\n", (efj::llu)minExpireTime,
            //    (efj::llu)minEntry.expireTime, (efj::llu)_origin, (efj::llu)currentTime);
            break;
        }

        // We are always removing from the queue. If a timer is periodic, we just re-insert it.
        _pop();

        // We set the expire count to 1 for oneshot timers.
        // oneshot  => remove it from the queue.
        // periodic => perform a key update.
        event->handle = minEntry.handle;
        // Report the absolute expire time, not the relative one.
        event->expiredTime = minEntry.expireTime;

        // Duration == 0 => oneshot
        if (minEntry.duration == 0) {
            //efj::print("Entry with expireTime %llu, is oneshot. Removing it.\n", (efj::llu)minExpireTime);

            // Oneshot timers no longer exist in the queue after being popped.
            // Leave the timer pointer in the user's handle alone, though.
            event->handle->entry = nullptr;
            event->expiredCount  = 1;
        }
        else {
            //efj::print("Entry with expireTime %llu, is periodic. Re-inserting it.\n", (efj::llu)minExpireTime);

            u64 elapsed = currentTime - minExpireTime;
            umm expiredCount = elapsed / minEntry.duration + 1;
            // We use the absolute expire time here, not relative to the origin.
            u64 nextExpireTime = minEntry.expireTime + expiredCount * minEntry.duration;
            //efj::print("Timer=%p, Elapsed=%llu, duration=%llu, expiredCount=%llu, expireTime=%llu nextExpireTime=%llu\n", minEntry.handle->timer,
            //        (efj::llu)elapsed, (efj::llu)minEntry.duration, (efj::llu)expiredCount, (efj::llu)minEntry.expireTime, (efj::llu)nextExpireTime);

            event->expiredCount = expiredCount;

            // Here we are setting the _next_ expire time, since we are re-inserting.
            //printf("Next expire time: %llu, current time: %llu\n", (efj::llu)nextExpireTime, (efj::llu)_currentTime);
            minEntry.expireTime = nextExpireTime;
            _insert(minEntry);
        }

        ++expiredCount;
    }

    // Slide the origin over to handle integer overflow. We assume time will advance by [0, UINT64_MAX) ticks,
    // and any timers inserted/re-inserted will expire in [1, UINT64_MAX) ticks from current time.
    _origin = _currentTime;
    return expiredCount;
}

EFJ_MACRO void
timer_queue::insert_relative(void* timer, u64 initial, u64 duration, efj::timer_handle* handle)
{
    efj_panic_if(_size == efj::array_size(_entries));

    handle->timer = timer;

    u64 expireTime = _currentTime + initial;
    efj::timer_entry entry = { handle, duration, expireTime };

    return _insert(entry);
}

EFJ_MACRO void
timer_queue::insert_absolute(void* timer, u64 expireTime, u64 duration, efj::timer_handle* handle)
{
    efj_panic_if(_size == efj::array_size(_entries));

    handle->timer = timer;
    efj::timer_entry entry = { handle, duration, expireTime };

    return _insert(entry);
}

// We remove a timer by swapping it with it's parent until it's the root and then removing it.
inline void
timer_queue::remove(efj::timer_handle* handle)
{
    if constexpr (TIMER_QUEUE_LOG_INPUTS) {
        efj::print("[%p] timer_queue::remove: %p\n", this, handle);
    }

    umm offset = handle->entry - _entries;
    efj_panic_if(offset >= _size);

    umm i = offset;
    while (i != 0) {
        umm parent = i >> 1;

        // IMPORTANT: A swap on the entries is not enough. We need to update the handle->entry back references of any
        // parent nodes we replace on our way up the tree.
        // We don't have to update the newly-inserted entry's back reference until it's in its final position.
        efj::swap(_entries[parent], _entries[i]);
        _entries[i].handle->entry = &_entries[i];
        i = parent;
    }

    _pop();
    handle->entry = nullptr;
}

inline void
timer_queue::set_time(u64 time)
{
    if constexpr (TIMER_QUEUE_LOG_INPUTS) {
        efj::print("[%p] timer_queue::set_time: %llu\n", this, (efj::llu)time);
    }

    _currentTime = time;
}

inline u64
timer_queue::wait_time() const
{
    if (!_size)
        return efj::timer_queue_infinite;

    const efj::timer_entry* next = &_entries[0];

    // Using the origin lets us detect origin < expireTime < currentTime,
    // which implies the expireTime is in the past.
    u64 expireTime  = next->expireTime  - _origin;
    u64 currentTime = _currentTime - _origin;
    if (expireTime > currentTime)
        return expireTime - currentTime;

    return 0;
}

inline u64
timer_queue::next_expire_time() const
{
    efj_panic_if(!_size);
    const efj::timer_entry* next = &_entries[0];
    return next->expireTime;
}
} // namespace efj
