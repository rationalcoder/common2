namespace efj
{

template <typename T> EFJ_MACRO const T* user_event::as() const
{ return value->as<T>(); }

template <typename T> EFJ_MACRO T* user_event::as()
{ return value->as<T>(); }


void _subscribe(efj::object* o, u32 id, efj::callback cb, efj::sub_type subType,
    const void** cached, const efj::component_info* info);

template <typename ClassT, typename ComponentT>
struct component_cast_notify_caller
{
    static void notify(efj::callback* cb, void* e)
    {
        const auto* userEvent = (efj::user_event*)e;
        const ComponentT* event = userEvent->value->as<ComponentT>();
        if (!event)
        {
            efj_panic("User event type doesn't match provided callback");
            return;
        }

        efj::_object_dispatch_event<ClassT>(cb, const_cast<ComponentT*>(event));
    }
};

template <typename ClassT> inline void
subscribe(u32 id, void (ClassT::*callback)(const efj::user_event& e),
          efj::sub_type subType)
{
    efj::object* o = efj::this_object();
    efj_panic_if(!o);

    efj::callback cb = efj::_make_object_callback(o, callback);
    _subscribe(o, id, cb, subType, nullptr, nullptr);
}

template <typename ClassT, typename ComponentT> inline void
subscribe(u32 id, void (ClassT::*callback)(const ComponentT& e),
          efj::sub_type subType)
{
    efj::object* o = efj::this_object();
    efj_panic_if(!o);

    efj::callback_caller* caller = &efj::component_cast_notify_caller<ClassT, ComponentT>::notify;
    efj::callback cb = efj::_make_custom_callback(o, callback, caller);
    _subscribe(o, id, cb, subType, nullptr, nullptr);
}

template <typename ClassT, typename ComponentT> inline void
subscribe_cached(u32 id, const ComponentT** value,
    void (ClassT::*callback)(const efj::user_event& e), efj::sub_type subType)
{
    using type_without_cv = typename std::remove_cv<ComponentT>::type;

    efj::object* o = efj::this_object();
    efj_panic_if(!o);

    efj::callback cb = efj::_make_object_callback(o, callback);
    _subscribe(o, id, cb, subType, (const void**)value, efj::info_of<type_without_cv>());
}

template <typename ClassT, typename ComponentT> inline void
subscribe_cached(u32 id, const ComponentT** value, 
    void (ClassT::*callback)(const ComponentT& e), efj::sub_type subType)
{
    using type_without_cv = typename std::remove_cv<ComponentT>::type;

    efj::object* o = efj::this_object();
    efj_panic_if(!o);

    efj::callback_caller* caller = &efj::component_cast_notify_caller<ClassT, type_without_cv>::notify;
    efj::callback cb = efj::_make_custom_callback(o, callback, caller);
    _subscribe(o, id, cb, subType, (const void**)value, efj::info_of<type_without_cv>());
}

template <typename ComponentT> EFJ_MACRO void
trigger_from(efj::object* o, u32 id, const ComponentT& e)
{
    efj::component_view view = { efj::info_of<ComponentT>(), &e };
    trigger_from(o, id, view);
}

template <typename ComponentT> EFJ_MACRO void
send_event_from(efj::object* source, efj::object* target, u32 id, const ComponentT& e)
{
    efj::component_view view = { efj::info_of<ComponentT>(), &e };
    send_event_from(source, target, id, view);
}

} // namespace efj
