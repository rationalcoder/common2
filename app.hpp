namespace efj
{

#ifdef EFJ_ENABLE_USER_CONTEXT
inline void init_user_context(efj::user_execution_context* ctx);
#else
struct user_execution_context {};
inline void init_user_context(efj::user_execution_context* ctx) {}
#endif

struct execution_context
{
    // Defaults to a heap allocator. Set to event-local memory in the context of an event, which
    // will almost always be the case outside of program initialization. This was done to
    // make sure structs and EFJ_OBJECTs that use efj::current_allocator() before the event loop
    // has started get a valid allocator. At any rate, some test programs and tools might not need
    // to start an event loop, and efj::current_allocator() needs to be valid for them, too.
    efj::allocator              curAllocator;

    // NOTE: perm is for things like event user data structs that are stored per object.
    // There shouldn't be too many of those types of things per object, and the minimum
    // allocation for an arena is 1 page, so 1 arena per efj object would be wasteful.
    // Temp and event are arenas with event scope. There needs to be two b/c
    // we want the things like message allocation to be as ergonomic as possible, and those
    // need to have event scope by default. The thing is, all procedures need a place to
    // allocate permanent memory and temporary memory, b/c temporary memory is expected to
    // act like a simple scratch space. If both temp and event allocators point to the same
    // memory arena, freeing temporary memory will either have to be a no-op or it will clobber
    // event-local allocations, both of which are problems. -bmartin 2/20/19
    efj::memory_arena           permMemory;
    efj::memory_arena           eventMemory;
    efj::memory_arena           tempMemory;

    // These allocators are per-thread b/c they provide non-thread-safe APIs for things besides normal allocation.
    // We could solve that problem by making their internals thread-safe with TLS or mutexes or whatever, by I chose
    // to do this to keep most of the thread-local stuff in one place.
    efj::heap_allocator         heapAllocator;
    efj::linear_allocator       linearAllocator;
    efj::arena_allocator        arenaAllocator;

    // State related to the current event being handled by a thread. Storing these supports
    // convenient free-function-mode APIs that would otherwise need to be passed state.
    efj::thread*                thisThread; // We have a current thread even if we don't have an object.
    efj::object*                thisObject;
    const char*                 thisEventName;
    int                         thisEpoll;
    int                         thisFd;
    flag32                      thisFdEvents;

    // Thread state that is convenient to have in the context.
    const char*                 name;
    u64                         realId;
    u32                         logicalId;

    bool                        logUnformatted;

    efj::user_execution_context user;
};

// I got paranoid about using metaprogramming to keep the execution context from getting a contructor,
// which generates slower context access code. I called this the :ContextConstructor problem, and at the time of writing,
// there is still code that mentions this issue. We now solve this by just having the pointer to the execution context be thread-local.
// We manually init these things once when initializing the context at the start of each thread.
//

// NOTE(bmartin): This kind of thread_local doesn't work with shared libraries. If we want to
// support them, we will need to manually allocate TLS (I think).

extern thread_local efj::execution_context* globalContext;
extern              efj::application        globalApp;

inline efj::application& app() { return globalApp; }
inline efj::execution_context& context() { return *globalContext; }
inline efj::user_execution_context& user_context() { return globalContext->user; }


//{ Context access

inline efj::allocator
set_allocator(efj::allocator alloc)
{
    efj_rare_assert(!alloc.is_null());

    efj::execution_context& ctx = efj::context();
    efj::allocator          old = ctx.curAllocator;

    ctx.curAllocator = alloc;
    return old;
}

EFJ_MACRO efj::allocator current_allocator()   { return efj::context().curAllocator; }

EFJ_MACRO efj::memory_arena& perm_memory()  { return efj::context().permMemory; }
EFJ_MACRO efj::memory_arena& event_memory() { return efj::context().eventMemory; }
EFJ_MACRO efj::memory_arena& temp_memory()  { return efj::context().tempMemory; }



inline auto this_object() -> efj::object* { return efj::context().thisObject; }
inline auto this_thread() -> efj::thread* { return efj::context().thisThread; }
inline int  this_epoll()                  { return efj::context().thisEpoll; }
inline int  this_fd()                     { return efj::context().thisFd; }
inline auto this_fd_events() -> flag32    { return efj::context().thisFdEvents; }

inline void _set_this_thread(efj::thread* t)   { efj::context().thisThread   = t; }
inline void _set_this_epoll(int fd)            { efj::context().thisEpoll    = fd; }
inline void _set_this_fd(int fd)               { efj::context().thisFd       = fd; }
inline void _set_this_fd_events(flag32 events) { efj::context().thisFdEvents = events; }

inline void
_set_this_object(efj::object* o EFJ_LOC_SIG)
{
#if EFJ_DEBUG_LOCATIONS
    efj::print("set_this_object(o=%s): %s:%d\n", o ? o->name : "(null)", loc.file, loc.line);
#endif

    efj::context().thisObject = o;
}


inline auto thread_name() -> const char* { return efj::context().name; }
inline u32  logical_thread_id() { return efj::context().logicalId; }
inline u64  real_thread_id()    { return efj::context().realId; }

//}


//{ Allocation stuff that needs to use the execution context.
#define efj_temp_scope() efj_arena_scope(efj::context().tempMemory)

EFJ_MACRO void*
allocate(umm size, umm alignment)
{
    efj::allocator alloc = efj::context().curAllocator;
    void* result = alloc.allocate(size, alignment);
    return result;
}

inline void*
allocate_array_copy(umm size, umm alignment, const void* data)
{
    void* space = efj::allocate(size, alignment);
    ::memcpy(space, data, size);

    return space;
}

inline void*
allocate_array_zero(umm size, umm alignment)
{
    void* space = efj::allocate(size, alignment);
    ::memset(space, 0, size);

    return space;
}

#define efj_allocate(size, alignment) efj::allocate((size), (alignment))
#define efj_allocate_bytes(n) efj::allocate((n), 1)
#define efj_allocate_array(n, type) (type*)efj::allocate((n)*sizeof(type), alignof(type))
#define efj_allocate_array_copy(n, type, data) (type*)efj::allocate_array_copy((n)*sizeof(type), alignof(type), (data))
#define efj_allocate_array_zero(n, type) (type*)efj::allocate_array_zero((n)*sizeof(type), alignof(type))
#define efj_allocate_type(type) ((type*)efj::allocate(sizeof(type), alignof(type)))
#define efj_new(type) efj_placement_new(efj::allocate(sizeof(type), alignof(type))) type
//}

enum thread_tasks : flag32
{
    thread_task_event_scheduling = 0x1,
    thread_task_logging          = 0x2,
    thread_task_config           = 0x4,
    thread_task_debugging        = 0x8,
    thread_task_watchdog         = 0x10,
};

struct thread
{
    const char*                  name;
    pthread_t                    pthread;
    u64                          realId; // filled in after the thread is created.
    u32                          logicalId;
    flag32                       logged;

    //{ @TS
    bool                         killed;
    bool                         waitingOnOtherThreads;
    bool                         crashed;
    //}

     // "thread-local" storage for data structures. Not actually in TLS.
    efj::memory_arena            arena;

    efj::array<u32>              cpus;
    int                          schedPolicy;
    int                          priority;
    flag32                       tasks;

    // NOTE(bmartin): Currently allocated out of the app arena, since this is read-only.
    efj::array<efj::object*>     objects;

    efj::eventsys_thread_ctx*    eventContext;
    efj::pcsys_thread_ctx*       pcContext;
    efj::uesys_thread_ctx*       ueContext;
    efj::logsys_thread_ctx*      logContext;
    efj::configsys_thread_ctx*   configContext;
    efj::debugsys_thread_ctx*    debugContext;
    efj::watchdog_thread_ctx*    watchdogContext;
    efj::testsys_thread_ctx*     testContext;

    template <typename... Objects_>
    void add_objects(Objects_&... objects);
    void add_object(efj::object* o);

    void add_cpu_affinity(u32 cpu);
    void set_priority(int policy, int pri);

    void use_for_event_scheduling(bool use);
    bool does_event_scheduling();

    void use_for_logging(bool use);
    bool does_logging();

    void use_for_config(bool use);
    bool does_config();

    void use_for_debugging(bool use);
    bool does_debugging();

    void use_for_watchdog(bool use);
    bool does_watchdog();
};

EFJ_MACRO efj::allocator get_heap_allocator() { return efj::context().heapAllocator; }
EFJ_MACRO efj::allocator get_event_allocator() { return efj::context().eventMemory; }
EFJ_MACRO efj::allocator get_temp_allocator() { return efj::context().tempMemory; }

EFJ_MACRO efj::heap_allocator* get_heap_allocator_impl() { return &efj::context().heapAllocator; }
EFJ_MACRO efj::linear_allocator* get_linear_allocator_impl() { return &efj::context().linearAllocator; }
EFJ_MACRO efj::arena_allocator* get_arena_allocator_impl() { return &efj::context().arenaAllocator; }

struct application
{
    application();

    // IMPORTANT(bmartin): The aplication struct MUST be global for initialization to
    // work correctly in all cases.
    bool                         _initted; // = 0 (false)

    // application storage for things with global scope like the list of objects.
    efj::memory_arena            _arena;

    efj::array<efj::thread*>     _threads { efj::delay_init };
    efj::array<efj::object*>     _objects { efj::delay_init };

    efj::eventsys_breakpoint     _threadStartedBp;

    efj::log_config              _logConfig;

    efj::event_system*           _eventSystem;
    efj::pc_system*              _pcSystem;
    efj::user_event_system*      _userEventSystem;
    efj::config_system*          _configSystem;
    efj::log_system*             _logSystem;
    efj::debug_system*           _debugSystem;
    efj::watchdog_system*        _watchdogSystem;
    efj::test_system*            _testSystem;

    // We need access to threads based on responsibilities.
    efj::thread*                 _mainThread;
    efj::thread*                 _loggingThread;
    efj::thread*                 _configThread;
    efj::thread*                 _eventSchedulingThread;
    efj::thread*                 _debugThread;
    efj::thread*                 _watchdogThread;

    // Max ID of events registered with _register_event_id(). Currently, event IDs
    // are assumed to be contiguous.
    s64                          _maxUserEventId;

    // Certain things are only allowed to happen during certain time,
    // and we need a way to assert this.
    //{ @TS
    bool                         _inittingObjects;
    bool                         _doneInittingObjects;
    bool                         _backgroundThreadsContinued;
    bool                         _started;
    bool                         _exiting;
    bool                         _assertionFailed;
    //}

    bool                         _exitEarly;
    int                          _earlyExitCode;

    umm                          _pageSize;

    // TODO: replace with just free-function implementations. No more free-functions calling methods,
    // since they are all implementation details, anyway.

    bool _init(int argc, const char** argv);
    int  _exit(int exitCode);

    auto _create_thread(const char* name) -> efj::thread*;

    void _begin_frame();
    bool _update(int* exitCode, int timeoutMs);
    void _end_frame();

    void _add_object(efj::object* o);
    bool _add_config_file(u32 id, const char* path, efj::config_type type);
};

//{ efj::application free-function API
inline bool asserted() { return __atomic_load_n(&efj::app()._assertionFailed, __ATOMIC_RELAXED); }

template <typename Object_> inline void set_logger(Object_& logger);
void set_log_file(const char* path);
void set_log_formatter(efj::log_formatter* formatter);
auto get_log_formatter() -> efj::log_formatter*;
auto default_log_formatter(int level, const char* file, int line, const efj::string& message) -> efj::string;

template <typename... Args_> inline void add_objects(Args_&... args);

inline bool init(int argc, const char** argv) { return efj::app()._init(argc, argv); }
inline int  exit(int code = efj::exit_normal) { return efj::app()._exit(code); }
inline void begin_frame() { efj::app()._begin_frame(); }
inline bool update(int* exitCode, int timeoutMs = -1) { return efj::app()._update(exitCode, timeoutMs); }
inline void end_frame() { efj::app()._end_frame(); }
inline int  exec(int argc, const char** argv);

inline auto objects() -> efj::array<efj::object*>& { return efj::app()._objects; }
inline auto threads() -> efj::array<efj::thread*>& { return efj::app()._threads; }
inline auto main_thread() -> efj::thread* { return efj::app()._threads[0]; }
inline auto logging_thread() -> efj::thread* { return efj::app()._loggingThread; }
inline auto config_thread() -> efj::thread* { return efj::app()._configThread; }
inline auto create_thread(const char* name) -> efj::thread* { return efj::app()._create_thread(name); }
inline u32  thread_count() { u32 count =  efj::app()._threads.size(); efj_assert(count); return count; }
inline u32  background_thread_count() { return efj::app()._threads.size() - 1; }

inline efj::thread*
thread_by_id(u32 ltid)
{
    if (ltid >= efj::app()._threads.size())
        return nullptr;

    return efj::app()._threads.at_unsafe(ltid);
}

inline umm
page_size()
{
    umm size = efj::app()._pageSize;
    efj_rare_assert(size >= 4_KB);
    return size;
}

inline int
exec(int argc, const char** argv)
{
    // Right now, we just let the event loop run even if init failed.
    /*bool initted = */efj::init(argc, argv);

    int exitCode = 0;
    for (;;) {
        efj::begin_frame();
        if (!efj::update(&exitCode))
            break;

        efj::end_frame();
        // Give the event loop a chance to run once if we failed to initialize.
        //if (!initted)
        //    break;
    }

    return efj::exit(exitCode);
}

} // namespace efj


