
namespace efj
{

struct fd;

struct fd_event
{
    efj::epoll_events events = 0;
    efj::fd*          fd     = nullptr;
};

void fd_create(const char* name, int fd, efj::epoll_events events, efj::fd* result);

// Create + monitor.
template <typename T> inline void
fd_add_os_handle(const char* name, int fd, efj::epoll_events events,
    efj::fd* result, void (T::*callback)(efj::fd_event&));

inline int fd_get_os_handle(efj::fd& fd);


template <typename T> inline void
fd_monitor(efj::fd& fd, efj::epoll_events events, void (T::*callback)(efj::fd_event&));

template <typename T> inline void
fd_monitor(efj::fd& fd, void (T::*callback)(efj::fd_event&));

inline void fd_monitor(efj::fd& fd);
//inline void fd_monitor(efj::fd& fd, flag32 events); TODO
inline void fd_unmonitor(efj::fd& fd);
inline bool fd_is_created(efj::fd& fd);
inline bool fd_is_monitored(efj::fd& fd);

bool make_nonblocking(int fd);
bool make_blocking(int fd);

inline bool make_nonblocking(efj::fd& fd);
inline bool make_blocking(efj::fd& fd);

inline ssize_t fd_write(efj::fd& fd, const void* buf, umm n);
inline ssize_t fd_read(efj::fd& fd, void* buf, umm n);
inline int fd_close(efj::fd& fd);

// TODO: Event API like the UDP one.

//}

} // namespace efj


