namespace efj
{

auto read_file(int fd, bool nulTerminate = false) -> efj::buffer;
auto read_file(const efj::string& path, bool nulTerminate = false) -> efj::buffer;

}
