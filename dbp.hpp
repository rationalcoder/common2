#pragma once

// NOTE: The intention was to not have to rely on pragma pack, but sizeof(<struct>) reports the size of a struct
// as if it was going to be used in an array, with padding added on the end to get back to the alignment of itself.
// We still make it a point for there to be no padding between members of these message structs, only at the end.
// Therefore, any message struct (event sub messages that only the parsers care about) should have pragma pack around them.
// If the struct ends with the same alignment of itself, pragma pack can be ommitted. The cost of accessing the fields
// of packed struct will mainly be felt by clients, which is fine since they will most likely be modern x86 platforms,
// which allow unaligned access at little to no cost. We assume that bandwidth is more important than extra instructions
// on the app side.
//
// Needs pragma pack:
// #pragma pack(push, 1)
// struct message
// {
//     uint64_t field1;
//     uint32_t field2;
// };
// #pragma pack(pop)
//
// Doesn't need pragma pack
// struct message
// {
//     uint64_t field1;
//     uint64_t field2;
// };

#define EFJ_DBP_VERSION_MAJOR 1
#define EFJ_DBP_VERSION_MINOR 0

#define EFJ_DBP_DEFAULT_PORT 65000

// This header needs to be standalone, since clients will need it.
#include <stdint.h>
#include <string.h> // memcpy, memmove

#ifndef EFJ_DBP_ASSERT
    #ifdef efj_assert
        #define EFJ_DBP_ASSERT efj_assert
    #else
        #include <assert.h>
        #define EFJ_DBP_ASSERT assert
    #endif
#endif

#ifndef EFJ_DBP_DEBUG
    #define EFJ_DBP_DEBUGGING 0
#else
    #define EFJ_DBP_DEBUGGING 1
#endif

#if EFJ_DBP_DEBUGGING
    #ifndef EFJ_DBP_LOG
        #include <stdio.h>
        #define EFJ_DBP_LOG(fmt, ...) printf(fmt "\n", __VA_ARGS__)
    #endif
#else
    #undef EFJ_DBP_LOG
    #define EFJ_DBP_LOG(fmt, ...)
#endif

#define EFJ_DBP_PARSE_BUF_MIN 4096

namespace efj
{

enum dbp_message_type
{
    dbp_message_invalid,
    dbp_message_connect,
    dbp_message_connect_resp,
    dbp_message_shutdown,
    dbp_message_shutdown_resp,
    dbp_message_register_thread,
    dbp_message_register_object,
    dbp_message_register_event,
    dbp_message_deregister_event,
    dbp_message_register_watch,
    dbp_message_thread_events,
    dbp_message_watch_edited,
    dbp_message_watch_edited_batch,
    dbp_message_thread_crashed,
    dbp_message_thread_exited,
    dbp_message_count_
};

enum dbp_value_type
{
    dbp_value_invalid,
    dbp_value_string,
    dbp_value_u64,
    dbp_value_s64,
    dbp_value_f64,
    dbp_value_u32,
    dbp_value_s32,
    dbp_value_f32,
    dbp_value_u16,
    dbp_value_s16,
    dbp_value_u8,
    dbp_value_s8,
    dbp_value_bool,
    dbp_value_count_
};

struct dbp_timestamp
{
    uint32_t sec;
    uint32_t nsec;
};

struct dbp_header
{
    uint32_t type;
    uint32_t size;
};

struct dbp_connect
{
    dbp_header header;
    uint32_t   versionMajor;
    uint32_t   versionMinor;
};

struct dbp_connect_resp
{
    dbp_header header;
    uint32_t   success;
    uint32_t   versionMajor;
    uint32_t   versionMinor;
};

struct dbp_shutdown
{
    dbp_header header;
};

struct dbp_shutdown_resp
{
    dbp_header header;
};

#pragma pack(push, 1)
struct dbp_register_thread
{
    dbp_header header;
    uint64_t   key;
    uint64_t   rtid;
    uint32_t   ltid;
    // char name[]; nul-terminated name after message.
    char*       name()       { return (char*)(this + 1); }
    const char* name() const { return (char*)(this + 1); }
};
#pragma pack(pop)

#pragma pack(push, 1)
struct dbp_register_object
{
    dbp_header header;
    uint64_t   key;
    uint32_t   ltid; // logical id of thread the object is on.
    uint32_t   id;
    // char name[]; nul-terminated name after message.

    char*       name()       { return (char*)(this + 1); }
    const char* name() const { return (char*)(this + 1); }
};
#pragma pack(pop)

#pragma pack(push, 1)
struct dbp_register_watch
{
    dbp_header header;
    uint64_t   watchKey;
    uint32_t   ltid;
    uint32_t   watchId;
    uint32_t   objectId;
    uint32_t   valueType;
    // char name[]; nul-terminated name after message.
    // type value; nul-terminated value for string, binary rep for others.

    char*       name()       { return (char*)(this + 1); }
    const char* name() const { return (char*)(this + 1); }

    // WARNING: This will likely not be aligned. so *(<prim>*)value() is UB.
    uint8_t*       value()       { return (uint8_t*)(this + 1) + (size_t)strlen(name()) + 1; }
    const uint8_t* value() const { return (uint8_t*)(this + 1) + (size_t)strlen(name()) + 1; }
};
#pragma pack(pop)

#pragma pack(push, 1)
struct dbp_register_event_type
{
    dbp_header header;
    uint16_t   id;
    // nul-terminated string

    char*       name()       { return (char*)(this + 1); }
    const char* name() const { return (char*)(this + 1); }
};
#pragma pack(pop)

struct dbp_event_field
{
    uint8_t type;
    // dynamic value based on type. strings are nul-terminated.

    uint8_t*       value()       { return (uint8_t*)(this + 1); }
    const uint8_t* value() const { return (uint8_t*)(this + 1); }
};

// IMPORTANT: These can come after thread-events with the registered ID, but _deregister_
// messages must come after thread-events with this ID and before any new events that
// have recycled the ID.
struct dbp_register_event
{
    dbp_header header;
    uint32_t   ltid;
    uint32_t   id; // thread-local source id.
    uint16_t   type; // id sent before through register_event_type
    // nul-terminated name

    char*       name()       { return (char*)(this + 1); }
    const char* name() const { return (char*)(this + 1); }
};

struct dbp_deregister_event
{
    dbp_header header;
    uint32_t   ltid;
    uint32_t   id; // thread-local source id.
};

// Can be sent individually or in a batch.
#pragma pack(push, 1)
struct dbp_watch_edited
{
    dbp_header header;
    uint64_t   watchKey;
    uint8_t    type;
    // dynamic value based on type. strings are nul-terminated.

    uint8_t*       value()       { return (uint8_t*)(this + 1); }
    const uint8_t* value() const { return (uint8_t*)(this + 1); }
};
#pragma pack(pop)

struct dbp_watch_edited_batch
{
    dbp_header header;
    uint32_t   ltid;
    uint32_t   count;
};

//{
struct dbp_thread_events
{
    dbp_header header;
    uint32_t   ltid;
};

enum dbp_event_data_type
{
    dbp_event_data_invalid  = 0,
    dbp_event_data_sentinel = 1,
    dbp_event_data_changes  = 2,
    dbp_event_data_notes    = 3,
    dbp_event_data_crash    = 4,
    dbp_event_data_exit     = 5,
    dbp_event_data_count_   = 6,
};

enum dbp_event_flags
{
    dbp_event_has_data = 0x1,
    dbp_event_is_stale = 0x2,
};

#pragma pack(push, 1)
struct dbp_thread_event
{
    dbp_timestamp begin;
    dbp_timestamp end;
    uint32_t      id;
    uint16_t      formatSize; // total bytes spent on event fields.
    uint8_t       flags;
};
#pragma pack(pop)

// Event data format:
// header1
// if header1 != sentinel:
// {
// header2
// data
// }

struct dbp_thread_event_data_header1 { uint8_t type; };
struct dbp_thread_event_data_header2 { uint16_t size; };

#pragma pack(push, 1)
struct dbp_watch_changed
{
    uint32_t watchId;
    uint8_t  type;
    // dynamic value based on type. strings are nul-terminated.
    //
    uint8_t*       value()       { return (uint8_t*)(this + 1); }
    const uint8_t* value() const { return (uint8_t*)(this + 1); }
};
#pragma pack(pop)

// These follow the event that exited/crashed.
// They don't really need headers, but they are really
// rare messages so it doesn't matter. This way, parsing is easier, and
// the messages will be the same if we start allowing these messages to
// come outside of the thread-events message in the future.

struct dbp_thread_crashed
{
    dbp_header header;
    uint32_t   ltid;
    int32_t    sig;
    // char sigName[]; // nul-terminated
    // char trace[]; // nul-terminated

    const char* signal_name() const { return (char*)(this + 1); }
    char*       signal_name()       { return (char*)(this + 1); }

    const char* trace() const { return signal_name() + (size_t)strlen(signal_name()) + 1; }
    char*       trace()       { return signal_name() + (size_t)strlen(signal_name()) + 1; }
};

struct dbp_thread_exited
{
    dbp_header header;
    uint32_t   ltid;
    int32_t    code;
};

//}

inline uint32_t
dbp_get_value_size(uint32_t type)
{
    switch (type) {
    case dbp_value_u64:
    case dbp_value_s64:
    case dbp_value_f64:  return 8;
    case dbp_value_u32:
    case dbp_value_s32:
    case dbp_value_f32:  return 4;
    case dbp_value_u16:
    case dbp_value_s16:  return 2;
    case dbp_value_u8:
    case dbp_value_s8:
    case dbp_value_bool: return 1;
    default:             return 0;
    }
}

inline uint32_t
dbp_get_value_size(uint32_t type, const uint8_t* value)
{
    uint32_t value_size = efj::dbp_get_value_size(type);
    if (value_size == 0) {
        value_size = (uint32_t)strlen((const char*)value) + 1;
    }

    return value_size;
}


// [PARSING]

enum dbp_token_type
{
    dbp_token_invalid                        = 1,
    dbp_token_unknown_begin                  = 2,
    dbp_token_unknown_content                = 3,
    dbp_token_unknown_end                    = 4,

    dbp_token_connect                        = 5,
    dbp_token_connect_resp                   = 6,
    dbp_token_shutdown                       = 7,
    dbp_token_shutdown_resp                  = 7,

    dbp_token_watch_edited_batch             = 9,

    dbp_token_deregister_event               = 10,

    // Dynamic messages have to have a begin, content, and end, in order.
    dbp_token_register_event_begin           = 11,
    dbp_token_register_event_content         = 12,
    dbp_token_register_event_end             = 13,
    dbp_token_register_thread_begin          = 14,
    dbp_token_register_thread_content        = 15,
    dbp_token_register_thread_end            = 16,
    dbp_token_register_object_begin          = 17,
    dbp_token_register_object_content        = 18,
    dbp_token_register_object_end            = 19,
    dbp_token_register_watch_begin           = 20,
    dbp_token_register_watch_content         = 21,
    dbp_token_register_watch_end             = 22,
    dbp_token_watch_edited_begin             = 23,
    dbp_token_watch_edited_content           = 24,
    dbp_token_watch_edited_end               = 25,

    //{ thread events or contents of thread events message
    dbp_token_thread_events_begin            = 26,
    dbp_token_thread_events_content          = 27,
    dbp_token_thread_events_end              = 28,

    dbp_token_thread_event                   = 29,
    dbp_token_event_format_begin             = 30,
    dbp_token_event_format_content           = 31,
    dbp_token_event_format_end               = 32,
    dbp_token_event_data_header1             = 33, // internal
    dbp_token_event_data_header2             = 34, // internal
    dbp_token_event_data_begin               = 35,
    dbp_token_event_data_content             = 36,
    dbp_token_event_data_end                 = 37,
    dbp_token_watch_changed_count            = 38,
    dbp_token_watch_changed_begin            = 39,
    dbp_token_watch_changed_content          = 40,
    dbp_token_watch_changed_end              = 41,
    dbp_token_thread_event_note_count        = 42,
    dbp_token_thread_event_note_begin        = 43,
    dbp_token_thread_event_note_content      = 44,
    dbp_token_thread_event_note_end          = 45,
    dbp_token_thread_crash_begin             = 46,
    dbp_token_thread_crash_content           = 47,
    dbp_token_thread_crash_end               = 48,
    dbp_token_thread_exit                    = 49,
    //}

    dbp_token_count_,
};

// TODO: General parser explanation. Explain what complete/incomplete token means, etc.

struct dbp_token
{
    dbp_token_type type;
    uint8_t*       data;
    size_t         size;
};

enum dbp_parser_state
{
    dbp_parser_state_invalid,
    dbp_parser_state_initial,
    dbp_parser_state_header,
    dbp_parser_state_dynamic,
    dbp_parser_state_fixed,
    dbp_parser_state_count_,
};

// TODO: better error reporting.
struct dbp_parser
{
    uint8_t* buf     = nullptr;
    uint8_t* at      = nullptr;
    uint8_t* end     = nullptr;
    uint8_t* parseAt = nullptr;

    const char* error     = nullptr;
    dbp_token   curToken  = {};
    dbp_token   prevToken = {};
    dbp_header  header    = {};

    dbp_parser_state state             = dbp_parser_state_invalid;
    dbp_token_type   dynamicBase       = dbp_token_invalid;
    uint32_t         tokenProgress     = 0;
    uint32_t         prevTokenProgress = 0;
    uint32_t         messageProgress   = 0;
    uint32_t         minBeginSize      = sizeof(dbp_header);

    void reset(void* buf, size_t size);
    const dbp_token* parse();

    inline const efj::dbp_token*
    _parse_fixed(const efj::dbp_header& header, uint32_t unparsedSize, efj::dbp_token_type type);

    inline const efj::dbp_token*
    _parse_dynamic(const efj::dbp_header& header, uint32_t unparsedSize, efj::dbp_token_type type);

    inline const efj::dbp_token* _complete_token();
    inline const efj::dbp_token* _incomplete_token();

    size_t read_size() const { return end - at; }
    void*  read_at() const { return at; }
    void   read_bytes(size_t size) { at += size; }
};

inline void
dbp_parser::reset(void* buf, size_t size)
{
    this->buf     = (uint8_t*)buf;
    this->at      = (uint8_t*)buf;
    this->parseAt = (uint8_t*)buf;
    this->end     = (uint8_t*)buf + size;
    this->state   = dbp_parser_state_initial;
    this->error   = nullptr;

    this->curToken          = {};
    this->curToken.data     = this->buf;
    this->prevToken         = {};
    this->tokenProgress     = 0;
    this->prevTokenProgress = 0;
    this->messageProgress   = 0;
    this->minBeginSize      = sizeof(dbp_header);
    this->dynamicBase       = dbp_token_invalid;
    this->header            = {};
}

inline const efj::dbp_token*
dbp_parser::_parse_dynamic(const efj::dbp_header& header, uint32_t unparsedSize, efj::dbp_token_type base)
{
    if (state != dbp_parser_state_dynamic) {
        tokenProgress += unparsedSize;

        // On certain messages, like thread events, having the client code accumulate to make sure that
        // they have at least the fixed part of the message is annoying, so don't return the begin token
        // until we have a minimum amount.
        if (tokenProgress < minBeginSize)
            return _incomplete_token();

        // If we have more than the minimum amount, truncate. We assume that the minimum amount can't be bigger
        // than size of the full message.
        if (tokenProgress > minBeginSize)
            tokenProgress = minBeginSize;

        if (minBeginSize > header.size) {
            error = "[dynamic begin]: Fixed size is bigger than the full message size";
            EFJ_DBP_ASSERT(false);
            return nullptr;
        }

        curToken.type   = base;
        messageProgress = tokenProgress;
        dynamicBase     = base;
        state           = dbp_parser_state_dynamic;
        return _complete_token();
    }

    if (messageProgress + unparsedSize < header.size) {
        curToken.type   = (dbp_token_type)(dynamicBase + 1); // *_content
        tokenProgress   += unparsedSize;
        messageProgress += unparsedSize;
        return _complete_token();
    }

    tokenProgress   += header.size - messageProgress;
    curToken.type   =  (dbp_token_type)(dynamicBase + 2); // *_end
    state           =  dbp_parser_state_header;
    messageProgress =  0;
    return _complete_token();
}

inline const efj::dbp_token*
dbp_parser::_parse_fixed(const efj::dbp_header& header, uint32_t unparsedSize, efj::dbp_token_type type)
{
    curToken.type = type;

    if (tokenProgress +  unparsedSize < header.size) {
        tokenProgress += unparsedSize;
        state         =  dbp_parser_state_fixed;
        return _incomplete_token();
    }

    tokenProgress = header.size;
    state         = dbp_parser_state_header;
    return _complete_token();
}

inline const efj::dbp_token*
dbp_parser::_complete_token()
{
    // Complete means the buffer contains enough room for the current token, and we have
    // finished parsing it. Parsing a full buffer of the minimum size will always end with
    // a complete token, since messages that can span multiple buffers are broken up into
    // sub tokens.
    curToken.size = tokenProgress;
    prevToken     = curToken;

    parseAt           += tokenProgress - prevTokenProgress;
    prevTokenProgress =  0;
    tokenProgress     =  0;

    // If we have parsed all currently read data and ended with a complete token, we can reset the
    // buffer without moving any data.
    if (parseAt == at) {
        at      = buf;
        parseAt = buf;
    }

    curToken.data = parseAt;

    //EFJ_DBP_LOG("Returning token: %d (%u)", prevToken.type, prevToken.size);

    return &prevToken;
}

inline const efj::dbp_token*
dbp_parser::_incomplete_token()
{
    // Incomplete means 1) There is enough room in the read buffer for the token, we just
    // haven't received enough data to complete it, or 2) the buffer is full, but ends with
    // a partial token, in which case we need to move the fragment to the beginning of the buffer
    // to make room for more data.
    parseAt           += tokenProgress - prevTokenProgress;
    prevTokenProgress =  tokenProgress;

    if (parseAt == end) {
        // We have a partial token at the end of the read buffer. Slide it down.
        // TODO: To be safe, we use memmove. Care can be taken with message sizes to make sure
        // this sliding can't result in overlapping regions.
        memmove(buf, curToken.data, tokenProgress);
        curToken.data = buf;
        at            = buf + tokenProgress;
        parseAt       = buf + tokenProgress;
    }

    return nullptr;
}

inline const efj::dbp_token*
dbp_parser::parse()
{
    uint32_t unparsedSize = (uint32_t)(at - parseAt);

    if (state == dbp_parser_state_invalid || error)
        return nullptr;

    if (unparsedSize == 0)
        return _incomplete_token();

    switch (state) {
    case dbp_parser_state_fixed:   return _parse_fixed(header,   unparsedSize, curToken.type);
    case dbp_parser_state_dynamic: return _parse_dynamic(header, unparsedSize, curToken.type);
    default: break;
    }

    // initial or header
    curToken.type = dbp_token_invalid;

    if (unparsedSize < sizeof(dbp_header)) {
        tokenProgress = unparsedSize;
        return _incomplete_token();
    }

    tokenProgress = sizeof(dbp_header);
    memcpy(&header, curToken.data, sizeof(header));

    if (header.type == dbp_message_invalid) {
        error = "Invalid message";
        EFJ_DBP_ASSERT(false);
        return nullptr;
    }

    //EFJ_DBP_LOG("parsed type: %d, size: %u", header.type, header.size);

    uint32_t remainingUnparsed = unparsedSize - sizeof(dbp_header);

    switch (header.type) {
    case dbp_message_connect:            return _parse_fixed(header, remainingUnparsed, dbp_token_connect);
    case dbp_message_connect_resp:       return _parse_fixed(header, remainingUnparsed, dbp_token_connect_resp);
    case dbp_message_shutdown:           return _parse_fixed(header, remainingUnparsed, dbp_token_shutdown);
    case dbp_message_shutdown_resp:      return _parse_fixed(header, remainingUnparsed, dbp_token_shutdown_resp);
    case dbp_message_watch_edited_batch: return _parse_fixed(header, remainingUnparsed, dbp_token_watch_edited_batch);
    case dbp_message_deregister_event:   return _parse_fixed(header, remainingUnparsed, dbp_token_deregister_event);

    case dbp_message_register_event:
        minBeginSize = sizeof(dbp_register_event);
        return _parse_dynamic(header, remainingUnparsed, dbp_token_register_event_begin);
    case dbp_message_register_thread:
        minBeginSize = sizeof(dbp_register_thread);
        return _parse_dynamic(header, remainingUnparsed, dbp_token_register_thread_begin);
    case dbp_message_register_object:
        minBeginSize = sizeof(dbp_register_object);
        return _parse_dynamic(header, remainingUnparsed, dbp_token_register_object_begin);
    case dbp_message_register_watch:
        minBeginSize = sizeof(dbp_register_watch);
        return _parse_dynamic(header, remainingUnparsed, dbp_token_register_watch_begin);
    case dbp_message_watch_edited:
        minBeginSize = sizeof(dbp_watch_edited);
        return _parse_dynamic(header, remainingUnparsed, dbp_token_watch_edited_begin);
    case dbp_message_thread_events:
        minBeginSize = sizeof(dbp_thread_events);
        return _parse_dynamic(header, remainingUnparsed, dbp_token_thread_events_begin);
    default:
        //EFJ_DBP_ASSERT(false);
        minBeginSize = sizeof(dbp_header);
        return _parse_dynamic(header, unparsedSize, dbp_token_unknown_begin);
    }

    error = "Internal parse error";
    EFJ_DBP_ASSERT(false);
    return nullptr;
}

enum dbp_event_parser_state
{
    dbp_event_parser_state_invalid                = 0,
    dbp_event_parser_state_initial                = 1,
    dbp_event_parser_state_header                 = 2,
    dbp_event_parser_state_event                  = 3,
    // We need "end" tokens but not "end" states.
    dbp_event_parser_state_event_format_begin     = 4,
    dbp_event_parser_state_event_format_content   = 5,
    dbp_event_parser_state_event_data_header1     = 6,
    dbp_event_parser_state_event_data_header2     = 7,
    dbp_event_parser_state_event_data_content     = 8,
    dbp_event_parser_state_event_data_fragment    = 9,
    dbp_event_parser_state_notes_header           = 10,
    dbp_event_parser_state_note_begin             = 11,
    dbp_event_parser_state_note_content           = 12,
    dbp_event_parser_state_changes_header         = 13,
    dbp_event_parser_state_change_begin           = 14,
    dbp_event_parser_state_change_primitive       = 15,
    dbp_event_parser_state_change_string          = 16,
    dbp_event_parser_state_crash_begin            = 17,
    dbp_event_parser_state_crash_content          = 18,
    dbp_event_parser_state_exit                   = 19,

    dbp_event_parser_state_count_
};

// Internal
struct dbp_event_data_parse_result
{
    dbp_event_parser_state nextState = dbp_event_parser_state_invalid;
    bool completedToken = false;
    bool done = false;
};

struct dbp_event_parser
{
    uint8_t* buf     = nullptr;
    uint8_t* at      = nullptr;
    uint8_t* end     = nullptr;
    uint8_t* parseAt = nullptr;

    uint32_t readProgress         = 0;
    uint32_t tokenProgress        = 0;
    uint32_t prevTokenProgress    = 0;
    uint16_t curFormatSize        = 0;
    uint16_t curFormatProgress    = 0;
    uint16_t curEventDataType     = 0;
    uint16_t curEventDataSize     = 0;
    uint16_t curEventDataProgress = 0;
    uint32_t notesLeft            = 0;
    uint32_t changesLeft          = 0;
    uint32_t changeValueSize      = 0;
    uint32_t threadCrashSize      = 0;
    uint32_t threadCrashProgress  = 0;
    uint8_t  eventFlags           = 0;

    dbp_event_parser_state state;
    dbp_event_parser_state fragmentedState;

    const char* error     = nullptr;
    dbp_token   curToken  = {};
    dbp_token   prevToken = {};

    void reset(void* buf, size_t size);
    bool read(const efj::dbp_token* tok);
    const efj::dbp_token* parse();
    const efj::dbp_token* _parse_event(uint32_t unparsedSize);
    const efj::dbp_token* _parse_event_format(uint32_t unparsedSize);
    const efj::dbp_token* _parse_event_data_headers(uint32_t unparsedSize);
    const efj::dbp_token* _parse_event_data(uint32_t unparsedSize);
    const efj::dbp_token* _parse_known_event_data(uint32_t unparsedSize);
    const efj::dbp_token* _parse_event_data_fragment(uint32_t unparsedSize);

    dbp_event_data_parse_result _parse_notes(uint32_t unparsedSize);
    dbp_event_data_parse_result _parse_changes(uint32_t unparsedSize);
    dbp_event_data_parse_result _parse_crash(uint32_t unparsedSize);
    dbp_event_data_parse_result _parse_exit(uint32_t unparsedSize);

    const efj::dbp_token* _complete_token();
    const efj::dbp_token* _incomplete_token();

    const efj::dbp_token* _incomplete_bytes();
    void _remove_bytes(const void* bytes, size_t size);
};

inline void
dbp_event_parser::reset(void* buf, size_t size)
{
    EFJ_DBP_ASSERT(size >= EFJ_DBP_PARSE_BUF_MIN);

    this->buf                  = (uint8_t*)buf;
    this->at                   = (uint8_t*)buf;
    this->parseAt              = (uint8_t*)buf;
    this->end                  = (uint8_t*)buf + size;
    this->state                = dbp_event_parser_state_initial;
    this->fragmentedState      = dbp_event_parser_state_invalid;
    this->error                = nullptr;

    this->curToken             = {};
    this->prevToken            = {};
    this->curToken.data        = this->buf;
    this->readProgress         = 0;
    this->tokenProgress        = 0;
    this->prevTokenProgress    = 0;
    this->curFormatSize        = 0;
    this->curFormatProgress    = 0;
    this->curEventDataType     = 0;
    this->curEventDataSize     = 0;
    this->curEventDataProgress = 0;
    this->notesLeft            = 0;
    this->changesLeft          = 0;
    this->changeValueSize      = 0;
    this->threadCrashSize      = 0;
    this->threadCrashProgress  = 0;
    this->eventFlags           = 0;
}

inline bool
dbp_event_parser::read(const efj::dbp_token* tok)
{
    //EFJ_DBP_LOG("=== read() pre, at = %u, parseAt = %u, tp = %u", at - buf, parseAt - buf, tokenProgress);

    if (tok->type == dbp_token_thread_events_begin) {
        //EFJ_DBP_LOG("\tSkipping begin");
        return false;
    }

    //EFJ_DBP_LOG("Tok size %u", tok->size);

    uint32_t remainingSize = (uint32_t)(tok->size - readProgress);
    if (remainingSize == 0) {
        readProgress = 0;
        //EFJ_DBP_LOG("\tRemaining size = 0");
        return false;
    }

    uint32_t copySize = (uint32_t)(end - at);
    if (copySize > remainingSize)
        copySize = remainingSize;

    if (copySize == 0) {
        error = "unable to read token";
        EFJ_DBP_ASSERT(false);
        return false;
    }

    memcpy(at, tok->data + readProgress, copySize);
    at           += copySize;
    readProgress += copySize;

    //EFJ_DBP_LOG("=== read(), at = %u, parseAt = %u, tp = %u", at - buf, parseAt - buf, tokenProgress);
    EFJ_DBP_ASSERT(at >= buf && at <= end && parseAt <= at);

    return true;
}

inline const efj::dbp_token*
dbp_event_parser::_parse_event(uint32_t unparsedSize)
{
    if (tokenProgress + unparsedSize < sizeof(dbp_thread_event)) {
        tokenProgress += unparsedSize;
        state = dbp_event_parser_state_event;
        return _incomplete_token();
    }
    tokenProgress = sizeof(dbp_thread_event);

    dbp_thread_event event = {};
    memcpy(&event, curToken.data, sizeof(dbp_thread_event));

    eventFlags           = event.flags;
    curFormatSize        = event.formatSize;
    curEventDataSize     = 0;
    curEventDataProgress = 0;

    curToken.type = dbp_token_thread_event;

    EFJ_DBP_LOG("Format size = %u", curFormatSize);

    // NOTE: It's more maintainable to just go to the next logical state regardless
    // of whether that type of data is actually present, but this was the easiest way of
    // handling formatSize == 0 at the time. Otherwise, format parsing would be the only
    // state that allowes unparsedSize == 0 to not result in an incomplete token.
    //
    if (curFormatSize) {
        state = dbp_event_parser_state_event_format_begin;
    }
    else {
        state = eventFlags & dbp_event_has_data
              ? dbp_event_parser_state_event_data_header1
              : dbp_event_parser_state_event;
    }

    return _complete_token();
}

inline const efj::dbp_token*
dbp_event_parser::_parse_event_format(uint32_t unparsedSize)
{
    if (unparsedSize == 0)
        return _incomplete_token();

    EFJ_DBP_LOG("Got here somehow");
    if (curFormatProgress + unparsedSize >= curFormatSize) {
        tokenProgress += unparsedSize;
        if (tokenProgress > curFormatSize)
            tokenProgress = curFormatSize;

        curFormatProgress += tokenProgress;

        curToken.type = dbp_token_event_format_end;
        state = eventFlags & dbp_event_has_data
              ? dbp_event_parser_state_event_data_header1
              : dbp_event_parser_state_event;

        return _complete_token();
    }

    tokenProgress     += unparsedSize;
    curFormatProgress += unparsedSize;

    // We don't really need a begin token for this right now, since the "begin"
    // data is currently in the event struct, but we do so just to follow the convention.
    // Well-written clients of the API shouldn't care either way, but still.

    if (state == dbp_event_parser_state_event_format_begin)
        curToken.type = dbp_token_event_format_begin;
    else
        curToken.type = dbp_token_event_format_content;

    state = dbp_event_parser_state_event_format_content;
    return _complete_token();
}

// Normal event data header parsing for when we are _not_ dealing with an event data fragments
// that need to be concatentated.
inline const efj::dbp_token*
dbp_event_parser::_parse_event_data_headers(uint32_t unparsedSize)
{
    if (unparsedSize == 0)
        return _incomplete_token();

    if (state == dbp_event_parser_state_event_data_header1) {
        EFJ_DBP_LOG("|\theader1");
        // This won't make sense until we rework the existing event data parsing to be based off total data size
        // instead of internal counts.
        //EFJ_DBP_ASSERT(curEventDataProgress == curEventDataSize);

        if (tokenProgress + unparsedSize < sizeof(dbp_thread_event_data_header1)) {
            tokenProgress += unparsedSize;
            return _incomplete_token();
        }
        tokenProgress = sizeof(dbp_thread_event_data_header1);

        dbp_thread_event_data_header1 header1 = {};
        memcpy(&header1, curToken.data, sizeof(dbp_thread_event_data_header1));
        state = header1.type == dbp_event_data_sentinel
              ? dbp_event_parser_state_event
              : dbp_event_parser_state_event_data_header2;

        EFJ_DBP_LOG("|\theader1 type: %u", header1.type);
        curEventDataType = header1.type;
        curToken.type = dbp_token_event_data_header1;
        return _complete_token();
    }

    if (state == dbp_event_parser_state_event_data_header2) {
        EFJ_DBP_LOG("|\theader2");
        //{ header2
        if (tokenProgress + unparsedSize < sizeof(dbp_thread_event_data_header2)) {
            tokenProgress += unparsedSize;
            return _incomplete_token();
        }
        tokenProgress = sizeof(dbp_thread_event_data_header2);

        dbp_thread_event_data_header2 header2 = {};
        memcpy(&header2, curToken.data, sizeof(dbp_thread_event_data_header2));
        //}

        curEventDataSize = header2.size;
        curEventDataProgress = 0;
        EFJ_DBP_LOG("|\theader2 size = %u", header2.size);

        switch (curEventDataType) {
        case dbp_event_data_changes: state = dbp_event_parser_state_changes_header; break;
        case dbp_event_data_notes:   state = dbp_event_parser_state_notes_header;   break;
        case dbp_event_data_crash:   state = dbp_event_parser_state_crash_begin;    break;
        case dbp_event_data_exit:    state = dbp_event_parser_state_exit;           break;
        default:
            state = dbp_event_parser_state_event_data_content;
            break;
        }
        curToken.type = dbp_token_event_data_header2;
        return _complete_token();
    }

    error = "invalid header parsing state";
    EFJ_DBP_ASSERT(!error);
    return nullptr;
}

// Generic event data block parsing for when we don't know the data type that's being sent.
inline const efj::dbp_token*
dbp_event_parser::_parse_event_data(uint32_t unparsedSize)
{
    EFJ_DBP_LOG("parse generic event data");

    if (unparsedSize == 0)
        return _incomplete_token();

    // We were unable to identify the current data block, so we are parsing it generically
    // and passing it through as unknown data. We don't have to worry about unknown data being
    // chunked up b/c there's no custom parser for it.
    if (curEventDataProgress + unparsedSize >= curEventDataSize) {
        tokenProgress += unparsedSize;
        if (tokenProgress > curEventDataSize)
            tokenProgress = curEventDataSize;

        // We know we're done, but this gives us something we can assert.
        curEventDataProgress += tokenProgress;

        curToken.type = dbp_token_event_data_end;
        state = dbp_event_parser_state_event_data_header1;
        EFJ_DBP_ASSERT(tokenProgress <= curEventDataSize);
        return _complete_token();
    }

    tokenProgress        += unparsedSize;
    curEventDataProgress += unparsedSize;
    curToken.type = dbp_token_event_data_content;
    EFJ_DBP_ASSERT(tokenProgress <= curEventDataSize);
    return _complete_token();
}

// In order to handle unknown event data types, the size of the event data must come before the event data
// itself. The event data may have have to be chunked up into multiple blocks when the event data can't be
// sent in full atomically. Therefore, all event data parsing goes through here so that the chunking is
// transparent to specialized event data parsing functions.
//
inline const efj::dbp_token*
dbp_event_parser::_parse_known_event_data(uint32_t unparsedSize)
{
    if (unparsedSize == 0)
        return _incomplete_token();

    EFJ_DBP_LOG("Event data...");
    // Limit the amount of unparsed size we can consume while parsing this data block to be the size of the
    // current data block.
    uint32_t dataRemainingInThisBlock = curEventDataSize - curEventDataProgress;
    uint32_t unparsedSizeInThisBlock = unparsedSize;
    if (unparsedSizeInThisBlock > dataRemainingInThisBlock)
        unparsedSizeInThisBlock = dataRemainingInThisBlock;

    // IMPORTANT: Calling this something different than prevTokenProgress b/c that's a member variable. Event data
    // parsing is still sort of a hack, so we need our own version since these parsers don't call the functions
    // that implicitly adjust prevTokenProgress. @Hack.
    uint32_t prevEventDataProgress = tokenProgress;
    dbp_event_data_parse_result parseResult = {};

    switch (state) {
    case dbp_event_parser_state_changes_header:   parseResult = _parse_changes(unparsedSizeInThisBlock); break;
    case dbp_event_parser_state_change_begin:     parseResult = _parse_changes(unparsedSizeInThisBlock); break;
    case dbp_event_parser_state_change_primitive: parseResult = _parse_changes(unparsedSizeInThisBlock); break;
    case dbp_event_parser_state_change_string:    parseResult = _parse_changes(unparsedSizeInThisBlock); break;
    case dbp_event_parser_state_notes_header:     parseResult = _parse_notes(unparsedSizeInThisBlock);   break;
    case dbp_event_parser_state_note_begin:       parseResult = _parse_notes(unparsedSizeInThisBlock);   break;
    case dbp_event_parser_state_note_content:     parseResult = _parse_notes(unparsedSizeInThisBlock);   break;
    case dbp_event_parser_state_crash_begin:      parseResult = _parse_crash(unparsedSizeInThisBlock);   break;
    case dbp_event_parser_state_crash_content:    parseResult = _parse_crash(unparsedSizeInThisBlock);   break;
    case dbp_event_parser_state_exit:             parseResult = _parse_exit(unparsedSizeInThisBlock);    break;
    default:
        error = "invalid code path";
        EFJ_DBP_ASSERT(!error);
        return nullptr;
    }

    if (parseResult.done) {
        // We are done parsing all the fragments for a whole logical event data block.
        EFJ_DBP_ASSERT(parseResult.completedToken && parseResult.nextState == dbp_event_parser_state_invalid);
        state = dbp_event_parser_state_event_data_header1;
        return _complete_token();
    }

    curEventDataProgress += tokenProgress - prevEventDataProgress;
    EFJ_DBP_LOG("Progress (%u/%u)", curEventDataProgress, curEventDataSize);

    // If there wasn't enough data, we need to see if we expect another data block of the same type.
    EFJ_DBP_ASSERT(curEventDataProgress <= curEventDataSize);
    if (curEventDataProgress == curEventDataSize) {
        EFJ_DBP_LOG("Fragment detected: (%u/%u)", curEventDataProgress, curEventDataSize);
        fragmentedState = parseResult.nextState == dbp_event_parser_state_invalid ? state : parseResult.nextState;
        state = dbp_event_parser_state_event_data_fragment;

        // We are parsing some specific type of event data that we know about.
        // If the specific parser completed a token, we don't have anything else to do.
        if (parseResult.completedToken)
            return _complete_token();
        else
            return parse();
    }

    if (parseResult.completedToken) {
        state = parseResult.nextState;
        return _complete_token();
    }

    // We didn't get a token, but we aren't out of unparsed data in this block yet,
    // so it really is an incomplete token.
    // Also, we assume that if a parse didn't return a complete token, there can't be a state transition.
    EFJ_DBP_ASSERT(parseResult.nextState == dbp_event_parser_state_invalid);
    return _incomplete_token();
}

// IMPORTANT: This state is currently special, since it's where we are transparently ignoring headers that
// are for data blocks that are logically part of the same data block but had to be sent in
// fragments for whatever reason. Here, we violate the original idea of always returning tokens
// that can reconstruct the input data perfectly. Instead, these excess data blocks are discarded
// as implementation details.
//
inline const efj::dbp_token*
dbp_event_parser::_parse_event_data_fragment(uint32_t unparsedSize)
{
    EFJ_DBP_LOG("event data fragment");
    // Don't adjust the token progress, since we are really just waiting for lookahead data.
    if (unparsedSize < sizeof(dbp_thread_event_data_header1)) {
        EFJ_DBP_LOG("| incomplete bytes 1");
        return _incomplete_bytes();
    }

    uint8_t* header1Start = curToken.data + tokenProgress;
    dbp_thread_event_data_header1 header1 = {};
    memcpy(&header1, header1Start, sizeof(dbp_thread_event_data_header1));
    EFJ_DBP_LOG("|\t header1 type=%u", header1.type);

    // We are expecting a continuation of the previous data type. No point in trying to be resilient against this.
    // We have to read the headers separately b/c the second one isn't present if the type is the sentinel.
    if (header1.type != curEventDataType) {
        error = "expected continuation of previous event data type";
        EFJ_DBP_ASSERT(!error);
        return nullptr;
    }

    if (unparsedSize < sizeof(dbp_thread_event_data_header1) + sizeof(dbp_thread_event_data_header2)) {
        EFJ_DBP_LOG("| incomplete bytes 2");
        return _incomplete_bytes();
    }

    uint8_t* header2Start = curToken.data + tokenProgress + sizeof(dbp_thread_event_data_header1);
    dbp_thread_event_data_header2 header2 = {};
    memcpy(&header2, header2Start, sizeof(dbp_thread_event_data_header2));

    EFJ_DBP_LOG("|\t header2 size=%u", header2.size);
    curEventDataSize     = header2.size;
    curEventDataProgress = 0;

    // Actually remove the header from the parse buffer to pretend that the data was contiguous.
    _remove_bytes(header1Start, sizeof(dbp_thread_event_data_header1) + sizeof(dbp_thread_event_data_header2));

    state = fragmentedState;
    return parse();
}

inline dbp_event_data_parse_result
dbp_event_parser::_parse_notes(uint32_t unparsedSize)
{
    dbp_event_data_parse_result result = {};

    if (unparsedSize == 0)
        return result;

    //EFJ_DBP_LOG("| parse notes");
    if (state == dbp_event_parser_state_notes_header) {
        if (tokenProgress + unparsedSize < sizeof(uint16_t)) {
            tokenProgress += unparsedSize;
            return result;
        }
        tokenProgress = sizeof(uint16_t);

        ((uint8_t*)&notesLeft)[0] = curToken.data[0];
        ((uint8_t*)&notesLeft)[1] = curToken.data[1];

        curToken.type = dbp_token_thread_event_note_begin;
        result.nextState = dbp_event_parser_state_note_begin;
        result.completedToken = true;
        return result;
    }

    for (uint32_t i = 0; i < unparsedSize; i++) {
        tokenProgress++;

        if (((char*)parseAt)[i] == '\0') {
            curToken.type = dbp_token_thread_event_note_end;

            if (notesLeft-- == 1) {
                // Last note.
                result.completedToken = true;
                result.done = true;
                return result;
            }

            result.nextState = dbp_event_parser_state_note_begin;
            result.completedToken = true;
            return result;
        }
    }

    // Current note hasn't terminated, yet.
    curToken.type = dbp_token_thread_event_note_content;
    result.nextState = dbp_event_parser_state_note_content;
    result.completedToken = true;
    return result;
}

inline dbp_event_data_parse_result
dbp_event_parser::_parse_changes(uint32_t unparsedSize)
{
    dbp_event_data_parse_result result = {};

    if (unparsedSize == 0)
        return result;

    EFJ_DBP_LOG("| parse changes");
    if (state == dbp_event_parser_state_changes_header) {
        EFJ_DBP_LOG("|\theader");
        if (tokenProgress + unparsedSize < sizeof(uint16_t)) {
            tokenProgress += unparsedSize;
            return result;
        }
        tokenProgress = sizeof(uint16_t);

        ((uint8_t*)&changesLeft)[0] = curToken.data[0];
        ((uint8_t*)&changesLeft)[1] = curToken.data[1];

        EFJ_DBP_LOG("|\tcount = %u", changesLeft);
        curToken.type = dbp_token_watch_changed_count;
        result.nextState = dbp_event_parser_state_change_begin;
        result.completedToken = true;
        return result;
    }

    if (state == dbp_event_parser_state_change_begin) {
        EFJ_DBP_LOG("|\tchange begin");
        if (tokenProgress + unparsedSize < sizeof(dbp_watch_changed)) {
            // Progress may overshoot. Clamp later. :DBPNeedsClamping :DBPClampProgress
            tokenProgress += unparsedSize;
            return result;
        }
        tokenProgress = sizeof(dbp_watch_changed);

        uint8_t type = curToken.data[offsetof(dbp_watch_changed, type)];
        EFJ_DBP_LOG("|\t\ttype: %u, offset: %u", type, offsetof(dbp_watch_changed, type));

        changeValueSize = dbp_get_value_size(type);
        if (changeValueSize == 0 && type != dbp_value_string) {
            error = "[watch changed]: Invalid watch changed type";
            EFJ_DBP_ASSERT(!error);
            return result;
        }

        curToken.type = dbp_token_watch_changed_begin;
        result.nextState = changeValueSize == 0
              ? dbp_event_parser_state_change_string
              : dbp_event_parser_state_change_primitive;

        result.completedToken = true;
        return result;
    }

    if (state == dbp_event_parser_state_change_primitive) {
        if (tokenProgress + unparsedSize < changeValueSize) {
            tokenProgress += unparsedSize;
            //EFJ_DBP_LOG("| incomplete changes prim");
            return result;
        }
        tokenProgress = changeValueSize;
        curToken.type = dbp_token_watch_changed_end;

        if (changesLeft-- == 1) {
            // Last change.
            result.completedToken = true;
            result.done = true;
            return result;
        }

        result.nextState = dbp_event_parser_state_change_begin;
        result.completedToken = true;
        return result;
    }

    if (state == dbp_event_parser_state_change_string) {
        for (uint32_t i = 0; i < unparsedSize; i++) {
            tokenProgress++;

            if (((char*)parseAt)[i] == '\0') {
                curToken.type = dbp_token_watch_changed_end;

                if (changesLeft-- == 1) {
                    // Last change.
                    result.completedToken = true;
                    result.done = true;
                    return result;
                }

                result.nextState = dbp_event_parser_state_change_begin;
                result.completedToken = true;
                return result;
            }
        }

        curToken.type = dbp_token_watch_changed_content;
        result.completedToken = true;
        return result;
    }

    error = "[watch changed]: invalid code path";
    EFJ_DBP_ASSERT(!error);
    return result;
}

inline dbp_event_data_parse_result
dbp_event_parser::_parse_crash(uint32_t unparsedSize)
{
    dbp_event_data_parse_result result = {};

    if (unparsedSize == 0)
        return result;

    if (state == dbp_event_parser_state_crash_begin) {
        if (tokenProgress + unparsedSize < sizeof(dbp_thread_crashed)) {
            tokenProgress += unparsedSize;
            return result;
        }
        tokenProgress = sizeof(dbp_thread_crashed);

        dbp_header header = {};
        memcpy(&header, curToken.data, sizeof(header));

        if (header.type != dbp_message_thread_crashed) {
            error = "expected thread crash";
            EFJ_DBP_ASSERT(!error);
            return result;
        }

        threadCrashSize     = header.size;
        threadCrashProgress = tokenProgress;

        curToken.type = dbp_token_thread_crash_begin;
        result.nextState = dbp_event_parser_state_crash_content;
        result.completedToken = true;
        return result;
    }

    if (threadCrashProgress + unparsedSize < threadCrashSize) {
        tokenProgress       += unparsedSize;
        threadCrashProgress += unparsedSize;
        curToken.type       =  dbp_token_thread_crash_content;

        result.completedToken = true;
        return result;
    }

    tokenProgress = threadCrashSize - threadCrashProgress;
    curToken.type = dbp_token_thread_crash_end;

    result.completedToken = true;
    result.done = true;
    return result;
}

inline dbp_event_data_parse_result
dbp_event_parser::_parse_exit(uint32_t unparsedSize)
{
    dbp_event_data_parse_result result = {};

    if (unparsedSize == 0)
        return result;

    if (tokenProgress + unparsedSize < sizeof(dbp_thread_exited)) {
        tokenProgress += unparsedSize;
        return result;
    }
    tokenProgress = sizeof(dbp_thread_exited);

    dbp_header header = {};
    memcpy(&header, curToken.data, sizeof(header));
    if (header.type != dbp_message_thread_exited) {
        error = "expected thread exit";
        EFJ_DBP_ASSERT(!error);
        return result;
    }

    curToken.type = dbp_token_thread_exit;
    result.completedToken = true;
    result.done = true;
    return result;
}

inline const efj::dbp_token*
dbp_event_parser::parse()
{
    if (error) return nullptr;
    EFJ_DBP_LOG("State: %d", state);

    const dbp_token* result = nullptr;

    uint32_t unparsedSize = (uint32_t)(at - parseAt);

    switch (state) {
    case dbp_event_parser_state_initial:              result = _parse_event(unparsedSize);               break;
    case dbp_event_parser_state_event:                result = _parse_event(unparsedSize);               break;
    case dbp_event_parser_state_event_format_begin:   result = _parse_event_format(unparsedSize);        break;
    case dbp_event_parser_state_event_format_content: result = _parse_event_format(unparsedSize);        break;
    case dbp_event_parser_state_event_data_header1:   result = _parse_event_data_headers(unparsedSize);  break;
    case dbp_event_parser_state_event_data_header2:   result = _parse_event_data_headers(unparsedSize);  break;
    case dbp_event_parser_state_event_data_content:   result = _parse_event_data(unparsedSize);          break;
    case dbp_event_parser_state_event_data_fragment:  result = _parse_event_data_fragment(unparsedSize); break;
    case dbp_event_parser_state_notes_header:         result = _parse_known_event_data(unparsedSize);    break;
    case dbp_event_parser_state_note_begin:           result = _parse_known_event_data(unparsedSize);    break;
    case dbp_event_parser_state_note_content:         result = _parse_known_event_data(unparsedSize);    break;
    case dbp_event_parser_state_changes_header:       result = _parse_known_event_data(unparsedSize);    break;
    case dbp_event_parser_state_change_begin:         result = _parse_known_event_data(unparsedSize);    break;
    case dbp_event_parser_state_change_primitive:     result = _parse_known_event_data(unparsedSize);    break;
    case dbp_event_parser_state_change_string:        result = _parse_known_event_data(unparsedSize);    break;
    case dbp_event_parser_state_crash_begin:          result = _parse_known_event_data(unparsedSize);    break;
    case dbp_event_parser_state_crash_content:        result = _parse_known_event_data(unparsedSize);    break;
    case dbp_event_parser_state_exit:                 result = _parse_known_event_data(unparsedSize);    break;
    }

    if (!result) {
        EFJ_DBP_LOG("Returning NULL tok");
    }
    else {
        EFJ_DBP_LOG("Returning tok %u %u %p", result->type, result->size, result->data);
    }

    return result;
}

inline const efj::dbp_token*
dbp_event_parser::_complete_token()
{
    //EFJ_DBP_LOG("=== Complete token pre, at = %u, parseAt = %u, tp = %u, ctd = %un", at - buf, parseAt - buf, tokenProgress, curToken.data - buf);

    // Complete means the buffer contains enough room for the current token, and we have
    // finished parsing it. Parsing a full buffer of the minimum size will always end with
    // a complete token, since messages that can span multiple buffers are broken up into
    // sub tokens.

    curToken.size = tokenProgress;
    prevToken     = curToken;

    // Previous progress is only for remembering incomplete token progress. Once we complete a token
    // and start a new one, the all token progress made in the next parse() should be considered.
    parseAt           += tokenProgress - prevTokenProgress;
    prevTokenProgress =  0;
    tokenProgress     =  0;

    // If we have parsed all currently read data and ended with a complete token, we can reset the
    // buffer without moving any data.
    if (parseAt == at) {
        at      = buf;
        parseAt = buf;
        curToken.data = buf;
        //EFJ_DBP_LOG("\tComplete token reset post, at = %u, parseAt = %u, tp = %u, ctd = %u", at - buf, parseAt - buf, tokenProgress, curToken.data - buf);
    }
    else {
        curToken.data = parseAt;
        //EFJ_DBP_LOG("=== Complete token normal, at = %u, parseAt = %u, tp = %u, ctd = %u", at - buf, parseAt - buf, tokenProgress, curToken.data - buf);
    }

    // For easier debugging. Tokens of zero length are valid, but we want messed up token copying logic to always crash.
    if (prevToken.size == 0)
        prevToken.data = nullptr;

    return &prevToken;
}

inline const efj::dbp_token*
dbp_event_parser::_incomplete_token()
{
    //EFJ_DBP_LOG("=== Incomplete token pre, at = %u, parseAt = %u, tp = %u, ptp = %u, ctd = %u", at - buf, parseAt - buf, tokenProgress, prevTokenProgress, curToken.data - buf);

    // Incomplete means 1) There is enough room in the read buffer for the token, we just
    // haven't received enough data to complete it, or 2) the buffer is full, but ends with
    // a partial token, in which case we need to move the fragment to the beginning of the buffer
    // to make room for more data.
    parseAt           += tokenProgress - prevTokenProgress;
    prevTokenProgress =  tokenProgress;

    if (parseAt == end) {
        // We have a partial token at the end of the read buffer. Slide it down.
        memmove(buf, curToken.data, tokenProgress);
        curToken.data = buf;
        at            = buf + tokenProgress;
        parseAt       = buf + tokenProgress;
        //EFJ_DBP_LOG("\tIncomplete token reset post, at = %u, parseAt = %u, tp = %u, ctd = %u", at - buf, parseAt - buf, tokenProgress, curToken.data - buf);
    }
    else {
        //EFJ_DBP_LOG("=== Incomplete token normal, at = %u, parseAt = %u, tp = %u, ctd = %u", at - buf, parseAt - buf, tokenProgress, curToken.data - buf);
    }

    return nullptr;
}

// This is like incomplete_token(), except we aren't actually making any token progress, and we are waiting for
// a certain amount of unparsed size to be available.
inline const efj::dbp_token*
dbp_event_parser::_incomplete_bytes()
{
    // If we need more data, but can't read any more into the parse buffer, we
    if (at == end) {
        size_t tokenProgressPlusUnparsed = end - curToken.data;
        // We have a partial token at the end of the read buffer. Slide it down.
        memmove(buf, curToken.data, tokenProgressPlusUnparsed);
        curToken.data = buf;
        at            = buf + tokenProgressPlusUnparsed;
        parseAt       = buf + tokenProgress; // We aren't making any parsing progress.
        //EFJ_DBP_LOG("\tIncomplete token reset post, at = %u, parseAt = %u, tp = %u, ctd = %u", at - buf, parseAt - buf, tokenProgress, curToken.data - buf);
    }

    return nullptr;
}

//
inline void
dbp_event_parser::_remove_bytes(const void* bytes, size_t size)
{
    uint8_t* removeBegin = (uint8_t*)bytes;
    uint8_t* removeEnd   = (uint8_t*)bytes + size;

    EFJ_DBP_ASSERT(removeBegin >= buf && removeBegin < end);
    EFJ_DBP_ASSERT(removeEnd >= removeBegin && removeEnd <= end);

    memmove(removeBegin, removeEnd, at - removeEnd);
    at -= size;
}

inline bool
dbp_accumulate(const efj::dbp_token* tok, void* buf, size_t size, size_t* progress)
{
    size_t copyAmount = size - *progress;
    if (copyAmount == 0)
        return true;

    if (tok->size < copyAmount)
        copyAmount = tok->size;

    memcpy((uint8_t*)buf + *progress, tok->data, copyAmount);
    *progress += copyAmount;

    return false;
}

inline uint32_t
dbp_to_ms(const efj::dbp_timestamp& time)
{
    return time.sec * 1000 + time.nsec / 1000000;
}

inline uint32_t
dbp_to_us(const efj::dbp_timestamp& time)
{
    return time.sec * 1000000 + time.nsec / 1000;
}

inline uint64_t
dbp_to_ns(const efj::dbp_timestamp& time)
{
    return (uint64_t)time.sec * 1000000000 + time.nsec;
}

} // namespace efj

