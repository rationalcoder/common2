namespace efj
{

inline void
test_output_queue::push(efj::test_output_type type, efj::object* source, efj::object* target,
    efj::producer* p, u32 id, const efj::component_view* components, umm count)
{
    efj::memory_arena* arena = _queue.push_begin();
    efj::test_output* output = efj_push_new(*arena, efj::test_output)();
    output->type     = type;
    output->source   = source;
    output->target   = target;
    output->producer = p;
    output->eventId  = id;

    efj::component* values = efj_push_array(*arena, count, efj::component);
    for (umm i = 0; i < count; i++)
        efj_placement_new(&values[i]) efj::component{components[i]};

    output->values = efj::view_of(values, count);
    _queue.push_end();
}

void _test_trigger(u32 id, const efj::component_view& c);
void _test_send_event(efj::object* to, u32 id, const efj::component_view& c);
void _test_disconnect_output(efj::object* o);
void _test_capture(efj::object* o, efj::test_output_queue* queue);
void _test_capture(efj::producer* o, efj::test_output_queue* queue);

template <typename ObjectT> inline void
test_enable_log_level(efj::log_level level)
{
    for (efj::object* o : efj::objects()) {
        if (strcmp(o->name, ObjectT::efj_get_name()) == 0) {
            efj::log_enable_level(o, level);
        }
    }
}

template <typename ObjectT> inline void
test_enable_log_level(ObjectT* o, efj::log_level level)
{
    efj::log_enable_level(&o->efj_object, level);
}

template <typename ObjectT> inline ObjectT*
test_get_object()
{
    efj_assert(efj::logical_thread_id() == 0);

    for (efj::object* o : efj::objects()) {
        if (strcmp(o->name, ObjectT::efj_get_name()) == 0) {
            return (ObjectT*)o;
        }
    }

    return nullptr;
}

template <typename ObjectT> inline void
test_disable(ObjectT* o)
{
    o->efj_object.enabled = false;
}

template <typename ObjectT> inline void
test_disable()
{
    test_disable(test_get_object<ObjectT>());
}


template <typename ObjectT> inline void
test_disable_all_but()
{
    test_diable_all_but(test_get_object<ObjectT>());
}

template <typename ObjectT> inline void
test_disable_all_but(ObjectT* oneToKeep)
{
    for (efj::object* o : efj::objects()) {
        if (o == &oneToKeep->efj_object)
            continue;

        o->enabled = false;
    }
}

template <typename ObjectT> inline void
test_disconnect_output(ObjectT* o)
{
    static_assert(efj::is_object<ObjectT>::value, "expected an EFJ_OBJECT");
    _test_disconnect_output(&o->efj_object);
}

template <typename ObjectT> inline void
test_capture(ObjectT* o, efj::test_output_queue* queue)
{
    static_assert(efj::is_object<ObjectT>::value, "expected an EFJ_OBJECT");
    _test_capture(&o->efj_object, queue);
}

template <typename T> inline void
test_capture(efj::producer_of<T>& p, efj::test_output_queue* queue)
{
    _test_capture(&p, queue);
}

template <typename... ArgsT> inline void
test_produce(efj::consumer_of<ArgsT...>& c, typename efj::type_forward<ArgsT>::type&... args)
{
    c.test_consume(args...);
}

template <typename EventT> inline void
test_trigger(u32 id, const EventT& e)
{
    efj::component_info* info = efj::info_of<EventT>();
    efj_panic_if(!info->is_trivial());

    _test_trigger(id, {info, &e});
}

template <typename EventT, typename ObjectT> inline void
test_send_event(ObjectT* o, u32 id, const EventT& e)
{
    static_assert(efj::is_object<ObjectT>::value, "expected EFJ_OBJECT");

    efj::component_info* info = efj::info_of<EventT>();
    efj_panic_if(!info->is_trivial());

    _test_send_event(&o->efj_object, id, {info, &e});
}

} // namespace efj
