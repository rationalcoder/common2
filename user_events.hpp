
namespace efj
{

// This means user events can't have dynamically allocated members.
// If you have a string or something, it has to point to memory that's also in the struct.
// We have this restriction to simplify and optimize the implementation.
#define EFJ_DEFINE_USER_EVENT(name) EFJ_DEFINE_TRIVIAL_COMPONENT(name, "1.0")

enum sub_type
{
    sub_invalid,
    //! Any subscribers on the thread of the triggering object are called synchronously, inline.
    //! For other threads, the event is queued, and subscribed objects handle the event when their
    //! thread pulls the event of its queue. The order that subscribed objects see events from one
    //! object is the same order they were triggered in, but events from multiple objects may be interleaved.
    sub_async,

    //! Subscribed objects handle the event one after another, without no other user events
    //! or normal events intervening, on the thread they live on.
    sub_sync,

    sub_count_
};

struct user_event
{
    efj::object*               sender = nullptr;
    u32                        type   = 0; // user-defined enum value.
    const efj::component_view* value  = nullptr;

    template <typename T> EFJ_MACRO const T* as() const;
    template <typename T> EFJ_MACRO T* as();
};

// These register event IDs 0-n to the system.
void register_user_event(u32 n);
void register_user_events(u32 n);

//! Registers a generic event handler that handles a specific event type.
template <typename ClassT, typename ComponentT> inline void
subscribe(u32, void (ClassT::*callback)(const ComponentT& e),
    efj::sub_type = efj::sub_async);

//! Registers a generic event handler that can handle multiple event types.
template <typename ClassT> inline void
subscribe(u32 id, void (ClassT::*callback)(const efj::user_event& e), efj::sub_type = efj::sub_async);

//! Used to update a pointer to thread-local copy of the latest value for an event, avoiding having
//! to maintain a copy of common things like config structs.
//!
//! \usage
//!
//! struct Config
//! {
//!     int one;
//!     int two;
//! };
//! EFJ_DEFINE_USER_EVENT(Config);
//!
//! struct Foo
//! {
//!     const Config* _config;
//!     // ...
//!     void onConfig(const Confg& cfg);
//! };
//!
//! bool Foo::init()
//! {
//!     efj::subscribe(EVENT_CONFIG, &_config, &Foo::onConfig);
//!     return true;
//! }
//!
template <typename ClassT, typename ComponentT> inline void
subscribe_cached(u32 id, const ComponentT** value,
    void (ClassT::*callback)(const efj::user_event& e), efj::sub_type subType);
template <typename ClassT, typename ComponentT> inline void
subscribe_cached(u32 id, const ComponentT** value,
    void (ClassT::*callback)(const ComponentT& e), efj::sub_type = efj::sub_async);

void trigger_from(efj::object* o, u32 id, const efj::component_view& e);

template <typename ComponentT> EFJ_MACRO void
trigger_from(efj::object* o, u32 id, const ComponentT& e);

// TODO: Always using efj::this_object() would probably be fine? It shouldn't be much slower.

#define efj_trigger(...) efj::trigger_from(&this->efj_object, __VA_ARGS__)
#define efj_trigger_from(o, ...) efj::trigger_from(&o->efj_object, __VA_ARGS__)


void send_event_from(efj::object* source, efj::object* target, u32 id, const efj::component_view& e);

template <typename ComponentT> EFJ_MACRO void
send_event_from(efj::object* source, efj::object* target, u32 id, const ComponentT& e);

#define efj_send_event(...) efj::send_event_from(&this->efj_object, __VA_ARGS__)

} // namespace efj

