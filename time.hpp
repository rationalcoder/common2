namespace efj
{

// NOTE: I've copied the operator overloading to each unit instead of creating some template
// base class thing to improve error messages. Having a few extra lines of code here just isn't
// that big of a deal considering that this code changes much slower than all the code that uses it.
//
// Also, if you want something more complicated with unit conversions, like doing std::duration-style
// casts to floats or doubles, just use the underlying integer itself. It's public for a reason.

struct nsec
{
    u64 value;
    constexpr nsec() : value() {}
    constexpr explicit nsec(u64 value) : value(value) {}

    nsec operator + (nsec rhs) const { return nsec(value + rhs.value); }
    nsec operator - (nsec rhs) const { return nsec(value - rhs.value); }
    nsec operator * (size_t factor) const { return nsec(value * factor); }
    nsec operator / (size_t factor) const { return nsec(value / factor); }

    bool operator == (nsec rhs) const { return value == rhs.value; }
    bool operator <  (nsec rhs) const { return value < rhs.value; }
    bool operator <= (nsec rhs) const { return value <= rhs.value; }
    bool operator >  (nsec rhs) const { return value > rhs.value; }
    bool operator >= (nsec rhs) const { return value >= rhs.value; }

    nsec& operator += (nsec rhs) { value += rhs.value; return *this; }
    nsec& operator -= (nsec rhs) { value -= rhs.value; return *this; }
    nsec& operator *= (size_t factor) { value *= factor; return *this; }
    nsec& operator /= (size_t factor) { value /= factor; return *this; }
};

struct usec
{
    u64 value;
    constexpr usec() : value() {}
    constexpr explicit usec(u64 value) : value(value) {}
    constexpr operator efj::nsec() const { return efj::nsec(value*1000); }

    usec operator + (usec rhs) const { return usec(value + rhs.value); }
    usec operator - (usec rhs) const { return usec(value - rhs.value); }
    usec operator * (size_t factor) const { return usec(value * factor); }
    usec operator / (size_t factor) const { return usec(value / factor); }

    bool operator == (usec rhs) const { return value == rhs.value; }
    bool operator <  (usec rhs) const { return value < rhs.value; }
    bool operator <= (usec rhs) const { return value <= rhs.value; }
    bool operator >  (usec rhs) const { return value > rhs.value; }
    bool operator >= (usec rhs) const { return value >= rhs.value; }

    usec& operator += (usec rhs) { value += rhs.value; return *this; }
    usec& operator -= (usec rhs) { value -= rhs.value; return *this; }
    usec& operator *= (size_t factor) { value *= factor; return *this; }
    usec& operator /= (size_t factor) { value /= factor; return *this; }
};

struct msec
{
    u64 value;
    constexpr msec() : value() {}
    constexpr explicit msec(u64 value) : value(value) {}
    constexpr operator efj::nsec() const { return efj::nsec(value*1000000); }
    constexpr operator efj::usec() const { return efj::usec(value*1000); }

    msec operator + (msec rhs) const { return msec(value + rhs.value); }
    msec operator - (msec rhs) const { return msec(value - rhs.value); }
    msec operator * (size_t factor) const { return msec(value * factor); }
    msec operator / (size_t factor) const { return msec(value / factor); }

    bool operator == (msec rhs) const { return value == rhs.value; }
    bool operator <  (msec rhs) const { return value < rhs.value; }
    bool operator <= (msec rhs) const { return value <= rhs.value; }
    bool operator >  (msec rhs) const { return value > rhs.value; }
    bool operator >= (msec rhs) const { return value >= rhs.value; }

    msec& operator += (msec rhs) { value += rhs.value; return *this; }
    msec& operator -= (msec rhs) { value -= rhs.value; return *this; }
    msec& operator *= (size_t factor) { value *= factor; return *this; }
    msec& operator /= (size_t factor) { value /= factor; return *this; }
};

struct sec
{
    u64 value;
    constexpr sec() : value() {}
    constexpr explicit sec(u64 value) : value(value) {}
    constexpr operator efj::nsec() const { return efj::nsec(value*1000000000); }
    constexpr operator efj::usec() const { return efj::usec(value*1000000); }
    constexpr operator efj::msec() const { return efj::msec(value*1000); }

    sec operator + (sec rhs) const { return sec(value + rhs.value); }
    sec operator - (sec rhs) const { return sec(value - rhs.value); }
    sec operator * (size_t factor) const { return sec(value * factor); }
    sec operator / (size_t factor) const { return sec(value / factor); }

    bool operator == (sec rhs) const { return value == rhs.value; }
    bool operator <  (sec rhs) const { return value < rhs.value; }
    bool operator <= (sec rhs) const { return value <= rhs.value; }
    bool operator >  (sec rhs) const { return value > rhs.value; }
    bool operator >= (sec rhs) const { return value >= rhs.value; }

    sec& operator += (sec rhs) { value += rhs.value; return *this; }
    sec& operator -= (sec rhs) { value -= rhs.value; return *this; }
    sec& operator *= (size_t factor) { value *= factor; return *this; }
    sec& operator /= (size_t factor) { value /= factor; return *this; }
};

namespace time_literals
{
constexpr efj::sec  operator ""_s  (unsigned long long value) { return efj::sec (value); }
constexpr efj::msec operator ""_ms (unsigned long long value) { return efj::msec(value); }
constexpr efj::usec operator ""_us (unsigned long long value) { return efj::usec(value); }
constexpr efj::nsec operator ""_ns (unsigned long long value) { return efj::nsec(value); }
}

using namespace time_literals;

// NOTE: ints are used for less annoying usage with system APIs. There's plenty of precision.
struct date_utc
{
    int year;
    int month;
    int day;
    int hour;
    int minute;
    int second;
    int micro;
};

// System-local monotonic time for timing.
struct timestamp
{
    u32 sec;
    u32 nsec;

    bool operator == (const timestamp& other) const { return sec == other.sec && nsec == other.nsec; }
    bool operator != (const timestamp& other) const { return sec != other.sec || nsec != other.nsec; }

    timestamp operator + (efj::nsec offset) const
    {
        return {sec  + (u32)(offset.value / 1000000000), nsec + (u32)(offset.value % 1000000000)};
    }

    timestamp& operator += (efj::nsec offset)
    {
        sec  += offset.value / 1000000000;
        nsec += offset.value % 1000000000;
        return *this;
    }
};

// Epoch of 1970
struct posix_time
{
    u32 sec;
    u32 nsec;

    bool operator == (const posix_time& other) const { return sec == other.sec && nsec == other.nsec; }
    bool operator != (const posix_time& other) const { return sec != other.sec || nsec != other.nsec; }

    posix_time operator + (efj::nsec offset) const
    {
        return {sec  + (u32)(offset.value / 1000000000), nsec + (u32)(offset.value % 1000000000)};
    }

    posix_time& operator += (efj::nsec offset)
    {
        sec  += offset.value / 1000000000;
        nsec += offset.value % 1000000000;
        return *this;
    }
};

// Epoch of 1900
struct ntp_time
{
    u32 sec;
    u32 frac; // 1/2^32 units.
};

//! Use this one for getting things like log message timestamps that you
//! want to convert into a usable date. Uses CLOCK_REALTIME, epoch 1970.
EFJ_MACRO efj::posix_time now_ptime();
EFJ_MACRO efj::posix_time to_ptime(const efj::ntp_time& time);

EFJ_MACRO efj::sec  to_sec(const efj::posix_time& time);
EFJ_MACRO efj::msec to_msec(const efj::posix_time& time);
EFJ_MACRO efj::usec to_usec(const efj::posix_time& time);
EFJ_MACRO efj::nsec to_nsec(const efj::posix_time& time);


//! Use this one for getting timestamps in NTP (epoch 1900, 32-bit fractional seconds).
//! The result is in host byte order.
EFJ_MACRO efj::ntp_time now_ntp();
EFJ_MACRO efj::ntp_time to_ntp(const efj::posix_time& time);

//{ Use these for profiling and timing.
EFJ_MACRO efj::timestamp now();

EFJ_MACRO efj::sec  now_sec();
EFJ_MACRO efj::msec now_msec();
EFJ_MACRO efj::usec now_usec();
EFJ_MACRO efj::nsec now_nsec();

EFJ_MACRO efj::sec  to_sec(const efj::timestamp& ts);
EFJ_MACRO efj::msec to_msec(const efj::timestamp& ts);
EFJ_MACRO efj::usec to_usec(const efj::timestamp& ts);
EFJ_MACRO efj::nsec to_nsec(const efj::timestamp& ts);

EFJ_MACRO efj::timestamp now_real();
EFJ_MACRO efj::sec  now_real_sec();
EFJ_MACRO efj::msec now_real_msec();
EFJ_MACRO efj::usec now_real_usec();
EFJ_MACRO efj::nsec now_real_nsec();
//}

//! Samples the current time with CLOCK_REALTIME, which is affected by NTP.
//! and coverts the result into a UTC date.
EFJ_MACRO efj::date_utc now_utc();

//! Performs UTC conversion on a posix timestamp.
efj::date_utc to_utc(const efj::posix_time& time);

//! Gets a timestamp suitable for RTP messages, using CLOCK_REALTIME, returning
//! posix time converted to 8 kHz.
EFJ_MACRO u32 now_8khz();

template <> inline efj::string to_string<>(const efj::date_utc& d);
template <> inline efj::string to_string<>(const efj::timestamp& ts);
template <> inline efj::string to_string<>(const efj::posix_time& ts);

} // end namespace efj

#if EFJ_USE_TIME_LITERALS
using namespace efj::time_literals;
#endif
