// Forward declations.

namespace efj
{

struct execution_context;
struct application;

struct thread;
struct object;
struct producer;
struct consumer;
struct component;
struct component_view;
struct memory_arena;
struct allocator;
struct allocator_impl;
struct heap_allocator;
struct linear_allocator;
using arena_allocator = linear_allocator;
struct allocator_data;
struct xml_document;
struct string;
struct callback;
struct user_event_queue;
struct user_event_pool;
struct user_event_sub_info;
struct user_event_system;
struct uesys_thread_ctx;
struct uesys_object_ctx;
struct debug_system;
struct debugsys_thread_ctx;
struct debugsys_object_ctx;
struct log_system;
struct logsys_thread_ctx;
struct logsys_object_ctx;
struct config_system;
struct configsys_thread_ctx;
struct configsys_object_ctx;
struct pc_system;
struct pcsys_thread_ctx;
struct watchdog_system;
struct watchdog_thread_ctx;

struct test_system;
struct testsys_thread_ctx;
struct testsys_object_ctx;
struct testsys_producer_ctx;
struct testsys_consumer_ctx;

bool _testsys_on_produce(efj::producer* p, efj::consumer* target, const efj::component_view* components, umm count);
void _testsys_on_consume(efj::consumer* c, const efj::component_view* components, umm count);

//{
struct log_message_ctx;
using log_formatter = efj::string(const efj::log_message_ctx& ctx, const efj::string& message);
struct log_queue;

void _queue_log_message(const char* data, umm size);
//}

struct config_file;
struct event_data;
struct user_event;
struct timer;
struct tcp_client;
struct tcp_server;
struct udp_socket;
struct serial_port;


struct fence;
struct fenced_event;
struct breakpoint;

// Needed to work around incomplete type.
void _pcsys_queue_consume(efj::thread* sourceThread, efj::consumer* c,
    const efj::component_view* components, umm count);

//{
inline auto set_allocator(efj::allocator alloc) -> efj::allocator;
inline auto this_object() -> efj::object*;
inline auto this_thread() -> efj::thread*;
inline int  this_epoll();
inline int  this_fd();
inline auto this_fd_events() -> flag32;
inline void _set_this_object(efj::object* o EFJ_LOC_SIG);

EFJ_MACRO auto perm_memory() -> efj::memory_arena&;
EFJ_MACRO auto temp_memory() -> efj::memory_arena&;
EFJ_MACRO auto event_memory() -> efj::memory_arena&;

EFJ_MACRO auto get_heap_allocator() -> efj::allocator;
EFJ_MACRO auto get_temp_allocator() -> efj::allocator;
EFJ_MACRO auto get_event_allocator() -> efj::allocator;

EFJ_MACRO efj::heap_allocator* get_heap_allocator_impl();
EFJ_MACRO efj::linear_allocator* get_linear_allocator_impl();
EFJ_MACRO efj::arena_allocator* get_arena_allocator_impl();
EFJ_MACRO auto current_allocator() -> efj::allocator;

inline auto thread_name() -> const char*;
inline u32  logical_thread_id();
inline u64  real_thread_id();
inline auto thread_by_id(u32 ltid) -> efj::thread*;
inline u32  thread_count();
inline void register_user_event(u32 id);

inline bool _initting_objects();
inline bool _done_initting_objects();
inline auto _get_sub_info(u32 id) -> efj::user_event_sub_info&;
inline void _add_object(efj::object* o);
//}

} // namespace efj
