
// Producer-Consumer system
//
// Calls to efj_produce() that dispatch to an object that lives on the same thread just
// calls through a function pointer directly to it, passing the data by reference.
// The job of this system is to handle the case of producing to an object that lives
// on another thread.
//

namespace efj
{

struct pcsys_thread_event : efj::eventsys_thread_event
{
    efj::component_view* components       = nullptr;
    umm                  componentCount   = 0;
    efj::consumer*       consumer         = nullptr;
};

struct pc_system;
struct pcsys_thread_ctx
{
    efj::pc_system*    sys   = nullptr;
    efj::memory_arena* arena = nullptr;
};

struct pc_system
{
    efj::memory_arena* arena = nullptr;
};

static void _pcsys_pre_init(efj::pc_system* sys, efj::memory_arena* arena);
static void _pcsys_init_thread_context(efj::pc_system* sys, efj::thread* thread, efj::memory_arena* arena);
static void _pcsys_post_object_init(efj::pc_system* sys);
static void _pcsys_queued_consume(efj::pc_system* sys);
//static void _pcsys_produce();
static void _pcsys_handle_deferred(efj::thread* thread);
static void _pcsys_handle_consume_event(efj::eventsys_thread_event* e);

void _pcsys_queue_consume(efj::thread* sourceThread, efj::consumer* c,
    const efj::component_view* components, umm count);

} // namespace efj

