namespace efj
{

using error_to_string_fn = const char*(int);

const char*
default_error_to_string(int value);

struct error_code
{
    efj::error_to_string_fn* _toString = &default_error_to_string;
    int                      _value    = 0;

    error_code() {}

    template <typename EnumT> EFJ_MACRO
    error_code(int value, const char* toString(EnumT))
        : _toString((efj::error_to_string_fn*)toString), _value(value)
    {}

    template <typename EnumT> EFJ_MACRO
    void set(int value, const char* toString(EnumT))
    {
        _toString = (efj::error_to_string_fn*)toString;
        _value = value;
    }

    EFJ_MACRO int value() const { return _value; }

    EFJ_MACRO bool exists() const { return _value != 0; }
    EFJ_MACRO operator bool() const { return _value != 0; }
};

EFJ_MACRO const char*
c_str(const efj::error_code& ec)
{
    return ec._toString(ec._value);
}

} // namespace
