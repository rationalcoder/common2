namespace efj
{

// Source: http://git.musl-libc.org/cgit/musl/tree/src/time/__secs_to_tm.c?h=v0.9.15
extern efj::date_utc
to_utc(const efj::posix_time& time)
{
    // 2000-03-01 (mod 400 year, immediately after feb29).
    constexpr auto kLeapYearEpoch   = 946684800LL + 86400*(31+29);
    constexpr int  kDaysPer400Years = 365*400 + 97;
    constexpr int  kDaysPer100Years = 365*100 + 24;
    constexpr int  kDaysPer4Years   = 365*4   + 1;

    static const char daysInMonth[] = {31,30,31,30,31,31,30,31,30,31,31,29};

    struct timespec spec = { (time_t)time.sec, (long)time.nsec };

    auto secs    = spec.tv_sec - kLeapYearEpoch;
    auto days    = secs / 86400;
    auto remSecs = secs % 86400;
    if (remSecs < 0) {
        remSecs += 86400;
        days--;
    }

    auto fourCenturyCycles = days / kDaysPer400Years;
    auto remDays           = days % kDaysPer400Years;
    if (remDays < 0) {
        remDays += kDaysPer400Years;
        fourCenturyCycles--;
    }

    auto centuryCycles = remDays / kDaysPer100Years;
    if (centuryCycles == 4) centuryCycles--;
    remDays -= centuryCycles * kDaysPer100Years;

    auto fourYearCycles = remDays / kDaysPer4Years;
    if (fourYearCycles == 25) fourYearCycles--;
    remDays -= fourYearCycles * kDaysPer4Years;

    auto remYears = remDays / 365;
    if (remYears == 4) remYears--;
    remDays -= remYears * 365;

    auto years = remYears + 4*fourYearCycles + 100*centuryCycles + 400*fourCenturyCycles;

    int months = 0;
    for (; daysInMonth[months] <= remDays; months++)
        remDays -= daysInMonth[months];

    // TODO: handle error when (years+100 > INT_MAX || years+100 < INT_MIN).

    efj::date_utc result;
    result.year   = years  + 2000;
    result.month  = months + 2;
    if (result.month >= 12) {
        result.month -= 12;
        result.year++;
    }
    result.month += 1; // Convert to 1-based.
    result.day    = remDays + 1;
    result.hour   = remSecs / 3600;
    result.minute = remSecs / 60 % 60;
    result.second = remSecs % 60;
    result.micro  = spec.tv_nsec / 1000;

    return result;
}

}
