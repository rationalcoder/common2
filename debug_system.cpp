//
// BUG: Connecting over and over with a debugger can cause events for the pending debugger to not happen. Not sure
// that this is not an issue with the testing code on the win32 side, though. Any way, we can set a timer for the pending client
// to communicate with us, and close the fd and reset to see if we can recover.
// TODO: We currently have the server event set as only monitoring input events and is oneshot.
// Maybe there is some error we aren't handling?
//

namespace efj
{

static const char* gDebugEventTypeStrings[efj::eventsys_event_count_];

// WARNING: You have to cast to uintptr_t first or you can get strange results going from 32 bit pointers to u64.
static EFJ_FORCE_INLINE u64 _pointer_to_key(void* p) { return (u64)(uptr)p; }

static ssize_t
_send_all(int fd, const void* buf, umm size, int flags = 0)
{
    efj_assert(fd != -1);

    ssize_t n = 0;
    for (umm sent = 0; sent < size; ) {
        n = ::send(fd, (uint8_t*)buf + sent, size - sent, MSG_NOSIGNAL | flags);
        if (n <= 0) {
            efj_assert(n != 0); // 0 means we tried to write 0 bytes, which we should never do.
            // EPIPE means that the debugger disconnected and we are writing to it before our socket is closed.
            // This is fine, and the disconnect will be handled by the debug thread.
            // ECONNRESET means almost the same thing, except we sent data that had yet to be read by the client.
            // Any other error is unexpected.
            if (errno != EPIPE && errno != ECONNRESET)
                efj_internal_log(log_level_warn, "Failed to write to debugger: %s", strerror(errno));

            return n;
        }

        sent += n;
    }

    return n;
}

static EFJ_FORCE_INLINE ssize_t
_send_locked(pthread_mutex_t* mutex, int fd, const void* buf, umm size, int flags = 0)
{
    pthread_mutex_lock(mutex);
    ssize_t result = _send_all(fd, buf, size, flags);
    pthread_mutex_unlock(mutex);

    return result;
}

//{ Internal data structure

//{ Edit-Queue

inline void
debugsys_edit_queue::init(umm n, efj::memory_arena& arena)
{
    memset(this, 0, sizeof(*this));

    auto* buf1 = efj_push_array(arena, n, efj::debugsys_watch_edit);
    auto* buf2 = efj_push_array(arena, n, efj::debugsys_watch_edit);

    buffer1.begin = buf1;
    buffer1.at    = buf1;
    buffer1.end   = buf1 + n;

    buffer2.begin = buf2;
    buffer2.at    = buf2;
    buffer2.end   = buf2 + n;

    frontBuffer   = &buffer1;
    backBuffer    = &buffer2;

    pthread_mutex_init(&swapMutex, nullptr);
    pthread_cond_init(&spaceAvailableCond, nullptr);
}

// Called on the debug thread.
//{
inline void
debugsys_edit_queue::push_batch(const efj::dbp_watch_edited_batch& batch)
{
    latestBatch = batch;
}

inline bool
debugsys_edit_queue::push(const efj::debugsys_watch_edit& edit)
{
    pthread_mutex_lock(&swapMutex);

    while (frontBuffer->at >= frontBuffer->end) {
        efj_rare_assert(frontBuffer->at == frontBuffer->end);

        // Wait for the thread with pending edits to service the queue.
        int result = pthread_cond_wait(&spaceAvailableCond, &swapMutex);
        if (result == 0)
            continue;

        efj_internal_log(log_level_warn, "Failed to queue a debug edit. Dropping it: %s", strerror(errno));

        goto check_batch_and_exit;
    }

    *frontBuffer->at++ = edit;

check_batch_and_exit:
    pthread_mutex_unlock(&swapMutex);

    // This state is only touched/needed by the debug thread.
    if (latestBatch.count == 1)
        return true;

    latestBatch.count--;
    return false;
}
//}

// Called on the thread that has pending edits.
inline efj::debugsys_edit_queue_buffer*
debugsys_edit_queue::swap_buffers()
{
    pthread_mutex_lock(&swapMutex);

    auto* result = frontBuffer;
    efj::swap(frontBuffer, backBuffer);

    bool bufferWasFull = frontBuffer->at >= frontBuffer->end;
    frontBuffer->at = frontBuffer->begin;

    pthread_mutex_unlock(&swapMutex);

    if (bufferWasFull)
        pthread_cond_signal(&spaceAvailableCond);

    return result;
}
//}

// Push data packed into dbp queue buffers. If the data doesn't fit fully in the space
// remaining, the data that does fit is copied over and the rest is copied into new buffers
// until all the data has been copied.
template <typename GetEmptyBufferFn> static EFJ_FORCE_INLINE void
_push_dbp_packed(efj::dbp_queue_buffer** tailBuffer, umm* bufferCount,
    const void* data, umm size, GetEmptyBufferFn getEmptyBuffer)
{
    for (umm progress = 0; progress < size; ) {
        umm availableSize = sizeof((*tailBuffer)->data) - (*tailBuffer)->writeProgress;
        umm copySize      = size - progress;
        efj_assert(availableSize <= sizeof((*tailBuffer)->data));

        if (copySize > availableSize)
            copySize = availableSize;

        if (copySize == 0) {
            efj::dbp_queue_buffer* newBuffer = getEmptyBuffer();
            efj_assert(*tailBuffer != newBuffer);
            (*tailBuffer)->next = newBuffer;
            *tailBuffer         = newBuffer;

            (*bufferCount)++;
            efj_assert((*tailBuffer)->next != *tailBuffer);
            continue;
        }

        memcpy((*tailBuffer)->data + (*tailBuffer)->writeProgress, (u8*)data + progress, copySize);
        (*tailBuffer)->writeProgress += copySize;

        progress += copySize;
    }
}

// Push data contiguously into dbp queue buffers. If the data doesn't fit fully in the space
// remaining, a new buffer is used. The size must be less than the size of one buffer + any
// data pushed into new buffers by the get-empty-buffer callback.
template <typename GetEmptyBufferFn> static EFJ_FORCE_INLINE void*
_push_dbp_contiguous(efj::dbp_queue_buffer** tailBuffer, umm* bufferCount,
    const void* data, umm size, GetEmptyBufferFn getEmptyBuffer)
{
    umm oldProgress      = (*tailBuffer)->writeProgress;
    umm newWriteProgress = oldProgress + size;

    if (newWriteProgress > sizeof((*tailBuffer)->data)) {
        efj::dbp_queue_buffer* newBuffer = getEmptyBuffer();
        efj_assert(size <= sizeof((*tailBuffer)->data) - newBuffer->writeProgress);
        oldProgress      = newBuffer->writeProgress;
        newWriteProgress = newBuffer->writeProgress + size;

        (*tailBuffer)->next = newBuffer;
        *tailBuffer         = newBuffer;
        (*bufferCount)++;
    }

    memcpy((*tailBuffer)->data + oldProgress, data, size);
    (*tailBuffer)->writeProgress = newWriteProgress;

    efj_assert((*tailBuffer)->next != *tailBuffer);
    return (*tailBuffer)->data + oldProgress;
}

inline void
dbp_buffer::init(efj::debug_system* sys, efj::free_list* freeList, efj::memory_arena* arena)
{
    memset(this, 0, sizeof(*this));

    this->sys         = sys;
    this->freeList    = freeList;
    this->arena       = arena;
    this->headBuffer  = freeList->get_or_allocate<efj::dbp_queue_buffer>(*arena);
    this->tailBuffer  = this->headBuffer;
    this->headBuffer->clear();
}

inline void
dbp_buffer::push(const void* data, umm size)
{
    for (umm progress = 0; progress < size; ) {
        umm remaining = size - progress;
        umm copySize = sizeof(tailBuffer->data) - tailBuffer->writeProgress;
        efj_assert(copySize <= sizeof(tailBuffer->data));

        if (copySize > remaining)
            copySize = remaining;

        if (copySize == 0) {
            auto* newBuffer = freeList->get_or_allocate<dbp_queue_buffer>(*arena);
            newBuffer->clear();

            tailBuffer->next = newBuffer;
            tailBuffer       = newBuffer;
            continue;
        }

        memcpy(tailBuffer->data + tailBuffer->writeProgress, (u8*)data + progress, copySize);
        tailBuffer->writeProgress += copySize;

        progress += copySize;
    }

    dataToWrite = true;
}

inline void
dbp_buffer::clear()
{
    // The dbp queue is thread-local, so we should be able to use its free list.
    for (efj::dbp_queue_buffer* buf = headBuffer; buf; buf = buf->next)
        freeList->add(buf);

    // The free list could be empty if clear was called on an empty/cleared buffer.
    headBuffer = freeList->get_or_allocate<efj::dbp_queue_buffer>(*arena);
    tailBuffer = headBuffer;
    headBuffer->clear();

    dataToWrite = false;
}

// Called by the debug thread to write a buffer straight to the debugger.
// This is intended to be called by the debug thread and is NOT thread-safe @TS.
//
// A write doesn't imply a clear(), because some buffers are reused.
//
inline void
dbp_buffer::write_to_debugger(int fd)
{
    if (dataToWrite) {
        for (efj::dbp_queue_buffer* buf = headBuffer; buf; buf = buf->next) {
            _send_locked(&sys->sendMutex, fd, buf->data, buf->writeProgress);
        }
    }
}

//{ thread_dbp_queue

inline void
thread_dbp_queue::init(efj::debug_system* sys, u32 ltid, efj::memory_arena* arena)
{
    this->sys        = sys;
    this->arena      = arena;
    this->headBuffer = nullptr;
    this->tailBuffer = nullptr;
    this->ltid       = ltid;

    pthread_mutex_init(&mutex, nullptr);
}

inline void
thread_dbp_queue::new_connection()
{
    free_buffers(this->headBuffer);
    this->headBuffer = nullptr;
    this->tailBuffer = nullptr;
}

inline efj::dbp_queue_buffer*
thread_dbp_queue::get_empty_buffer()
{
    efj_assert(ltid == efj::logical_thread_id());

    // We only need to lock around the free list access, not the potential allocation.
    pthread_mutex_lock(&mutex);
    auto* result = (efj::dbp_queue_buffer*)freeList.get();
    pthread_mutex_unlock(&mutex);

    if (!result) {
        result = freeList.allocate<dbp_queue_buffer>(*arena);
    }

    result->clear();
    return result;
}

inline void
thread_dbp_queue::push_buffers(efj::dbp_queue_buffer* head, efj::dbp_queue_buffer* tail)
{
    // We're just trying to make sure that we don't append a bunch of buffers that have little
    // data in them. The idea is to just append the given list to ours, but handling the head
    // buffer specially, potentially appending what's there into our tail buffer to save space.

    efj_assert(head);
    efj_assert(!tail->next);

    pthread_mutex_lock(&mutex);

    // If our list is empty, we can't really do anything accept start with the buffers given to us.
    if (!headBuffer) {
        //printf("[%u] Appending (Empty)\n", efj::logical_thread_id());
        headBuffer = head;
        tailBuffer = tail;
        pthread_mutex_unlock(&mutex);
        return;
    }

    // If their head buffer is reasonable full, we can just append what's given.
    // If there is more than one buffer, we also just append b/c that would only happen if a thread
    // wrote a lot of data that had to be contiguous, and it had to start a new buffer. We assume this
    // is rare / only happens during testing, so we don't care about saving memory.
    //
    if (head->next || head->writeProgress >= sizeof(head->data) / 2) {
        //printf("[%u] Appending\n", efj::logical_thread_id());
        tailBuffer->next = head;
        tailBuffer       = tail;
        pthread_mutex_unlock(&mutex);
        return;
    }

    // Only one buffer. See if we can just pull _all_ of its data into our tail buffer.
    umm copySize = sizeof(tailBuffer->data) - tailBuffer->writeProgress;
    if (copySize >= headBuffer->writeProgress) {
        copySize = head->writeProgress;
        //printf("[%u] Merging %zu bytes\n", efj::logical_thread_id(), copySize);

        memcpy(tailBuffer->data + tailBuffer->writeProgress, head->data, copySize);
        tailBuffer->writeProgress += copySize;

        freeList.add(head);
    }
    else {
        //printf("[%u] Can't merge %zu bytes\n", efj::logical_thread_id(), copySize);
        // NOTE: We used to try to merge buffers even when the merge would be partial, leaving data remaining in
        // the given head buffer. This is a bad idea for a few reasons:
        // 1. It's incorrect. The write of each buffer is assumed to be atomic be producing threads. If one
        // thread-events message is broken up into two messages, for example, interleaving data could make
        // it to the debugger who would assume the data is part of the same message.
        // 2. It's slow b/c a buffer that's potentially almost full has to be slid down with memmove,
        // and the space savings are minimal.
        tailBuffer->next = head;
        tailBuffer       = head;
        efj_assert(!head->next);
    }

    pthread_mutex_unlock(&mutex);
}

inline void
thread_dbp_queue::free_buffers(efj::dbp_queue_buffer* buffers)
{
    // Right now, it's probably faster to lock around the freeing of all the buffers, since we expect the buffer
    // count to be low, but we only really need to lock around the free list access, so the best case would be to use
    // a lock-free free-list. That being said, this only synhronizes with the debug thread, so it should't be that big
    // of a deal. @Speed.
    pthread_mutex_lock(&mutex);

    for (efj::dbp_queue_buffer* cur = buffers; cur; ) {
        efj::dbp_queue_buffer* next = cur->next;
        freeList.add(cur);
        cur = next;
    }

    pthread_mutex_unlock(&mutex);
}

inline void
thread_dbp_queue::write_to_debugger(int fd)
{
    // NOTE: We can't just exit if an atomic push hasn't been completed yet because
    // we might end up in a very unlucky case in which a thread is in the middle
    // of a push when the debugger tries to write it out indefinitely. Instead we sample how much
    // data is available and send that.

    pthread_mutex_lock(&mutex);
    efj::dbp_queue_buffer* sampledHeadBuffer = this->headBuffer;
    efj::dbp_queue_buffer* sampledTailBuffer = this->tailBuffer;
    efj_assert(!sampledTailBuffer || !sampledTailBuffer->next);

    this->headBuffer = nullptr;
    this->tailBuffer = nullptr;
    pthread_mutex_unlock(&mutex);

    for (efj::dbp_queue_buffer* buf = sampledHeadBuffer; buf; ) {
        efj_assert(buf->writeProgress);

        if (DEBUGSYS_VALIDATE_DBP_BUFFERS && buf->validate) {
            buf->validate(buf->validateData, buf);
        }

        _send_locked(&sys->sendMutex, fd, buf->data, buf->writeProgress);

        efj::dbp_queue_buffer* next = buf->next;

        pthread_mutex_lock(&mutex);
        freeList.add(buf);
        pthread_mutex_unlock(&mutex);

        buf = next;
    }
    //pthread_mutex_unlock(&mutex);
}
//}

//{
inline void
thread_dbp_buffer::init(efj::thread_dbp_queue* queue, u32 ltid)
{
    memset(this, 0, sizeof(*this));

    this->queue = queue;
    this->ltid  = ltid;
}

inline void
thread_dbp_buffer::new_connection()
{
    efj_assert(ltid == efj::logical_thread_id());

    if (headBuffer)
        queue->free_buffers(headBuffer);

    this->headBuffer  = queue->get_empty_buffer();
    this->tailBuffer  = this->headBuffer;
    this->bufferCount = 1;
}

inline void
thread_dbp_buffer::push_packed(const void* data, umm size)
{
    _push_dbp_packed(&tailBuffer, &bufferCount, data, size, [this]() {
        return queue->get_empty_buffer();
    });
}

inline void*
thread_dbp_buffer::push_contiguous(const void* data, umm size)
{
    return _push_dbp_contiguous(&tailBuffer, &bufferCount, data, size, [this]() {
        return queue->get_empty_buffer();
    });
}

inline void
thread_dbp_buffer::write_to_queue()
{
    efj_assert(ltid == efj::logical_thread_id());

    if (headBuffer->writeProgress) {
        queue->push_buffers(headBuffer, tailBuffer);

        efj::dbp_queue_buffer* buf = queue->get_empty_buffer();
        headBuffer = buf;
        tailBuffer = buf;

        bufferCount = 1;
    }
}

inline void
thread_dbp_buffer::write_to_queue_if_needed()
{
    if (bufferCount >= DEBUGSYS_THREAD_DBP_BUFFER_COUNT_THRESHOLD)
        write_to_queue();
}

inline void
thread_dbp_buffer::write_to_debugger(int fd)
{
    // We have to lock around the whole thing for now. :StartCaptureBufferProblem
    pthread_mutex_lock(&queue->sys->sendMutex);
    for (efj::dbp_queue_buffer* buf = headBuffer; buf; buf = buf->next) {
        _send_all(fd, buf->data, buf->writeProgress);
    }
    pthread_mutex_unlock(&queue->sys->sendMutex);

    new_connection();
}

//}

inline void
thread_events_buffer::init(efj::thread_dbp_queue* queue, u32 ltid)
{
    memset(this, 0, sizeof(*this));

    this->queue = queue;
    this->ltid  = ltid;
}

inline void
thread_events_buffer::new_connection()
{
    efj_assert(ltid == efj::logical_thread_id());

    if (headBuffer) {
        queue->free_buffers(headBuffer);
    }

    this->headBuffer  = _get_empty_buffer();
    this->tailBuffer  = this->headBuffer;
    this->bufferCount = 1;

    // NOTE: We can't call the normal helper function for completing event messages here at the moment b/c
    // all data-pushing helper functions assume a non-null tail buffer.

    efj::dbp_thread_events msg = {};
    msg.header.type = dbp_message_thread_events;
    msg.header.size = sizeof(msg);
    msg.ltid        = ltid;

    if (!DEBUGSYS_DEBUG_THREAD_EVENTS_BUFFER || ltid == 0) {
        threadEvents = (efj::dbp_thread_events*)_push_contiguous(&msg, sizeof(msg), "Thread Events Header");
        efj_assert(headBuffer->writeProgress == sizeof(dbp_thread_events));
        efj_assert(threadEvents->header.type == dbp_message_thread_events);
        efj_assert(threadEvents->header.size == sizeof(dbp_thread_events));
    }
}

EFJ_FORCE_INLINE efj::dbp_queue_buffer*
thread_events_buffer::_get_empty_buffer()
{
    if (DEBUGSYS_VALIDATE_DBP_BUFFERS) {
        efj::dbp_queue_buffer* buf = queue->get_empty_buffer();
        buf->validateData = this;
        buf->validate     = &_validate_buffer;
        return buf;
    }
    else {
        return queue->get_empty_buffer();
    }
}

inline void
thread_events_buffer::_validate_buffer(void* udata, efj::dbp_queue_buffer* buf)
{
    // XXX
    //auto* self = (efj::thread_events_buffer*)udata;
    //auto* events = (efj::dbp_thread_events*)buf->data;

    //efj_assert(events->header.type == efj::dbp_message_thread_events);
    //efj_assert(events->header.size == buf->writeProgress);
    //efj_assert(events->ltid == self->ltid);
    //efj_assert(self->ltid < 4); // XXX
}

inline void
thread_events_buffer::push_event(efj::dbp_thread_event& e)
{
    if (DEBUGSYS_DEBUG_THREAD_EVENTS_BUFFER) {
        if (ltid != 0) {
            return;
        }
    }

    eventHasData = e.flags & dbp_event_has_data;
    _push_contiguous(&e, sizeof(dbp_thread_event), "Event");
}

inline void
thread_events_buffer::begin_all_event_data()
{
    if (DEBUGSYS_DEBUG_THREAD_EVENTS_BUFFER) {
        if (ltid != 0) {
            return;
        }

        efj_internal_log(efj::log_level_debug, "%s", __func__);
    }

    if (beganAllEventData)
        efj_panic_if(!endedAllEventData); // Mismatched begin/end

    beganAllEventData = true;
    endedAllEventData = false;
}

inline void
thread_events_buffer::begin_event_data(efj::dbp_event_data_type type, const char* tag)
{
    if (DEBUGSYS_DEBUG_THREAD_EVENTS_BUFFER) {
        if (ltid != 0) {
            return;
        }

        efj_internal_log(efj::log_level_debug, "%s: %s", __func__, tag);
    }

    // Finish the previous event data block, the pointer to which may be unaligned.
    if (curDataSizeAddress) {
        // @Alignment.
        ((uint8_t*)curDataSizeAddress)[0] = ((uint8_t*)&curDataSize)[0];
        ((uint8_t*)curDataSizeAddress)[1] = ((uint8_t*)&curDataSize)[1];
    }

    curDataType = type;
    curDataSize = 0;

    // Pack into an unaligned struct here to push in one call instead of two. It should be correct
    // with two calls, one for each header, though. @Alignment
    efj::debugsys_thread_event_data_headers headers;
    headers.header1.type = curDataType;
    headers.header2.size = 0;

    void* space = _push_contiguous(&headers, sizeof(headers), "Event Data Headers");
    curDataSizeAddress = &((efj::debugsys_thread_event_data_headers*)space)->header2.size;
}

inline void
thread_events_buffer::push_event_data(const void* data, umm size, const char* tag)
{
    if (DEBUGSYS_DEBUG_THREAD_EVENTS_BUFFER) {
        if (ltid != 0) {
            return;
        }

        efj_internal_log(efj::log_level_debug, "%s: %s: %zu bytes", __func__, tag, size);
    }

    _push_packed(data, size, tag);
    curDataSize += size;
}

inline void
thread_events_buffer::end_all_event_data()
{
    if (DEBUGSYS_DEBUG_THREAD_EVENTS_BUFFER) {
        if (ltid != 0) {
            return;
        }

        efj_internal_log(efj::log_level_debug, "%s", __func__);
    }

    // Finish the previous event data block, the pointer to which may be unaligned.
    if (curDataSizeAddress) {
        // @Alignment.
        ((uint8_t*)curDataSizeAddress)[0] = ((uint8_t*)&curDataSize)[0];
        ((uint8_t*)curDataSizeAddress)[1] = ((uint8_t*)&curDataSize)[1];
    }

    if (eventHasData) {
        efj::dbp_thread_event_data_header1 sentinel = { dbp_event_data_sentinel };
        _push_packed(&sentinel, sizeof(dbp_thread_event_data_header1), "Data Sentinel");
    }

    endedAllEventData = true;
}

EFJ_FORCE_INLINE efj::dbp_queue_buffer*
thread_events_buffer::_complete_message_and_start_new_buffer()
{
    threadEvents->header.size = tailBuffer->writeProgress;

    efj::dbp_queue_buffer* buf = queue->get_empty_buffer();

    efj::dbp_thread_events msg = {};
    msg.header.type = dbp_message_thread_events;
    msg.header.size = sizeof(dbp_thread_events);
    msg.ltid        = ltid;

    efj_assert(sizeof(buf->data) < sizeof(msg));
    memcpy(buf->data, &msg, sizeof(msg));
    threadEvents = (efj::dbp_thread_events*)&buf->data;
    buf->writeProgress = sizeof(msg);

    // IMPORTANT: Don't wire up the buffer and append it to the tail, here. This function
    // is used as a callback for functions that do the wiring themselves. Also, don't increase
    // the buffer count or anything like that.
    return buf;
}

inline void
thread_events_buffer::_push_packed(const void* data, umm size, const char* tag)
{
    if (DEBUGSYS_DEBUG_THREAD_EVENTS_BUFFER) {
        if (ltid != 0)
            return;

        efj_internal_log(efj::log_level_debug, "%s: %s: %zu bytes", __func__, tag, size);
    }

    // If we fill up the current buffer while pushing packed, that means the buffer is full,
    // so the thread-events message size is one full buffer's size.
    _push_dbp_packed(&tailBuffer, &bufferCount, data, size, [this]() {
        return _complete_message_and_start_new_buffer();
    });
}

inline void*
thread_events_buffer::_push_contiguous(const void* data, umm size, const char* tag)
{
    if (DEBUGSYS_DEBUG_THREAD_EVENTS_BUFFER) {
        if (ltid != 0)
            return nullptr;

        efj_internal_log(efj::log_level_debug, "%s: %s: %zu bytes", __func__, tag, size);
    }

    return _push_dbp_contiguous(&tailBuffer, &bufferCount, data, size, [this]() {
        return _complete_message_and_start_new_buffer();
    });
}

inline void
thread_events_buffer::_write_to_queue_unlocked()
{
    // The last buffer will still have the default size for the thread events message, since
    // we don't update it while we are pushing data.
    threadEvents->header.size = tailBuffer->writeProgress;

    if (headBuffer->writeProgress > sizeof(efj::dbp_thread_events)) {
        efj_assert(!DEBUGSYS_DEBUG_THREAD_EVENTS_BUFFER || ltid == 0);

        if (DEBUGSYS_VALIDATE_DBP_BUFFERS) {
            int i = 0; // for debugger introspection.
            for (efj::dbp_queue_buffer* buf = headBuffer; buf; buf = buf->next, i++) {
                auto* msg = (dbp_thread_events*)buf->data;
                efj_assert(msg->header.type == dbp_message_thread_events);
                efj_assert(msg->header.size > sizeof(dbp_thread_events));
            }
        }

        threadEvents->header.size = tailBuffer->writeProgress;

        queue->push_buffers(headBuffer, tailBuffer);
        efj::dbp_queue_buffer* buf = queue->get_empty_buffer();

        efj_assert(buf->writeProgress == 0);
        headBuffer  = buf;
        tailBuffer  = buf;
        bufferCount = 1;

        efj::dbp_thread_events msg = {};
        msg.header.type = dbp_message_thread_events;
        msg.header.size = sizeof(dbp_thread_events);
        msg.ltid        = ltid;

        threadEvents = (efj::dbp_thread_events*)_push_contiguous(&msg, sizeof(msg), "Thread Events Header");
    }
}

EFJ_FORCE_INLINE void
thread_events_buffer::write_to_queue()
{
    // TODO: Check a generation counter on the debug thread and only call this function if we
    // know the thread hasn't made any progress since the last time we checked. Otherwise, the debug
    // thread just slows down other threads every time the send timer expires. @Speed
    if (DEBUGSYS_DEBUG_THREAD_EVENTS_BUFFER) {
        if (ltid != 0) {
            return;
        }

        efj_internal_log(efj::log_level_debug, "%s", __func__);
    }

    _write_to_queue_unlocked();
}

EFJ_FORCE_INLINE void
thread_events_buffer::write_to_queue_if_needed()
{
    if (DEBUGSYS_DEBUG_THREAD_EVENTS_BUFFER) {
        if (ltid != 0) {
            return;
        }

        efj_internal_log(efj::log_level_debug, "%s", __func__);
    }

    //efj_internal_log(efj::log_level_debug, "bufferCount: %zu", bufferCount);
    if (bufferCount >= DEBUGSYS_THREAD_DBP_BUFFER_COUNT_THRESHOLD)
        _write_to_queue_unlocked();
}

static void
_debugsys_init_object_context(efj::debug_system& sys, efj::object& object)
{
    // FIXME: This could happen before the system or any thread contexts are intialized at all,
    // if the EFJ_OBJECT is a global. This should be handled in efj::object().
    efj_assert(efj::app()._debugSystem->preInitted);
    object.debugContext = efj_push_new(efj::perm_memory(), efj::debugsys_object_ctx);

    efj::debugsys_object_ctx* ctx = object.debugContext;
    ctx->watchedPrimitives.reset(efj::perm_memory());
}

static void
_debugsys_pre_init(efj::debug_system& sys, efj::memory_arena& arena)
{
    sys = {};

    sys.arena = &arena;
    sys.serverFd          = -1;
    sys.pendingClientFd   = -1;
    sys.connectedClientFd = -1;
    efj_panic_if(pthread_rwlock_init(&sys.clientLock, nullptr) != 0);
    efj_panic_if(pthread_mutex_init(&sys.sendMutex, nullptr) != 0);

    //{ :EventSourceTypes
    gDebugEventTypeStrings[efj::eventsys_event_invalid]  = "invalid";
    gDebugEventTypeStrings[efj::eventsys_event_timer]    = "timer";
    gDebugEventTypeStrings[efj::eventsys_event_waitable] = "waitable";
    gDebugEventTypeStrings[efj::eventsys_event_epoll]    = "epoll";

    for (int i = 0; i < efj::eventsys_event_count_; i++) {
        // This table got out of sync with the event sources.
        efj_panic_if(!gDebugEventTypeStrings[i]);
    }
    //}

    // We can't initialize some of the state stored in the system that is specific to the debug thread until later
    // when we know what the debug thread is, :DebugThreadDetection

    sys.preInitted = true;
}

//}

// IMPORTANT: This happens right after thread creation (not spawning). Therefore, we can't grab things like the
// number of objects that live on a thread here b/c we won't know those things until efj::init() time.
static void
_debugsys_init_thread_context(efj::debug_system& sys, efj::thread& thread, efj::memory_arena& arena)
{
    efj::debugsys_thread_ctx* ctx = thread.debugContext;

    ctx->thread = &thread;
    ctx->arena  = &arena;
    ctx->sys    = &sys;
    ctx->watchedPrimitives.reset(arena);
    ctx->watchedStrings.reset(arena);
    ctx->touchedObjects.reset(arena);

    efj::eventsys_waitable_desc editQueueDesc = {};
    editQueueDesc.udata.as_ptr = &ctx->editQueue;
    editQueueDesc.dispatch     = &_debugsys_handle_edit_queue_event;
    efj::_eventsys_create_waitable_event(&thread, "Debug Edit Queue", &editQueueDesc, &ctx->editQueueEvent);
    efj::_eventsys_add_waitable_event(&thread, &ctx->editQueueEvent);

    efj::eventsys_waitable_desc startCaptureDesc = {};
    startCaptureDesc.udata.as_ptr   = ctx;
    startCaptureDesc.dispatch       = &_debugsys_handle_start_capture_event;
    startCaptureDesc.completedFence = &sys.startCaptureFence;
    efj::_eventsys_create_waitable_event(&thread, "Start Capture", &startCaptureDesc, &ctx->startCaptureEvent);
    efj::_eventsys_add_waitable_event(&thread, &ctx->startCaptureEvent);

    // This is set during debugsys_start, once we know the system has been initialized.
    // :DebugThreadDetection
    // ctx->isDebugThread = ;

    // :FirstEventFlag
    ctx->curEvent.id = UINT32_MAX;

    ctx->dbpQueue.init(&sys, thread.logicalId, ctx->arena);
    ctx->startCaptureBuffer.init(&ctx->dbpQueue, thread.logicalId);
    ctx->threadEventsBuffer.init(&ctx->dbpQueue, thread.logicalId);
    ctx->eventRegistrationBuffer.init(&ctx->dbpQueue, thread.logicalId);
    ctx->editQueue.init(DEBUGSYS_WATCH_EDIT_QUEUE_SIZE, arena);

    constexpr umm kNoteBufferSize = DEBUGSYS_NOTE_CACHE_SIZE * DEBUGSYS_NOTE_SIZE_MAX;
    char* noteBuffer = (char*)efj_push_bytes(arena, kNoteBufferSize);
    ctx->noteBuffer.begin = noteBuffer;
    ctx->noteBuffer.at    = noteBuffer;
    ctx->noteBuffer.end   = noteBuffer + kNoteBufferSize;

    ctx->initted = true;
}

static void
_push_register_event_message(efj::thread_dbp_buffer& buffer, efj::eventsys_event_data& edata)
{
    efj::eventsys_event* e = edata.event;

    alignas(dbp_register_event) u8 msgBuf[sizeof(dbp_register_event) + DEBUGSYS_EVENT_NAME_MAX];
    dbp_register_event* msg = (dbp_register_event*)msgBuf;

    const char* name = e->name;
    umm nameSizeIncludingNul = strlen(name) + 1; // include nul
    if (nameSizeIncludingNul > DEBUGSYS_EVENT_NAME_MAX)
        nameSizeIncludingNul = DEBUGSYS_EVENT_NAME_MAX;

    char* nameDest = msg->name();
    memcpy(nameDest, name, nameSizeIncludingNul);
    nameDest[nameSizeIncludingNul-1] = '\0';

    umm fullMessageSize = sizeof(dbp_register_event) + nameSizeIncludingNul;
    msg->header.type = dbp_message_register_event;
    msg->header.size = fullMessageSize;
    msg->ltid        = e->owningThread->logicalId;
    msg->id          = e->edata->id;

    buffer.push_packed(msgBuf, fullMessageSize);
}

static void
_push_deregister_event_message(efj::thread_dbp_buffer& buffer, efj::eventsys_event_data& edata)
{
    efj::eventsys_event* e = edata.event;

    dbp_deregister_event msg = {};
    msg.header.type = dbp_message_deregister_event;
    msg.header.size = sizeof(msg);
    msg.ltid        = e->owningThread->logicalId;
    msg.id          = e->edata->id;

    buffer.push_packed(&msg, sizeof(msg));
}

//{ Update the list of thread-local events, and queue the appropriate messages if we are capturing events.
static void
_debugsys_register_event(efj::debugsys_thread_ctx& ctx, efj::eventsys_event_data& edata)
{
    edata.debugContext.edata = &edata;
    efj::list_push_back(&ctx.registeredEvents, &edata.debugContext.node);

    if (!ctx.captureEvents)
        return;

    _push_register_event_message(ctx.eventRegistrationBuffer, edata);
}

static void
_debugsys_deregister_event(efj::debugsys_thread_ctx& ctx, efj::eventsys_event_data& edata)
{
    efj_assert(ctx.initted);

    efj::list_remove(&edata.debugContext.node);

    if (!ctx.captureEvents)
        return;

    _push_deregister_event_message(ctx.eventRegistrationBuffer, edata);
}
//}

// We need to describe the system to the debugger after every connection, but most of the system
// isn't allowed to change after init(), so we can build the initial messages once.
//
static void
_cache_app_description(efj::debug_system& sys)
{
    efj::dbp_buffer* buffer = &sys.appDescriptionBuffer;

    efj::dbp_register_thread regThread = {};
    efj::dbp_register_object regObject = {};

    regThread.header.type = efj::dbp_message_register_thread;
    regObject.header.type = efj::dbp_message_register_object;

    for (efj::thread* thread : efj::threads()) {
        regThread.header.size = sizeof(efj::dbp_register_thread);

        umm nameSize = strlen(thread->name) + 1; // include nul
        regThread.header.size += nameSize;
        regThread.key         =  _pointer_to_key(thread);
        regThread.ltid        =  thread->logicalId;
        regThread.rtid        =  thread->realId;

        buffer->push(&regThread, sizeof(regThread));
        buffer->push(thread->name, nameSize);

        for (efj::object* object : thread->objects) {
            regObject.header.size = sizeof(efj::dbp_register_object);

            umm nameSize = strlen(object->name) + 1; // include nul
            regObject.header.size += nameSize;
            regObject.key         =  _pointer_to_key(object);
            regObject.ltid        =  thread->logicalId;
            regObject.id          =  object->id;

            buffer->push(&regObject, sizeof(regObject));
            buffer->push(object->name, nameSize);
        }
    }
}

static void
_debugsys_pre_object_init(efj::debug_system& sys, efj::thread* debugThread)
{
    // :DebugThreadDetection
    sys.debugThread = debugThread;

    efj::debugsys_thread_ctx* debugThreadContext = sys.debugThread->debugContext;
    sys.debugThread->debugContext->isDebugThread = true;
    sys.appDescriptionBuffer.init(&sys, &sys.dbpQueueBufferFreeList, debugThreadContext->arena);
    efj::_eventsys_create_fence(&sys.startCaptureFence);
}

static void
_debugsys_post_object_init(efj::debug_system& sys)
{
    sys.objectCount = efj::objects().size();

    efj::eventsys_timer_desc timerDesc = {};
    timerDesc.udata.as_ptr = &sys;
    timerDesc.dispatch     = &_debugsys_handle_send_timer_event;
    timerDesc.initial      = 200_ms;
    timerDesc.interval     = 200_ms;
    efj::_eventsys_create_timer(sys.debugThread, "Send Debug Events", &timerDesc, &sys.sendTimerEvent);

    // TODO: The TCP API should probably have an internal API that internal systems can use. We could've just tried
    // to have internal systems use EFJ_OBJECTS, and that may still happen in the future, but it's still hairy to
    // implement at the time of writing. For example, there could be some kind of _tcp_translate() to translate
    // raw event-data structs into tcp events. Then, we would only need an internal way of creating and monitoring
    // a tcp_server without an object callback. We would get the raw event and just translate it using the API.
    //  -bmartin 3/11/21
    sys.serverFd = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    int reuse = true;
    setsockopt(sys.serverFd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse));
    efj::make_nonblocking(sys.serverFd);

    struct sockaddr_in address;
    address.sin_addr.s_addr = htonl(INADDR_ANY);
    address.sin_port        = htons(EFJ_DBP_DEFAULT_PORT);
    address.sin_family      = AF_INET;

    if (::bind(sys.serverFd, (struct sockaddr*)&address, sizeof(address)) == -1) {
        if (!efj::testing()) {
            efj_internal_log(efj::log_level_warn, "DBP server failed to bind() to port %u: %s: debugging disabled",
                    EFJ_DBP_DEFAULT_PORT, strerror(errno));
        }

        return;
    }

    if (::listen(sys.serverFd, 0) == -1) {
        efj_internal_log(efj::log_level_warn, "DBP server failed to listen(): %s: debugging disabled", strerror(errno));
        return;
    }

    efj::eventsys_epoll_desc serverDesc = {};
    serverDesc.dispatch     = &_debugsys_handle_server_event;
    serverDesc.udata.as_ptr = &sys;
    serverDesc.events       = efj::io_in | efj::io_oneshot;

    efj::_eventsys_create_epoll_event(sys.debugThread, "Debug Server", sys.serverFd, &serverDesc, &sys.serverEvent);
    efj::_eventsys_add_epoll_event(sys.debugThread, &sys.serverEvent);

    _cache_app_description(sys);

    efj_log_info("Debugging on port 65000");
}

//{
// Event-local read-locking of the connected-client fd. We don't care if the connected-client
// fd is -1 or valid, but we do care that it remains consistent during the handling of any event.
//
static inline int
_lock_client_fd(efj::debugsys_thread_ctx& ctx)
{
    // We don't have to lock the client lock if we are on the debug thread,
    // since client updates only happen on that thread. :NoClientRaceOnDebugThread
    if (!ctx.isDebugThread) {
        efj_assert(!ctx.lockedClient);
        //printf("locking client on thread %s %d\n", ctx.thread->name, ctx.captureEvents);
        pthread_rwlock_rdlock(&ctx.sys->clientLock);
        ctx.lockedClient = true;
    }

    return ctx.sys->connectedClientFd;
}

static inline void
_unlock_client_fd(efj::debugsys_thread_ctx& ctx)
{
    // :NoClientRaceOnDebugThread
    if (!ctx.isDebugThread) {
        efj_assert(ctx.lockedClient);
        //printf("unlocking client on thread %s %d\n", ctx.thread->name, ctx.captureEvents);

        int res = pthread_rwlock_unlock(&ctx.sys->clientLock);
        ctx.lockedClient = false;

        // FIXME: This doesn't really seem to work. The man page says we can get EPERM if we aren't holding the
        // lock, but it also says it's undefined. In practice, it seems this function returns 0 even in that case.
        // @NeedsTesting
        if (res != 0) {
            efj_internal_log(efj::log_level_crit, "Tried to unlock the client mutex without holding it: %s",
                strerror(errno));
            efj_panic("unlocked a mutex without holding it");
        }
    }
}
//}


//{ Event handling hooks
static EFJ_FORCE_INLINE void
_debugsys_begin_event_impl(efj::debugsys_thread_ctx& ctx, const efj::eventsys_event_data* edata)
{
    //efj::print("Event: %s\n", edata->event->name);
    ctx.touchedObjects.clear();

    // NOTE(bmartin): We want to reevaluate whether we should be capturing events every event, but
    // the case of a debugger disconnecting is rare, so we will only check to see if there is a client
    // connected at the beginning of each event, and trust that decision throughout the event. This means
    // that if a client connects or disconnects during an event, we may skip or store one event that we
    // weren't supposed to, which is fine. This way, thread-dbp storing functions can just check a boolean.

    if (ctx.startCapture) {
        ctx.captureEvents = true;
        ctx.startCapture = false;
    }

    if (ctx.captureEvents) {
        int fd = _lock_client_fd(ctx);

        // Right now, we need to turn off event capturing ourselves, since we don't have a "Stop Capture" thread event.
        if (fd == -1) {
            //printf("Turning off event capture on thread %s\n", ctx.thread->name);
            _unlock_client_fd(ctx);
            ctx.captureEvents = false;
            return;
        }
    }

    struct timespec now;
    ::clock_gettime(CLOCK_MONOTONIC, &now);

    efj::dbp_timestamp dbpNow = { (u32)now.tv_sec, (u32)now.tv_nsec };

    ctx.curEvent.begin = dbpNow;
    ctx.curEvent.id    = edata->id;
    ctx.curEvent.flags = 0;
}

static EFJ_MACRO void
_debugsys_object_touched_impl(efj::object* object EFJ_LOC_SIG)
{
    efj_assert(object);

    // Make sure objects are only "touched" once consecutively during an event.
    // Otherwise, we probably have redundant calls to this function that need to be removed.
    efj::debugsys_thread_ctx* ctx = object->thread->debugContext;

#if EFJ_DEBUG_LOCATIONS
    efj::print("%s touched from %s:%d, last object: %s\n", object->name, loc.file, loc.line,
        ctx->lastTouchedObject ? ctx->lastTouchedObject->name : "(null)");
#endif
    // NOTE: There is a time before event loops are started where we may not have a current event,
    // like during app init() and while testing. In those cases, an object may be touched back-to-back
    // without that being an error. TODO: We would probably like some kind of anonymous/manual event
    // API to support this to make debugger output look reasonable for test code in the future, but we
    // currently don't support debugging test code, so it doesn't matter. For now, we just special-case
    // this condition.
    //
    // :FirstEventFlag
    //
#if EFJ_DEBUG_OBJECT_TOUCHING
    if (ctx->curEvent.id != UINT32_MAX && ctx->lastTouchedObject == object)
        efj_internal_log(efj::log_level_warn, "%s touched twice in the same event", object->name);
#endif

    ctx->lastTouchedObject = object;

    object->thread->debugContext->touchedObjects.add(object);
}

//{ thread change data
struct watch_change_info
{
    efj::watched_primitive* watch;
    u32                     valueSize;
};

struct thread_change_info
{
    efj::array<efj::watch_change_info> changes;
};


static bool
_update_watch_values(efj::debugsys_thread_ctx& ctx, efj::thread_change_info* info)
{
    info->changes.reset(efj::temp_memory());

    // NOTE: Right now, objects may be added to the touched list multiple times. We want to process them uniquely.
    // TODO: We almost certainly want to leave the touched list as duplicates, so we can track the order of
    // that object are touched in.
    //
    bool touchedObjectHandled[ctx.sys->objectCount] = {};

    // We need to do this in 2 passes b/c the number of changes has to come before the actual change data,
    // and we don't know the number of changes until we have looped through all of the watches.

    // IMPORTANT: Changes are only reported for objects touched by this event. This doesn't help debug threading
    // issues where watch values are poked by other threads.
    for (efj::object* object : ctx.touchedObjects) {
        if (touchedObjectHandled[object->id])
            continue;

        if (object->id > ctx.sys->objectCount) {
            efj_assert(false);
            continue;
        }
        touchedObjectHandled[object->id] = true;

        for (efj::watched_primitive* watch : object->debugContext->watchedPrimitives) {
            u32 valueSize = (u32)dbp_get_value_size((dbp_value_type)watch->type);
            if (valueSize == 0) {
                efj_assert(false);
                continue;
            }

            // @Speed: Special casing could be faster for primitives.
            if (memcmp(watch->as_u8, &watch->old_u8, valueSize) != 0) {
                auto* change      = info->changes.add_space();
                change->watch     = watch;
                change->valueSize = valueSize;

                memcpy(&watch->old_u8, watch->as_u8, valueSize);
            }
        }

        // TODO: strings
    }

    return info->changes.size() > 0;
}

static void
_push_thread_changes(efj::debugsys_thread_ctx& ctx, const efj::thread_change_info* info)
{
    efj_rare_assert(info->changes.size() <= EFJ_UINT16_MAX);
    u16 changeCount = (u16)info->changes.size();

    if (changeCount) {
        efj_assert(ctx.watchedPrimitives.size() || ctx.watchedStrings.size());
    }

    if (changeCount) {
        ctx.threadEventsBuffer.begin_event_data(dbp_event_data_changes, "Changes");
        ctx.threadEventsBuffer.push_event_data(&changeCount, sizeof(u16), "Watch Count");

        for (auto& change : info->changes) {
            dbp_watch_changed watchChanged = {};

            watchChanged.watchId = change.watch->id;
            watchChanged.type    = change.watch->type;

            ctx.threadEventsBuffer.push_event_data(&watchChanged, sizeof(watchChanged), "Watch Changed Struct");
            ctx.threadEventsBuffer.push_event_data(change.watch->as_u8, change.valueSize, "Value");
        }
    }
}
//}

// Pushes the actual event struct, format data, and common event data.
static void
_push_thread_event(efj::debugsys_thread_ctx& ctx, const struct timespec& timestamp, u8 flags = 0)
{
    ctx.curEvent.flags      = flags;
    ctx.curEvent.end.sec    = (u32)timestamp.tv_sec;
    ctx.curEvent.end.nsec   = (u32)timestamp.tv_nsec;
    ctx.curEvent.formatSize = 0;

    bool hasData = false;

    efj::thread_change_info changes;
    hasData = hasData || _update_watch_values(ctx, &changes);

    // Notes for this event have already been stored in the note buffer.
    // TODO: push notes
    ctx.noteBuffer.at = ctx.noteBuffer.begin;

    if (hasData) ctx.curEvent.flags |= dbp_event_has_data;

    if (DEBUGSYS_DEBUG_THREAD_EVENTS_BUFFER) {
        if (ctx.thread->logicalId == 0)
            efj_internal_log(efj::log_level_debug, "%s: %d", __func__, ctx.curEvent.id);
    }

    ctx.threadEventsBuffer.push_event(ctx.curEvent);
    _push_thread_changes(ctx, &changes);
}

static EFJ_FORCE_INLINE void
_debugsys_end_event_impl(efj::debugsys_thread_ctx& ctx, const efj::eventsys_event_data* edata)
{
    ctx.lastTouchedObject = nullptr;

    //efj::print("End event %s\n", edata->event->name);
    if (!ctx.captureEvents) return;

    if (ctx.crashed) {
        // If we've crashed, make sure that this isn't being called on the thread that
        // crashed, since that should never happen.

        if (ctx.thread == efj::this_thread()) {
            efj_assert(false);
            return;
        }
    }

    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);

    ctx.threadEventsBuffer.begin_all_event_data();

    if (DEBUGSYS_DEBUG_THREAD_EVENTS_BUFFER) {
        if (ctx.thread->logicalId == 0)
            efj_internal_log(efj::log_level_debug, "%s: %d", __func__, ctx.curEvent.id);
    }

    u8 flags = 0;
    if (edata->stale) flags |= efj::dbp_event_is_stale;

    _push_thread_event(ctx, now, flags);
    ctx.threadEventsBuffer.end_all_event_data();

    _unlock_client_fd(ctx);
}

static EFJ_FORCE_INLINE void
_debugsys_begin_frame_impl(efj::debugsys_thread_ctx&)
{
}

// Called from the debug thread or whichever thread called exit. Not thread-safe.
static void
_send_all_thread_events(efj::debug_system& sys)
{
    // :NoClientRaceOnDebugThread
    if (sys.connectedClientFd == -1)
        return;

    for (efj::thread* thread : efj::threads()) {
        thread->debugContext->dbpQueue.write_to_debugger(sys.connectedClientFd);
    }
}

static EFJ_FORCE_INLINE void
_debugsys_end_frame_impl(efj::debugsys_thread_ctx& ctx)
{
    if (!ctx.captureEvents)
        return;

    // IMPORTANT: The order matters here. Write events before registrations and deregistrations. This is to
    // maintain the order between thread events and the event regsiter/deregister messages specified in the protocol.
    ctx.threadEventsBuffer.write_to_queue();
    ctx.eventRegistrationBuffer.write_to_queue();
}
//}

// On the debug thread.
static void
_debugsys_handle_server_event(efj::eventsys_event_data* edata)
{
    auto* sys = (efj::debug_system*)edata->udata.as_ptr;

    if (edata->epoll.events & efj::io_in) {
        struct sockaddr_in clientAddress = {};
        socklen_t len = sizeof(clientAddress);

        sys->pendingClientFd = ::accept(sys->serverFd, (struct sockaddr*)&clientAddress, &len);
        inet_ntop(AF_INET, &clientAddress.sin_addr, sys->debuggerIp, sizeof(sys->debuggerIp));

        efj_internal_log(log_level_info, "Accepted debug connection from (%s)", sys->debuggerIp);

        efj::eventsys_epoll_desc desc = {};
        desc.dispatch     = &_debugsys_handle_pending_client_event;
        desc.udata.as_ptr = sys;
        desc.events       = efj::io_in | efj::io_rdhup;
        efj::_eventsys_create_epoll_event(sys->debugThread, "Pending Debugger", sys->pendingClientFd,
                &desc, &sys->pendingClientEvent);

        efj::_eventsys_add_epoll_event(sys->debugThread, &sys->pendingClientEvent);

        sys->dbpParser.reset(sys->dbpParseBuf, sizeof(sys->dbpParseBuf));
    }
}

static void
_rearm_server_event(efj::debug_system* sys)
{
    // The server event is io_oneshot, so we don't have to worry about stale events, meaning
    // we can just rearm the event instead of destroying and re-creating it, like we have to do
    // for the client.
    efj::epoll_events rearmEvents = efj::io_in | efj::io_oneshot;
    efj::_eventsys_mod_epoll_event(sys->debugThread, &sys->serverEvent, &rearmEvents, nullptr);
}

// On the debug thread. A potential debugger is connecting. We need to handle the connection
// handshake and figure out if the connection is going to be accepted.
static void
_debugsys_handle_pending_client_event(efj::eventsys_event_data* edata)
{
    auto* sys = (efj::debug_system*)edata->udata.as_ptr;
    efj_assert(efj::this_thread() == sys->debugThread);

    efj::dbp_parser* parser = &sys->dbpParser;

    // If the read size is zero, either token parsing has failed or the parse buffer wasn't reset correctly.
    umm readSize = parser->read_size();
    efj_assert(readSize);

    ssize_t nRead = ::read(edata->epoll.fd, parser->read_at(), readSize);

    efj::epoll_events events = edata->epoll.events;
    bool disconnected = nRead <= 0 || events & efj::io_rdhup || events & efj::io_hup || events & efj::io_err;

    if (disconnected) {
        efj_internal_log(log_level_info, "Pending debugger (%s) disconnected.", sys->debuggerIp);
        sys->pendingClientFd = -1;
        efj::_eventsys_destroy_epoll_event(sys->debugThread, &sys->pendingClientEvent);
        _rearm_server_event(sys);
        return;
    }

    parser->read_bytes(nRead);
    for (const efj::dbp_token* tok = parser->parse(); tok; tok = parser->parse()) {
        switch (tok->type) {
        case efj::dbp_token_connect: {
            // Shouldn't get this message more than once. It only makes sense for pending clients.
            if (sys->connectedClientFd != -1)
                break;

            efj::dbp_connect msg = {};
            memcpy(&msg, tok->data, tok->size);

            efj_internal_log(log_level_info, "Debugger connected: %s: version %u.%u.", sys->debuggerIp,
                msg.versionMajor, msg.versionMinor);

            // FIXME: Sure, it makes sense that we can reject the connection, but it's really the debugger's responsibility
            // to check for compatibility. We have no way of knowing what versions this debugger supports. @Robustness
            dbp_connect_resp resp = {};
            resp.header.type = dbp_message_connect_resp;
            resp.header.size = sizeof(resp);
            resp.success     = true;
            _send_all(sys->pendingClientFd, &resp, sizeof(resp));

            sys->appDescriptionBuffer.write_to_debugger(sys->pendingClientFd);

            // NOTE: We currently have to this b/c destroying the epoll event currently implies closing the fd. @Cleanup
            int connectedClientFd = dup(sys->pendingClientFd);

            // NOTE: We have to destroy and create a new event b/c the event API currently only allows changing the object
            // callback when calling monitor(). We are handling these events directly through the dispatch function, which
            // is expected to not change. TODO: Consider changing the event API to allow this. Maybe that would just be a
            // generic monitor call with flags indicating whether pending events should be marked as stale or not. @Cleanup
            //
            // efj::eventsys_epoll_desc desc = {}; // some desc
            // efj::_eventsys_monitor_epoll_event(&event, &desc, efj::monitor_not_stale /*efj::monitor_mark_stale*/);
            //
            sys->pendingClientFd = -1;
            efj::_eventsys_destroy_epoll_event(sys->debugThread, &sys->pendingClientEvent);

            // :NoClientRaceOnDebugThread
            pthread_rwlock_wrlock(&sys->clientLock);
            sys->connectedClientFd = connectedClientFd;
            pthread_rwlock_unlock(&sys->clientLock);

            efj::eventsys_epoll_desc desc = {};
            desc.dispatch     = &_debugsys_handle_connected_client_event;
            desc.udata.as_ptr = sys;
            desc.events       = efj::io_in | efj::io_rdhup;
            efj::_eventsys_create_epoll_event(sys->debugThread, "Debugger", sys->connectedClientFd, &desc,
                &sys->connectedClientEvent);
            efj::_eventsys_add_epoll_event(sys->debugThread, &sys->connectedClientEvent);

            for (efj::thread* thread : efj::threads())
                efj::_eventsys_trigger(&thread->debugContext->startCaptureEvent);

            // NOTE: At the time of writing, the debug thread doesn't wait for threads to realize that a connection has dropped.
            // If we don't wait for them to notice, we could end up with data from the previous connection in a thread's queue
            // if the debug thread tries to send data for a thread that hasn't finished handling its start-capture event.
            // Right now, we sync up by waiting for all threads to handle their start-capture-event before starting our timer, but
            // we could do this a different way, e.g. by checking a flag before sending events for a thread.
            //
            sys->startCaptureFence.wait_for_hits(efj::thread_count()-1);

            efj::_eventsys_start_timer(&sys->sendTimerEvent);
            break;
        }
        default:
            efj_invalid_code_path();
            break;
        }
    }
}

// On the debug thread.
static void
_debugsys_handle_connected_client_event(efj::eventsys_event_data* edata)
{
    auto* sys = (efj::debug_system*)edata->udata.as_ptr;
    efj_assert(efj::this_thread() == sys->debugThread);

    efj::dbp_parser* parser = &sys->dbpParser;

    // @Temporary
    static u8 watchEditedBuf[sizeof(efj::dbp_watch_edited) + 128] alignas(efj::dbp_watch_edited);
    static umm watchEditedProgress = 0;

    efj::epoll_events events = edata->epoll.events;
    umm nRead = 0;

    bool disconnected = events & efj::io_rdhup || events & efj::io_hup || events & efj::io_err;
    if (!disconnected && events & efj::io_in) {
        umm readSize = parser->read_size();
        efj_assert(readSize);

        nRead = ::read(edata->epoll.fd, parser->read_at(), readSize);
        if (nRead <= 0)
            disconnected = true;
    }

    if (disconnected) {
        efj_internal_log(log_level_info, "Debugger (%s) disconnected.", sys->debuggerIp);
        efj::_eventsys_stop_timer(&sys->sendTimerEvent);

        //{
        // Make sure we don't close the socket while some thread is trying to write to it, the race
        // being that some third thread could open a file and get the recycled FD number just before the write.
        // :NoClientRaceOnDebugThread
        pthread_rwlock_wrlock(&sys->clientLock);
        sys->connectedClientFd = -1;
        pthread_rwlock_unlock(&sys->clientLock);
        //}

        efj::_eventsys_destroy_epoll_event(sys->debugThread, &sys->connectedClientEvent);
        _rearm_server_event(sys);
        return;
    }

    //printf("Read %d bytes\n", nRead);

    parser->read_bytes(nRead);
    for (auto* tok = parser->parse(); tok; tok = parser->parse()) {
    switch (tok->type) {
    case efj::dbp_token_watch_edited_batch: {
        efj::dbp_watch_edited_batch msg = {};
        memcpy(&msg, tok->data, sizeof(msg));

        auto* thread = efj::thread_by_id(msg.ltid);
        if (thread) {
            //efj_internal_log(efj::log_level_debug, "Got batch for thread %u (%s), size = %u", msg.ltid, thread->name, msg.count);
            thread->debugContext->editQueue.push_batch(msg);
        }

        break;
    }
    case efj::dbp_token_watch_edited_begin:
        watchEditedProgress = 0;
    case efj::dbp_token_watch_edited_content:
    case efj::dbp_token_watch_edited_end: {
        efj::dbp_accumulate(tok, &watchEditedBuf, sizeof(watchEditedBuf), &watchEditedProgress);
        if (tok->type == efj::dbp_token_watch_edited_end) {
            auto* msg   = (efj::dbp_watch_edited*)watchEditedBuf;
            auto* watch = (efj::watched_primitive*)msg->watchKey;

            efj_assert(watch && "Debugger edited a watch before it was registered?");

            if (watch->type != efj::debug_value_string) {
                efj::debugsys_watch_edit edit = {};
                edit.o    = watch->object;
                edit.cb   = watch->cb;
                edit.dst  = watch->as_u8;
                edit.type = (efj::debug_value_type)watch->type; // @Robustness. Assumes dbp value type and edit value type are the same.
                edit.size = watchEditedProgress - sizeof(*msg);

                memcpy(edit.src, msg->value(), edit.size);

                auto* thread = watch->thread;
                if (thread) {
                    //efj_internal_log(efj::log_level_debug, "Queueing edit on thread %u (%s)", thread->logicalId, thread->name);
                    // We only tell the other thread about the edits in its queue when we are done pushing the current batch.
                    if (thread->debugContext->editQueue.push(edit)) {
                        //efj_internal_log(efj::log_level_debug, "Triggering event fd");
                        efj::_eventsys_trigger(&thread->debugContext->editQueueEvent);
                    }
                }
            }
        }

        break;
    }
    default:
        efj_internal_log(log_level_warn, "Invalid DBP token read from debug client: %d", tok->type);
        disconnected = true;
        break;
    }
    }

    if (parser->error) {
        efj_internal_log(log_level_warn, "DBP parse error: %s. Disconnecting.", parser->error);
        disconnected = true;
    }
}

// On the debug thread. Time to send DBP to the debugger.
static void
_debugsys_handle_send_timer_event(efj::eventsys_event_data* edata)
{
    auto* sys = (efj::debug_system*)edata->udata.as_ptr;
    efj_assert(efj::this_thread() == sys->debugThread);
    efj_assert(sys->connectedClientFd != -1);

    _send_all_thread_events(*sys);
}

// On the thread that has a pending edit.
static void
_debugsys_handle_edit_queue_event(efj::eventsys_event_data* edata)
{
    auto* queue = (efj::debugsys_edit_queue*)edata->udata.as_ptr;

    //efj_internal_log(efj::log_level_debug, "[%u] Got edit queue events", efj::logical_thread_id());

    efj::debugsys_edit_queue_buffer* buf = queue->swap_buffers();
    for (efj::debugsys_watch_edit* edit = buf->begin; edit != buf->at; edit++) {
        //efj_internal_log(efj::log_level_debug, "Got edit on thread %s", thread->name);

        if (edit->cb.type == efj::callback_invalid) {
            //printf("poking watch at %p\n", edit->dst);
            // No callback was specified, so we just overwrite the value directly.
            memcpy(edit->dst, edit->src, edit->size);
            continue;
        }

        // TODO: more generic callback that can also be implemented by threads for thread-level values
        // that aren't associated with an object (like the current logging level).
        if (edit->o)
            edit->o->notify_value_edit(edit->type, edit->dst, edit->src, edit->size, edit->cb);
    }
}

// Handled on every thread to send initial data to the debugger, e.g. current watch values,
// event registrations, etc.
static void
_debugsys_handle_start_capture_event(efj::eventsys_event_data* edata)
{
    auto* ctx = (efj::debugsys_thread_ctx*)edata->udata.as_ptr;

    ctx->dbpQueue.new_connection();
    ctx->threadEventsBuffer.new_connection();
    ctx->eventRegistrationBuffer.new_connection();

    // We use a thread_dbp_buffer for this so the debug thread will do the writing to the socket and
    // so we will reuse this memory for thread events messages. Otherwise, we will just be wasting memory
    // and cause a bunch of threads to block on each other at the start of each connection as they all try
    // to write to the socket atomically.
    // IMPORTANT: We can _not_ write to the thread dbp queue here, b/c the buffers in the dbp buffer we are using are broken
    // up arbitrarily. Other buffers may make it to the debugger in-between these if we queue them up, so we send them to the debugger directly.
    // This required a hack that added a write_to_debugger() function to the thread_dbp_buffer, which really shouldn't have one. Doing this means
    // we can reuse this memory, though.
    // @Slow: All threads will lock up when writing their descriptions.
    // TODO: Just Make all messages thread-local or something, so we can lock only around buffers or however much data
    // we want to try to send in one write. :StartCaptureBufferProblem
    //
    // NOTE: We _could_ get this to work while writing it to the thread dbp queue if we made sure to write all messages contiguously, but that would be a pain.
    //
    efj::thread_dbp_buffer* buffer = &ctx->startCaptureBuffer;
    buffer->new_connection();

    efj::dbp_register_watch regWatch = {};
    regWatch.header.type = efj::dbp_message_register_watch;

    regWatch.ltid = ctx->thread->logicalId;
    for (efj::watched_primitive& watch : ctx->watchedPrimitives) {
        regWatch.header.size = sizeof(efj::dbp_register_watch);

        regWatch.watchKey  = _pointer_to_key(&watch);
        regWatch.watchId   = watch.id;
        regWatch.objectId  = watch.object ? watch.object->id : UINT32_MAX;
        regWatch.valueType = watch.type;
        //printf("Watch key: %llu %p\n", regWatch.watchKey, &watch);

        umm valueSize = efj::dbp_get_value_size((efj::dbp_value_type)watch.type);
        efj_assert(valueSize != 0);

        if (valueSize == 0) {
            efj_internal_log(log_level_warn, "Skipping invalid watch \"%s::%s\"",
                    watch.object->name, watch.name.c_str());
            continue;
        }

        u32 watchNameSize = watch.name.capacity(); // include nul
        regWatch.header.size += watchNameSize + valueSize;

        buffer->push_packed(&regWatch, sizeof(regWatch));
        buffer->push_packed(watch.name.data, watchNameSize);
        buffer->push_packed(watch.as_u8, valueSize);
    }

    // FIXME: @Incomplete. Strings aren't really implemented, yet. And if they ever are,
    // they are probably going to have to be represented by string_builders instead of plain
    // strings, because of the need to handle changes of variable lengths to and from the
    // debugger. We would just clear() and overwrite the user-facing builder with a shallow-copy
    // of the internal string_builder.
    for (efj::watched_string& watch : ctx->watchedStrings) {
        regWatch.watchKey  = _pointer_to_key(&watch);
        regWatch.watchId   = watch.id;
        regWatch.objectId  = watch.object ? watch.object->id : UINT32_MAX;
        regWatch.valueType = efj::dbp_value_string;
        //printf("Watch key: %llu %u\n", regWatch.watchKey, sizeof(&watch));

        u32 watchNameSize  = watch.name.capacity();
        u32 watchValueSize = watch.size + 1; // include nul

        regWatch.header.size += watchNameSize + watchValueSize;

        buffer->push_packed(&regWatch, sizeof(regWatch));
        buffer->push_packed(watch.name.data, watchNameSize);
        buffer->push_packed(watch.value, watchValueSize); // include nul
    }

    efj::debugsys_event_ctx* cur = nullptr;
    efj_list_for_each_entry(cur, &ctx->registeredEvents, node) {
        _push_register_event_message(*buffer, *cur->edata);
    }

    // @Hack :StartCaptureBufferProblem
    buffer->write_to_debugger(ctx->sys->connectedClientFd);
    ctx->startCapture = true;
}

// Called from whichever thread called efj::exit(). The application is exiting, all threads are stopped
// (as best as we could), and the debug system will no longer be receiving events. We need to cleanup the
// connection and send any final messages here.
static void
_debugsys_handle_exit(efj::debug_system& sys)
{
    // We can't really do any cleanup or communicate with the debugger if we crashed
    // while executing debug code, since we don't know what state could be corrupted or what the cause
    // of the crash was. For example, we could have gone unresponsive trying to send data to the debugger
    // b/c the debugger wasn't able to read data fast enough. That will cause send()s that we do here to block.
    // We could handle that specific case better, but in general we should bail out.
    //
    // TODO: Check for the current thread crashing in debug code in the signal handler, so we can set one global
    // flag and just check that instead of doing this loop.
    //
    for (auto* thread : efj::threads()) {
        if (__atomic_load_n(&thread->debugContext->beingModified, __ATOMIC_CONSUME))
            return;
    }

    efj::thread* thisThread = efj::this_thread();

    // NOTE: Technically, this races with the debug thread when accessing the connected-client-fd under weird cases
    // like us failing to stop the debug thread and failing to lock the client fd, but that's such a
    // rare case that it doesn't matter. Worst-case scenario, we rely on our platforms atomicity of reads and writes
    // for ints to read the fd number of the connected client or -1. In either case, this code should work.
    //

    if (thisThread)
        _lock_client_fd(*thisThread->debugContext);

    if (sys.connectedClientFd != -1) {
        _send_all_thread_events(sys);

        dbp_shutdown msg = {};
        msg.header.type = dbp_message_shutdown;
        msg.header.size = sizeof(msg);

        _send_all(sys.connectedClientFd, &msg, sizeof(msg));

        // We are technically waiting for the shutdown response, but we don't care about reading it,
        // and we are just making a best effort to make sure that the debugger has received all the data that
        // we've sent to it, so we can just read any amount or get any error/timeout and that should be enough,
        // since we manually query how much data we have in the send-queue and wait for it to be zero.
        //{
        struct timespec timeout = {};
        timeout.tv_sec  = 0;
        timeout.tv_nsec = 5000000;

        fd_set set;
        FD_ZERO(&set);
        FD_SET(sys.connectedClientFd, &set);

        ::pselect(sys.connectedClientFd + 1, &set, nullptr, nullptr, &timeout, nullptr);
        //}

        const useconds_t kMaxWaitMicro = 1000000;
        const useconds_t kSleepMicro   = 100000;
        useconds_t waitTime = 0;

        for(;;) {
            if (waitTime >= kMaxWaitMicro)
                break;

            int outstanding = 0;
            ::ioctl(sys.connectedClientFd, SIOCOUTQ, &outstanding);
            if(outstanding == 0)
                break;

            ::usleep(kSleepMicro);
            waitTime += kSleepMicro;
        }

        // Right now we let the kernel close the socket, so it automatically lingers.
        // TODO: set the SO_LINGER option and close the socket here.
        // ::close(sys.connectedClientFd);
    }

    if (thisThread)
        _unlock_client_fd(*thisThread->debugContext);
}


//{
// Entry points for thread context modifications that are here so we don't mess up tracking whether
// debug code is executed correctly. We only care about this when a thread crashes and we need to
// determine what information we can send to the debugger. We could techinally crash in the debug
// code that is sending thread events to the debugger, for example, in which case all we can do
// is close the connection. TODO: If we use out-of-band data, or have logical connections with
// the debugger, we can report this kind of issue.
//
// NOTE: The use of an exchange instead of a store is to allow for acquire semantics, to
// guarantee that if another thread sees sys.beingModified = true, the flag was set before anything
// was actually modified.
//
static void
_debugsys_begin_frame(efj::debugsys_thread_ctx& ctx)
{
    efj_check(__atomic_exchange_n(&ctx.beingModified, true, __ATOMIC_ACQUIRE) == false);
    _debugsys_begin_frame_impl(ctx);
    __atomic_store_n(&ctx.beingModified, false, __ATOMIC_RELEASE);
}

static void
_debugsys_end_frame(efj::debugsys_thread_ctx& ctx)
{
    efj_check(__atomic_exchange_n(&ctx.beingModified, true, __ATOMIC_ACQUIRE) == false);
    _debugsys_end_frame_impl(ctx);
    __atomic_store_n(&ctx.beingModified, false, __ATOMIC_RELEASE);
}

// Called at the beginning and end of the handling of each event handled by a thread.
static void
_debugsys_begin_event(efj::debugsys_thread_ctx& ctx, const efj::eventsys_event_data* data)
{
    efj_check(__atomic_exchange_n(&ctx.beingModified, true, __ATOMIC_ACQUIRE) == false);
    _debugsys_begin_event_impl(ctx, data);
    __atomic_store_n(&ctx.beingModified, false, __ATOMIC_RELEASE);
}

static void
_debugsys_end_event(efj::debugsys_thread_ctx& ctx, const efj::eventsys_event_data* data)
{
    efj_check(__atomic_exchange_n(&ctx.beingModified, true, __ATOMIC_ACQUIRE) == false);
    _debugsys_end_event_impl(ctx, data);
    __atomic_store_n(&ctx.beingModified, false, __ATOMIC_RELEASE);
}

extern void
_debugsys_object_touched(efj::object* object EFJ_LOC_SIG)
{
    efj_check(__atomic_exchange_n(&object->thread->debugContext->beingModified, true, __ATOMIC_ACQUIRE) == false);
    _debugsys_object_touched_impl(object EFJ_LOC_ARGS_FWD);
    __atomic_store_n(&object->thread->debugContext->beingModified, false, __ATOMIC_RELEASE);
}
//}

// Called in the context of the shared signal handler for all signals. This function must be async-signal safe.
static void
_debugsys_handle_signal(efj::debugsys_thread_ctx& ctx, int sig, siginfo_t*, void* ucontext)
{
    if (ctx.crashed) {
        char msg[] = "[c2] The debug system's signal handler was reentered\n This shouldn't happen!\n";
        efj_ignored_write(STDERR_FILENO, msg, sizeof(msg));
        return;
    }
    ctx.crashed = true;

    ::clock_gettime(CLOCK_MONOTONIC, &ctx.crashTime);
}

// Called from the watchdog thread when it is time to exit after a crash.
// The thread context is the context of the thread that crashed, not the watchdog thread.
static void
_debugsys_thread_crashed(efj::debugsys_thread_ctx& ctx, int sig, const efj::string& trace)
{
    if (!ctx.captureEvents)
        return;

    if (__atomic_load_n(&ctx.beingModified, __ATOMIC_CONSUME)) {
        if (ctx.thread->does_logging()) {
            auto msg = efj::fmt("[c2] Crashed in debug code on thread \"%s\". Duplicating trace in since it won't get logged:\n%s\n",
                ctx.thread->name, trace.c_str());

            efj_ignored_write(STDERR_FILENO, msg.c_str(), msg.size);
        }
        else {
            efj_internal_log(efj::log_level_crit,
                    "Crashed while executing debug code. Unable to report this to the debugger.");
        }

        return;
    }

    // FIXME: We can't shouldn't write to the thread's queue from the watchdog thread, or we have to be careful about it.
    // The debug thread could have been the one to crash (or it could crash right after the original thread crashed), in which
    // case we can't rely on it to send the remaining data. If we just write this buffer to the thread's queue and write the queue
    // to the debugger, we race with the debug thread.

    ctx.threadEventsBuffer.begin_all_event_data();
    _push_thread_event(ctx, ctx.crashTime, dbp_event_has_data);
    ctx.threadEventsBuffer.begin_event_data(dbp_event_data_crash, "Crash");

    const char* signalName = strsignal(sig);
    umm signalNameSize = (umm)strlen(signalName);

    // @Cleanup. This code would be much simpler and less error-prone if we just made multiple push_event_data
    // calls. This code is not performance intensive.
    u8 msgBuf[sizeof(dbp_thread_crashed) + signalNameSize + trace.size + 2] alignas(dbp_thread_crashed) = {};

    auto* msg = (dbp_thread_crashed*)msgBuf;
    msg->header.type = dbp_message_thread_crashed;
    msg->header.size = sizeof(msgBuf);
    msg->ltid        = ctx.thread->logicalId;
    msg->sig         = sig;

    memcpy(msgBuf + sizeof(dbp_thread_crashed), signalName, signalNameSize + 1);
    memcpy(msgBuf + sizeof(dbp_thread_crashed) + signalNameSize + 1, trace.data, trace.size + 1);

    ctx.threadEventsBuffer.push_event_data(msg, sizeof(msgBuf), "Crash Message");
    ctx.threadEventsBuffer.end_all_event_data();

    _send_all_thread_events(*ctx.sys);

    // Since we shouldn't reach the end of this event, we have to do some cleanup.
    // :EventCleanup
    _unlock_client_fd(ctx);
}

// Called in the context of whichever thread called efj::exit(). This function isn't called from a signal handler.
// IMPORTANT: This is just a callback for when the app is exiting. This is different from the debug exit handler,
// which is called after all threads have stopped handling events and we are cleaning up. What we can do here is
// record information about what event was being handled when this thread called efj::exit().
//
static void
_debugsys_exit(efj::debugsys_thread_ctx& ctx, int code)
{
    if (!ctx.captureEvents)
        return;

    efj_assert(!ctx.crashed);

    struct timespec now = {};
    ::clock_gettime(CLOCK_MONOTONIC, &now);

    ctx.threadEventsBuffer.begin_all_event_data();
    efj_internal_log(efj::log_level_debug, "%s: %d", __func__, ctx.curEvent.id);
    _push_thread_event(ctx, now, dbp_event_has_data);
    ctx.threadEventsBuffer.begin_event_data(dbp_event_data_exit, "Exit");

    dbp_thread_exited msg = {};
    msg.header.type = dbp_message_thread_exited;
    msg.header.size = sizeof(msg);
    msg.ltid        = ctx.thread->logicalId;
    msg.code        = code;

    ctx.threadEventsBuffer.push_event_data(&msg, sizeof(msg), "Exit Message");
    ctx.threadEventsBuffer.end_all_event_data();

    // :EventCleanup
    _unlock_client_fd(ctx);
}

} // namespace efj
