namespace efj
{

template <typename T>
struct meta_object { static_assert(efj::type_dependent_false<T>::value, "[c2] no meta_object defined for your type!"); };


using meta_version_fn = const char*();
using meta_copy_fn    = void*(const void*);

// Place to store function pointers to all the meta object functions. It's often useful to pass one of these
// around so code doesn't need to be generated for _every_ meta type in a program. Instead, you can just take
// one of these and do it in a type erased fashion. This is similar to what we would get with virtual functions,
// but that makes defining meta objects more annoying and means we would end up going through a vtable instead of
// just directly using the function pointer that we need.
//
struct meta_object_base
{
    efj::meta_version_fn* versionFn = nullptr;
    efj::meta_copy_fn*    copyFn    = nullptr;
};

// Helper for not having to write this explicit specialization and inheritance out.
// IMPORTANT: Make sure you use this in the efj namespace.
#define EFJ_DEFINE_META_OBJECT(_type)\
template <> struct meta_object<_type> : efj::meta_object_base

#define EFJ_META_OBJECT(_type)\
static meta_object<_type>& instance() { static efj::meta_object<_type> val; return val; }\
meta_object()\
{\
    versionFn = &version;\
    copyFn    = (efj::meta_copy_fn*)&copy;\
}\

/* Usage:

EFJ_DEFINE_META_OBJECT(TypeName)
{
    // Yeah, it's annoying to have to use two different macros, but C++ outside of OOP wasn't thought through very well.
    // We want to provide code that automatically wires up the type-erased function pointers for the user.
    EFJ_META_OBJECT(TypeName);

    static const char* version() { return "1.0"; }

    static copy(const TypeName& msg)
    {
    }
};

*/

// free-function API

template <typename T> EFJ_FORCE_INLINE const char*
meta_version()
{
    return efj::meta_object<T>::version();
}

template <typename T> EFJ_FORCE_INLINE T*
meta_copy(const T& val)
{
    return (T*)efj::meta_object<T>::copy(val);
}

} // namespace efj

