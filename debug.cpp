namespace efj
{

extern void
debug_add_note(const char*, ...)
{
}

extern void
_debug_add_watch(const efj::string& name, char* buf, umm size, efj::callback cb, efj::thread* thread)
{
    // TODO: string support.
    efj_not_implemented();
    return;

    auto* object       = efj::this_object();
    auto* owningThread = thread ? thread : object->thread;
    auto* ctx          = owningThread->debugContext;
    u32   id           = ctx->watchedStrings.size();

    auto& watch  = *ctx->watchedStrings.add_default();
    watch.thread = owningThread;
    watch.object = object;
    watch.cb     = cb;
    watch.size   = size;
    watch.id     = id;

    efj_allocator_scope(*ctx->arena);
    watch.name = name.dup();

    //watch.id = object->debugContext->watchedStrings.size();
    //object->debugContext->watchedStrings.add(&watch);
}

#define EFJ__DEFINE_ADD_WATCH_CB(_type)\
extern void \
_debug_add_watch(const efj::string& name, _type* value, efj::callback cb, efj::thread* thread)\
{\
    auto* object       = efj::this_object();\
    auto* owningThread = thread ? thread : object->thread;\
    auto* ctx          = owningThread->debugContext;\
    u32   id           = ctx->watchedPrimitives.size();\
\
    auto& watch       = *ctx->watchedPrimitives.add_default();\
    watch.thread      = owningThread;\
    watch.object      = object;\
    watch.cb          = cb;\
    watch.type        = dbp_value_##_type;\
    watch.id          = id;\
    watch.as_##_type  = value;\
    watch.old_##_type = *value;\
\
    efj_allocator_scope(*ctx->arena);\
    watch.name = name.dup();\
\
    if (object) {\
        object->debugContext->watchedPrimitives.add(&watch);\
    }\
}

EFJ__DEFINE_ADD_WATCH_CB(s8)
EFJ__DEFINE_ADD_WATCH_CB(s16)
EFJ__DEFINE_ADD_WATCH_CB(s32)
EFJ__DEFINE_ADD_WATCH_CB(s64)
EFJ__DEFINE_ADD_WATCH_CB(u8)
EFJ__DEFINE_ADD_WATCH_CB(u16)
EFJ__DEFINE_ADD_WATCH_CB(u32)
EFJ__DEFINE_ADD_WATCH_CB(u64)
EFJ__DEFINE_ADD_WATCH_CB(bool)
EFJ__DEFINE_ADD_WATCH_CB(f32)
EFJ__DEFINE_ADD_WATCH_CB(f64)

#undef EFJ__DEFINE_ADD_WATCH_CB

} // namespace efj
