namespace efj
{

// TODO: cache this at the start of each event. @Slow
inline bool
should_log(int level)
{
    if (!(efj::this_thread()->logged & level)) return false;

    auto* o = efj::this_object();
    if (!o) return true;
    if (!(o->logContext->logged & level)) return false;

    return true;
}

EFJ_MACRO void
log_unformatted(bool yes)
{
    efj::context().logUnformatted = yes;
}

EFJ_MACRO void
log_enable_level(efj::object* o, efj::log_level level)
{
    o->logContext->logged |= level;
}

EFJ_MACRO void
log_disable_level(efj::object* o, efj::log_level level)
{
    o->logContext->logged &= ~level;
}

EFJ_MACRO efj::log_level
log_get_level(efj::object* o, efj::log_level level)
{
    return o->logContext->logged;
}


EFJ_MACRO void
log_set_level(efj::object* o, efj::log_level level)
{
    o->logContext->logged = level;
}

inline void
_log(int level, const char* file, int line, const char* fmt, ...)
{
    va_list va;
    va_start(va, fmt);
    _log_va(level, file, line, fmt, va);
    va_end(va);
}

inline void
_logx(int level, const char* file, int line, const char* fmt, ...)
{
    va_list va;
    va_start(va, fmt);
    _log_va(level, file, line, fmt, va);
    va_end(va);
}

} // end namespace efj
