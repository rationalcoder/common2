namespace efj
{

static const efj::nsec WATCHDOG_CHECK_ACTIVITY_INTERVAL = 2_s;

// Turn asserts into crashes to get a good stack trace.
extern void
assertion_failed(const char* format, const char* file, int line, const char* func)
{
    // Give tests a way to detect that we have crashed due to an assertion failure even though
    // we may decide to cause an actual crash to get a better stack trace.
    __atomic_store_n(&efj::app()._assertionFailed, true, __ATOMIC_RELAXED);

    // If we have already asserted, don't crash a second time or the watchdog system will detect
    // a "bad crash" and not print any stack information.
    static thread_local bool asserted = false;
    efj::print_error(format, file, line, func);

    if (asserted) {
        ::raise(SIGABRT);
        return;
    }
    asserted = true;

    // We use address 10 instead of 0 to avoid UB sanitizer warnings.
    // We can't get away from address sanitizer warnings, though.
    int* crash_here = (int*)0x10;
    *crash_here = 10;
}

// Write and error and try to re-raise a signal. If we can't, we just have to exit.
static void
_handle_bad_crash(int sig, const char* buf, int size)
{
    efj::print_error("[c2] error: Handling bad crash on thread %u (%s) (pid=%d)\n",
         efj::logical_thread_id(), efj::this_thread()->name, getpid());

    efj_ignored_write(STDERR_FILENO, buf, (size_t)size);

    // Right now, we always just raise a seg fault, since we know the default
    // handler should core dump and that the signal is thread-directed.
    int reraisedSignal = SIGSEGV;

    struct sigaction defaultAction = {};
    defaultAction.sa_handler = SIG_DFL;

    if (::sigaction(reraisedSignal, &defaultAction, nullptr) != 0) {
        efj::print_error("[c2] error: Failed to reset to default handler for signal %d (%s). Exiting.\n",
            sig, strsignal(sig));

        ::_exit(efj::exit_bad_crash);
    }

    // Ignore aborts b/c reraising a that signal in this thread will crash. This is probably b/c raise() is called
    // on this thread already. @NeedsTesting.
    if (sig != SIGABRT && ::pthread_kill(::pthread_self(), reraisedSignal) != 0) {
        efj::print_error("[c2] error: Failed to reraise signal %d (%s). Exiting.\n", sig, strsignal(sig));

        // We are forcefully exiting here without waiting for other threads to do anything, so we sleep for a bit
        // to give threads like the logging thread a chance to finish logging. :SleepForABitBeforeHardExit
        usleep(1000000);
        ::_exit(efj::exit_bad_crash);
    }

    // :SleepForABitBeforeHardExit
    usleep(1000000);
    ::_exit(efj::exit_bad_crash);
}

//! Prints a minimal stack trace trace in an async-signal-safe way (without name demangling).
static void
_print_trace_safe(int fd, void* const* symbols, umm count, char* buf, umm bufSize)
{
    // We are doing more work than necessary by calling sprintf to do buffer size checking for us,
    // but it shouldn't matter since this is only called when crashing.

    int offset = 0;
    int result = stbsp_snprintf(buf, bufSize, "[%s] Trace:\n{\n", efj::this_thread()->name);
    if (result > 0)
        offset += result;

    for (umm i = 0; i < count; i++) {
        int result = stbsp_snprintf(buf + offset, bufSize - offset, "0x%x\n", symbols[i]);
        if (result > 0)
            offset += result;
    }

    result = stbsp_snprintf(buf + offset, bufSize - offset, "}\n");
    if (result > 0)
        offset += result;

    // If we assert, here, we will get a bad crash without stack information, which is fine.
    umm messageSize = (umm)offset;
    //printf("Message size: %zu, buf size: %zu\n", messageSize, bufSize);
    efj_assert(messageSize <= bufSize);
    efj_ignored_write(fd, buf, messageSize);
}

// TODO: Special case the watchdog thread, which shouldn't crash. We should probably only core dump in the
// watchdog thread, for example.
static void
_handle_signal(int sig, siginfo_t* action, void* ucontext)
{
    char warningBuf[512];

    efj::thread* thread = efj::this_thread();
    efj::watchdog_thread_ctx* ctx = thread->watchdogContext;
    efj::watchdog_system* sys = efj::app()._watchdogSystem;

    efj::print("Handling signal %d (%s) on thread %s (pid=%d)\n", sig, strsignal(sig), thread->name, getpid());

    // NOTE: We originally reverted to the original signal handlers that were installed before we set our
    // own, but when we are crashing in a bad way, we don't really want to trust any signal handler other
    // than the default.

    switch (sig) {
    case EFJ_THREAD_UNRESPONSIVE_SIGNAL:
    case SIGSEGV:
    case SIGABRT:
    case SIGBUS:
    case SIGILL: {
        
        // Flush log messages if the watchdog thread isn't running. 
        if (!__atomic_load_n(&efj::app()._backgroundThreadsContinued, __ATOMIC_RELAXED))
            efj::_logsys_write_log_messages(efj::app()._logSystem);
        // Flush test log messages if a test crashed the main thread.
        if (efj::testing() && efj::this_thread() == efj::app()._mainThread)
            efj::_testsys_write_log_messages(globalCurrentTest);

        if (ctx->crashed) {
            int size = stbsp_snprintf(warningBuf, sizeof(warningBuf),
                "[c2] error: Signal handler re-entered with signal %d (%s)\n", sig, strsignal(sig));

            _handle_bad_crash(sig, warningBuf, (umm)size);
        }
        __atomic_store_n(&ctx->crashed, true, __ATOMIC_RELAXED);

        // Only let one thread crash once.
        int value     = 1;
        int prevValue = 0;
        __atomic_exchange(&sys->threadCrashed, &value, &prevValue, __ATOMIC_RELAXED);

        // Some other thread got here before we did.
        if (prevValue == 1) {
            efj::print_error("[c2] warning: Ignoring crash on thread \"%.20s\" b/c another thread already did.\n",
                thread->name);

            pthread_exit(NULL);
            break;
        }

        // All of these are relaxed because we expect the appropriate barriers to be issued when triggering an event.
        __atomic_store_n(&sys->crashedThread, thread, __ATOMIC_RELAXED);
        __atomic_store_n(&sys->crashedObject, efj::this_object(), __ATOMIC_RELAXED);
        __atomic_store_n(&sys->crashedEventName, efj::context().thisEventName, __ATOMIC_RELAXED);
        __atomic_store_n(&sys->crashSignal, sig, __ATOMIC_RELAXED);

        // The watchdog thread shouldn't crash, but when it does, it's a big deal and we can't really do anything
        // about it except try to core dump. We also can't do anything if we crashed before initializing
        // the watchdog system.
        bool watchdogThreadRunning = __atomic_load_n(&efj::app()._backgroundThreadsContinued, __ATOMIC_RELAXED);
        if (!watchdogThreadRunning || thread == __atomic_load_n(&sys->watchdogThread, __ATOMIC_RELAXED)) {
            int size = 0;

            void* trace[50];
            int count = ::backtrace(trace, efj::array_size(trace));
            if (count < 0) count = 0;

            _print_trace_safe(STDERR_FILENO, trace, count, warningBuf, sizeof(warningBuf));

            if (!watchdogThreadRunning) {
                size = stbsp_snprintf(warningBuf, sizeof(warningBuf),
                    "[c2] error: Thread %s crashed with signal %d (%s) before the watchdog thread started. "
                    "Calling the default signal handler for a crash.\n", thread->name, sig, strsignal(sig));
            }
            else {
                size = stbsp_snprintf(warningBuf, sizeof(warningBuf),
                    "[c2] error: Watchdog thread crashed with signal %d (%s). Calling default signal handler for a crash.\n",
                    sig, strsignal(sig));
            }

            _handle_bad_crash(sig, warningBuf, (umm)size);
        }

        ctx->lastBtSymbolCount = ::backtrace(ctx->lastBtSymbols, WATCHDOG_BACKTRACE_SYMBOL_COUNT);

        efj::_debugsys_handle_signal(*thread->debugContext, sig, action, ucontext);
        efj::_eventsys_handle_thread_crashed(thread);
        efj::_eventsys_trigger(&sys->threadCrashedEvent);

        if (!__atomic_load_n(&efj::app()._started, __ATOMIC_RELAXED)) {
            //efj::print_error("Here 1\n");
            _print_trace_safe(STDERR_FILENO, ctx->lastBtSymbols, ctx->lastBtSymbolCount,
                warningBuf, sizeof(warningBuf));

            int size = stbsp_snprintf(warningBuf, sizeof(warningBuf),
                "[c2] error: Crashed with signal %d (%s) before app started\n", sig, strsignal(sig));

            _handle_bad_crash(sig, warningBuf, size);
        }

        pthread_exit(NULL);
        break;
    }
    default:
        break;
    }

    efj::_debugsys_handle_signal(*thread->debugContext, sig, action, ucontext);
}

static void
_watchdog_pre_init(efj::watchdog_system* sys, efj::memory_arena* arena)
{
    sys->arena = arena;

    // NOTE(bmartin): Make sure that libgcc is loaded before we try to use it in
    // the context of a signal handler to avoid a potential malloc(). We could statically link glibc and libgcc,
    // but often times people forget to do that or don't want to worry
    // about that in test programs. See 'man backtrace' for more information. After reading the source,
    // if you don't ask for at least one symbol, the function early-outs without loading libgcc.
    //
    void* dummy = nullptr;
    backtrace(&dummy, 1);

    struct sigaction sa = {};
    sa.sa_flags     = SA_SIGINFO;
    sa.sa_sigaction = efj::_handle_signal;

    sigaction(SIGSEGV, &sa, nullptr);
    sigaction(SIGILL,  &sa, nullptr);
    sigaction(SIGABRT, &sa, nullptr);
    sigaction(EFJ_THREAD_UNRESPONSIVE_SIGNAL, &sa, nullptr);
}

static void
_watchdog_init_thread_context(efj::watchdog_system* sys, efj::thread* thread, efj::memory_arena* arena)
{
    efj::watchdog_thread_ctx* ctx = thread->watchdogContext;
    ctx->sys = sys;
    ctx->arena = arena;

    efj::eventsys_waitable_desc checkInDesc = {};
    checkInDesc.udata.as_ptr = thread;
    checkInDesc.dispatch     = &_watchdog_handle_checkin_event;
    efj::_eventsys_create_waitable_event(thread, "Check-in", &checkInDesc, &ctx->checkInEvent);
    efj::_eventsys_add_waitable_event(thread, &ctx->checkInEvent);
}

static void
_watchdog_pre_object_init(efj::watchdog_system* sys, efj::thread* watchdogThread)
{
    sys->watchdogThread = watchdogThread;

    efj::eventsys_waitable_desc threadCrashedDesc = {};
    threadCrashedDesc.udata.as_ptr = sys;
    threadCrashedDesc.dispatch     = &_watchdog_handle_thread_crashed_event;
    efj::_eventsys_create_waitable_event(sys->watchdogThread, "Thread Crashed", &threadCrashedDesc,
        &sys->threadCrashedEvent);

    efj::eventsys_timer_desc checkActivityDesc = {};
    checkActivityDesc.udata.as_ptr = sys;
    checkActivityDesc.dispatch     = &_watchdog_handle_check_activity_event;
    checkActivityDesc.initial      = WATCHDOG_CHECK_ACTIVITY_INTERVAL;
    checkActivityDesc.interval     = WATCHDOG_CHECK_ACTIVITY_INTERVAL;
    efj::_eventsys_create_timer(sys->watchdogThread, "Check Activity", &checkActivityDesc,
        &sys->checkActivityTimer);

    efj::_eventsys_start_timer(&sys->checkActivityTimer);

    // Make sure to get this count after we've created the watchdog thread. Even though we can't check
    // the activity of it, we need to be able to detect it and skip over it.
    u32 threadCount = efj::threads().size();
    sys->threadActivityTable = efj_push_array_zero(*sys->arena, threadCount, efj::thread_activity_entry);
    sys->threadActivityStateTable = efj_push_array_zero(*sys->arena, threadCount, efj::thread_activity_state);

    for (efj::thread* thread : efj::threads()) {
        sys->threadActivityStateTable[thread->logicalId].thread = thread;
    }

    efj::_eventsys_add_waitable_event(sys->watchdogThread, &sys->threadCrashedEvent);
}

static void
_watchdog_post_object_init(efj::watchdog_system*)
{
}

static void
_watchdog_on_thread_activity(efj::thread* thread)
{
    u32* cur = &thread->watchdogContext->sys->threadActivityTable[thread->logicalId].cur;
    __atomic_fetch_add(cur, 1, __ATOMIC_RELAXED);
}

static void
_watchdog_handle_checkin_event(efj::eventsys_event_data* edata)
{
    auto* thread = (efj::thread*)edata->udata.as_ptr;
    _watchdog_on_thread_activity(thread);
}

static void
_watchdog_handle_check_activity_event(efj::eventsys_event_data* edata)
{
    efj::watchdog_system* sys = (efj::watchdog_system*)edata->udata.as_ptr;
    efj_assert(efj::this_thread() == sys->watchdogThread);

    if (sys->threadUnresponsive)
        return;

    u32 threadCount = efj::threads().size();

    for (u32 i = 0; i < threadCount; i++) {
        efj::thread_activity_entry* entry = &sys->threadActivityTable[i];
        efj::thread_activity_state* state = &sys->threadActivityStateTable[i];

        // NOTE(bmartin): Nothing we can do about this thread at the moment. We assume it never hangs. An alternative
        // would be to setup a timer signal, and do this handling there, but we'd be in the context of a signal handler,
        // which might make things a bit tricky.
        if (state->thread == sys->watchdogThread)
            continue;

        u32 cur = __atomic_load_n(&entry->cur, __ATOMIC_RELAXED);
        if (cur != state->prev) {
            state->prev = cur;
            state->toldToCheckIn = false;
        }
        else {
            if (!state->toldToCheckIn) {
                // Tell the thread to check in.
                efj::_eventsys_trigger(&state->thread->watchdogContext->checkInEvent);
                state->toldToCheckIn = true;
            }
            else {
                __atomic_store_n(&sys->threadUnresponsive, true, __ATOMIC_RELAXED);

                // In each case, we have to handle the case where the logging thread is the one that is hung, which
                // is why we log and write to stderr.

                // Unresponsive thread.
                if (pthread_kill(state->thread->pthread, EFJ_THREAD_UNRESPONSIVE_SIGNAL) != 0) {
                    auto s = efj::fmt("Failed to kill unresponsive thread \"%s\": %s\n", state->thread->name,
                        strerror(errno));

                    efj_ignored_write(STDERR_FILENO, s.c_str(), s.size);
                    efj_internal_log(efj::log_level_crit, "%.*s", (int)s.size-1, s.c_str());

                    // FIXME: We can hang here for some reason.
                    // We will never get the thread-crashed event since we failed to send the signal,
                    // so all we can do is exit here.
                    efj::exit(efj::exit_thread_unresponsive);
                }
                else {
                    auto s = efj::fmt("Thread \"%s\" unresponsive. Killing it and exiting.\n", state->thread->name);
                    efj_ignored_write(STDERR_FILENO, s.c_str(), s.size);
                    efj_internal_log(efj::log_level_crit, "%.*s", (int)s.size-1, s.c_str());

                    // Handle exiting in thread crashed event.
                }
            }
        }
    }
}

static void
_watchdog_write_crash_error(bool tryToLog, const efj::string& msg,
    const char* file = nullptr, int line = 0)
{
    // NOTE: If the logging thread is blocked on a thread that crashed, this isn't enough for it to go out.
    // :LoggingThreadBlocked
    if (tryToLog) {
        efj::log(efj::log_level_crit, file, line, msg);
        efj::_logsys_handle_deferred(efj::this_thread());
    }
    else {
        // No file => no formatting besides the newline.
        efj::string error = line
                          ? efj::fmt("[c2] %s:%d: %s\n", file, line, msg.c_str())
                          : efj::fmt("%s\n", msg.c_str());

        efj_ignored_write(STDERR_FILENO, error.c_str(), error.size);
    }
}

// #include <cxxabi.h>
extern "C" char*
__cxa_demangle(const char* __mangled_name, char* __output_buffer,
size_t* __length, int* __status);

// On watchdog thread.
static void
_watchdog_handle_thread_crashed_event(efj::eventsys_event_data* edata)
{
    auto* sys = (efj::watchdog_system*)edata->udata.as_ptr;
    efj_assert(efj::this_thread() == sys->watchdogThread);

    int crashSignal = __atomic_load_n(&sys->crashSignal, __ATOMIC_RELAXED);

    efj::thread* crashedThread    = __atomic_load_n(&sys->crashedThread, __ATOMIC_RELAXED);
    efj::object* crashedObject    = __atomic_load_n(&sys->crashedObject, __ATOMIC_RELAXED);
    const char*  crashedEventName = __atomic_load_n(&sys->crashedEventName, __ATOMIC_RELAXED);

    efj::watchdog_thread_ctx* crashedThreadCtx = crashedThread->watchdogContext;
    bool crashedOnLoggingThread = (crashedThread == efj::app()._loggingThread);

    if (crashSignal == EFJ_THREAD_UNRESPONSIVE_SIGNAL) {
        _watchdog_write_crash_error(!crashedOnLoggingThread,
            efj::fmt("Thread \"%s\" unresponsive:\nObject: %s, Event Source: %s", crashedThread->name,
                crashedObject ? crashedObject->name : "none", crashedEventName ? crashedEventName : "none"),
            __FILE__, __LINE__);
    }
    else {
        _watchdog_write_crash_error(!crashedOnLoggingThread,
            efj::fmt("Thread \"%s\" crashed with signal %d: %s:\nObject: %s, Event Source: %s",
                crashedThread->name, crashSignal, strsignal(crashSignal),
                crashedObject ? crashedObject->name : "none", crashedEventName ? crashedEventName : "none"),
                __FILE__, __LINE__);
    }

    char** symbolNames = backtrace_symbols(crashedThreadCtx->lastBtSymbols, crashedThreadCtx->lastBtSymbolCount);

    efj::log_unformatted(true);

    efj::string_builder bt(efj::alloc_temp);

    char* demangledSymbol = nullptr;
    umm demangledLength = 0;
    int demangleStatus = 0;

    for (int i = 0; i < crashedThreadCtx->lastBtSymbolCount; i++) {
        umm symbolLength = strlen(symbolNames[i]);

        bool  demangled = false;
        char* nameBegin = strchr(symbolNames[i], '(');
        char* nameEnd   = nullptr;

        if (nameBegin && *++nameBegin) {
            nameEnd = strchr(nameBegin, '+');
            if (nameEnd) {
                *nameEnd = '\0';

                demangledSymbol = __cxa_demangle(nameBegin, demangledSymbol, &demangledLength, &demangleStatus);
                if (demangledSymbol && demangleStatus == 0) {
                    demangled = true;
                }
            }
        }

        if (demangled) {
            int preambleSize  = (int)(nameBegin-symbolNames[i]);
            int postambleSize = (int)((nameBegin+symbolLength) - (nameEnd+1));

            bt.append_fmt("%.*s%s+%.*s\n", preambleSize, symbolNames[i],
                demangledSymbol, postambleSize, nameEnd+1);
        }
        else {
            if (nameEnd)
                *nameEnd = '+';

            bt.append_fmt("%s\n", symbolNames[i]);
        }
    }

    ::free(demangledSymbol);
    ::free(symbolNames);

    //efj_internal_log(efj::log_level_crit, "{\n%s}\n", bt.str().c_str());
    _watchdog_write_crash_error(!crashedOnLoggingThread, efj::fmt("{\n%s}\n", bt.str().c_str()));

    efj::log_unformatted(false);

    efj::_debugsys_thread_crashed(*crashedThread->debugContext, crashSignal, bt.str());

    if (crashSignal == EFJ_THREAD_UNRESPONSIVE_SIGNAL)
        efj::exit(efj::exit_thread_unresponsive);
    else
        efj::exit(efj::exit_crash);
}

static bool 
_watchdog_is_thread_crashed(efj::thread* thread)
{
    return __atomic_load_n(&thread->watchdogContext->crashed, __ATOMIC_RELAXED);
}

} // namespace efj
