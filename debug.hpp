namespace efj
{

// Internal :DebugEventSourceContextVisible
struct debug_event_source_context
{
    efj::debug_event_source_context* parent = nullptr;
    efj::object*                     object = nullptr;
    const char*                      name   = nullptr;
    u32                              id     = 0;
};

enum debug_value_type
{
    debug_value_invalid,
    debug_value_string,
    debug_value_u64,
    debug_value_s64,
    debug_value_f64,
    debug_value_u32,
    debug_value_s32,
    debug_value_f32,
    debug_value_u16,
    debug_value_s16,
    debug_value_u8,
    debug_value_s8,
    debug_value_bool,
    debug_value_count_
};

// TODO
#define efj_debug_dump()

void debug_add_note(const char* fmt, ...);

void _debug_add_watch(const efj::string& name, char* value, umm size, efj::callback cb, efj::thread* thread = nullptr);
void _debug_add_watch(const efj::string& name, s8*   value, efj::callback cb, efj::thread* thread = nullptr);
void _debug_add_watch(const efj::string& name, s16*  value, efj::callback cb, efj::thread* thread = nullptr);
void _debug_add_watch(const efj::string& name, s32*  value, efj::callback cb, efj::thread* thread = nullptr);
void _debug_add_watch(const efj::string& name, s64*  value, efj::callback cb, efj::thread* thread = nullptr);
void _debug_add_watch(const efj::string& name, u8*   value, efj::callback cb, efj::thread* thread = nullptr);
void _debug_add_watch(const efj::string& name, u16*  value, efj::callback cb, efj::thread* thread = nullptr);
void _debug_add_watch(const efj::string& name, u32*  value, efj::callback cb, efj::thread* thread = nullptr);
void _debug_add_watch(const efj::string& name, u64*  value, efj::callback cb, efj::thread* thread = nullptr);
void _debug_add_watch(const efj::string& name, bool* value, efj::callback cb, efj::thread* thread = nullptr);
void _debug_add_watch(const efj::string& name, f32*  value, efj::callback cb, efj::thread* thread = nullptr);
void _debug_add_watch(const efj::string& name, f64*  value, efj::callback cb, efj::thread* thread = nullptr);

// For values edited transparently to objects.
EFJ_FORCE_INLINE void debug_add_watch(const efj::string& name, char* value, umm size, efj::thread* thread = nullptr) { _debug_add_watch(name, value, size, efj::null_callback(), thread); }
EFJ_FORCE_INLINE void debug_add_watch(const efj::string& name, s8*   value, efj::thread* thread = nullptr) { _debug_add_watch(name, value, efj::null_callback(), thread); }
EFJ_FORCE_INLINE void debug_add_watch(const efj::string& name, s16*  value, efj::thread* thread = nullptr) { _debug_add_watch(name, value, efj::null_callback(), thread); }
EFJ_FORCE_INLINE void debug_add_watch(const efj::string& name, s32*  value, efj::thread* thread = nullptr) { _debug_add_watch(name, value, efj::null_callback(), thread); }
EFJ_FORCE_INLINE void debug_add_watch(const efj::string& name, s64*  value, efj::thread* thread = nullptr) { _debug_add_watch(name, value, efj::null_callback(), thread); }
EFJ_FORCE_INLINE void debug_add_watch(const efj::string& name, u8*   value, efj::thread* thread = nullptr) { _debug_add_watch(name, value, efj::null_callback(), thread); }
EFJ_FORCE_INLINE void debug_add_watch(const efj::string& name, u16*  value, efj::thread* thread = nullptr) { _debug_add_watch(name, value, efj::null_callback(), thread); }
EFJ_FORCE_INLINE void debug_add_watch(const efj::string& name, u32*  value, efj::thread* thread = nullptr) { _debug_add_watch(name, value, efj::null_callback(), thread); }
EFJ_FORCE_INLINE void debug_add_watch(const efj::string& name, u64*  value, efj::thread* thread = nullptr) { _debug_add_watch(name, value, efj::null_callback(), thread); }
EFJ_FORCE_INLINE void debug_add_watch(const efj::string& name, bool* value, efj::thread* thread = nullptr) { _debug_add_watch(name, value, efj::null_callback(), thread); }
EFJ_FORCE_INLINE void debug_add_watch(const efj::string& name, f32*  value, efj::thread* thread = nullptr) { _debug_add_watch(name, value, efj::null_callback(), thread); }
EFJ_FORCE_INLINE void debug_add_watch(const efj::string& name, f64*  value, efj::thread* thread = nullptr) { _debug_add_watch(name, value, efj::null_callback(), thread); }


EFJ_FORCE_INLINE void
debug_add_watch(const efj::string& name, char* value, umm size,
                void callback(efj::object*, char* oldValue, const char* newValue, umm newSize),
                efj::thread* thread = nullptr)
{
    efj::callback cb = {};
    cb.type          = efj::callback_proc;
    cb.proc          = (void*)callback;

    _debug_add_watch(name, (char*)value, size, cb);
}

#define EFJ__DEFINE_ADD_WATCH_PROC(_type)\
EFJ_FORCE_INLINE void \
debug_add_watch(const efj::string& name, _type* value,\
                void callback(efj::object*, _type*, _type),\
                efj::thread* thread = nullptr)\
{\
    efj::callback cb = {};\
    cb.type          = efj::callback_proc;\
    cb.proc          = (void*)callback;\
\
    _debug_add_watch(name, value, cb);\
}

EFJ__DEFINE_ADD_WATCH_PROC(s8)
EFJ__DEFINE_ADD_WATCH_PROC(s16)
EFJ__DEFINE_ADD_WATCH_PROC(s32)
EFJ__DEFINE_ADD_WATCH_PROC(s64)
EFJ__DEFINE_ADD_WATCH_PROC(u8)
EFJ__DEFINE_ADD_WATCH_PROC(u16)
EFJ__DEFINE_ADD_WATCH_PROC(u32)
EFJ__DEFINE_ADD_WATCH_PROC(u64)
EFJ__DEFINE_ADD_WATCH_PROC(bool)
EFJ__DEFINE_ADD_WATCH_PROC(f32)
EFJ__DEFINE_ADD_WATCH_PROC(f64)

#undef EFJ__DEFINE_ADD_WATCH_PROC

//template <typename Class_> EFJ_FORCE_INLINE void
//debug_add_watch(const efj::string& name, const s8* value, void (Class_::*callback)(s8))
//{
//    efj::callback cb = {};
//    cb.type          = efj::callback_memfn;
//    memcpy(&cb.memfn, &callback, sizeof(cb.memfn));
//
//    _debug_add_watch(name, (char*)value, size, cb);
//}
//
//void debug_add_watch(const efj::string& name, const s16*  value, void cb(s16));
//void debug_add_watch(const efj::string& name, const s32*  value, void cb(s32));
//void debug_add_watch(const efj::string& name, const s64*  value, void cb(s64));
//void debug_add_watch(const efj::string& name, const u8*   value, void cb(u8));
//void debug_add_watch(const efj::string& name, const u16*  value, void cb(u16));
//void debug_add_watch(const efj::string& name, const u32*  value, void cb(u32));
//void debug_add_watch(const efj::string& name, const u64*  value, void cb(u64));
//void debug_add_watch(const efj::string& name, const bool* value, void cb(bool));
//void debug_add_watch(const efj::string& name, const f32*  value, void cb(f32));
//void debug_add_watch(const efj::string& name, const f64*  value, void cb(f64));


// :ObjectNeedsDebugHooks
extern void _debugsys_object_touched(efj::object* o EFJ_LOC_SIG);
} // namespace efj

