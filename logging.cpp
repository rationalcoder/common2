namespace efj
{

extern efj::string
default_log_formatter(const efj::log_message_ctx& ctx, const efj::string& msg)
{
    auto now = efj::to_string(efj::to_utc(ctx.time));

    switch (ctx.level) {
    case efj::log_level_debug:
        return efj::fmt("%.*s [debug] %s:%d: %.*s\n", (int)now.size, now.data,
            ctx.file, ctx.line, (int)msg.size, msg.data);
    case efj::log_level_info:
        return efj::fmt("%.*s [info] %s:%d: %.*s\n", (int)now.size, now.data,
            ctx.file, ctx.line, (int)msg.size, msg.data);
    case efj::log_level_warn:
        return efj::fmt("%.*s [warn] %s:%d: %.*s\n", (int)now.size, now.data,
            ctx.file, ctx.line, (int)msg.size, msg.data);
    case efj::log_level_crit:
        return efj::fmt("%.*s [crit] %s:%d: %.*s\n", (int)now.size, now.data,
            ctx.file, ctx.line, (int)msg.size, msg.data);
    default:
        return efj::fmt("%.*s [%d] %s:%d: %.*s\n", (int)now.size, now.data,
            ctx.level, ctx.file, ctx.line, (int)msg.size, msg.data);
    }
}

extern void
_log_va(int level, const char* file, int line, const char* fmt, va_list va)
{
    //struct timespec before = {};
    //clock_gettime(CLOCK_MONOTONIC, &before);

    efj::log_system* sys = efj::app()._logSystem;

    efj::string msg = efj::vfmt(fmt, va);

    efj::log_message_ctx ctx = {};
    ctx.object    = efj::this_object();
    ctx.file      = file;
    ctx.line      = line;
    ctx.level     = level;
    ctx.time      = efj::now_ptime();
    ctx.formatted = !efj::context().logUnformatted;

    efj::_logsys_queue_log_message(sys, &ctx, msg);

    //struct timespec after = {};
    //clock_gettime(CLOCK_MONOTONIC, &after);

    //u32 sec_diff = after.tv_sec - before.tv_sec;
    //u32 nsec_diff = after.tv_nsec - before.tv_nsec;
    //u32 diff_micro = sec_diff * 1000000 + nsec_diff / 1000;

    //printf("Diff: %u\n", diff_micro);
}

extern void
log(int level, const char* file, int line, const efj::string& msg)
{
    efj::log_system* sys = efj::app()._logSystem;

    efj::log_message_ctx ctx = {};
    ctx.object    = efj::this_object();
    ctx.file      = file;
    ctx.line      = line;
    ctx.level     = level;
    ctx.time      = efj::now_ptime();
    ctx.formatted = !efj::context().logUnformatted;

    efj::_logsys_queue_log_message(sys, &ctx, msg);
}

extern void
print(const char* fmt, ...)
{
    efj_temp_scope();

    va_list va;
    va_start(va, fmt);
    efj::string msg = efj::vfmt(fmt, va);
    efj_ignored_write(STDOUT_FILENO, msg.c_str(), msg.size);
    va_end(va);
}

extern void
print(const efj::string& msg)
{
    efj_ignored_write(STDOUT_FILENO, msg.c_str(), msg.size);
}

extern void
print_error(const char* fmt, ...)
{
    // Use a fixed buffer in case temp memory is corrupted.
    char buf[1024];

    va_list va;
    va_start(va, fmt);

    // We could do what efj::fmt() does and run different code based on whether we are running with address sanitizer,
    // but I'm just using vsnprintf always b/c this call is rare.
    int len = vsnprintf(buf, sizeof(buf), fmt, va);
    efj_ignored_write(STDERR_FILENO, buf, len);
    va_end(va);
}

extern void
print_error(const efj::string& msg)
{
    efj_ignored_write(STDERR_FILENO, msg.c_str(), msg.size);
}

} // end namespace efj
