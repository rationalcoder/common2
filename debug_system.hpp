// The debug system.
//
// Responsible for handling connections from and communication with a debugger over
// the debug protocol (DBP). This includes the handling of all events necessary to support
// interaction with the debugger, e.g. implementing custom thread events to implement the editing
// of watch values like the current log level for a thread or object.
//
// The implementations of other systems hook into the debug system by calling debug system functions
// in various places. No structures besides the system and context structs and the structs visible
// in the public debug API should be referenced even in internal code in other systems.
//

// Threading situation:
// TODO

// TODO: We've moved away from references in the other system APIs, so that should happen here, too.

namespace efj
{

// Constants and options used in the system.
enum
{
    // Pending DBP is buffered in linked-lists of chunks of data. This is the size of the data in each node.
    // There are thread-local lists and lists stored and managed only by the debug thread.
    DEBUGSYS_DBP_QUEUE_BUFFER_SIZE = 512,
    // Number of dbp queue buffers a thread can have pending before being forcibly written to the thread's DBP
    // queue (which is visible to and accessed by the debug thread).
    DEBUGSYS_THREAD_DBP_BUFFER_COUNT_THRESHOLD = 5,

    DEBUGSYS_WATCH_EDIT_QUEUE_SIZE = 64,
    DEBUGSYS_NOTE_SIZE_MAX = 128,
    DEBUGSYS_NOTE_CACHE_SIZE = 10,
    DEBUGSYS_EVENT_NAME_MAX = 128, // including nul => max strlen() size is n-1.

    // Debugging the debug code

    DEBUGSYS_VALIDATE_DBP_BUFFERS = false,
    // Don't queue DBP for any thread except the main thread and add logs for the thread local events buffer.
    DEBUGSYS_DEBUG_THREAD_EVENTS_BUFFER = false,
    // Force the chunking of dbp thread event data to test the chunking logic.
#define DEBUGSYS_DEBUG_EVENT_DATA_CHUNKING 0

};

// IDs for events internal to the debug system.
enum debugsys_event_source_type
{
    debugsys_event_invalid,
    debugsys_event_server,
    debugsys_event_client,
    debugsys_event_send_timer,
    debugsys_event_edit_queue,
    debugsys_event_count_
};

struct watched_primitive
{
    efj::thread*  thread;
    efj::object*  object;
    efj::string   name;
    efj::callback cb;
    u32           id;
    u32           type; // dbp type

    union
    {
        u64*  as_u64;
        u32*  as_u32;
        u16*  as_u16;
        u8*   as_u8;
        s64*  as_s64;
        s32*  as_s32;
        s16*  as_s16;
        s8*   as_s8;
        f64*  as_f64;
        f32*  as_f32;
        bool* as_bool;
    };

    union
    {
        u64  old_u64;
        u32  old_u32;
        u16  old_u16;
        u8   old_u8;
        s64  old_s64;
        s32  old_s32;
        s16  old_s16;
        s8   old_s8;
        f64  old_f64;
        f32  old_f32;
        bool old_bool;
    };
};

struct watched_string
{
    efj::thread*        thread;
    efj::object*        object;
    efj::string         name;
    efj::callback       cb;

    char*               value;
    efj::string_builder oldValue;
    u32                 size;
    u32                 id;
};

struct dbp_queue_buffer
{
    // For validation of these buffers being written out by the thread's dbp queue in debug mode.
    // NULL => no validation to do.
    void*                  validateData;
    void                   (*validate)(void* udata, efj::dbp_queue_buffer* buf);

    efj::dbp_queue_buffer* next;
    umm                    writeProgress;
    // Don't change this to a pointer whithout checking for sizeof(data).
    u8                     data[DEBUGSYS_DBP_QUEUE_BUFFER_SIZE];

    void clear() { validateData = nullptr; validate = nullptr; next = nullptr; writeProgress = 0; }
};

struct thread_dbp_queue
{
    efj::debug_system*     sys;
    efj::memory_arena*     arena;
    efj::free_list         freeList;
    efj::dbp_queue_buffer* headBuffer;
    efj::dbp_queue_buffer* tailBuffer;
    u32                    ltid;

    pthread_mutex_t        mutex;

    void init(efj::debug_system* sys, u32 ltid, efj::memory_arena* arena);
    void new_connection();
    void push_buffers(efj::dbp_queue_buffer* head, efj::dbp_queue_buffer* tail);
    void free_buffers(efj::dbp_queue_buffer* head);
    void write_to_debugger(int fd);
    efj::dbp_queue_buffer* get_empty_buffer();
};

struct dbp_buffer
{
    efj::debug_system*     sys;
    efj::memory_arena*     arena;
    efj::dbp_queue_buffer* headBuffer;
    efj::dbp_queue_buffer* tailBuffer;
    efj::free_list*        freeList;
    bool                   dataToWrite;

    void init(efj::debug_system* sys, efj::free_list* queueBufferList, efj::memory_arena* arena);
    void push(const void* data, umm size);
    void clear();
    void write_to_queue(efj::thread_dbp_queue* queue);
    void write_to_debugger(int fd);
};

struct debug_system
{
    efj::memory_arena*           arena;
    efj::thread*                 debugThread;

    efj::eventsys_epoll_event    serverEvent;
    efj::eventsys_epoll_event    pendingClientEvent;
    efj::eventsys_epoll_event    connectedClientEvent;
    efj::eventsys_timer_event    sendTimerEvent;
    efj::eventsys_fence          startCaptureFence;

    char                         debuggerIp[INET_ADDRSTRLEN];

    int                          serverFd;

    int                          pendingClientFd;
    //{ @TS. Accessed by all threads  and the debug thread
    volatile int                 connectedClientFd;
    pthread_rwlock_t             clientLock;
    // While write()s are atomic, write() calls may be broken up, in which case
    // we need to make sure write()s  from different threads aren't interleaved.
    pthread_mutex_t              sendMutex;
    //}

    // Some buffers are created and maintained by the debug thread, so the system has its
    // own queue-buffer free list, since we don't need any synchronization.
    //{
    efj::dbp_buffer              appDescriptionBuffer;
    efj::free_list               dbpQueueBufferFreeList;
    //}

    efj::dbp_parser              dbpParser;
    char                         dbpParseBuf[EFJ_DBP_PARSE_BUF_MIN];

    bool                         sendEvents;
    u32                          objectCount; // cached from app state during context creation.

    bool                         preInitted;
};

// TODO: strings. Don't want to think about it right now. It will either require
// having separate storage for strings or changing the queue to be an arena and handling
// the case of limiting strings to certain size or dealing with string edits being to big for
// the entire size of the queue. @Incomplete
//
struct debugsys_watch_edit
{
    efj::object*          o;
    efj::callback         cb;
    u8*                   dst;
    umm                   size;
    efj::debug_value_type type;
    // This way it should be easy to add support for strings later, by making it src[] and plopping
    // the data after the struct in the buffer.
    u8                    src[8] alignas(u64);
};

struct debugsys_edit_queue_buffer
{
    efj::debugsys_watch_edit* begin;
    efj::debugsys_watch_edit* at;
    efj::debugsys_watch_edit* end;
};

// A place to store pending edits to watch values for a thread.
// SPSC debug thread -> any other thread.
struct debugsys_edit_queue
{
    efj::debugsys_edit_queue_buffer* frontBuffer;
    efj::debugsys_edit_queue_buffer* backBuffer;
    efj::debugsys_edit_queue_buffer  buffer1;
    efj::debugsys_edit_queue_buffer  buffer2;

    efj::dbp_watch_edited_batch      latestBatch;

    pthread_mutex_t                  swapMutex;
    pthread_cond_t                   spaceAvailableCond;

    void init(umm editCount, efj::memory_arena& arena);
    void push_batch(const efj::dbp_watch_edited_batch& batch);
    // Returns true if the current edit batch is completed.
    bool push(const efj::debugsys_watch_edit& edit);
    efj::debugsys_edit_queue_buffer* swap_buffers();
};

#pragma pack(push, 1)
struct debugsys_thread_event_data_headers
{
    efj::dbp_thread_event_data_header1 header1 = {};
    efj::dbp_thread_event_data_header2 header2 = {};
};
#pragma pack(pop)

struct thread_dbp_buffer
{
    efj::thread_dbp_queue* queue;
    efj::dbp_queue_buffer* headBuffer;
    efj::dbp_queue_buffer* tailBuffer;
    umm                    bufferCount;
    u32                    ltid;

    void init(efj::thread_dbp_queue* queue, u32 ltid);
    void new_connection();
    void push_packed(const void* data, umm size);
    void* push_contiguous(const void* data, umm size);
    void write_to_queue();
    void write_to_queue_if_needed();

    // @Hack We want to be able to do this for now. :StartCaptureBufferProblem
    void write_to_debugger(int fd);
};

// Place to build the data for thread_events messages before it's written out to the thread's dbp queue.
struct thread_events_buffer
{
    efj::thread_dbp_queue*  queue;
    efj::dbp_queue_buffer*  headBuffer;
    efj::dbp_queue_buffer*  tailBuffer;
    umm                     bufferCount;
    u32                     ltid;

    // This points to the events message stored at the start of each queue-buffer. It should always be aligned,
    // but right now this isn't asserted or anything. @Alignment
    efj::dbp_thread_events* threadEvents;

    u32                     curDataSize;
    u8                      curDataType;
    // The pointers to event data contents are unaligned. Don't write to them normally. @Alignment
    u16*                    curDataSizeAddress;
    u8*                     curDataMoreAddress;

    bool                    eventHasData;
    bool                    beganAllEventData; // for debugging misuse of begin_all_event_data().
    bool                    endedAllEventData; // for debugging misuse of end_all_event_data().

    void init(efj::thread_dbp_queue* queue, u32 ltid);
    void new_connection();
    void push_event(efj::dbp_thread_event& e);
    void begin_all_event_data();
    void end_all_event_data();
    void begin_event_data(efj::dbp_event_data_type type, const char* tag);
    void push_event_data(const void* data, umm size, const char* tag);

    efj::dbp_queue_buffer* _complete_message_and_start_new_buffer();
    void _push_packed(const void* data, umm size, const char* tag);
    void* _push_contiguous(const void* data, umm size, const char* tag);
    void _write_to_queue_unlocked();

    void write_to_queue();
    void write_to_queue_if_needed();

    EFJ_FORCE_INLINE efj::dbp_queue_buffer* _get_empty_buffer();
    static void _validate_buffer(void* udata, efj::dbp_queue_buffer* buf);
};

// Intermediate storage for event notes.
// TODO: @Cleanup. Are we ever really going to support notes? It doesn't really look like it.
struct debugsys_note_buffer
{
    char* begin;
    char* at;
    char* end;
};

struct debugsys_event_ctx
{
    efj::list_head            node;
    // We don't really need this field, since this struct is embedded in one. @Space
    efj::eventsys_event_data* edata;
};

struct debugsys_thread_ctx
{
    efj::debug_system*                sys;
    efj::thread*                      thread;
    efj::memory_arena*                arena;

    efj::thread_dbp_queue             dbpQueue;
    efj::thread_events_buffer         threadEventsBuffer;
    efj::thread_dbp_buffer            eventRegistrationBuffer;
    efj::thread_dbp_buffer            startCaptureBuffer;

    // @Hack. We currently write to this event type so we don't have to do any type
    // conversions or anything, but internally this type is packed, so we could run
    // into alignment problems depending on the other fields of this struct. We can
    // detect this problem at runtime with @UB sanitizer. My solution for now is to
    // make just this field aligned well enough that setting it's fields shouldn't be
    // a problem.
    efj::dbp_thread_event             curEvent alignas(void*);
    efj::debugsys_note_buffer         noteBuffer;

    efj::debugsys_edit_queue          editQueue;
    efj::eventsys_waitable_event      editQueueEvent;
    efj::eventsys_waitable_event      startCaptureEvent;

    bool                              isDebugThread;
    bool                              startCapture;
    bool                              captureEvents;

    bool                              gotFirstEvent;

    // We can't rely on information in this context if we crash while modifying it, so we
    // need a flag to tell us when we are running debug code that can modify this struct.
    volatile int                      beingModified;
    bool                              crashed;
    struct timespec                   crashTime;

    //{ Only modified at init() time. Storage for watched values.
    efj::array<watched_primitive>     watchedPrimitives;
    efj::array<watched_string>        watchedStrings;
    //}

    // @Hack This can be added to before the thread context is initialized to avoid having a thread_pre_init().
    // If other system's contexts are initialized first, they can create events which then get registered before
    // we are initted. We could also use a constructor, here. :RegisteredEventsHack
    efj::list_head                    registeredEvents = EFJ_LIST_HEAD_INITIALIZER(registeredEvents);

    efj::array<object*>               touchedObjects { efj::delay_init };
    efj::object*                      lastTouchedObject;

    bool                              lockedClient;
    bool                              initted;
};

struct debugsys_object_ctx
{
    efj::array<watched_primitive*> watchedPrimitives;
};

// Called before efj::init(), during app contruction.
static void _debugsys_pre_init(efj::debug_system& sys, efj::memory_arena& arena);
// Called during thread/object initialization, which may happen before pre_init()
static void _debugsys_init_thread_context(efj::debug_system& sys, efj::thread& thread, efj::memory_arena& arena);
static void _debugsys_init_object_context(efj::debug_system& sys, efj::object& object);
// Called before object init.
static void _debugsys_pre_object_init(efj::debug_system& sys, efj::thread* debugThread);
// Called after efj::init() has finished. The event system must be initialized before the debug system.
static void _debugsys_post_object_init(efj::debug_system& sys);

// Called during the create_* API for all event sources and when event sources are implicitly created/destroyed.
// This happens during and after efj::init().
static void _debugsys_register_event(efj::debugsys_thread_ctx& ctx, efj::eventsys_event_data& edata);
static void _debugsys_deregister_event(efj::debugsys_thread_ctx& ctx, efj::eventsys_event_data& edata);

// Called at the beginning/end of each frame on the debug thread.
static void _debugsys_begin_frame(efj::debugsys_thread_ctx& ctx);
static void _debugsys_end_frame(efj::debugsys_thread_ctx& ctx);
// Called at the beginning and end of the handling of each event handled by a thread.
static void _debugsys_begin_event(efj::debugsys_thread_ctx& ctx, const efj::eventsys_event_data* data);
static void _debugsys_end_event(efj::debugsys_thread_ctx& ctx, const efj::eventsys_event_data* data);

// Called in the context of the shared signal handler for all signals. This function must be async-signal safe.
static void _debugsys_handle_signal(efj::debugsys_thread_ctx& ctx, int sig, siginfo_t*, void* ucontext);

// Called by the watchdog thread when it's time to exit. The context here is the context of the thread
// that crashed, not the context of the watchdog thread.
static void _debugsys_thread_crashed(efj::debugsys_thread_ctx& ctx, int sig, const efj::string& trace);

// Called in the context of whichever thread called efj::exit(). This function isn't called from a signal handler.
static void _debugsys_exit(efj::debugsys_thread_ctx& ctx, int code);

// Actual exit handler for the debug system. Called after all threads have stopped.
static void _debugsys_handle_exit(efj::debug_system& sys);

// This is in both debug.hpp and here b/c 'efj::object' needs to call this, but I didn't want expose this code to users.
// We could put all the code that calls it in object.cpp, but that would prevent inlining of a lot of small wrapper
// methods.
//
// :ObjectNeedsDebugHooks
extern void _debugsys_object_touched(efj::object* o EFJ_LOC_SIG);

// Internal event handlers called when an event added by the debug system is triggered.
// On the debug thread:
static void _debugsys_handle_server_event(efj::eventsys_event_data* edata);
static void _debugsys_handle_pending_client_event(efj::eventsys_event_data* edata);
static void _debugsys_handle_connected_client_event(efj::eventsys_event_data* edata);
static void _debugsys_handle_send_timer_event(efj::eventsys_event_data* edata);

// On any thread:
// Watch edits pending.
static void _debugsys_handle_edit_queue_event(efj::eventsys_event_data* edata);
// New debugger. Send initial data.
static void _debugsys_handle_start_capture_event(efj::eventsys_event_data* edata);

} // namespace efj
