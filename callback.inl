namespace efj
{

// Generate one event callback procedure for each object instead of one wrapper per unique callback in each object.
// We just assume all member function callbacks are callable as void T::callback(void*).
// :ObjectInl
template <typename T> inline void
_object_dispatch_event(efj::callback* cb, void* e)
{
    efj::object* oldObject = efj::this_object();
    efj::object* o = cb->object;
    efj::_set_this_object(o EFJ_LOC_ARGS);

    efj::_debugsys_object_touched(o EFJ_LOC_ARGS);

    // This branch should be very predictable since it's unlikely for someone writing an object to mix
    // free-function and member function callbacks.
    if (cb->type == efj::callback_memfn) {
        using CB = void (T::*)(void*);
        (((T*)o)->*(*(CB*)&cb->memfn))(e);
    }
    else {
        ((void(*)(efj::object*, void*))cb->proc)(o, e);
    }

    efj::_set_this_object(oldObject EFJ_LOC_ARGS);
}

template <typename T, typename EventT> EFJ_MACRO efj::callback
_make_object_callback(efj::object* o, void (T::*callback)(EventT& e))
{
    efj::callback cb = {};
    cb.object = o;
    cb.type   = efj::callback_memfn;
    cb.caller = &efj::_object_dispatch_event<T>;
    ::memcpy(&cb.memfn, &callback, sizeof(callback));

    static_assert(sizeof(callback) <= sizeof(cb.memfn),
            "efj::callback::memfn not big enough.");

    return cb;
}

template <typename T, typename... ArgsT> EFJ_MACRO efj::callback
_make_custom_callback(efj::object* o, void (T::*callback)(ArgsT...), efj::callback_caller* caller)
{
    efj_assert(caller);

    efj::callback cb = {};
    cb.object = o;
    cb.type   = efj::callback_memfn;
    cb.caller = caller;
    ::memcpy(&cb.memfn, &callback, sizeof(callback));

    static_assert(sizeof(callback) <= sizeof(cb.memfn),
            "efj::callback::memfn not big enough.");

    return cb;
}

} // namespace efj
