namespace efj
{

struct serializer_chunk
{
    efj::serializer_chunk* next = nullptr;
    char data[128];
};

// @Temporary @Incomplete. Currently uses temp memory.
// TODO: Move the serializer out of here and take it seriously.
struct serializer
{
    efj::serializer_chunk* _chunks = &_firstChunk;
    efj::serializer_chunk* _tail = &_firstChunk;
    umm                    _chunkCount = 0;
    umm                    _tailFilled = 0;
    efj::serializer_chunk  _firstChunk;

    umm size() const { return _chunkCount * 128 + _tailFilled; }

    void writeBytes(const char* tag, const void* data, umm size)
    {
        // Copied from string_builder::append().
        u32 progress = 0;

        for (;;) {
            const umm space = 128 - _tailFilled;
            const umm dataRemaining = size - progress;
            const umm maxFromThisChunk = space < dataRemaining ? space : dataRemaining;
            memcpy(_tail->data + _tailFilled, (u8*)data + progress, maxFromThisChunk);

            progress += maxFromThisChunk;

            if (progress == size) {
                _tailFilled += maxFromThisChunk;
                break;
            }

            // We've exhausted this chunk, and we need a new one.
            auto* next   = efj_push_type(efj::temp_memory(), efj::serializer_chunk);
            next->next   = nullptr;
            _tail->next  = next;
            _tail        = next;
            _tailFilled  = 0;
            ++_chunkCount;
        }
    }
};

// WARNING: If we add a copy_constructor() to the component info, we have to pass an extra bool
// to the destroy function to let the callee know if the component was copy constructed because
// we usually call the copy() function which collates memory allocations, making calling destroy()
// unnecessary. However, if we allocated the memory for the object and it's contents separately by
// calling copy_construct(), destroy() may be necessary to free its contents. Otherwise, destroy()
// functions will always need to be called and the callee would be responsible for checking memory
// locations of contents to see if the struct and contents were allocated together, which is ugly,
// slow, and error prone.
//
//using component_copy_ctor_fn = void*(void*, const void*, efj::allocator*);
//using component_destroy_fn = void(void*, efj::allocator*, bool copyConstructed);

using component_copy_fn = void*(const void*, efj::allocator*);
using component_destroy_fn = void(void*, efj::allocator*);
using component_serialize_fn = void*(efj::serializer& sz, const void*);

enum component_flags_
{
    component_is_message = 0x1,
    component_is_trivial = 0x2,
};

using component_flags = int;

// Meta information stored once per component-type, globally.
//
// WARNING: We expect these objects to only be constructed in the global
// info accessor. These objects should not be instantiated through other means
// since we rely on thread-safe static initializers to increment component ids.
// Contruction of one of these is effectively registration.
//
struct component_info
{
    static u32 globalRunningId;

    const char*                  name;
    efj::component_copy_fn*      copy;
    //efj::component_copy_ctor_fn* copyConstruct;
    efj::component_destroy_fn*   destroy;
    efj::component_serialize_fn* serialize;
    efj::component_flags         flags;
    umm                          size;
    umm                          alignment;
    u32                          id = 0;

    component_info(const char* name, efj::component_copy_fn* copy,
        //efj::component_copy_ctor_fn* copyConstruct,
        efj::component_destroy_fn* destroy,
        efj::component_serialize_fn* serialize,
        efj::component_flags flags, umm size, umm alignment)
        : name(name), copy(copy), /*copyConstruct(copyConstruct),*/ destroy(destroy),
          serialize(serialize), flags(flags), size(size), alignment(alignment)
    {
        // This constructor may be executed at runtime, since we decided to let components be
        // registered at runime instead of doing our own compile-time prepass.
        id = __atomic_add_fetch(&globalRunningId, 1, __ATOMIC_RELAXED);
    }

    EFJ_MACRO bool is_trivial() const { return flags & efj::component_is_trivial; }
    EFJ_MACRO bool is_message() const { return flags & efj::component_is_message; }
};

template <typename T>
struct component_spec
{
    static_assert(efj::type_dependent_false<T>::value,
        "[c2] no efj::component defined for your type!");
};

template <typename T> EFJ_MACRO T*
copy_component(const T& val)
{
    return efj::component_spec<T>::global_info().copy(&val);
}

template <typename T> EFJ_MACRO T*
copy_component(const T& val, efj::allocator alloc)
{
    efj_allocator_scope(alloc);
    return efj::component_spec<T>::global_info().copy(&val);
}

template <typename T> EFJ_MACRO T*
serialize_component(efj::serializer& sz, const T& val)
{
    efj::component_spec<T>::global_info().serialize(sz, &val);
}

#define EFJ_COMPONENT(_type)                                                            \
static efj::component_info* global_info()                                               \
{                                                                                       \
    constexpr efj::component_flags flags =                                              \
        (std::is_base_of<efj::message, _type>::value ? efj::component_is_message : 0) | \
        (kIsTrivial ? efj::component_is_trivial : 0);                                   \
                                                                                        \
    static efj::component_info info(#_type,                                             \
            (efj::component_copy_fn*)&component_spec::copy,                             \
            (efj::component_destroy_fn*)&component_spec::destroy,                       \
            (efj::component_serialize_fn*)&component_spec::serialize,                   \
            flags,                                                                      \
            sizeof(_type),                                                              \
            alignof(_type));                                                            \
                                                                                        \
    return &info;                                                                       \
}

template <typename T> EFJ_MACRO void
call_destructor(T& val)
{
    val.~T();
}

template <typename T> EFJ_MACRO u32 id_of()
{ return efj::component_spec<typename std::remove_cv<T>::type>::global_info()->id; }

template <typename T> EFJ_MACRO efj::component_info*
info_of()
{ return efj::component_spec<typename std::remove_cv<T>::type>::global_info(); }

#define EFJ_DEFINE_TRIVIAL_COMPONENT(_type, _version)                           \
namespace efj                                                                   \
{                                                                               \
template <>                                                                     \
struct component_spec<_type>                                                    \
{                                                                               \
    enum { kIsTrivial = true };                                                 \
                                                                                \
    EFJ_COMPONENT(_type);                                                       \
    static const char* version() { return _version; }                           \
                                                                                \
    static _type* copy(const _type* rhs, efj::allocator* alloc)                 \
    {                                                                           \
        return efj_allocator_new(*alloc, _type)(*rhs);                          \
    }                                                                           \
                                                                                \
    static _type* copy_construct(_type* lhs, const _type* rhs, efj::allocator*) \
    {                                                                           \
        *lhs = *rhs;                                                            \
        return lhs;                                                             \
    }                                                                           \
                                                                                \
    static void destroy(_type* value, efj::allocator*)                          \
    {                                                                           \
        efj::call_destructor(*value);                                           \
    }                                                                           \
                                                                                \
    static void serialize(efj::serializer& sz, const _type* value)              \
    {                                                                           \
        sz.writeBytes("value", value, sizeof(_type));                           \
    }                                                                           \
};                                                                              \
}

// This is one consequence of making efj::component an RAII type. It's annoying to try to
// do expansion of a type list (T...) and populate an array of RAII components, which
// is what we want to do in the producer/consumer implementation. Therefore, to keep that
// part easy and fast, we have this type that doesn't store the allocator and can be
// struct-initialized, e.g. '{ arg1, arg2, argn... }...'.
//
// NOTE: I'm not sure I made the right decision in making this type, since we have other
// structs that are views, like efj::message and efj::string. Maybe we should have
// efj::component be a view type, and have efj::component_copy for the RAII version.
//
struct component_view
{
    const efj::component_info* _info;
    const void*                _value;

    // TODO: debug info
    //efj::object*         srcObject;
    //const char*          file;
    //int                  line;

    EFJ_MACRO component_view copy(efj::allocator alloc = efj::current_allocator()) const
    {
        return { _info, _info->copy(_value, &alloc) };
    }

    EFJ_MACRO u32 id() const
    {
        return _info ? _info->id : 0;
    }

    EFJ_MACRO bool is_message() const { return _info->flags & efj::component_is_message; }

    template <typename T> EFJ_MACRO const T* as() const
    {
        return _info->id == efj::id_of<T>() ? (T*)_value : nullptr;
    }

    template <typename T> EFJ_MACRO T* as()
    {
        return _info->id == efj::id_of<T>() ? (T*)_value : nullptr;
    }

    EFJ_MACRO const void* value() const { return _value; }
    EFJ_MACRO const char* name() const { return _info->name; }
};

// TODO: Rest of the component_view conversion API.
struct component
{
    const efj::component_info* _info;
    void*                      _value;
    efj::allocator             _alloc;

    // TODO: debug info
    //efj::object*         srcObject;
    //const char*          file;
    //int                  line;

    // We need to at least have an allocator to handle assignment.
    component(efj::allocator alloc = efj::current_allocator())
        : _info(), _value(), _alloc(alloc)
    {}

    // We need to at least have an allocator to handle assignment.
    component(const efj::component_view& c, efj::allocator alloc = efj::current_allocator())
        : _info(c._info), _alloc(alloc)
    {
        _value = c._info->copy(c._value, &_alloc);
    }

    component(const component& c, efj::allocator alloc = efj::current_allocator())
        : _info(c._info), _alloc(alloc)
    {
        _value = c._info->copy(c._value, &_alloc);
    }

    component(component&& other)
        : _info(other._info), _value(other._value), _alloc(other._alloc)
    {
        other._value = nullptr;
    }

    ~component()
    {
        if (_value) {
            if (!_info->is_trivial())
                _info->destroy(_value, &_alloc);

            efj_allocator_free(_alloc, _value);
        }
    }

    component& operator = (const component& other)
    {
        if (_value) {
            if (!_info->is_trivial())
                _info->destroy(_value, &_alloc);

            efj_allocator_free(_alloc, _value);
        }

        _value = _info->copy(other._value, &_alloc);
        return *this;
    }

    component& operator = (component&& other)
    {
        if (_value) {
            if (!_info->is_trivial())
                _info->destroy(_value, &_alloc);

            efj_allocator_free(_alloc, _value);
        }

        if (_alloc != other._alloc) {
            _info = other._info;
            _value = _info->copy(_value, &_alloc);
            return *this;
        }

        _info = other._info;
        _value = other._value;
        other._value = nullptr;
        return *this;
    }

    EFJ_MACRO operator efj::component_view() const
    {
        return efj::component_view { _info, _value };
    }

    EFJ_MACRO u32 id() const
    {
        return _info ? _info->id : 0;
    }

    EFJ_MACRO bool is_trivial() const { return _info->flags & efj::component_is_trivial; }
    EFJ_MACRO bool is_message() const { return _info->flags & efj::component_is_message; }

    template <typename T> EFJ_MACRO const T* as() const
    {
        return _info->id == efj::id_of<T>() ? (T*)_value : nullptr;
    }

    template <typename T> EFJ_MACRO T* as()
    {
        return _info->id == efj::id_of<T>() ? (T*)_value : nullptr;
    }

    EFJ_MACRO const void* value() const { return _value;  }
    EFJ_MACRO void* value() { return _value;  }

    EFJ_MACRO const char* name() const { return _info->name; }
};

} // namespace efj

