namespace efj
{

// @Temporary
static const char* kConsoleGreen = "\033[32m";
static const char* kConsoleRed   = "\033[31m";
static const char* kConsoleReset = "\033[0m";

efj::testing_global globalTesting;
efj::test_case*     globalCurrentTest;

static void
_testsys_pre_init(efj::test_system* sys, efj::memory_arena* arena)
{
    sys->arena = arena;

    sys->capturedOutputs.reset(*arena);
    sys->tests.reset(*arena);
    sys->failedTests.reset(*arena);
}

// NOTE: This also inits any sub contexts for objects like the producer/consumer ones.
static void
_testsys_init_object_context(efj::test_system* sys, efj::object* object)
{
    object->testContext = efj_push_new(*sys->arena, efj::testsys_object_ctx);

    for (efj::producer* p = object->producers; p; p = p->next) {
        p->testContext = efj_push_new(*sys->arena, efj::testsys_producer_ctx);
    }

    for (efj::consumer* c = object->consumers; c; c = c->next) {
        c->testContext = efj_push_new(*sys->arena, efj::testsys_consumer_ctx);
    }
}

static void
_testsys_init_thread_context(efj::test_system*, efj::thread*, efj::memory_arena*)
{
}

// @Incomplete
static void
_testsys_capture_to_hex(efj::test_system* sys, efj::producer* p, const efj::string& path, int fd)
{
    p->testContext->output.path = path;
    p->testContext->output.fd   = fd;

    if (!p->testContext->output.captured) {
        p->testContext->output.captured = true;
        sys->capturedOutputs.add(&p->testContext->output);
    }
}

static void
_testsys_capture_to_hex(efj::test_system* sys, efj::consumer* c, const efj::string& path, int fd)
{
    c->testContext->output.path = path;
    c->testContext->output.fd   = fd;

    if (!c->testContext->output.captured) {
        c->testContext->output.captured = true;
        sys->capturedOutputs.add(&c->testContext->output);
    }
}

static void
_testsys_capture_to_network(efj::test_system* sys, efj::producer* p, const efj::string& host, u16 port)
{
    if (!p->testContext->output.captured) {
        p->testContext->output.captured = true;

        p->testContext->output.socket = ::socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        p->testContext->output.addr   = efj::net_make_sock_addr(host.c_str(), port);
        sys->capturedOutputs.add(&p->testContext->output);
    }
}

static void
_testsys_capture_to_network(efj::test_system* sys, efj::consumer* c, const efj::string& host, u16 port)
{
    if (!c->testContext->output.captured) {
        c->testContext->output.captured = true;

        c->testContext->output.socket = ::socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        c->testContext->output.addr   = efj::net_make_sock_addr(host.c_str(), port);
        sys->capturedOutputs.add(&c->testContext->output);
    }
}

static void
_testsys_pre_object_init(efj::test_system* sys, int argc, const char** argv)
{
    // FIXME :ArgumentParsing
    sys->executablePath = argv[0];

    struct option options[] = {
        { "c2-test", optional_argument },
        {}
    };

    for (;;) {
        int index = -1;
        int c = getopt_long_only(argc, (char* const*)argv, "", options, &index);
        if (c == -1)
            break;

        if (index == 0) {
            sys->testing = true;

            if (::optarg) {
                sys->runSingleTestCase = true;

                // Once we know we are running a single test case, we know we are in test mode, so
                // we set the flag that the rest of the framework checks. @TS
                globalTesting.value = true;

                efj::test_case* matchingTest = nullptr;
                for (umm i = 0; i < sys->tests.size(); i++) {
                    if (strcmp(::optarg, sys->tests[i].name) == 0)
                        matchingTest = &sys->tests[i];
                }

                sys->singleTestCase = matchingTest;
            }
        }
    }

    if (!sys->testing)
        return;

    if (sys->testInit.run)
        sys->testInit.run();
}

static void
_testsys_post_object_init(efj::test_system* sys)
{
}

static void
_testsys_register_test(efj::test_system* sys, const char* name, efj::test_func* run)
{
    efj::test_case* test = sys->tests.add_default();
    test->name = name;
    test->run  = run;
}

static void
_testsys_register_test_init(efj::test_system* sys, const char* name, efj::test_func* run)
{
    sys->testInit.name = name;
    sys->testInit.run  = run;
}

static bool
_run_test(efj::test_system* sys, efj::test_case* test)
{
    efj_arena_scope(efj::temp_memory());
    efj::reset(sys->curTestArena);

    efj_allocator_scope(sys->curTestArena);
    globalCurrentTest        = test;
    globalCurrentTest->arena = &sys->curTestArena;
    globalCurrentTest->failureMessages.reset(sys->curTestArena);
    globalCurrentTest->logMessages.reset(sys->curTestArena);
    globalCurrentTest->notes.reset(sys->curTestArena);

    efj::print("\033[32m[ RUN      ]\033[0m %s\n", test->name);

    test->run();

    size_t failCount = test->failCount;
    size_t passCount = test->testCount ? test->testCount - failCount : 0;

    if (failCount) {
        sys->failedTests.add(test);

        for (const efj::string& msg : test->logMessages) {
            efj::print(msg);
        }
        
        for (const efj::string& msg : test->failureMessages) {
            efj::print(msg);
        }

        efj::print("\033[31m[  FAILED  ]\033[0m %s: %zu/%zu passed (%zu failed)\n", test->name,
            passCount, test->testCount, failCount);

        return false;
    }
    else {
        efj::print("\033[32m[       OK ]\033[0m %s\n", test->name);
    }

    return true;
}

static void
_print_test_summary(efj::test_system* sys, int total, int passed, int failed)
{
    efj::print("%s[==========]%s %d test%s ran\n", kConsoleGreen, kConsoleReset, total, total == 1 ? "" : "s");
    efj::print("%s[  PASSED  ]%s %d passed\n", kConsoleGreen, kConsoleReset, passed);

    if (failed)
        efj::print("%s[  FAILED  ]%s %d failed, listed below\n", kConsoleRed, kConsoleReset, failed);

    for (const efj::test_case* test : sys->failedTests) {
        efj::print("%s[  FAILED  ]%s %s\n", kConsoleRed, kConsoleReset, test->name);
    }
}

static void
_testsys_start(efj::test_system* sys, bool* shouldExit, int* exitCode)
{
    if (!sys->testing)
        return;

    sys->curTestArena = efj::allocate_arena("Current Test", 4_KB);
    sys->testNotesArena = efj::allocate_arena("Test Notes", 4_KB);

    const char* fifoPath = ".TEST_FIFO";

    // In-memory format for communicating between processes.
    struct test_result
    {
        umm  runCount;
        umm  passCount;
        umm  failCount;
        bool didNotCrash; // We want 0 == unset == crashed.
    };

    if (sys->runSingleTestCase) {
        // Exit quietly if we couldn't find the user's specific test.
        if (!sys->singleTestCase) {
            *shouldExit = true;
            *exitCode   = efj::exit_test_fail;
            return;
        }

        *shouldExit = true;
        *exitCode   = _run_test(sys, sys->singleTestCase) ? efj::exit_test_pass : efj::exit_test_fail;

        // This shouldn't need to be non-blocking, but I've left it this way just in case the fifo wasn't
        // cleaned up properly and we are being run without a parent process.
        int fifoFd = ::open(fifoPath, O_WRONLY | O_NONBLOCK);
        //int fifoFd = ::open(fifoPath, O_WRONLY);
        efj_defer { ::close(fifoFd); };

        if (fifoFd != -1) {
            test_result result = {};
            result.runCount    = sys->singleTestCase->passCount + sys->singleTestCase->failCount;
            result.passCount   = sys->singleTestCase->passCount;
            result.failCount   = sys->singleTestCase->failCount;
            result.didNotCrash = true;

            efj_ignored_write(fifoFd, &result, sizeof(result));
        }
        else {
            // We aren't being run as a child process, so we are responsible for printing the summary.
            if (sys->singleTestCase->failCount)
                _print_test_summary(sys, 1, 0, 1);
            else
                _print_test_summary(sys, 1, 1, 0);
        }

        return;
    }

    int totalTestCount  = 0;
    int passedTestCount = 0;
    int failedTestCount = 0;

    efj_defer { _print_test_summary(sys, totalTestCount, passedTestCount, failedTestCount); };

    if (!sys->tests.size()) {
        *shouldExit = true;
        *exitCode = efj::exit_test_pass;
        return;
    }

    // FIXME: We just kind of assume nobody else will read/write from this except us and our child processes.
    // I assume someone else (or the same person) would have to be logged in as the same user and running a test
    // at the same time in the same directory for this to be an issue, but still.

    // This first unlink() is just in case we failed to cleanup properly.
    unlink(fifoPath);
    int fifoResult = ::mkfifo(fifoPath, S_IRUSR | S_IWUSR);
    efj_defer { unlink(fifoPath); };

    if (fifoResult != 0) {
        efj_internal_log(efj::log_level_crit, "[c2] error: Failed to make fifo at %s: %s",
            fifoPath, strerror(errno));

        *shouldExit = true;
        *exitCode = efj::exit_test_fail;
        return;
    }

    int fifoFd = ::open(fifoPath, O_RDONLY | O_NONBLOCK);
    efj_defer { ::close(fifoFd); };

    if (fifoFd < 0) {
        efj_internal_log(efj::log_level_crit, "[c2] error: Failed to open test-result fifo at %s: %s",
            fifoPath, strerror(errno));

        *shouldExit = true;
        *exitCode = efj::exit_test_fail;
        return;
    }

    for (efj::test_case& test : sys->tests) {
        test_result testResult = {};
        pid_t pid = -1;

        efj::string testName = efj::fmt("--c2-test=%s", test.name);
        const char* args[] = { sys->executablePath, testName.c_str(), nullptr };
        int spawnResult = ::posix_spawn(&pid, sys->executablePath, nullptr, nullptr, (char* const*)args, nullptr);
        if (spawnResult != 0) {
            efj::print_error("[c2] error: Failed to spawn test \"%s\" as a child process: %s\n",
                test.name, strerror(errno));
        }

        if (::waitpid(pid, nullptr, 0) != pid) {
            efj::print_error("[c2] error: Failed to wait for \"%s\" as a child process: %s\n",
                test.name, strerror(errno));
        }

        // We don't require successfully reading any number of bytes for this to work.
        ::read(fifoFd, &testResult, sizeof(testResult));
        if (!testResult.didNotCrash || testResult.failCount) {
            ++failedTestCount;
            sys->failedTests.add(&test);
        }
        else {
            ++passedTestCount;
        }

        ++totalTestCount;
    }

    *shouldExit = true;
    *exitCode = failedTestCount > 0 ? efj::exit_test_fail : efj::exit_test_pass;
}

static void
_testsys_report_failure(efj::test_system*, const efj::string& msg, const efj::source_location& loc)
{
    for (efj::test_note* note : globalCurrentTest->notes)
        globalCurrentTest->failureMessages.add(note->msg.dup(*globalCurrentTest->arena));

    efj::string decorated = efj::fmt("%s:%d: Failure\n  %s\n", loc.file, loc.line, msg.c_str());
    globalCurrentTest->failureMessages.add(decorated.dup(*globalCurrentTest->arena));
    globalCurrentTest->failCount++;
}

static void
_write_output(efj::test_captured_output* output, const efj::component_view* components, umm count)
{
    // @Temporary. We only handle the first component at the moment, and only if it's a message.

    if (output->captured && components[0].is_message()) {
        // TODO: components[i].as<efj::message>();
        //auto* msg = static_cast<efj::message*>(components[0].value());
        auto* msg = components[0].as<efj::message>();

        if (output->socket != -1) {
            efj::udp_send_to(output->socket, msg->data, msg->size, output->addr);
        }

        if (output->fd) {
            efj_temp_scope();
            efj::string hex = efj::to_hex(msg->data, msg->size, true);
            efj_ignored_write(output->fd, hex.c_str(), hex.size);
        }

// This works for non-messages as well, but writing over UDP is trickier in this case since
// we would either have to do an iovec scatter/gather write or copy it into a temp buffer, and
// there isn't a udp gather-write API at the moment.
#if 0
        {
            efj_temp_scope();

            efj::serializer sz;
            components[0].info->serialize(sz, components[0].value);

            for (auto* chunk = sz._chunks; chunk; chunk = chunk->next) {
                umm size = chunk == sz._tail ? sz._tailFilled : 128;
                efj::string hex = efj::to_hex(chunk->data, size, true);
                efj_ignored_write(output->fd, hex.c_str(), hex.size);
            }
        }
#endif
    }
}

extern bool
_testsys_on_produce(efj::producer* p, efj::consumer* target, const efj::component_view* components, umm count)
{
    efj_assert(count > 0);

    efj::testsys_producer_ctx* producerContext = p->testContext;
    if (producerContext->output.captured)
        _write_output(&producerContext->output, components, count);

    if (!efj::testing())
        return true;

    efj::testsys_object_ctx* objectContext = p->object->testContext;
    efj::test_output_queue* outputQueue = p->testContext->outputQueue
                                        ? p->testContext->outputQueue
                                        : objectContext->outputQueue;
    if (outputQueue) {
        outputQueue->push_pc(p, target, components, count);
    }

    // Continue to produce as long as this object's output hasn't been disconnected.
    return !objectContext->disconnected;
}

extern void
_testsys_on_consume(efj::consumer* c, const efj::component_view* components, umm count)
{
    efj::testsys_consumer_ctx* ctx = c->testContext;
    if (ctx->output.captured)
        _write_output(&ctx->output, components, count);
}

static bool
_testsys_on_log(const efj::log_message_ctx* ctx, const efj::string& msg)
{
    if (!globalCurrentTest)
        return true;

    if (ctx->formatted) {
        auto formatter = efj::get_log_formatter();
        efj::string formatted = formatter(*ctx, msg).dup(*globalCurrentTest->arena);
        globalCurrentTest->logMessages.add(formatted);
    }
    else {
        globalCurrentTest->logMessages.add(msg);
    }

    return false;
}

static bool
_testsys_on_trigger_event(efj::object* source, u32 id, const efj::component_view& e)
{
    if (!efj::testing())
        return true;

    efj::testsys_object_ctx* objectContext = source->testContext;
    if (objectContext->outputQueue) {
        objectContext->outputQueue->push_ue(source, nullptr, id, e);
    }

    return !objectContext->disconnected;
}

static bool
_testsys_on_send_event(efj::object* source, efj::object* target, u32 id, const efj::component_view& e)
{
    if (!efj::testing())
        return true;

    efj::testsys_object_ctx* objectContext = source->testContext;
    if (objectContext->outputQueue) {
        objectContext->outputQueue->push_ue(source, target, id, e);
    }

    return !objectContext->disconnected;
}

static void
_testsys_write_log_messages(efj::test_case* test)
{
    for (const efj::string& msg : test->logMessages)
        efj::print(msg);
}

static void
_testsys_push_note(const efj::string& msg, efj::test_note* note)
{
    note->mark = efj::begin_scope(efj::app()._testSystem->testNotesArena);
    note->msg = msg.dup(efj::app()._testSystem->testNotesArena);
    efj::globalCurrentTest->notes.add(note);
}

static void
_testsys_pop_note(efj::test_note* note)
{
    efj::globalCurrentTest->notes.pop();
    efj::end_scope(efj::app()._testSystem->testNotesArena, note->mark);
}

} // namespace efj
