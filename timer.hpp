namespace efj
{

// Opaque.
struct timer
{
    efj::eventsys_timer_event event   = {};
    bool                      started = false;
    efj::timer_type           type    = efj::timer_invalid_;
};

struct timer_spec
{
    efj::sec  s;
    efj::nsec ns;
};

struct timer_event
{
    efj::timer* source = nullptr;
    efj::nsec   expiredTime = {};
    u64         expiredCount = 0;
};

// A zero inital timeout or delay means the timer will expire immediately
// (as opposed to being disabled).
bool create_oneshot_timer(const char* name, efj::nsec delay, efj::timer* result);
bool create_oneshot_timer(const char* name, efj::timer* result);
bool create_periodic_timer(const char* name, efj::nsec initial, efj::nsec interval, efj::timer* result);
bool create_periodic_timer(const char* name, efj::nsec interval, efj::timer* result);
bool create_periodic_timer(const char* name, efj::timer* result);

// Create + monitor, but don't start it.
template <typename T> inline void
create_oneshot_timer(const char* name, efj::nsec delay,
    efj::timer* result, void (T::*callback)(efj::timer_event& e));

template <typename T> inline void
create_periodic_timer(const char* name, efj::nsec initial, efj::nsec interval,
    efj::timer* result, void (T::*callback)(efj::timer_event& e));

template <typename T> inline void
create_periodic_timer(const char* name, efj::nsec interval,
    efj::timer* result, void (T::*callback)(efj::timer_event& e));

// Create, monitor, and start the timer.
template <typename T> inline void
add_oneshot_timer(const char* name, efj::nsec delay,
                  efj::timer* result,
                  void (T::*callback)(efj::timer_event& e));

template <typename T> inline void
add_periodic_timer(const char* name, efj::nsec interval,
                   efj::timer* result,
                   void (T::*callback)(efj::timer_event& e));

template <typename T> inline void
add_periodic_timer(const char* name, efj::nsec initial, efj::nsec interval,
                   efj::timer* result,
                   void (T::*callback)(efj::timer_event& e));

//{ Enable/Disable/Replace timer callbacks without needing to start/stop timers.
//
template <typename T_> inline void
timer_monitor(efj::timer& t, void (T_::*cb)(efj::timer_event& e));

//}

inline bool timer_is_running();
inline bool timer_is_oneshot();

void timer_stop(efj::timer& t);
void timer_start(efj::timer& t);
void timer_start(efj::timer& t, efj::nsec timeout);
void timer_start_absolute(efj::timer& t, efj::nsec expireTime);
void timer_start_absolute(efj::timer& t, efj::nsec expireTime, efj::nsec timeout);
void timer_set_timeout(efj::timer& t, efj::nsec timeout);
void timer_set_timeout(efj::timer& t, efj::nsec initial, efj::nsec interval);
void timer_destroy(efj::timer& t);

} // namespace efj
