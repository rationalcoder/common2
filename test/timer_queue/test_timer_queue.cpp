#include <stdio.h>

// Putting this up here b/c we are testing internals.
#define EFJ_C2_IMPLEMENTATION
#include <c2.hpp>

#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdlib.h>
#include <iostream>
#include <utest.h>


void printEntries(efj::timer_queue& queue)
{
    for (umm i = 0; i < queue.size(); i++)
        std::cout << queue._entries[i].expireTime << " ";

    std::cout << std::endl;
}

UTEST(oneshot, insert_in_order_expire_in_order)
{
    efj::timer_queue queue;
    queue.init(0);

    efj::timer_handle handles[20];

    queue.set_time(0);

    for (uintptr_t i = 0; i < 20; i++)
    {
        uintptr_t expireTime = i + 1;
        queue.insert_absolute_oneshot((void*)expireTime, expireTime, &handles[i]);
    }

    efj::timer_queue_event events[20];
    umm n = queue.poll(events);
    ASSERT_EQ(0u, n);

    //{
    queue.set_time(0);
    n = queue.poll(events);
    ASSERT_EQ(0u, n);
    ASSERT_EQ(20u, queue.size());
    //}

    //{
    queue.set_time(1);
    n = queue.poll(events);
    ASSERT_EQ(1u, n);
    for (umm i = 0; i < n; i++)
    {
        ASSERT_EQ(i + 1, events[i].expiredTime);
    }
    ASSERT_EQ(19u, queue.size());
    //}

    //{
    queue.set_time(3);
    n = queue.poll(events);
    ASSERT_EQ(2u, n);
    for (umm i = 0; i < n; i++)
    {
        ASSERT_EQ(i + 2, events[i].expiredTime);
    }
    ASSERT_EQ(17u, queue.size());
    //}

    //{
    queue.set_time(20);
    n = queue.poll(events);
    ASSERT_EQ(17u, n);
    for (umm i = 0; i < n; i++)
    {
        ASSERT_EQ(i + 4, events[i].expiredTime);
    }
    ASSERT_EQ(0u, queue.size());
    //}
}

UTEST(oneshot, insert_reversed_expire_in_order)
{
    efj::timer_queue queue;
    queue.init(0);

    umm timeOffset = 1;

    efj::timer_handle handles[20];
    for (int i = 0; i < 20; i++, timeOffset += 3)
    {
        std::cout << "i=" << i << " offset=" << timeOffset << std::endl;
        queue.set_time(0);

        for (int i = 19; i >= 0; i--)
        {
            queue.insert_absolute_oneshot((void*)(uintptr_t)i, i + timeOffset, &handles[i]);
        }

        efj::timer_queue_event events[20];
        umm n = queue.poll(events);
        ASSERT_EQ(0u, n);

        //{
        queue.set_time(timeOffset - 1);
        n = queue.poll(events);
        ASSERT_EQ(0u, n);
        ASSERT_EQ(20u, queue.size());
        //}

        //{
        queue.set_time(timeOffset);
        n = queue.poll(events);
        ASSERT_EQ(1u, n);
        for (umm i = 0; i < n; i++)
        {
            ASSERT_EQ(i + timeOffset, events[i].expiredTime);
        }
        ASSERT_EQ(19u, queue.size());
        //}

        //{
        queue.set_time(timeOffset + 2);
        n = queue.poll(events);
        ASSERT_EQ(2u, n);
        for (umm i = 0; i < n; i++)
        {
            ASSERT_EQ(i + timeOffset + 1, events[i].expiredTime);
        }
        ASSERT_EQ(17u, queue.size());
        //}

        //{
        queue.set_time(timeOffset + 19);
        n = queue.poll(events);
        ASSERT_EQ(17u, n);
        for (umm i = 0; i < n; i++)
        {
            ASSERT_EQ(i + timeOffset + 3, events[i].expiredTime);
        }
        ASSERT_EQ(0u, queue.size());
        //}
    }
}

UTEST(periodic, insert_in_order_expire_in_order)
{
    efj::timer_queue queue;
    queue.init(0);

    u64 duration = 1;
    umm timerCount = 10;

    efj::timer_handle handles[timerCount];
    for (uintptr_t i = 0; i < timerCount; i++)
    {
        uintptr_t expireTime = i + 1;
        queue.insert_absolute((void*)expireTime, expireTime, duration, &handles[i]);
    }

    efj::timer_queue_event events[20];
    umm n = queue.poll(events);
    ASSERT_EQ(0u, n);

    //{
    // [1][2][3][4]
    //  1  2  3  4
    queue.set_time(0);
    n = queue.poll(events);
    ASSERT_EQ(0u, n);
    ASSERT_EQ(timerCount, queue.size());
    //}

    //{
    //    [1][2][3][4]
    // T = 1  2  3  4
    queue.set_time(1);
    n = queue.poll(events);
    ASSERT_EQ(1u, n);
    ASSERT_EQ(1u, events[0].expiredTime); ASSERT_EQ(1u, (umm)events[0].handle->timer);
    ASSERT_EQ(timerCount, queue.size());
    //}

    // Order of events should come in top->down first, then bottom->right.
    // Here, we should get 2,1,3, for example.

    //{
    //    [1]
    //    [2][3][4]
    // T = 2  3  4
    queue.set_time(3);
    n = queue.poll(events);
    ASSERT_EQ(3u, n);

    // NOTE: The order of the timers coming out being a bit backwards sometimes is
    // due to the heap-insert behavior of not swapping with a node's parent if
    // its value is not strictly less than its parent's. This doesn't make for
    // the best test, but whatever. We just want to make sure that the events
    // come out with the correct timer pointers. We assume that if the timers are right
    // for this limited case, they are correct for all other cases.

    // Timers that have been rearmed should start expiring at the same time.
    //ASSERT_EQ(2u, events[0].expiredTime); ASSERT_EQ(1u, (umm)events[0].handle->timer);
    //ASSERT_EQ(2u, events[1].expiredTime); ASSERT_EQ(2u, (umm)events[1].handle->timer);
    //ASSERT_EQ(3u, events[2].expiredTime); ASSERT_EQ(3u, (umm)events[2].handle->timer);

    //ASSERT_EQ(2u, events[0].expiredCount);
    //ASSERT_EQ(2u, events[1].expiredCount);
    //ASSERT_EQ(1u, events[2].expiredCount);

    for (umm i = 0; i < n; i++)
    {
        if ((umm)events[i].handle->timer == 1)
            ASSERT_EQ(2u, events[i].expiredTime);
        else if ((umm)events[i].handle->timer == 2)
            ASSERT_EQ(2u, events[i].expiredTime);
        else if ((umm)events[i].handle->timer == 3)
            ASSERT_EQ(3u, events[i].expiredTime);

        ASSERT_EQ(events[i].handle - handles,  (intptr_t)events[i].handle->timer - 1);
    }

    ASSERT_EQ(timerCount, queue.size());
    //}

    //{
    //    [1]
    //    [2]
    //    [3]
    //    [4][5][6][7][8][9][10]
    // T = 4  5  6  7  8  9  10
    queue.set_time(10);
    n = queue.poll(events);
    ASSERT_EQ(10u, n);

    for (int i = 0; i < 4; i++)
    {
        ASSERT_EQ(4u, events[i].expiredTime);
        ASSERT_EQ(events[i].handle - handles,  (intptr_t)events[i].handle->timer - 1);
    }

    for (umm i = 4; i < n; i++)
    {
        ASSERT_EQ(i + 1, events[i].expiredTime);
        ASSERT_EQ(i + 1, (umm)events[i].handle->timer);
    }

    ASSERT_EQ(timerCount, queue.size());
    //}
}

UTEST(periodic, insert_reversed_expire_in_order)
{
    efj::timer_queue queue;
    queue.init(0);

    u64 duration = 1;
    umm timerCount = 10;

    efj::timer_handle handles[timerCount];
    for (int i = timerCount-1; i >= 0; i--)
    {
        queue.insert_absolute((void*)(uintptr_t)(i + 1), i + 1, duration, &handles[i]);
    }

    efj::timer_queue_event events[20];
    umm n = queue.poll(events);
    ASSERT_EQ(0u, n);

    queue.set_time(20);
    n = queue.poll(events);
    ASSERT_EQ(timerCount, n);

    for (umm i = 0; i < n; i++)
    {
        ASSERT_EQ(i + 1, events[i].expiredTime);
        ASSERT_EQ(i + 1, (umm)events[i].handle->timer);
    }
}

UTEST(periodic, rearmed_properly)
{
    efj::timer_queue queue;
    queue.init(0);

    u64 durations[] = { 2, 3, 5, 7, 9 };

    efj::timer_handle handles[efj::array_size(durations)];
    for (umm i = 0; i < efj::array_size(durations); i++)
    {
        queue.insert_absolute((void*)(uintptr_t)(durations[i]), durations[i], durations[i], &handles[i]);
    }

    efj::timer_queue_event events[20];

    for (umm i = 0; i < efj::array_size(durations); i++)
    {
        queue.set_time(durations[i]);
        umm n = queue.poll(events);
        ASSERT_EQ(1u, n);
        ASSERT_EQ(durations[i], events[0].expiredTime);
        ASSERT_EQ(durations[i]*2, events[0].handle->entry->expireTime);
        queue.remove(events[0].handle);
        ASSERT_EQ(efj::array_size(durations) - i - 1, queue.size());
    }
}

// When timers are popped off the queue, the lowest-priority timer
// is moved to the root and swapped back down as part of a normal
// priority-queue algorithm. A similar algorithm happens during insertion.
// We want to make sure these handles are updated appropriately.
UTEST(mixed, entry_handles_are_updated)
{
    efj::timer_queue queue;
    queue.init(0);

    efj::timer_handle handles[6];
    // We want to test
    // 1. The first timer being re-inserted at the end and staying there.
    // 2. The first timer being re-inserted at the end and moving somewhere else.
    queue.insert_absolute((void*)0, 0, 100, &handles[0]);
    queue.insert_absolute((void*)1, 1, 1, &handles[1]);
    queue.insert_absolute((void*)2, 2, 1, &handles[2]);
    queue.insert_absolute((void*)3, 3, 1, &handles[3]);
    queue.insert_absolute((void*)4, 4, 1, &handles[4]);
    queue.insert_absolute((void*)5, 100, 1, &handles[5]);

    //        (0,0)
    //   (1,1)     (2,2)
    // (3,3)(4,4)(5,100)

    efj::timer_queue_event events[20];

    // Pop off the first timer.
    umm n = queue.poll(events, 1);
    ASSERT_EQ(1u, n);
    ASSERT_EQ(queue.size(), 6u);
    ASSERT_EQ(events[0].handle->timer, (void*)0);
    ASSERT_EQ(events[0].handle->entry, &queue._entries[queue.size()-1]);

    //        (1,1)
    //   (3,3)     (2,2)
    // (5,100)(4,4)(0,100)

    struct Expectation
    {
        umm       id;
        umm       expireTime;
        ptrdiff_t handleIndex; // The index of the handle passed during insertion.
    };

    {
        Expectation expected[] = {
            { 1,   1, 1 },
            { 3,   3, 3 },
            { 2,   2, 2 },
            { 5, 100, 5 },
            { 4,   4, 4 },
            { 0, 100, 0 },
        };

        for (int i = 0; i < (int)efj::array_size(expected); i++) {
            ASSERT_EQ(queue._entries[i].handle->entry - &queue._entries[0], i);
            ASSERT_EQ(queue._entries[i].handle - &handles[0], expected[i].handleIndex);
            ASSERT_EQ((umm)queue._entries[i].handle->timer, expected[i].id);
            ASSERT_EQ(queue._entries[i].expireTime, expected[i].expireTime);
        }
    }

    //for (int i = 0; i < (int)queue.size(); i++) {
    //    printf("entries[%d]: (%zu,%zu,%zu,%zu)\n", i, (umm)queue._entries[i].handle->timer, queue._entries[i].expireTime,
    //            queue._entries[i].handle - &handles[0], queue._entries[i].handle->entry - &queue._entries[0]);
    //}
    //printf("\n");

    //        (1,1)
    //   (3,3)     (2,2)
    // (5,100)(4,4)(0,100)

    ASSERT_EQ((umm)handles[3].entry->handle->timer, 3u);
    queue.stop(&handles[3]);
    ASSERT_EQ(queue.size(), 5u);
    ASSERT_EQ(handles[3].entry, nullptr);

    //           (1,1)
    //      (4,4)     (2,2)
    // (5,100)(0,100)

    {
        Expectation expected[] = {
            { 1,   1, 1 },
            { 4,   4, 4 },
            { 2,   2, 2 },
            { 5, 100, 5 },
            { 0, 100, 0 },
        };

        //for (int i = 0; i < (int)efj::array_size(expected); i++) {
        //    printf("entries[%d]: (%zu,%zu,%zu,%zu)\n", i, (umm)queue._entries[i].handle->timer, queue._entries[i].expireTime,
        //        queue._entries[i].handle - &handles[0], queue._entries[i].handle->entry - &queue._entries[0]);
        //}

        for (int i = 0; i < (int)efj::array_size(expected); i++) {
            ASSERT_EQ(queue._entries[i].handle->entry - &queue._entries[0], i);
            ASSERT_EQ(queue._entries[i].handle - &handles[0], expected[i].handleIndex);
            ASSERT_EQ((umm)queue._entries[i].handle->timer, expected[i].id);
            ASSERT_EQ(queue._entries[i].expireTime, expected[i].expireTime);
        }
    }
}

// This bug should have been caught be the general handle updating test, but it wasn't,
// so I've isolated it here.
UTEST(bug, handle_entries_not_updated_remove)
{
    efj::timer_queue queue;
    queue.init(0);

    efj::timer_handle handles[6];
    queue.insert_absolute((void*)0, 0, 100, &handles[0]);
    queue.insert_absolute((void*)1, 1, 1, &handles[1]);
    queue.insert_absolute((void*)2, 2, 1, &handles[2]);
    queue.insert_absolute((void*)3, 3, 1, &handles[3]);
    queue.insert_absolute((void*)4, 4, 1, &handles[4]);
    queue.insert_absolute((void*)5, 100, 1, &handles[5]);

    //        (0,0)
    //   (1,1)     (2,2)
    // (3,3)(4,4)(5,100)

    //efj::timer_queue_event events[20];
    queue.stop(&handles[5]);

    for (int i = 0; i < (int)queue.size(); i++) {
        ASSERT_EQ(queue._entries[i].handle->entry - &queue._entries[0], i);
    }
}

// This bug happens when the next absolute expire time of a periodic timer is > UINT32_MAX.
// I turned on timer_queue tracing to help build this repro, which is why it looks very
// specific and redundant, but theoretically we could just make sure the origin + duration
// is > UINT32_MAX, which is why we do a second test to confirm our understanding of the bug.
UTEST(bug, invalid_periodic_expire_time_on_32_bit)
{
    // Test the original bug
    efj::timer_queue queue;
    queue.init(656992355134977);
    efj::timer_handle handle;
    queue.start(&handle, 0, 2000000000, 2000000000, false);

    queue.set_time(656992383969447);
    queue.set_time(656994355380615);

    efj::timer_queue_event events[16] = {};
    umm n = queue.poll(events);
    EXPECT_EQ(n, 1u);

    queue.set_time(656994357635447);
    queue.set_time(656994358351489);
    n = queue.poll(events);
    EXPECT_EQ(n, 0u);
    EXPECT_EQ(queue.next_expire_time(), 656992355134977llu + 4000000000llu);

    // Test our understanding of the bug
    efj::timer_queue queue2;
    queue2.init(UINT32_MAX);
    efj::timer_handle handle2;
    queue2.start(&handle2, 0, 1, 1, false);
    queue2.set_time((u64)UINT32_MAX + 1);
    n = queue2.poll(events);
    EXPECT_EQ(n, 1u);
    EXPECT_EQ(queue2.next_expire_time(), (u64)UINT32_MAX + 2);
}

int main(int argc, char** argv)
{
    return utest_main(argc, argv);
}

UTEST_STATE();

