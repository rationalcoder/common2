#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdlib.h>
#include <signal.h>
#include <setjmp.h>
#include <utest.h>
#include <c2.hpp>

struct SignalContext
{
    sig_atomic_t segFaultCount     = 0;
    sig_atomic_t abortCount        = 0;
    sig_atomic_t expansionCount    = 0;
    sig_atomic_t expectingSegFault = false;
    sig_atomic_t expectingAbort    = false;
    sigjmp_buf   jumpBuf;
};

static SignalContext gContext;

void clear_context()
{
    efj_placement_new(&gContext) SignalContext();
}

void handle_assertion_failed()
{
    gContext.abortCount++;
    gContext.expectingAbort = false;
    siglongjmp(gContext.jumpBuf, true);
}

void signal_handler(int sig)
{
    switch (sig) {
    case SIGSEGV:
        if (gContext.expectingAbort && efj::asserted()) {
            handle_assertion_failed();
            break;
        }

        if (gContext.expectingSegFault) {
            gContext.segFaultCount++;
            gContext.expectingSegFault = false;
            siglongjmp(gContext.jumpBuf, true);
        }
        else {
            signal(SIGSEGV, SIG_DFL);
            raise(SIGSEGV);
        }

        break;
    case SIGABRT:
        if (gContext.expectingAbort) {
            handle_assertion_failed();
        }
        else {
            signal(SIGABRT, SIG_DFL);
            raise(SIGABRT);
        }
        break;
    default:
        fprintf(stderr, "Unhandled signal %d\n", sig);
        break;
    }
}

bool validate_chunk(efj::memory_arena_chunk* chunk, const char* chunkName)
{
    if (chunk->at < chunk->start || chunk->at > chunk->end) {
        bool lessThanStart = chunk->at < chunk->start;
        printf("error: arena \"at\" placed outside of allocation space in '%s' (%s). Failing current test.\n", chunkName,
            lessThanStart ? "at < start" : "at > end");
        printf("at: %zu, start: %zu, end: %zu\n", (umm)chunk->at,
            (umm)chunk->start, (umm)chunk->end);
        return false;
    }

    // Check that the actual chunk itself was not placed within the chunk bounds.
    if ((u8*)chunk >= chunk->start && (u8*)chunk < chunk->end) {
        printf("error: arena chunk struct for '%s' placed inside of allocation space. Failing current test.\n", chunkName);
        printf("chunk address: %zu, start: %zu, end: %zu\n", (umm)chunk,
        (umm)chunk->start, (umm)chunk->end);
        return false;
    }

    if (!chunk->next || !chunk->prev) {
        printf("error: arena chunk '%s' has a null %s pointer. Failing current test\n",
            chunkName, !chunk->next ? "next" : "prev");
        return false;
    }

    return true;
}

u8* arena_expansion_counter(efj::memory_arena* arena, umm size, umm alignment)
{
    u8* result = efj::expand_arena(arena, size, alignment);
    gContext.expansionCount++;

    if (!validate_chunk(arena->curChunk, "cur") || !validate_chunk(arena->tailChunk, "tail"))
        return nullptr;

    return result;
}

void* push_and_write(efj::memory_arena& arena, umm size, umm alignment)
{
    u8* result = (u8*)efj_push(arena, size, alignment);
    if (!efj::is_aligned(result, alignment)) {
        if (alignment <= EFJ_CACHELINE_SIZE) {
            printf("Alignment issue.\n");
            efj_assert(false);
        }
    }

    memset(result, 0, size);
    return result;
}

int count_mapped_chunks(efj::memory_arena& arena)
{
    int count = 1;
    for (efj::memory_arena_chunk* cur = arena.tailChunk->next; cur != arena.tailChunk; cur = cur->next)
        count++;

    return count;
}

// Distance between cur and tail.
int count_available_chunks(efj::memory_arena& arena)
{
    int count = 0;
    for (efj::memory_arena_chunk* cur = arena.curChunk; cur != arena.tailChunk; cur = cur->next)
        count++;

    return count;
}


UTEST(general, literals)
{
    ASSERT_EQ(1ULL, 1_B);
    ASSERT_EQ(1024ULL, 1_KB);
    ASSERT_EQ(1048576ULL, 1_MB);
    ASSERT_EQ(1073741824ULL, 1_GB);

    ASSERT_EQ(3 * 1ULL, 3_B);
    ASSERT_EQ(3 * 1024ULL, 3_KB);
    ASSERT_EQ(3 * 1048576ULL, 3_MB);
    ASSERT_EQ(3 * 1073741824ULL, 3_GB);
}

UTEST(general, alignment)
{
    u8* p = (u8*)0xb7d2d000;
    ASSERT_TRUE(efj::is_aligned(p, 4_KB));
    ASSERT_EQ(p, efj::align_up(p, 4_KB));
    ASSERT_EQ(p, efj::align_down(p, 4_KB));
    ASSERT_EQ(p + 4,  efj::aligned_offset(p, 4, 1));
    ASSERT_EQ(p + 4,  efj::aligned_offset(p, 4, 2));
    ASSERT_EQ(p + 4,  efj::aligned_offset(p, 4, 4));
    ASSERT_EQ(p + 8,  efj::aligned_offset(p, 4, 8));
    ASSERT_EQ(p + 16, efj::aligned_offset(p, 4, 16));
    ASSERT_EQ(p + 32, efj::aligned_offset(p, 4, 32));
    ASSERT_EQ(p + 64, efj::aligned_offset(p, 4, 64));
    ASSERT_EQ(p + 0,  efj::aligned_offset(p, 0, 4_KB));
    // TODO: ...
}

UTEST(fixed, sanity)
{
    clear_context();

    umm kPageSize         = sysconf(_SC_PAGESIZE);
    umm kChunkSize        = 4_KB;
    umm kUsableChunkSize  = 4_KB - sizeof(efj::memory_arena_chunk);
    umm kMaxSize          = 64_KB;
    umm kAlignedChunkSize = kChunkSize < kPageSize ? kPageSize : kChunkSize;

    auto arena = efj::allocate_arena("Test", kChunkSize, kMaxSize);
    arena.expand = &arena_expansion_counter;

    ASSERT_EQ(arena.type, efj::arena_type_fixed);
    ASSERT_EQ(arena.chunkSize, kAlignedChunkSize);
    ASSERT_EQ(efj::align_up(kMaxSize + sizeof(efj::memory_arena_chunk), 4_KB), arena.maxSize);
    ASSERT_EQ(arena.curChunk, arena.tailChunk);

    u8* i = (u8*)efj_push_type(arena, int);
    ASSERT_EQ(i, arena.curChunk->start);
    ASSERT_EQ(arena.curChunk->at, arena.curChunk->start + sizeof(int));
    ASSERT_EQ(arena.curChunk->start + kUsableChunkSize, arena.curChunk->end);

    efj::reset(arena);
    ASSERT_EQ(arena.curChunk->at, arena.curChunk->start);
    ASSERT_EQ(arena.curChunk->start + kUsableChunkSize, arena.curChunk->end);

    for (int i = 0; i < 8; i++) {
        //printf("Checking push (i = %d) (%u)\n", i, 1);
        u8* space = (u8*)push_and_write(arena, 8_KB, 1);
        ASSERT_EQ(i+1, gContext.expansionCount);
        //printf("Space: %p\n", space);
        ASSERT_EQ(arena.curChunk->start + i * 8_KB, space);
        *space = 10;
    }

    //for (umm i = 0; i < kMaxSize; i++) {
    //    efj_push(arena, 1, 1);
    //}

    ASSERT_EQ(64_KB, efj::arena_size(arena));
    ASSERT_EQ((arena.curChunk->start - sizeof(efj::memory_arena_chunk)) + 68_KB, arena.curChunk->end);
    efj::reset(arena);
    // 'end' isn't reset to avoid calling mprotect unnecessarily.
    ASSERT_EQ((arena.curChunk->start - sizeof(efj::memory_arena_chunk)) + 68_KB, arena.curChunk->end);
    ASSERT_EQ(arena.curChunk, arena.tailChunk);
    ASSERT_EQ(arena.curChunk->start, arena.curChunk->at);

    efj::free_arena(arena);
}

UTEST(fixed, chunks_added)
{
    auto arena = efj::allocate_arena("Test", 4_KB, 16_KB);

    push_and_write(arena, 4097, 1);
    ASSERT_EQ(arena.curChunk->start - sizeof(efj::memory_arena_chunk) + 2*4_KB, arena.curChunk->end);

    push_and_write(arena, 4096, 1);
    ASSERT_EQ(arena.curChunk->start - sizeof(efj::memory_arena_chunk) + 3*4_KB, arena.curChunk->end);

    efj::free_arena(arena);
}

UTEST(fixed, page_protection_after_allocation)
{
    clear_context();

    auto arena = efj::allocate_arena("Test", 4_KB, 64_KB);
    arena.expand = &arena_expansion_counter;

    // The first page should be accessible, but the rest should not be.

    umm offset = 0;
    for (umm i = 0; i < 16; i++, offset += 4_KB) {
        gContext.expectingSegFault = true;

        if (sigsetjmp(gContext.jumpBuf, true)) {
            ASSERT_EQ(i, (umm)gContext.segFaultCount);
        }
        else {
            *(arena.curChunk->start + offset) = 0;
        }
    }

    efj::free_arena(arena);
}

UTEST(fixed, pages_writable_after_allocation)
{
    clear_context();

    auto arena = efj::allocate_arena("Test", 4_KB, 64_KB);
    ASSERT_EQ(arena.maxSize, 68_KB);

    for (int i = 0; i < 16; i++) {
        push_and_write(arena, 4_KB, 1);
    }

    gContext.expectingSegFault = true;

    if (sigsetjmp(gContext.jumpBuf, true)) {
        ASSERT_EQ(0, gContext.segFaultCount);
    }

    // Test the extra page before the guard page.
    if (sigsetjmp(gContext.jumpBuf, true)) {
        ASSERT_EQ(0, gContext.segFaultCount);
    }
    else {
        *(arena.curChunk->start + 64_KB) = 0;
    }

    // Test the guard page.
    if (sigsetjmp(gContext.jumpBuf, true)) {
        ASSERT_EQ(1, gContext.segFaultCount);
    }
    else {
        *(arena.curChunk->start + 68_KB) = 0;
    }

    efj::free_arena(arena);
}

// This tests a regression where we were committing the smallest amount
// of 4KB pages necessary instead of the chunk size, _while_ setting the
// chunk's end pointer properly, which left a non-committed page.
UTEST(fixed, pages_writable_after_allocation_8KB)
{
    clear_context();

    auto arena = efj::allocate_arena("Test", 8_KB, 64_KB);
    ASSERT_EQ(arena.maxSize, 72_KB);

    gContext.expectingSegFault = true;

    if (sigsetjmp(gContext.jumpBuf, true)) {
        ASSERT_EQ(0, gContext.segFaultCount);
    }
    else {
        push_and_write(arena, 4_KB - sizeof(efj::memory_arena_chunk), 1);
    }

    for (int i = 0; i < 16; i++) {
        if (sigsetjmp(gContext.jumpBuf, true)) {
            ASSERT_EQ(0, gContext.segFaultCount);
        }
        else {
            push_and_write(arena, 4_KB, 1);
        }
    }

    // The underlying allocation should be padded with chunk-size bytes
    // to account for the arena chunk header and keep the padding usable.
    // Otherwise, we would try to allocate 8KB at the end and see we only
    // have 4KB, which would fail unnecessarily.

    // Test the first extra page before the guard page.
    if (sigsetjmp(gContext.jumpBuf, true)) {
        ASSERT_EQ(0, gContext.segFaultCount);
    }
    else {
        *(arena.curChunk->start + 64_KB) = 0;
    }

    // Test the second extra page before the guard page.
    if (sigsetjmp(gContext.jumpBuf, true)) {
        ASSERT_EQ(0, gContext.segFaultCount);
    }
    else {
        *(arena.curChunk->start + 68_KB) = 0;
    }

    // Test the guard page.
    if (sigsetjmp(gContext.jumpBuf, true)) {
        ASSERT_EQ(1, gContext.segFaultCount);
    }
    else {
        *(arena.curChunk->start + 72_KB) = 0;
    }

    efj::free_arena(arena);
}

UTEST(fixed, pages_freed_to_prot_none)
{
    clear_context();

    auto arena = efj::allocate_arena("Test", 4_KB, 64_KB);

    for (int i = 0; i < 16; i++) {
        push_and_write(arena, 4_KB, 1);
    }

    efj::free_arena(arena);

    umm offset = 0;
    for (umm i = 0; i < 16; i++, offset += 4_KB) {
        gContext.expectingSegFault = true;

        if (sigsetjmp(gContext.jumpBuf, true)) {
            ASSERT_EQ(i+1, (umm)gContext.segFaultCount);
        }
        else {
            *(arena.curChunk->start + offset) = 0;
        }
    }
}

// Introducing holes by using alignments larger than the page size is no longer possible/allowed,
// since we moved chunk structs to the beginning of chunks instead of the end.
#if 0
UTEST(fixed, no_holes_in_page_protection)
{
    clear_context();

    auto arena = efj::allocate_arena("Test", 4_KB, 60_KB);

    // Introduce some holes where entire pages are skipped due to alignment.
    push_and_write(arena, 12_KB, 8_KB);
    push_and_write(arena, 12_KB, 8_KB);
    push_and_write(arena, 12_KB, 8_KB);

    // Allocate the remaining space.
    umm remaining = (arena.curChunk->start + arena.maxSize) - arena.curChunk->at;
    if (remaining) {
        push_and_write(arena, remaining, 4_KB);
    }

    umm offset = 0;
    for (umm i = 0; i < 15; i++, offset += 4_KB) {
        gContext.expectingSegFault = true;

        if (sigsetjmp(gContext.jumpBuf, true)) {
            printf("Faulted (i = %zu)\n", i);
        }
        else {
            *(arena.curChunk->start + offset) = 0;
        }
    }

    ASSERT_EQ(0, gContext.segFaultCount);

    efj::free_arena(arena);
}
#endif

UTEST(fixed, expands_until_full)
{
    clear_context();

    auto arena = efj::allocate_arena("Test", 4_KB, 16_KB);
    arena.expand = &arena_expansion_counter;

    for (int i = 0; i < 4; i++) {
        push_and_write(arena, 4_KB, 1);
    }

    // Right now, the chunk size overlaps the chunk struct, while the max size is the full usable space, so
    // we really do have at least 16_KB of usable space, but we will have one extra expansion.
    ASSERT_EQ(4, gContext.expansionCount);
    ASSERT_EQ((arena.curChunk->start - sizeof(efj::memory_arena_chunk)) + 20_KB, arena.curChunk->end);
    efj::free_arena(arena);
}

UTEST(fixed, doesnt_expand_after_full)
{
    clear_context();

    auto arena = efj::allocate_arena("Test", 4_KB, 16_KB);
    arena.expand = &arena_expansion_counter;

    for (int i = 0; i < 4; i++) {
        push_and_write(arena, 4_KB, 1);
    }

    // We have one extra page of memory that we can use since the memory needed for
    // the chunk struct gets padded out to a full page.
    push_and_write(arena, 4_KB - sizeof(efj::memory_arena_chunk), 1);

    gContext.expectingAbort = true;

    ASSERT_EQ(4, gContext.expansionCount);
    if (!sigsetjmp(gContext.jumpBuf, true)) {
        push_and_write(arena, 1, 1);
    }
    ASSERT_EQ(4, gContext.expansionCount);
    ASSERT_EQ(1, gContext.abortCount);

    efj::free_arena(arena);
}

UTEST(dynamic, sanity)
{
    clear_context();

    auto arena = efj::allocate_arena("Test", 8_KB);
    arena.expand = &arena_expansion_counter;

    ASSERT_EQ(arena.curChunk, arena.tailChunk);
    ASSERT_EQ(arena.chunkSize, 8_KB);
    ASSERT_EQ(arena.maxSize, SIZE_MAX);

    ASSERT_EQ(0u, efj::arena_size(arena));

    push_and_write(arena, 4_KB - sizeof(efj::memory_arena_chunk), 1);
    push_and_write(arena, 4_KB, 1);

    ASSERT_EQ(0, gContext.expansionCount);

    push_and_write(arena, 4_KB - sizeof(efj::memory_arena_chunk), 1);
    push_and_write(arena, 4_KB, 1);
    ASSERT_EQ(1, gContext.expansionCount);
    ASSERT_EQ(8_KB, (arena.tailChunk->end - arena.tailChunk->start) + sizeof(efj::memory_arena_chunk));

    ASSERT_EQ(arena.tailChunk, arena.curChunk);
    ASSERT_NE(arena.tailChunk, arena.tailChunk->next);

    push_and_write(arena, 4_KB, 1);
    push_and_write(arena, 4_KB, 2);
    push_and_write(arena, 4_KB, 4);
    push_and_write(arena, 4_KB, 8);
    push_and_write(arena, 4_KB, 16);
    push_and_write(arena, 4_KB, 1);
    push_and_write(arena, 4_KB, 1);
    push_and_write(arena, 4_KB, 1);
    push_and_write(arena, 4_KB, 1);
    push_and_write(arena, 4_KB, 1);

    efj::reset(arena);
    efj::free_arena(arena);
}

UTEST(dynamic, use_after_reset)
{
    clear_context();

    auto arena = efj::allocate_arena("Test", 8_KB);
    arena.expand = &arena_expansion_counter;

    for (int i = 0; i < 3; i++) {
        push_and_write(arena, 8_KB - sizeof(efj::memory_arena_chunk), 1);
    }

    efj::reset(arena);

    for (int i = 0; i < 3; i++) {
        push_and_write(arena, 16_KB - sizeof(efj::memory_arena_chunk), 1);
    }

    efj::reset(arena);

    for (int i = 0; i < 3; i++) {
        push_and_write(arena, 32_KB - sizeof(efj::memory_arena_chunk), 1);
    }

    int chunkCount = count_mapped_chunks(arena);

    ASSERT_EQ(4, chunkCount);
    ASSERT_EQ(8, gContext.expansionCount);

    efj::free_arena(arena);
}

UTEST(dynamic, small_pages_unmapped)
{
    efj_temp_scope();
    clear_context();

    auto arena = efj::allocate_arena("Test", 8_KB);
    arena.expand = &arena_expansion_counter;

    efj::array<u8*> originalChunks(efj::temp_memory());

    for (int i = 0; i < 3; i++) {
        u8* space = (u8*)push_and_write(arena, 8_KB - sizeof(efj::memory_arena_chunk), 1);
        originalChunks.add(space);
    }

    efj::reset(arena);

    for (int i = 0; i < 3; i++) {
        push_and_write(arena, 32_KB - sizeof(efj::memory_arena_chunk), 1);
    }

    umm chunkIdx = 0;
    for (u8* chunk : originalChunks) {
        gContext.expectingSegFault = true;
        if (chunkIdx == 0) {
            if (!sigsetjmp(gContext.jumpBuf, true)) {
                *chunk = 10;
            }
            else {
                ASSERT_EQ(0, gContext.segFaultCount);
            }
        }
        else {
            if (!sigsetjmp(gContext.jumpBuf, true)) {
                *chunk = 10;
            }
        }

        chunkIdx++;
    }

    ASSERT_EQ(2, gContext.segFaultCount);

    efj::free_arena(arena);
}

UTEST(dynamic, pages_freed_to_prot_none)
{
    efj_temp_scope();
    clear_context();

    auto arena = efj::allocate_arena("Test", 8_KB);

    efj::array<u8*> allocations(efj::temp_memory());
    efj::array<u8*> chunks(efj::temp_memory());

    for (int i = 0; i < 10; i++) {
        u8* space = (u8*)push_and_write(arena, 4_KB - sizeof(efj::memory_arena_chunk), 1);
        allocations.add(space);
    }

    for (auto* chunk = arena.tailChunk->next; ; chunk = chunk->next) {
        chunks.add(chunk->start);

        if (chunk == arena.tailChunk)
            break;
    }

    efj::free_arena(arena);

    for (u8* space : chunks) {
        gContext.expectingSegFault = true;
        if (!sigsetjmp(gContext.jumpBuf, true)) {
            *space = 10;
        }
    }

    for (u8* space : allocations) {
        gContext.expectingSegFault = true;
        if (!sigsetjmp(gContext.jumpBuf, true)) {
            *space = 10;
        }
    }

    ASSERT_EQ(allocations.size() + chunks.size(), (umm)gContext.segFaultCount);
}

UTEST(dynamic, allocation_too_big_for_first_chunk)
{
    efj_temp_scope();
    clear_context();

    auto arena = efj::allocate_arena("Test", 8_KB);
    arena.expand = &arena_expansion_counter;

    push_and_write(arena, 17111, 16);

    efj::free_arena(arena);
}

UTEST(dynamic, allocation_too_big_for_next_chunk)
{
    efj_temp_scope();
    clear_context();

    auto arena = efj::allocate_arena("Test", 8_KB);
    arena.expand = &arena_expansion_counter;

    push_and_write(arena, 16_KB, 16);
    ASSERT_EQ(1, gContext.expansionCount);

    efj_reset(arena);

    push_and_write(arena, 16_KB, 16);
    // 8KB -> 16KB and we reused the 16KB chunk.
    ASSERT_EQ(2, count_mapped_chunks(arena));

    efj_reset(arena);
    push_and_write(arena, 32_KB-sizeof(efj::memory_arena_chunk), 1);
    // 8KB -> 32KB and we freed the 16KB chunk.
    ASSERT_EQ(2, count_mapped_chunks(arena));
    ASSERT_EQ(32_KB-sizeof(efj::memory_arena_chunk), (umm)(arena.tailChunk->end - arena.tailChunk->start));

    efj::free_arena(arena);
}

UTEST(dynamic, recycle_sanity)
{
    auto arena = efj::allocate_arena("Test", 4_KB);

    efj::memory_arena_chunk* startCur  = nullptr;
    efj::memory_arena_chunk* startTail = nullptr;

    // Get some chunks, reset, and allocate chunks to get back to where we were.
    for (int count = 0; count < 16; count++) {
        for (int i = 0; i < 16; i++) {
            push_and_write(arena, 4_KB - sizeof(efj::memory_arena_chunk), 1);
        }

        if (count == 0) {
            startCur  = arena.curChunk;
            startTail = arena.tailChunk;
        }

        ASSERT_EQ(startCur, arena.curChunk);
        ASSERT_EQ(startTail, arena.tailChunk);

        ASSERT_EQ(16, count_mapped_chunks(arena));
        ASSERT_EQ(0, count_available_chunks(arena));
        ASSERT_EQ(arena.curChunk, arena.tailChunk);

        efj_reset(arena);

        ASSERT_EQ(arena.curChunk, arena.tailChunk->next);
        ASSERT_EQ(arena.tailChunk, arena.curChunk->prev);
        ASSERT_EQ(16, count_mapped_chunks(arena));
        ASSERT_EQ(15, count_available_chunks(arena));
    }

    efj::memory_arena_chunk* emptyChunk = efj::get_current_chunk(arena);

    efj::memory_arena_chunk* midChunk = nullptr;
    int recycledCountCount = 4;
    for (int i = 0; i < 8; i++) {
        // Recycling up to a mark is non-inclusive.
        if (i == recycledCountCount+1)
            midChunk = efj::get_current_chunk(arena);

        push_and_write(arena, 4_KB - sizeof(efj::memory_arena_chunk), 1);
    }

    ASSERT_EQ(16, count_mapped_chunks(arena));
    ASSERT_EQ(8, count_available_chunks(arena));

    efj::recycle_chunks(arena, emptyChunk);

    ASSERT_EQ(16, count_mapped_chunks(arena));
    ASSERT_EQ(8, count_available_chunks(arena));

    efj::recycle_chunks(arena, midChunk);

    ASSERT_EQ(16, count_mapped_chunks(arena));
    ASSERT_EQ(8 + recycledCountCount, count_available_chunks(arena));

    efj_reset(arena);
    ASSERT_EQ(16, count_mapped_chunks(arena));
    ASSERT_EQ(15, count_available_chunks(arena));

    // Soak test. Add different sizes that will ultimately accumulate and cause chunks to be
    // unmapped and replaced, while making sure we end up the the same chunk count at the end.
    auto* prevChunk = efj::get_current_chunk(arena);
    for (int i = 0; i < 100; i++) {
        push_and_write(arena, 4_KB - 128 + i, 16);
        prevChunk = efj::get_current_chunk(arena);
        efj::recycle_chunks(arena, prevChunk);
    }

    ASSERT_EQ(16, count_mapped_chunks(arena));
    ASSERT_EQ(15, count_available_chunks(arena));
}

int main(int argc, const char** argv)
{
    struct sigaction sa = {};
    sa.sa_handler = signal_handler;

    sigaction(SIGSEGV, &sa, nullptr);
    sigaction(SIGABRT, &sa, nullptr);

    return utest_main(argc, argv);
}

UTEST_STATE();

#define EFJ_C2_IMPLEMENTATION
#include <c2.hpp>

