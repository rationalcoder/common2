#include <stdio.h>
#include <c2.hpp>
#include <stdint.h>
#include <stdarg.h>
#include <stdlib.h>
#include <utest.h>

// IMPORTANT: Build/run with address sanitizer (CFLGAS += -fsanitize=address, LDFLAGS += -lasan).
// Ignore warnings about stb_sprintf for now.
//
// TODO list
// [ ] = todo
// [.] => in progress
// [x] => done
//
// [x] bit_ceil (computing next power of 2 for array capacity)
// [.] full API coverage
// [x] allocator where resize-in-place is implemented (memory_arena)
// [x] move/copy/assignment (with and without allocators begin the same)
// [x] non-trivial types

struct NonTrivialStats
{
    int ctor = 0;
    int copyCtor = 0;
    int moveCtor = 0;
    int dtor = 0;
    int assign = 0;
    int copyAssign = 0;
    int moveAssign = 0;

    void clear()
    {
        efj_placement_new(this) NonTrivialStats{};
    }

    bool operator == (const NonTrivialStats& other) const
    {
        return ctor == other.ctor && copyCtor == other.copyCtor && moveCtor == other.moveCtor
            && dtor == other.dtor && assign == other.assign && copyAssign == other.copyAssign
            && moveAssign == other.moveAssign;
    }
};

struct NonTrivialType
{
    NonTrivialStats& stats;
    int i = 0;

    NonTrivialType(int i, NonTrivialStats& stats) : stats(stats), i(i) { stats.ctor++; }
    NonTrivialType(const NonTrivialType& other) : stats(other.stats), i(other.i) { stats.copyCtor++; }
    NonTrivialType(NonTrivialType&& other) : stats(other.stats), i(other.i) { stats.moveCtor++; }
    ~NonTrivialType() { stats.dtor++; }
    NonTrivialType& operator = (const NonTrivialType& other) { stats.copyAssign++; i = other.i; return *this; }
    NonTrivialType& operator = (NonTrivialType&& other) { stats.moveAssign++; i = other.i; return *this; }

    bool operator == (const NonTrivialType& other) const { return i == other.i; }
    bool operator == (int i) const { return i == this->i; }
};


UTEST(array, exponential_growth)
{
    ASSERT_EQ(efj::bit_ceil(0), 1u);
    ASSERT_EQ(efj::bit_ceil(1), 1u);
    ASSERT_EQ(efj::bit_ceil(2), 2u);
    ASSERT_EQ(efj::bit_ceil(3), 4u);
    ASSERT_EQ(efj::bit_ceil(4), 4u);
    ASSERT_EQ(efj::bit_ceil(5), 8u);
    ASSERT_EQ(efj::bit_ceil(6), 8u);
    ASSERT_EQ(efj::bit_ceil(7), 8u);
    ASSERT_EQ(efj::bit_ceil(8), 8u);
    ASSERT_EQ(efj::bit_ceil(9), 16u);
    ASSERT_EQ(efj::bit_ceil(10), 16u);
    ASSERT_EQ(efj::bit_ceil(11), 16u);
    ASSERT_EQ(efj::bit_ceil(12), 16u);
    ASSERT_EQ(efj::bit_ceil(13), 16u);
    ASSERT_EQ(efj::bit_ceil(14), 16u);
    ASSERT_EQ(efj::bit_ceil(15), 16u);
    ASSERT_EQ(efj::bit_ceil(16), 16u);
    ASSERT_EQ(efj::bit_ceil(17), 32u);
    ASSERT_EQ(efj::bit_ceil(18), 32u);

    ASSERT_EQ(efj::bit_ceil(127), 128u);
    ASSERT_EQ(efj::bit_ceil(4000), 4096u);
}

UTEST(array, sanity)
{
    efj::array<int> stuff;
    for (int i = 0; i < 200; i++)
        stuff.add(i);

    for (int i = 0; i < 200; i++)
        ASSERT_EQ(stuff[i], i);

    efj::array<int> stuff2 = stuff;
    for (int i = 0; i < 200; i++)
        stuff2.add(i + 200);

    for (int i = 0; i < 400; i++)
        ASSERT_EQ(stuff2[i], i);

    int expected = 0;
    for (int i : stuff)
        ASSERT_EQ(i, expected++);

    expected = 0;
    for (int i : stuff2)
        ASSERT_EQ(i, expected++);

    ASSERT_EQ(stuff.front(), 0);
    ASSERT_EQ(stuff.back(), 199);

    ASSERT_EQ(stuff2.front(), 0);
    ASSERT_EQ(stuff2.back(), 399);
}

UTEST(array, sanity_with_temp_memory)
{
    efj::array<int> stuff{efj::alloc_temp};
    for (int i = 0; i < 200; i++)
        stuff.add(i);

    for (int i = 0; i < 200; i++)
        ASSERT_EQ(stuff[i], i);

    efj::array<int> stuff2 = stuff;
    for (int i = 0; i < 200; i++)
        stuff2.add(i + 200);

    for (int i = 0; i < 400; i++)
        ASSERT_EQ(stuff2[i], i);

    int expected = 0;
    for (int i : stuff)
        ASSERT_EQ(i, expected++);

    expected = 0;
    for (int i : stuff2)
        ASSERT_EQ(i, expected++);
}

UTEST(array, reserve)
{
    efj::array<int> stuff;
    stuff.reserve(20);
    ASSERT_EQ(stuff._cap, 32u);

    int* oldData = stuff.data();
    stuff.reserve(21);
    ASSERT_EQ(stuff._cap, 32u);
    ASSERT_EQ(oldData, stuff.data());

    stuff.reserve(32);
    ASSERT_EQ(stuff._cap, 32u);
    ASSERT_EQ(oldData, stuff.data());

    stuff.reserve(33);
    ASSERT_EQ(stuff._cap, 64u);
    ASSERT_NE(oldData, stuff.data());

    oldData = stuff.data();
    stuff.reserve(0);
    ASSERT_EQ(stuff._cap, 64u);
    ASSERT_EQ(oldData, stuff.data());

    for (int i = 0; i < 64; i++)
        stuff.add(i);

    ASSERT_EQ(stuff._cap, 64u);
    ASSERT_EQ(oldData, stuff.data());

    for (int i = 0; i < 64; i++)
        ASSERT_EQ(i, stuff[i]);

    stuff.add((int)stuff.size());

    ASSERT_EQ(stuff._cap, 128u);
    ASSERT_NE(oldData, stuff.data());
}

UTEST(array, reserve_non_trivial)
{
    NonTrivialStats stats;

    efj::array<NonTrivialType> stuff;
    stuff.reserve(20);
    ASSERT_EQ(stuff._cap, 32u);
    ASSERT_TRUE(stats == NonTrivialStats{});

    auto* oldData = stuff.data();
    stuff.reserve(21);
    ASSERT_EQ(stuff._cap, 32u);
    ASSERT_EQ(oldData, stuff.data());

    stuff.reserve(32);
    ASSERT_EQ(stuff._cap, 32u);
    ASSERT_EQ(oldData, stuff.data());

    // Expansion.
    stuff.reserve(33);
    ASSERT_EQ(stuff._cap, 64u);
    ASSERT_NE(oldData, stuff.data());
    ASSERT_TRUE(stats == NonTrivialStats{});

    oldData = stuff.data();
    stuff.reserve(0);
    ASSERT_EQ(stuff._cap, 64u);
    ASSERT_EQ(oldData, stuff.data());

    // We need to include the constructors/destructors we expect for temporaries in our counts.
    for (int i = 0; i < 64; i++)
        stuff.add({i, stats});
    ASSERT_EQ(stats.ctor, 64);
    ASSERT_EQ(stats.copyCtor, 0);
    ASSERT_EQ(stats.moveCtor, 64);
    ASSERT_EQ(stats.dtor, 64);

    ASSERT_EQ(stuff._cap, 64u);
    ASSERT_EQ(oldData, stuff.data());

    for (int i = 0; i < 64; i++)
        ASSERT_EQ(stuff[i].i, i);

    stats.clear();
    stuff.add({(int)stuff.size(), stats});
    ASSERT_EQ(stats.ctor, 1);
    ASSERT_EQ(stats.copyCtor, 0);
    ASSERT_EQ(stats.moveCtor, 65);
    ASSERT_EQ(stats.dtor, 65);

    ASSERT_EQ(stuff._cap, 128u);
    ASSERT_NE(oldData, stuff.data());

    stats.clear();
    stuff.reset();
    ASSERT_EQ(stats.dtor, 65);
}

UTEST(array, resize_in_place)
{
    // We should be able to resize arrays in place in arenas by default.
    efj::memory_arena arena = efj::allocate_arena("Test", 16_KB, 64_KB);

    auto* curChunk = efj::get_current_chunk(arena);

    efj::array<int> stuff{arena};
    stuff.add(stuff.size());
    int* prevData = stuff.data();
    for (int i = 0; i < 127; i++)
        stuff.add(stuff.size());

    ASSERT_EQ(stuff._cap, 128u);
    ASSERT_EQ(prevData, stuff.data());
    ASSERT_EQ(curChunk->at, (u8*)(stuff.data() + stuff._cap));
    for (int i = 0; i < 128; i++)
        ASSERT_EQ(i, stuff[i]);

    // If another allocation is in the way, resize_in_place should fail.
    char* bytes = (char*)efj_push_bytes(arena, 20);
    memset(bytes, 'l', 20);

    //efj::print("Expecting resize\n");
    for (int i = 0; i < 128; i++)
        stuff.add(stuff.size());

    ASSERT_EQ(stuff._cap, 256u);
    ASSERT_NE(prevData, stuff.data());
    //efj::print("Capacity: %zu, size: %zu\n", stuff._cap, stuff.size());

    // But after that, it should succeed again.

    // Add one to cause a resize.
    prevData = stuff.data();
    // New allocation of size 512.
    //efj::print("Expecting resize: %zu, %zu\n", stuff.size(), stuff._cap);
    stuff.add(0);
    ASSERT_EQ(prevData, stuff.data());

    prevData = stuff.data();

    // Cause a resize to 1024.
    for (size_t i = 0; i < 256; i++)
        stuff.add(stuff.size());

    //efj::print("Capacity: %zu, size: %zu\n", stuff._cap, stuff.size());
    ASSERT_EQ(stuff._cap, 1024u);
    ASSERT_EQ(prevData, stuff.data());

    // Make sure our little allocation didn't get overwritten.
    for (int i = 0; i < 20; i++)
        ASSERT_EQ(bytes[i], 'l');

    efj_push_bytes(arena, 64_KB);
}

UTEST(array, resize_in_place_non_trivial)
{
    NonTrivialStats stats;

    // We should be able to resize arrays in place in arenas by default.
    // NOTE: that we have to change the chunk size here to account for how much larger
    // the non-trivial type is. Otherwise, we would get failures to resize in place when expected.
    // This won't be necessary if/when we simplify the fixed arena allocation to just be one
    // fixed-sized chunk.
    //
    efj::memory_arena arena = efj::allocate_arena("Test", 32_KB, 32_KB);

    auto* curChunk = efj::get_current_chunk(arena);

    efj::array<NonTrivialType> stuff{arena};
    stuff.add({(int)stuff.size(), stats});
    ASSERT_EQ(stats.ctor, 1);
    ASSERT_EQ(stats.copyCtor, 0);
    ASSERT_EQ(stats.moveCtor, 1);
    ASSERT_EQ(stats.dtor, 1);

    auto* prevData = stuff.data();
    for (int i = 0; i < 127; i++)
        stuff.add({(int)stuff.size(), stats});
    ASSERT_EQ(stats.ctor, 128);
    ASSERT_EQ(stats.dtor, 128);
    ASSERT_EQ(stats.copyCtor, 0);
    ASSERT_EQ(stats.moveCtor, 128);

    ASSERT_EQ(stuff._cap, 128u);
    ASSERT_EQ(prevData, stuff.data());
    ASSERT_EQ(curChunk->at, (u8*)(stuff.data() + stuff._cap));
    for (int i = 0; i < 128; i++)
        ASSERT_EQ(i, stuff[i].i);

    // If another allocation is in the way, resize_in_place should fail.
    char* bytes = (char*)efj_push_bytes(arena, 20);
    memset(bytes, 'l', 20);

    //efj::print("Expecting resize\n");
    stats.clear();
    for (int i = 0; i < 128; i++)
        stuff.add({(int)stuff.size(), stats});

    ASSERT_EQ(stats.ctor, 128); // Temporaries constructed.
    ASSERT_EQ(stats.dtor, 256); // Temporaries destructed + elements moved.
    ASSERT_EQ(stats.copyCtor, 0); // Temporaries copied into the array.
    ASSERT_EQ(stats.moveCtor, 256);

    ASSERT_EQ(stuff._cap, 256u);
    ASSERT_NE(prevData, stuff.data());
    //efj::print("Capacity: %zu, size: %zu\n", stuff._cap, stuff.size());

    // But after that, it should succeed again.

    // Add one to cause a resize.
    prevData = stuff.data();
    // New allocation of size 512.
    //efj::print("Expecting resize: %zu, %zu\n", stuff.size(), stuff._cap);
    stuff.add({0, stats});
    ASSERT_EQ(prevData, stuff.data());

    prevData = stuff.data();

    // Cause a resize to 1024.
    for (size_t i = 0; i < 256; i++)
        stuff.add({(int)stuff.size(), stats});

    //efj::print("Capacity: %zu, size: %zu\n", stuff._cap, stuff.size());
    ASSERT_EQ(stuff._cap, 1024u);
    ASSERT_EQ(prevData, stuff.data());

    // Make sure our little allocation didn't get overwritten.
    for (int i = 0; i < 20; i++)
        ASSERT_EQ(bytes[i], 'l');

    ssize_t finalSize = stuff.ssize();
    stats.clear();
    stuff.reset();

    ASSERT_EQ(stats.dtor, finalSize);
}

UTEST(array, resize_in_place_with_reserve)
{
    // We should be able to resize arrays in place in arenas by default.
    efj::memory_arena arena = efj::allocate_arena("Test", 16_KB, 16_KB);

    auto* curChunk = efj::get_current_chunk(arena);

    efj::array<int> stuff{arena};
    stuff.reserve(1);
    int* prevData = stuff.data();
    stuff.reserve(128);

    ASSERT_EQ(stuff._cap, 128u);
    ASSERT_EQ(prevData, stuff.data());
    ASSERT_EQ(curChunk->at, (u8*)(stuff.data() + stuff._cap));

    // If another allocation is in the way, resize_in_place should fail.
    char* bytes = (char*)efj_push_bytes(arena, 20);
    memset(bytes, 'l', 20);

    //efj::print("Expecting resize\n");
    stuff.reserve(256);

    ASSERT_EQ(stuff._cap, 256u);
    ASSERT_NE(prevData, stuff.data());
    //efj::print("Capacity: %zu, size: %zu\n", stuff._cap, stuff.size());

    // But after that, it should succeed again.

    // Add one to cause a resize.
    prevData = stuff.data();
    // New allocation of size 512.
    //efj::print("Expecting resize: %zu, %zu\n", stuff.size(), stuff._cap);
    stuff.reserve(257);
    ASSERT_EQ(prevData, stuff.data());

    prevData = stuff.data();

    // Cause a resize to 1024.
    stuff.reserve(513);

    //efj::print("Capacity: %zu, size: %zu\n", stuff._cap, stuff.size());
    ASSERT_EQ(stuff._cap, 1024u);
    ASSERT_EQ(prevData, stuff.data());

    // Make sure our little allocation didn't get overwritten.
    for (int i = 0; i < 20; i++)
        ASSERT_EQ(bytes[i], 'l');
}

UTEST(array, copy_constructor)
{
    efj::array<int> stuff1;
    for (int i = 0; i < 100; i++)
        stuff1.add(i);

    efj::array<int> stuff2{stuff1};
    ASSERT_TRUE(stuff1._alloc == stuff2._alloc);
    ASSERT_EQ(stuff2._cap, stuff1._size);
    ASSERT_EQ(stuff2._size, stuff1._size);
    ASSERT_NE(stuff2._data, stuff1._data);

    for (int i = 0; i < 100; i++)
        ASSERT_EQ(stuff1[i], i);

    for (int i = 0; i < 100; i++)
        ASSERT_EQ(stuff2[i], i);
}

UTEST(array, copy_constructor_non_trivial)
{
    NonTrivialStats stats;

    {
        efj::array<NonTrivialType> stuff1;
        for (int i = 0; i < 100; i++)
            stuff1.add({i, stats});

        stats.clear();
        efj::array<NonTrivialType> stuff2{stuff1};
        ASSERT_TRUE(stuff1._alloc == stuff2._alloc);
        ASSERT_EQ(stuff2._cap, stuff1._size);
        ASSERT_EQ(stuff2._size, stuff1._size);
        ASSERT_NE(stuff2._data, stuff1._data);

        for (int i = 0; i < 100; i++)
            ASSERT_EQ(stuff1[i].i, i);

        for (int i = 0; i < 100; i++)
            ASSERT_EQ(stuff2[i].i, i);
    }

    ASSERT_EQ(stats.copyCtor, 100);
    ASSERT_EQ(stats.dtor, 200);
}

UTEST(array, copy_assignment)
{
    efj::array<int> stuff1;
    for (int i = 0; i < 100; i++)
        stuff1.add(i);

    efj::array<int> stuff2;
    stuff2.add(0);

    stuff2 = stuff1;
    ASSERT_TRUE(stuff1._alloc == stuff2._alloc);
    ASSERT_EQ(stuff2._cap, stuff1._size);
    ASSERT_EQ(stuff2._size, stuff1._size);
    ASSERT_NE(stuff2._data, stuff1._data);

    for (int i = 0; i < 100; i++)
        ASSERT_EQ(stuff1[i], i);

    for (int i = 0; i < 100; i++)
        ASSERT_EQ(stuff2[i], i);
}

UTEST(array, copy_assignment_non_trivial)
{
    NonTrivialStats stats;

    {
        efj::array<NonTrivialType> stuff1;
        for (int i = 0; i < 100; i++)
            stuff1.add({i, stats});

        stats.clear();
        efj::array<NonTrivialType> stuff2;
        stuff2.add({0, stats});

        stuff2 = stuff1;
        ASSERT_TRUE(stuff1._alloc == stuff2._alloc);
        ASSERT_EQ(stuff2._cap, stuff1._size);
        ASSERT_EQ(stuff2._size, stuff1._size);
        ASSERT_NE(stuff2._data, stuff1._data);

        for (int i = 0; i < 100; i++)
            ASSERT_EQ(stuff1[i].i, i);

        for (int i = 0; i < 100; i++)
            ASSERT_EQ(stuff2[i].i, i);
    }

    ASSERT_EQ(stats.copyCtor, 100);
    ASSERT_EQ(stats.moveCtor, 1);
    ASSERT_EQ(stats.dtor, 200 + 1 + 1);
}

UTEST(array, move_constructor)
{
    {
        // Move with same allocator.
        efj::array<int> stuff1;
        for (int i = 0; i < 100; i++)
            stuff1.add(i);

        int* movedData = stuff1.data();
        size_t prevCap = stuff1._cap;
        size_t prevSize = stuff1.size();

        efj::array<int> stuff2{efj::move(stuff1)};
        ASSERT_EQ(stuff1._data, nullptr);
        ASSERT_EQ(stuff1._size, 0u);
        ASSERT_EQ(stuff2._data, movedData);
        ASSERT_EQ(stuff2._cap,  prevCap);
        ASSERT_EQ(stuff2._size, prevSize);
    }

    // Move with same allocator implicitly.
    efj::array<int> stuff1{efj::alloc_temp};
    for (int i = 0; i < 100; i++)
        stuff1.add(i);

    efj::allocator prevAllocator = stuff1._alloc;
    int* movedData = stuff1.data();
    size_t prevCap = stuff1._cap;
    size_t prevSize = stuff1.size();

    efj::array<int> stuff2{efj::move(stuff1)};
    ASSERT_EQ(stuff1._data, nullptr);
    ASSERT_EQ(stuff1._size, 0u);
    ASSERT_EQ(stuff2._data, movedData);
    ASSERT_EQ(stuff2._cap,  prevCap);
    ASSERT_EQ(stuff2._size, prevSize);
    ASSERT_TRUE(prevAllocator == stuff2._alloc);

    for (int i = 0; i < 100; i++)
        ASSERT_EQ(stuff2[i], i);
}

UTEST(array, move_constructor_non_trivial)
{
    NonTrivialStats stats;
    {
        // Move with same allocator.
        efj::array<NonTrivialType> stuff1;
        for (int i = 0; i < 100; i++)
            stuff1.add({i, stats});

        stats.clear();
        auto* movedData = stuff1.data();
        size_t prevCap = stuff1._cap;
        size_t prevSize = stuff1.size();

        efj::array<NonTrivialType> stuff2{efj::move(stuff1)};
        ASSERT_EQ(stuff1._data, nullptr);
        ASSERT_EQ(stuff1._size, 0u);
        ASSERT_EQ(stuff2._data, movedData);
        ASSERT_EQ(stuff2._cap,  prevCap);
        ASSERT_EQ(stuff2._size, prevSize);
    }

    ASSERT_EQ(stats.ctor, 0);
    ASSERT_EQ(stats.copyCtor, 0);
    ASSERT_EQ(stats.moveCtor, 0);
    ASSERT_EQ(stats.dtor, 100);

    // Move with same allocator implicitly.
    efj::array<NonTrivialType> stuff1{efj::alloc_temp};
    for (int i = 0; i < 100; i++)
        stuff1.add({i, stats});

    efj::allocator prevAllocator = stuff1._alloc;
    auto* movedData = stuff1.data();
    size_t prevCap = stuff1._cap;
    size_t prevSize = stuff1.size();

    stats.clear();
    efj::array<NonTrivialType> stuff2{efj::move(stuff1)};
    ASSERT_EQ(stuff1._data, nullptr);
    ASSERT_EQ(stuff1._size, 0u);
    ASSERT_EQ(stuff2._data, movedData);
    ASSERT_EQ(stuff2._cap,  prevCap);
    ASSERT_EQ(stuff2._size, prevSize);
    ASSERT_TRUE(prevAllocator == stuff2._alloc);

    for (int i = 0; i < 100; i++)
        ASSERT_EQ(stuff2[i].i, i);

    ASSERT_EQ(stats.ctor, 0);
    ASSERT_EQ(stats.copyCtor, 0);
    ASSERT_EQ(stats.moveCtor, 0);
    ASSERT_EQ(stats.dtor, 0);
}

UTEST(array, move_assignment)
{
    // Should copy b/c the allocators are different.
    {
        efj::array<int> stuff2;
        efj::allocator prevAllocator = {};
        int* movedData = nullptr;
        size_t prevSize = 0;
        {
            efj::array<int> stuff1{efj::alloc_temp};
            for (int i = 0; i < 100; i++)
                stuff1.add(i);

            prevAllocator = stuff1._alloc;
            movedData = stuff1.data();
            prevSize = stuff1.size();

            stuff2 = efj::move(stuff1);
        }

        ASSERT_NE(stuff2._data, movedData);
        // We compare capacity to the previous size on purpose.
        ASSERT_EQ(stuff2._cap,  prevSize);
        ASSERT_EQ(stuff2._size, prevSize);
        ASSERT_TRUE(prevAllocator != stuff2._alloc);

        for (int i = 0; i < 100; i++)
            ASSERT_EQ(stuff2[i], i);
    }

    // Should move b/c the allocators are the same.
    efj::array<int> stuff2;
    efj::allocator prevAllocator = {};
    int* movedData = nullptr;
    size_t prevSize = 0;
    size_t prevCap = 0;
    {
        efj::array<int> stuff1;
        for (int i = 0; i < 100; i++)
            stuff1.add(i);

        prevAllocator = stuff1._alloc;
        movedData = stuff1.data();
        prevSize = stuff1.size();
        prevCap = stuff1._cap;

        stuff2 = efj::move(stuff1);
    }

    ASSERT_EQ(stuff2._data, movedData);
    ASSERT_EQ(stuff2._cap,  prevCap);
    ASSERT_EQ(stuff2._size, prevSize);
    ASSERT_TRUE(prevAllocator == stuff2._alloc);

    for (int i = 0; i < 100; i++)
        ASSERT_EQ(stuff2[i], i);
}

UTEST(array, move_assignment_non_trivial)
{
    NonTrivialStats stats;

    // Should copy b/c the allocators are different.
    {
        efj::array<NonTrivialType> stuff2;
        efj::allocator prevAllocator = {};
        NonTrivialType* movedData = nullptr;
        size_t prevSize = 0;
        {
            efj::array<NonTrivialType> stuff1{efj::alloc_temp};
            for (int i = 0; i < 100; i++)
                stuff1.add({i, stats});

            stats.clear();
            prevAllocator = stuff1._alloc;
            movedData = stuff1.data();
            prevSize = stuff1.size();

            stuff2 = efj::move(stuff1);
        }

        ASSERT_NE(stuff2._data, movedData);
        // We compare capacity to the previous size on purpose.
        ASSERT_EQ(stuff2._cap,  prevSize);
        ASSERT_EQ(stuff2._size, prevSize);
        ASSERT_TRUE(prevAllocator != stuff2._alloc);

        for (int i = 0; i < 100; i++)
            ASSERT_EQ(stuff2[i].i, i);

        ASSERT_EQ(stats.ctor, 0);
        ASSERT_EQ(stats.copyCtor, 100);
        ASSERT_EQ(stats.moveCtor, 0);
        ASSERT_EQ(stats.dtor, 100);
    }

    // Should move b/c the allocators are the same.
    efj::array<NonTrivialType> stuff2;
    efj::allocator prevAllocator = {};
    NonTrivialType* movedData = nullptr;
    size_t prevSize = 0;
    size_t prevCap = 0;
    {
        efj::array<NonTrivialType> stuff1;
        for (int i = 0; i < 100; i++)
            stuff1.add({i, stats});

        stats.clear();
        prevAllocator = stuff1._alloc;
        movedData = stuff1.data();
        prevSize = stuff1.size();
        prevCap = stuff1._cap;

        stuff2 = efj::move(stuff1);
    }

    ASSERT_EQ(stuff2._data, movedData);
    ASSERT_EQ(stuff2._cap,  prevCap);
    ASSERT_EQ(stuff2._size, prevSize);
    ASSERT_TRUE(prevAllocator == stuff2._alloc);

    for (int i = 0; i < 100; i++)
        ASSERT_EQ(stuff2[i].i, i);

    ASSERT_EQ(stats.ctor, 0);
    ASSERT_EQ(stats.copyCtor, 0);
    ASSERT_EQ(stats.moveCtor, 0);
    ASSERT_EQ(stats.dtor, 0);
}

UTEST(array, remove)
{
    efj::array<int> stuff;
    for (int i = 0; i < 100; i++)
        stuff.add(i);

    stuff.remove_nth(50);
    ASSERT_EQ(stuff.ssize(), 99);

    for (int i = 0; i < 50; i++)
        ASSERT_EQ(stuff[i], i);

    for (int i = 50; i < 99; i++)
        ASSERT_EQ(stuff[i], i + 1);

    stuff.remove_nth_unordered(50);
    ASSERT_EQ(stuff.ssize(), 98);

    for (int i = 0; i < 50; i++)
        ASSERT_EQ(stuff[i], i);

    ASSERT_EQ(stuff[50], 99);

    for (int i = 51; i < 98; i++)
        ASSERT_EQ(stuff[i], i + 1);

    stuff.pop();
    ASSERT_EQ(stuff.ssize(), 97);
    ASSERT_EQ(stuff.back(), 97);

    bool found = stuff.remove(3);
    ASSERT_TRUE(found);
    ASSERT_EQ(stuff.ssize(), 96);
    ASSERT_EQ(stuff.back(), 97);

    found = stuff.remove(100);
    ASSERT_EQ(stuff.ssize(), 96);
    ASSERT_FALSE(found);
}

UTEST(array, remove_non_trivial)
{
    NonTrivialStats stats;
    efj::array<NonTrivialType> stuff;
    for (int i = 0; i < 100; i++)
        stuff.add({i, stats});

    stats.clear();
    stuff.remove_nth(50);
    ASSERT_EQ(stuff.ssize(), 99);
    ASSERT_EQ(stats.moveAssign, 49);
    ASSERT_EQ(stats.dtor, 1);

    for (int i = 0; i < 50; i++)
        ASSERT_EQ(stuff[i].i, i);

    for (int i = 50; i < 99; i++)
        ASSERT_EQ(stuff[i].i, i + 1);

    stats.clear();
    stuff.remove_nth_unordered(50);
    ASSERT_EQ(stuff.ssize(), 98);
    ASSERT_EQ(stats.moveAssign, 1);
    ASSERT_EQ(stats.dtor, 1);

    for (int i = 0; i < 50; i++)
        ASSERT_EQ(stuff[i].i, i);

    ASSERT_EQ(stuff[50].i, 99);

    for (int i = 51; i < 98; i++)
        ASSERT_EQ(stuff[i].i, i + 1);

    stats.clear();
    stuff.pop();
    ASSERT_EQ(stuff.ssize(), 97);
    ASSERT_EQ(stuff.back().i, 97);
    ASSERT_EQ(stats.moveAssign, 0);
    ASSERT_EQ(stats.dtor, 1);

    stats.clear();
    bool found = stuff.remove(3);
    ASSERT_TRUE(found);
    ASSERT_EQ(stuff.ssize(), 96);
    ASSERT_EQ(stuff.back().i, 97);
    ASSERT_EQ(stats.moveAssign, 93);
    ASSERT_EQ(stats.dtor, 1);

    found = stuff.remove(100);
    ASSERT_EQ(stuff.ssize(), 96);
    ASSERT_FALSE(found);
}

int main(int argc, char** argv)
{
    return utest_main(argc, argv);
}

UTEST_STATE();

#define EFJ_C2_IMPLEMENTATION
#include <c2.hpp>

