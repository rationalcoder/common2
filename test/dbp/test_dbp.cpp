#include <utest.h>
#include <c2.hpp>

int main(int argc, const char** argv)
{
    // TODO: Write a fuzzer and check to see that all tokens returned by parsers accumulate
    // to match the data read exactly.

    return utest_main(argc, argv);
}

UTEST_STATE();

#define EFJ_C2_IMPLEMENTATION
#include <c2.hpp>
