namespace efj
{

static efj::xml_element*
add_element(efj::xml_element* parent, const yxml_t& yxml)
{
    auto* elem = efj_new(efj::xml_element);
    elem->_parent = parent;
    elem->_name   = efj::copy_string(yxml.elem, yxml_symlen((yxml_t*)&yxml, yxml.elem));

    if (parent) {
        elem->_next = parent->_children;
        parent->_children = elem;
        parent->_childrenCount++;
    }

    return elem;
}

static efj::xml_attribute*
add_attribute(efj::xml_element* e, const yxml_t& yxml)
{
    auto* attr = efj_new(efj::xml_attribute);
    attr->_name = efj::copy_string(yxml.attr, yxml_symlen((yxml_t*)&yxml, yxml.attr));

    attr->_next = e->_attribs;
    e->_attribs = attr;

    e->_attribCount++;
    return attr;
}

template <typename T_>
struct stable_key
{
    T_* key;
    umm srcIndex;
};

static void
sort_children(efj::xml_element* elem)
{
    using xml_element_key = stable_key<xml_element>;

    if (elem->_childrenCount == 0)
        return;

    efj_temp_scope();

    auto* stableKeys = efj_push_array(efj::temp_memory(), elem->_childrenCount, xml_element_key);

    auto* child = elem->_children;
    for (umm i = 0; i < elem->_childrenCount; i++, child = child->_next) {
        stableKeys[i].key      = child;
        stableKeys[i].srcIndex = i;
    }

    qsort(stableKeys, elem->_childrenCount, sizeof(stableKeys[0]), [](const void* lhs, const void* rhs) -> int {
        auto* key1 = (xml_element_key*)lhs;
        auto* key2 = (xml_element_key*)rhs;

        int cmp = strcmp(key1->key->_name.c_str(), key2->key->_name.c_str());
        if (cmp != 0) return cmp;

        return key1->srcIndex < key2->srcIndex ? -1 : 1;
    });

    elem->_childrenSorted = efj_allocate_array(elem->_childrenCount, efj::xml_element*);
    for (umm i = 0; i < elem->_childrenCount; i++) {
        auto* e = stableKeys[i].key;
        elem->_childrenSorted[i] = e;
        e->_sortedIndex          = i;
    }
}

static void
sort_attributes(efj::xml_element* elem)
{
    using xml_attribute_key = stable_key<xml_attribute>;

    if (elem->_attribCount == 0)
        return;

    efj_temp_scope();

    auto* stableKeys = efj_push_array(efj::temp_memory(), elem->_attribCount, xml_attribute_key);

    auto* attr = elem->_attribs;
    for (umm i = 0; i < elem->_attribCount; i++, attr = attr->_next) {
        stableKeys[i].key      = attr;
        stableKeys[i].srcIndex = i;
    }

    qsort(stableKeys, elem->_attribCount, sizeof(stableKeys[0]), [](const void* lhs, const void* rhs) -> int {
        auto* key1 = (xml_attribute_key*)lhs;
        auto* key2 = (xml_attribute_key*)rhs;

        int cmp = strcmp(key1->key->_name.c_str(), key2->key->_name.c_str());
        if (cmp != 0) return cmp;

        return key1->srcIndex < key2->srcIndex ? -1 : 1;
    });

    elem->_attribsSorted = efj_allocate_array(elem->_attribCount, efj::xml_attribute*);
    for (umm i = 0; i < elem->_attribCount; i++) {
        auto* a = stableKeys[i].key;
        elem->_attribsSorted[i] = a;
    }
}

static efj::string
make_error(const yxml_t& state, const efj::string& msg)
{
    return efj::fmt("%u:%u: %s", state.line, (u32)state.byte-1, msg.c_str());
}

extern efj::parsed_xml_file
parse_xml_file(const efj::buffer& buf)
{
    efj_temp_scope();

    parsed_xml_file result;

    constexpr umm kBufferSize = 8_KB;

    yxml_t yxml;
    char*  yxmlBuffer = (char*)efj_push_bytes(efj::temp_memory(), kBufferSize);
    yxml_init(&yxml, yxmlBuffer, kBufferSize);

    yxml_ret_t yxmlStatus = YXML_OK;

    xml_element*   parent = nullptr;
    xml_element*   elem   = nullptr;
    xml_attribute* attr   = nullptr;

    efj::string_builder elemContent(efj::alloc_temp);
    efj::string_builder attrValue(efj::alloc_temp);

    // TODO: string_builder

    for (umm i = 0; i < buf.size; i++) {
        auto ret = yxml_parse(&yxml, buf.data[i]);
        if (ret < 0) {
            yxmlStatus = ret;
            break;
        }

        switch (ret) {
        case YXML_OK: continue;
        case YXML_ELEMSTART:
            // WARNING: To avoid having a string builder for each element, we assume elements that have children don't
            // have meaningful content, so we can just clear the current content and use the builder for the child.
            // This behavior only makes sense for xml config files that we are used to using for config.
            elemContent.clear();

            elem = add_element(parent, yxml);
            elem->_parent = parent;
            //printf("Setting parent of %s = %s\n", elem->_name.c_str(), parent ? parent->_name.c_str() : NULL);
            parent = elem;

            //printf("Push parent %s\n", parent ? parent->_name.c_str() : NULL);

            if (!result.doc._root)
                result.doc._root = elem;

            break;
        case YXML_ATTRSTART:
            attr = add_attribute(elem, yxml);
            break;
        case YXML_ATTRVAL:
            attrValue.append(yxml.data);
            break;
        case YXML_ATTREND:
            attr->_value = attrValue.dup();
            attrValue.clear();
            break;
        case YXML_CONTENT:
            elemContent.append(yxml.data);
            break;
        case YXML_ELEMEND:
            elem->_content = elemContent.dup();
            elemContent.clear();

            efj_reverse_list(&elem->_attribs, _next);
            efj_reverse_list(&elem->_children, _next);

            sort_children(elem);
            sort_attributes(elem);

            parent = elem->_parent;
            elem   = elem->_parent;
            break;
        default: break;
        }
    }

    if (yxmlStatus != YXML_OK) {
        switch (yxmlStatus) {
        case YXML_ESTACK: result.error = make_error(yxml, "file too big").dup(); break;
        case YXML_EREF:   result.error = make_error(yxml, "invalid character reference").dup(); break;
        case YXML_ECLOSE: result.error = make_error(yxml, efj::fmt("expected closing tag for '%s'", yxml.elem)).dup(); break;
        case YXML_ESYN:   result.error = make_error(yxml, "syntax error").dup(); break;
        default:          result.error = make_error(yxml, "unknown error").dup(); break;
        }

        return result;
    }

    yxmlStatus = yxml_eof(&yxml);
    if (yxmlStatus != YXML_OK) {
        result.error = make_error(yxml, "unexpected end-of-file").dup();
        return result;
    }

    return result;
}

} // namespace efj

