namespace efj
{

inline
string::string(const char* s)
{
    efj_assert(s);

    umm size = 0;
    while (s[size] != '\0')
        size++;

    char* data = (char*)efj_push_bytes(efj::temp_memory(), size + 1);
    ::memcpy(data, s, size + 1);

    this->data = data;
    this->size = size;
}

inline bool
string::operator == (const efj::string& rhs) const
{
    if (size != rhs.size)
        return false;

    for (umm i = 0; i < size; i++) {
        if (data[i] != rhs.data[i])
            return false;
    }

    return true;
}

inline bool
string::operator != (const efj::string& rhs) const
{
    return !(*this == rhs);
}

inline bool string::starts_with(const char* s) const { return size && strncmp(data, s, strlen(s)) == 0; }
inline bool string::starts_with(char c) const { return size && data[0] == c; }

inline efj::string
string::dup(efj::allocator alloc) const
{
    char* space = (char*)efj_allocator_alloc(alloc, capacity(), 1);
    ::memcpy(space, data, capacity());

    return efj::string::shallow_copy(space, size);
}

inline efj::string
string::dup(efj::memory_arena& arena) const
{
    char* space = (char*)efj_push_bytes(arena, capacity());
    ::memcpy(space, data, capacity());

    return efj::string::shallow_copy(space, size);
}

inline efj::string
copy_string(const char* text, umm size)
{
    char* space = (char*)efj_allocate_bytes(size + 1);
    ::memcpy(space, text, size);
    space[size] = '\0';

    return efj::string::shallow_copy(space, size);
}

inline efj::string
copy_string(const char* text, umm size, efj::memory_arena& arena)
{
    char* space = (char*)efj_push_bytes(arena, size + 1);
    ::memcpy(space, text, size);
    space[size] = '\0';

    return efj::string::shallow_copy(space, size);
}

// Copy to fixed buffer. Always nul-terminates, event if truncated.
template <umm SizeV> inline efj::string
copy_string(const efj::string& s, char(&buf)[SizeV])
{
    static_assert(SizeV > 0, "output buffer too small");

    // The string size already doesn't include the nul.
    const umm minSize = s.size < SizeV-1 ? s.size : SizeV-1;

    for (umm i = 0; i < minSize; i++)
        buf[i] = s[i];

    buf[minSize] = '\0';
    return efj::string::shallow_copy(buf, SizeV);
}

inline efj::string
temp_string(const char* text, umm size)
{
    char* space = (char*)efj_push_bytes(efj::temp_memory(), size + 1);
    ::memcpy(space, text, size);
    space[size] = '\0';

    return efj::string::shallow_copy(space, size);
}

//{ string_builder

// @Incomplete
inline
string_builder::string_builder(efj::allocator alloc)
{
    _alloc      = alloc;
    _nChunks    = 1;
    _tailFilled = 0;
    _head.next  = nullptr;
    _tail       = &_head;
    _freeList   = {};
}

inline
string_builder::string_builder()
{
    efj_placement_new(this) string_builder(efj::current_allocator());
}

inline
string_builder::string_builder(const efj::string& s)
{
    efj_placement_new(this) string_builder(efj::alloc_temp);
    append(s);
}

inline
string_builder::~string_builder()
{
    if (_alloc.has_free()) {
        for (auto* chunk = &_head; chunk; ) {
            auto* next = chunk->next;
            _alloc.free(chunk);

            chunk = next;
        }
    }
}

inline void
string_builder::append(const char* s)
{
    u32 size = (u32)strlen(s);
    u32 progress = 0;

    for (;;) {
        const u32 space = efj::string_chunk_size - _tailFilled;
        const u32 dataRemaining = size - progress;
        const u32 maxFromThisChunk = space < dataRemaining ? space : dataRemaining;
        memcpy(_tail->data + _tailFilled, s + progress, maxFromThisChunk);

        progress += maxFromThisChunk;

        if (progress == size) {
            _tailFilled += maxFromThisChunk;
            break;
        }

        // We've exhausted this chunk, and we need a new one.
        auto* next   = _freeList.get_or_allocate<efj::string_chunk>(_alloc);
        next->next   = nullptr;
        _tail->next  = next;
        _tail        = next;
        _tailFilled  = 0;
        _nChunks    += 1;
    }
}

inline void
string_builder::append(const efj::string& s)
{
    u32 progress = 0;

    for (;;) {
        const u32 space = efj::string_chunk_size - _tailFilled;
        const u32 dataRemaining = s.size - progress;
        const u32 maxFromThisChunk = space < dataRemaining ? space : dataRemaining;
        memcpy(_tail->data + _tailFilled, s.data + progress, maxFromThisChunk);

        progress += maxFromThisChunk;

        if (progress == s.size) {
            _tailFilled += maxFromThisChunk;
            break;
        }

        // We've exhausted this chunk, and we need a new one.
        auto* next   = _freeList.get_or_allocate<efj::string_chunk>(_alloc);
        next->next   = nullptr;
        _tail->next  = next;
        _tail        = next;
        _tailFilled  = 0;
        _nChunks    += 1;
    }
}

inline void
string_builder::append_fmt(const char* fmt, ...)
{
    va_list va;
    va_start(va, fmt);

    append(efj::vfmt(fmt, va));

    va_end(va);
}

inline void
string_builder::set(const efj::string& s)
{
    clear();
    append(s);
}

inline void
string_builder::set(const char* s)
{
    clear();
    append(s);
}

inline efj::string
string_builder::str() const
{
    umm copyAmount = size();
    umm filled     = 0;

    char* space = (char*)efj_push_bytes(efj::temp_memory(), copyAmount + 1);

    // The last one is the only one that can be partially full.
    for (auto* cur = &_head; cur; cur = cur->next) {
        umm size = cur->next ? efj::string_chunk_size : _tailFilled;
        memcpy(space + filled, cur->data, size);
        filled += efj::string_chunk_size;
    }

    space[copyAmount] = '\0';
    return efj::string::shallow_copy(space, copyAmount);
}

inline efj::string
string_builder::dup(efj::allocator alloc) const
{
    umm copyAmount = size();
    umm filled     = 0;

    char* space = (char*)efj_allocator_alloc(alloc, copyAmount + 1, 1);

    // The last one is the only one that can be partially full.
    for (auto* cur = &_head; cur; cur = cur->next) {
        umm size = cur->next ? efj::string_chunk_size : _tailFilled;
        memcpy(space + filled, cur->data, size);
        filled += efj::string_chunk_size;
    }

    space[copyAmount] = '\0';
    return efj::string::shallow_copy(space, copyAmount);
}

inline efj::string
string_builder::dup(efj::memory_arena& arena) const
{
    umm copyAmount = size();
    umm filled     = 0;

    char* space = (char*)efj_push_bytes(arena, copyAmount + 1);

    // The last one is the only one that can be partially full.
    for (auto* cur = &_head; cur; cur = cur->next) {
        umm size = cur->next ? efj::string_chunk_size : _tailFilled;
        memcpy(space + filled, cur->data, size);
        filled += efj::string_chunk_size;
    }

    space[copyAmount] = '\0';
    return efj::string::shallow_copy(space, copyAmount);
}

inline void
string_builder::clear()
{
    for (auto* cur = _head.next; cur; cur = cur->next) {
        _freeList.add(cur);
    }

    _head.next  = nullptr;
    _tail       = &_head;
    _tailFilled = 0;
    _nChunks    = 1;
}

//}

} // end namespace efj
