namespace efj
{

//{ app

inline void
_add_object(efj::object* o)
{
    efj::app()._add_object(o);
}

template <typename Object_> inline void
set_logger(Object_& o)
{
    // FIXME: not implemented since p/c refactor.
    efj_not_implemented();
    //static_assert(efj::is_consumer_of<Object_, const efj::log_buffer>::value,
    //    "Logger object must be an EFJ_OBJECT and a EFJ_CONSUMER(efj::log_buffer)");

    //efj::app()._logConfig.object = &o.efj_object;
    //efj::app()._logConfig.cb     = efj::consume_call_wrapper<Object_, efj::log_buffer>::consume;
}

//}

//{ thread

template <typename... Objects_> inline void
thread::add_objects(Objects_&... objects)
{
    using Expand = int[];
    Expand{(add_object(&objects.efj_object), 0)...};
}

inline void
thread::add_object(efj::object* o)
{
    objects.add(o);
}

inline void
thread::add_cpu_affinity(u32 cpu)
{
    cpus.add(cpu);
}

inline void
thread::set_priority(int policy, int pri)
{
    this->schedPolicy = policy;
    this->priority    = pri;
}

inline void
thread::use_for_event_scheduling(bool use)
{
    if (use) tasks |=  efj::thread_task_event_scheduling;
    else     tasks &= ~efj::thread_task_event_scheduling;
}

inline bool
thread::does_event_scheduling()
{
    return tasks & efj::thread_task_event_scheduling;
}

inline void
thread::use_for_logging(bool use)
{
    if (use) tasks |=  efj::thread_task_logging;
    else     tasks &= ~efj::thread_task_logging;
}

inline bool
thread::does_logging()
{
    return tasks & efj::thread_task_logging;
}

inline void
thread::use_for_config(bool use)
{
    if (use) tasks |= efj::thread_task_config;
    else     tasks &= ~efj::thread_task_config;
}

inline bool
thread::does_config()
{
    return tasks & efj::thread_task_config;
}

inline void
thread::use_for_debugging(bool use)
{
    if (use) tasks |=  efj::thread_task_debugging;
    else     tasks &= ~efj::thread_task_debugging;
}

inline bool
thread::does_debugging()
{
    return tasks & efj::thread_task_debugging;
}

inline void
thread::use_for_watchdog(bool use)
{
    if (use) tasks |=  efj::thread_task_watchdog;
    else     tasks &= ~efj::thread_task_watchdog;
}

inline bool
thread::does_watchdog()
{
    return tasks & efj::thread_task_watchdog;
}

//}

} // namespace efj
